using AfGD;
using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.UI;
using ISA.util;
using ISA.VR;
using RengeGames.HealthBars;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class AudioPinHandler : AbstractPinHandler
{

    [SerializeField]
    private AudioSource playbackSource;

    [SerializeField]
    private RadialSegmentedHealthBar playbackProgressBar;
    [SerializeField]
    private SpriteToggler playButtonSpriteToggler;

    private bool isInPlayState;



    public void Initialize(WhiteboardHandler parentWhiteboard, AudioClip clip, int id, Vector3 localPinPosition, Vector2 textureSpaceCoordinateOnDrawingBoard)
    {
        gameObject.name = "Audio Pin";
        Initialize(parentWhiteboard, id, localPinPosition, textureSpaceCoordinateOnDrawingBoard);

        playbackSource.clip = clip;
        playbackProgressBar.SetSegmentCount(clip.length);
        playButtonSpriteToggler.ChangeSprite(0);
        
        isInPlayState = false;

        modelRoot.GetComponentInChildren<ISAUIInteractable>().onTriggerPressed.AddListener(SendPlaybackToggleRequest);

        UpdateUI();
    }



    public void SendPlaybackToggleRequest()
    {
        AudioEvent e = new AudioEvent();
        e.systemName = ModelLoader.instance.VisualizationModel.name;
        e.clientId = SceneManager.INSTANCE.udpClient.id;
        e.pinId = this.id;

        e.play = !playbackSource.isPlaying;
        e.playbackTime = playbackSource.time;

        HTTPRequestHandler.INSTANCE.SubmitEvent(e, null, delegate (string msg) { Debug.LogError("Audio Playback failed: " + msg); });
    }

    public void TogglePlayback(bool playNow, float playbackTime, long serverSideEventTimestamp)
    {
        StartCoroutine(TooglePlaybackRoutine(playNow, playbackTime, serverSideEventTimestamp));
    }

    private IEnumerator TooglePlaybackRoutine(bool playNow, float playbackTime, long serverSideEventTimestamp)
    {
        long clientSideTimestampNow = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();
        long eventStart = serverSideEventTimestamp + 500;
        long millisToWaitUntilStart = eventStart - clientSideTimestampNow + TimeUtil.timeDifferenceToServerMilliSeconds;
        float secsToWaitUntilStart = millisToWaitUntilStart / 1000f;

        playbackSource.time = playbackTime;
        playButtonSpriteToggler.ChangeSprite(1);

        if (millisToWaitUntilStart > 0)
        {
            // event arrived early (good case)
            // wait for a short time, then play
            //Debug.Log("Waiting for " + secsToWaitUntilStart + " seconds.");
            yield return new WaitForSeconds(secsToWaitUntilStart);
        }
        else
        {
            // event arrived delayed (e.g., bad network or later connect)
            // fast forward
            float secsToForward = Mathf.Abs(secsToWaitUntilStart);

            if(playbackTime + secsToForward >= playbackSource.clip.length)
            {
                //Debug.Log("Aborting Playback Toggle, event is too old.");
                playbackSource.Stop();
                playButtonSpriteToggler.ChangeSprite(0);
                isInPlayState = false;
                yield break;
            }
            else
            {
                //Debug.Log("Skipping " + secsToForward + " seconds.");
                playbackSource.time += secsToForward;
            }
        }

        //Debug.Log("  > Start playback time = " + playbackSource.time + " - requested playback time = " + playbackTime);

        if (playNow)
        {
            playbackSource.Play();
            playButtonSpriteToggler.ChangeSprite(1);
            isInPlayState = true;
        }
        else
        {
            playbackSource.Stop();
            playButtonSpriteToggler.ChangeSprite(0);
            isInPlayState = false;
        }
        UpdateUI();
    }



    private void Update()
    {
        if (!isInPlayState)
            return;

        if (!playbackSource.isPlaying)
        {
            // clip finished
            playbackSource.Stop();
            playButtonSpriteToggler.ChangeSprite(0);
            isInPlayState = false;
            playbackSource.time = 0;
        }

        UpdateUI();
    }

    private void UpdateUI()
    {
        playbackProgressBar.SetRemovedSegments(playbackSource.time);
    }



    public override void Destroy()
    {
        base.Destroy();
    }



    public override void OnHandDetach()
    {
        base.OnHandDetach();
    }

}
