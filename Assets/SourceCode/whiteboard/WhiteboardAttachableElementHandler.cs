using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WhiteboardAttachableElementHandler : MonoBehaviour
{
    public GameObject ElementToMap;

    public bool DeleteAfterWhiteboardAttachment = false;

    [SerializeField]
    private LayerMask RayCastingMask;

    [SerializeField]
    private GameObject AttachmentPointIndicator;
    private Vector3 initialAttachmentPointIndicatorPosition;
    private Quaternion initialAttachmentPointIndicatorRotation;

    private bool attachedToHand;

    public UnityEvent onAttachment = new UnityEvent();



    public void Start()
    {
        HideIndicator();
        initialAttachmentPointIndicatorPosition = AttachmentPointIndicator.transform.localPosition;
        initialAttachmentPointIndicatorRotation = AttachmentPointIndicator.transform.localRotation;
    }

    public void HideIndicator()
    {
        AttachmentPointIndicator.SetActive(false);
    }



    public void OnAttachedToHand()
    {
        if(this.enabled)
        {
            attachedToHand = true;
        }
    }

    public void OnDetachedFromhand()
    {
        //if (this.enabled)
        {
            attachedToHand = false;
            HideIndicator();
        }
    }

    void Update()
    {
        if (!attachedToHand)
            return;

        if (Physics.Raycast(transform.position, -transform.up, out RaycastHit hit, 0.5f, RayCastingMask))
        {
            if (hit.collider.gameObject.TryGetComponent<WhiteboardHandler>(out _))
            {
                AttachmentPointIndicator.SetActive(true);

                //if (Mathf.Abs(AttachmentPointIndicator.transform.lossyScale.x - 1f) > 0.01f)
                //    AttachmentPointIndicator.transform.localScale /= AttachmentPointIndicator.transform.parent.lossyScale.x;

                AttachmentPointIndicator.transform.position = hit.point + Mathf.Abs(Mathf.Pow(Mathf.Sin(Time.time * 5), 9)) * hit.normal * 0.02f;
                AttachmentPointIndicator.transform.rotation = Quaternion.LookRotation(hit.normal);

                return;
            }
        }

        HideIndicator();
    }



    float lastAttachmentTimestamp;
    void OnTriggerEnter(Collider collider)
    {
        if (!this.enabled)
            return;
        if (!attachedToHand)
            return;

        if (collider.TryGetComponent(out WhiteboardHandler boardHandler1)) {
            if (Physics.Raycast(transform.position, -transform.up, out RaycastHit hit, 2f, RayCastingMask))
            {
                if (hit.collider.gameObject.TryGetComponent(out WhiteboardHandler boardHandler2))
                {
                    if (!boardHandler1.Equals(boardHandler2))
                        return;
                    if (Time.time - lastAttachmentTimestamp < 0.5f)
                        return;

                    AttachToBoard(boardHandler2, hit);
                    lastAttachmentTimestamp = Time.time;
                    onAttachment.Invoke();

                    if (DeleteAfterWhiteboardAttachment)
                        Destroy(gameObject);
                }
            }
        }
    }

    private void AttachToBoard(WhiteboardHandler board, RaycastHit hit)
    {
        Vector2 textureSpaceCoordinate = hit.textureCoord * board.textureSize;
        if (ElementToMap.TryGetComponent(out AbstractVisualElement3D element))
            board.AddSoftwarePin(element, hit.point, textureSpaceCoordinate);
        else if (ElementToMap.TryGetComponent(out MicrophoneHandler mic))
            board.AddAudioPin(mic, hit.point, textureSpaceCoordinate);
        //else if (ElementToMap.TryGetComponent(out HandheldCameraPhoto photo))
        //    board.AddPhoto(photo, hit.point, transform.rotation);
        else
            board.SpawnErrorNotification("Pinning element could not be performed", "Trying to pin an element with unspecified pinning behavior!", Vector3.zero);
    }

}
