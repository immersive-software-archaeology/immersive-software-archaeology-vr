﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



// (cf. https://stackoverflow.com/questions/46144205/point-in-polygon-using-winding-number)

public class ModulePolygon : WhiteboardShape
{
    private List<SoftwareElementPinHandler> ContainedPins;



    public ModulePolygon(WhiteboardHandler parentBoard, List<List<Vector2>> registeredDrawnLinePoints, List<Vector2> flatListOfPoints, Color color) : base(parentBoard, registeredDrawnLinePoints, flatListOfPoints, color)
    {
        ResetPinContainment();
    }

    public void ResetPinContainment()
    {
        ContainedPins = new List<SoftwareElementPinHandler>();
    }

    public List<SoftwareElementPinHandler> getPins()
    {
        return new List<SoftwareElementPinHandler>(ContainedPins);
    }

    public bool Contains(SoftwareElementPinHandler pin)
    {
        return ContainedPins.Contains(pin);
    }

    public bool AddPin(SoftwareElementPinHandler pin)
    {
        if (ContainedPins.Contains(pin))
            return false;
        ContainedPins.Add(pin);
        return true;
    }

    public bool RemovePin(SoftwareElementPinHandler pin)
    {
        return ContainedPins.Remove(pin);
    }



    // Works only for closed polygons??
    public float getArea()
    {
        // Add the first point to the end.
        int num_points = flatListOfPoints.Count;
        Vector2[] pts = new Vector2[num_points + 1];
        flatListOfPoints.CopyTo(pts, 0);
        pts[num_points] = flatListOfPoints[0];

        // Get the areas.
        float area = 0;
        for (int i = 0; i < num_points; i++)
        {
            area +=
                (pts[i + 1].x - pts[i].x) *
                (pts[i + 1].y + pts[i].y) / 2;
        }

        // Return the result.
        return Mathf.Abs(area);
    }

    public override bool isValid()
    {
        return flatListOfPoints.Count > 3;
    }

    protected override bool Highlight(DrawingBoardShapeHighlightModes mode)
    {
        foreach (SoftwareElementPinHandler pin in ContainedPins)
            pin.HighlightModuleContainment(highlightColor);

        return base.Highlight(mode);
    }

    protected override void SetLineRendererPositions(LineRenderer lineRenderer)
    {
        lineRenderer.positionCount = flatListOfPoints.Count + 1;
        for (int i = 0; i <= flatListOfPoints.Count; i++)
        {
            Vector2 point;
            if (i == flatListOfPoints.Count)
                point = flatListOfPoints[0];
            else
                point = flatListOfPoints[i];
            Vector3 worldPosition = UVUtils.UvTo3DWorld(parentBoard.transform.GetComponent<MeshFilter>(), new Vector2(point.x / parentBoard.textureSize.x, point.y / parentBoard.textureSize.y));
            Vector3 localPosition = parentBoard.transform.parent.InverseTransformPoint(worldPosition);
            //lineRenderer.SetPosition(i, worldPosition - parentBoard.transform.up * 0.002f);
            lineRenderer.SetPosition(i, localPosition + Vector3.forward * 0.002f);
        }
    }



    public bool Contains(Vector2 location)
    {
        Vector2[] polygonPointsWithClosure = PolygonPointsWithClosure();

        int windingNumber = 0;

        for (int pointIndex = 0; pointIndex < polygonPointsWithClosure.Length - 1; pointIndex++)
        {
            Edge edge = new Edge(polygonPointsWithClosure[pointIndex], polygonPointsWithClosure[pointIndex + 1]);
            windingNumber += AscendingIntersection(location, edge);
            windingNumber -= DescendingIntersection(location, edge);
        }

        return windingNumber != 0;
    }

    private Vector2[] PolygonPointsWithClosure()
    {
        // _points should remain immutable, thus creation of a closed point set (starting point repeated)
        return new List<Vector2>(flatListOfPoints)
        {
            new Vector2(flatListOfPoints[0].x, flatListOfPoints[0].y)
        }.ToArray();
    }

    private static int AscendingIntersection(Vector2 location, Edge edge)
    {
        if (!edge.AscendingRelativeTo(location)) { return 0; }

        if (!edge.LocationInRange(location, ModulePolygonOrientation.Ascending)) { return 0; }

        return Wind(location, edge, ModulePolygonPosition.Left);
    }

    private static int DescendingIntersection(Vector2 location, Edge edge)
    {
        if (edge.AscendingRelativeTo(location)) { return 0; }

        if (!edge.LocationInRange(location, ModulePolygonOrientation.Descending)) { return 0; }

        return Wind(location, edge, ModulePolygonPosition.Right);
    }

    private static int Wind(Vector2 location, Edge edge, ModulePolygonPosition position)
    {
        if (edge.RelativePositionOf(location) != position) { return 0; }

        return 1;
    }

    private class Edge
    {
        private readonly Vector2 _startPoint;
        private readonly Vector2 _endPoint;

        public Edge(Vector2 startPoint, Vector2 endPoint)
        {
            _startPoint = startPoint;
            _endPoint = endPoint;
        }

        public ModulePolygonPosition RelativePositionOf(Vector2 location)
        {
            double positionCalculation =
                (_endPoint.y - _startPoint.y) * (location.x - _startPoint.x) -
                (location.y - _startPoint.y) * (_endPoint.x - _startPoint.x);

            if (positionCalculation > 0) { return ModulePolygonPosition.Left; }

            if (positionCalculation < 0) { return ModulePolygonPosition.Right; }

            return ModulePolygonPosition.Center;
        }

        public bool AscendingRelativeTo(Vector2 location)
        {
            return _startPoint.x <= location.x;
        }

        public bool LocationInRange(Vector2 location, ModulePolygonOrientation orientation)
        {
            if (orientation == ModulePolygonOrientation.Ascending) return _endPoint.x > location.x;

            return _endPoint.x <= location.x;
        }
    }

    private enum ModulePolygonPosition
    {
        Left,
        Right,
        Center
    }

    private enum ModulePolygonOrientation
    {
        Ascending,
        Descending
    }
}