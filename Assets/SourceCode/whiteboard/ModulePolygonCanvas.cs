﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ModulePolygonCanvas : AbstractWhiteboardElementCanvasHandler, WhiteboardPointerEventListener
{

    public TMP_Text containmentCountText;
    public Button generateFacadeCodeButton;

    private ModulePolygon module;



    public void Initialize(WhiteboardHandler board, ModulePolygon module)
    {
        Color.RGBToHSV(module.color, out float h, out float s, out float v);
        color = Color.HSVToRGB(h, s, 0.35f);

        transform.Find("Background").GetComponent<BeveledUIMesh>().color = color;
        transform.Find("Background").GetComponent<BeveledUIMesh>().GenerateMesh();

        incomingRelationsCanvas.Find("Background").GetComponent<BeveledUIMesh>().color = color;
        incomingRelationsCanvas.Find("Background").GetComponent<BeveledUIMesh>().GenerateMesh();

        outgoingRelationsCanvas.Find("Background").GetComponent<BeveledUIMesh>().color = color;
        outgoingRelationsCanvas.Find("Background").GetComponent<BeveledUIMesh>().GenerateMesh();

        base.Initialize(board);
        board.AddPointerEventListener(this);

        this.module = module;

        // positioning
        {
            Vector2 centerCoordinate = module.getCenter();
            //float moduleRadius = module.getBoundingCircleRadius(centerCoordinate);

            Vector2 spawnUV = centerCoordinate;
            spawnUV.x = module.getLowestX();
            spawnUV /= board.textureSize;

            //transform.parent = board.transform;
            transform.position = UVUtils.UvTo3DWorld(board.transform.GetComponent<MeshFilter>(), spawnUV);
            transform.localPosition += 0.035f * Vector3.forward;
            transform.rotation = Quaternion.LookRotation(board.transform.up, board.transform.forward);
        }

        generateFacadeCodeButton.onClick.AddListener(delegate { generateFacadeCode(); });

        //GetComponent<Canvas>().worldCamera = SceneManager.INSTANCE.GetActivePointer().eventCamera;
        SceneManager.INSTANCE.RegisterCanvas(GetComponent<Canvas>());
        gameObject.SetActive(false);
    }

    protected override IEnumerator initializeRelations()
    {
        //Vector3 fallbackPinPosition = transform.position - transform.up * 0.1f;
        relatedElementToEntryObjectMap.Clear();

        List<AbstractVisualElement3D> incomingRelationOriginElements = new List<AbstractVisualElement3D>();
        List<AbstractVisualElement3D> outgoingRelationTargetElements = new List<AbstractVisualElement3D>();
        foreach (SoftwareElementPinHandler pin in module.getPins())
        {
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetIncomingRelationships(RelationType.TYPE_REFERENCE))
                incomingRelationOriginElements.Add(relation.globalOriginElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetIncomingRelationships(RelationType.METHOD_CALL))
                incomingRelationOriginElements.Add(relation.globalOriginElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetIncomingRelationships(RelationType.FIELD_ACCESS))
                incomingRelationOriginElements.Add(relation.globalOriginElement);
            yield return null;

            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetOutgoingRelationships(RelationType.TYPE_REFERENCE))
                outgoingRelationTargetElements.Add(relation.globalTargetElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetOutgoingRelationships(RelationType.METHOD_CALL))
                outgoingRelationTargetElements.Add(relation.globalTargetElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetOutgoingRelationships(RelationType.FIELD_ACCESS))
                outgoingRelationTargetElements.Add(relation.globalTargetElement);
            yield return null;
        }

        {
            HashSet<SpaceStation3D> incomingRelationOriginStations = createSetOfContainingStations(incomingRelationOriginElements);
            foreach (SoftwareElementPinHandler pin in module.getPins())
                if(pin.modelElement is SpaceStation3D)
                    incomingRelationOriginStations.Remove((SpaceStation3D) pin.modelElement);
            yield return null;

            yield return StartCoroutine(instantiateRelationEntriesForStations(incomingRelationOriginStations, incomingRelationEntryDummy));

            numberOfIncomingRelationsText.text = incomingRelationOriginStations.Count.ToString();
        }
        {
            HashSet<SpaceStation3D> outgoingRelationTargetStations = createSetOfContainingStations(outgoingRelationTargetElements);
            foreach (SoftwareElementPinHandler pin in module.getPins())
                if (pin.modelElement is SpaceStation3D)
                    outgoingRelationTargetStations.Remove((SpaceStation3D)pin.modelElement);
            yield return null;

            yield return StartCoroutine(instantiateRelationEntriesForStations(outgoingRelationTargetStations, outgoingRelationEntryDummy));

            numberOfOutgoingRelationsText.text = outgoingRelationTargetStations.Count.ToString();
        }
    }



    public override void Destroy()
    {
        base.Destroy();
        board.moduleCanvases.Remove(module);
        board.RemovePointerEventListener(this);
        Destroy(gameObject);
    }

    public void NotifyOnHoverStart(Vector3 worldPosition, Vector2 uvPosition) { }

    public void NotifyOnHoverEnd() { }

    public void NotifyOnHoverUpdate(Vector3 worldPosition, Vector2 uvPosition)
    {
        if (module.Contains(uvPosition * board.textureSize))
        {
            module.SpawnHighlightRoutine(DrawingBoardShapeHighlightModes.HOVER, 0, 0.5f);
            foreach(ArrowPolygon arrow in board.arrows)
            {
                if(arrow.startModule == module)
                    arrow.SpawnHighlightRoutine(DrawingBoardShapeHighlightModes.HOVER, 0, 0.5f);
            }
        }
    }

    public void NotifyOnTriggerPressed(Vector3 worldPosition, Vector2 uvPosition)
    {
        if (!module.Contains(uvPosition * board.textureSize))
            return;

        if (!gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
            updateUI();
        }
        else
        {
            Hide();
        }
    }

    public void NotifyOnTriggerReleased() { }

    public override void NotifyOnBoardUpdate()
    {
        listOfRelatedElementsIsUpdated = false;
        updateUI();
    }



    private void updateUI()
    {
        if (gameObject.activeInHierarchy)
            SceneManager.INSTANCE.StartCoroutine(Show());

        containmentCountText.text = module.getPins().Count.ToString();

        bool containsPins = module.getPins().Count > 0;

        generateFacadeCodeButton.interactable = containsPins;
        incomingRelationsCanvas.gameObject.SetActive(containsPins);
        outgoingRelationsCanvas.gameObject.SetActive(containsPins);
        //if (module.getPins().Count > 0)
        //{
        //    generateFacadeCodeButton.transform.GetChild(0).GetComponent<TMP_Text>().color = new Color(1, 1, 1, 1);
        //    generateFacadeCodeButton.transform.GetChild(1).GetComponent<Image>().color = new Color(1, 1, 1, 1);
        //}
        //else
        //{
        //    generateFacadeCodeButton.transform.GetChild(0).GetComponent<TMP_Text>().color = new Color(1, 1, 1, 0.25f);
        //    generateFacadeCodeButton.transform.GetChild(1).GetComponent<Image>().color = new Color(1, 1, 1, 0.25f);
        //}
    }



    private void generateFacadeCode()
    {
        string classifierNames = "";
        foreach (SoftwareElementPinHandler pin in module.getPins())
        {
            if (pin.modelElement is SpaceStation3D)
                classifierNames += pin.modelElement.qualifiedName + ",";
            else if (pin.modelElement is TransparentCapsule3D)
                classifierNames += collectCommaSeperatedClassifierNames((TransparentCapsule3D)pin.modelElement);
        }

        _ = HTTPRequestHandler.INSTANCE.SendRequest("code-gen/facade/?systemName=" + ModelLoader.instance.VisualizationModel.name + "&classifierQualifiedNames=" + classifierNames);
    }

    private string collectCommaSeperatedClassifierNames(TransparentCapsule3D capsule)
    {
        string result = "";
        foreach (TransparentCapsule3D child in capsule.containedSubCapsules)
            result += collectCommaSeperatedClassifierNames(child);
        foreach (SpaceStation3D station in capsule.directContainment.containedSpaceStations)
            result += station.qualifiedName + ",";
        return result;
    }
}