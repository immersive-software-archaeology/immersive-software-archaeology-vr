﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPolygon : WhiteboardShape
{

    public ModulePolygon startModule { private set; get; }
    public ModulePolygon endModule { private set; get; }

    private readonly int originalPointCount;



    public ArrowPolygon(WhiteboardHandler parentBoard, ModulePolygon startModule, ModulePolygon endModule, Color color, List<List<Vector2>> registeredDrawnLinePoints, List<Vector2> flatListOfPoints) : base(parentBoard, registeredDrawnLinePoints, flatListOfPoints, color)
    {
        this.startModule = startModule;
        this.endModule = endModule;

        //Color.RGBToHSV(color, out float h, out float s, out _);
        //this.color = Color.HSVToRGB(h, s, 0.25f);

        originalPointCount = flatListOfPoints.Count;
    }

    public override bool isValid()
    {
        return flatListOfPoints.Count > 0.5f * originalPointCount;
    }

    protected override void SetLineRendererPositions(LineRenderer lineRenderer)
    {
        lineRenderer.positionCount = flatListOfPoints.Count;
        for (int i = 0; i < flatListOfPoints.Count; i++)
        {
            Vector2 point = flatListOfPoints[i];
            Vector3 worldPosition = UVUtils.UvTo3DWorld(parentBoard.transform.GetComponent<MeshFilter>(), new Vector2(point.x / parentBoard.textureSize.x, point.y / parentBoard.textureSize.y));
            Vector3 localPosition = parentBoard.transform.parent.InverseTransformPoint(worldPosition);
            //lineRenderer.SetPosition(i, worldPosition - parentBoard.transform.up * 0.002f);
            lineRenderer.SetPosition(i, localPosition + Vector3.forward * 0.002f);
        }
    }
}