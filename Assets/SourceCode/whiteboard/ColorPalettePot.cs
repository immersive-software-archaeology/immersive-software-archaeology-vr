using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(Collider))]
public class ColorPalettePot : MonoBehaviour
{

    private Renderer _renderer;
    private Collider _collider;

    [SerializeField]
    private Color color;

    void OnEnable()
    {
        _renderer = GetComponent<Renderer>();
        _collider = GetComponent<Collider>();

        _renderer.material.color = color;

        float dimension = Mathf.Ceil(Mathf.Sqrt(transform.parent.childCount));
        float x = (transform.GetSiblingIndex() % dimension) / dimension;
        float y = Mathf.Floor(transform.GetSiblingIndex() / dimension) / dimension;
        _renderer.material.mainTextureOffset = new Vector2(x, y);
        _renderer.material.mainTextureScale = new Vector2(1f / dimension, 1f / dimension);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent == null)
            return;

        if(other.transform.parent.TryGetComponent(out PenHandler pen))
        {
            pen.SetColor(color);
        }
    }

}
