using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Valve.VR;
using Valve.VR.InteractionSystem;



public enum PenMode
{
    FREEHAND, MODULE, ARROW, ERASER
}



public class PenHandler : PermanentAttachable
{

    public static readonly float INTERPOLATION_PRECISION = 1f;

    [SerializeField]
    private Transform raycastOriginTransform;

    public Color penColor = new Color(15f / 255f, 15f / 255f, 15f / 255f, 0.5f);
    public int penColorAsInteger { get; private set; }

    [SerializeField]
    private bool usePenPressure = false;
    [SerializeField]
    private float penHeight = 0.1f;
    [SerializeField]
    private int penSize = 5;
    [SerializeField]
    private int eraserSize = 20;
    [SerializeField]
    private PenMode penMode = PenMode.FREEHAND;
    private PenMode lastPenMode = PenMode.FREEHAND;

    public float PenSize
    {
        get { return penSize; }
    }

    [SerializeField]
    private MaterialChanger penTipMaterialChanger;

    [SerializeField]
    private VideoPlayer explanationVideoPlayer;
    [SerializeField]
    private RawImage explanationVideoImage;

    [SerializeField]
    private VideoClip freehandExplanationVideo;
    [SerializeField]
    private VideoClip moduleExplanationVideo;
    [SerializeField]
    private VideoClip arrowExplanationVideo;

    [SerializeField]
    private Image freehandModeDisplayBackground;
    [SerializeField]
    private Image moduleModeDisplayBackground;
    [SerializeField]
    private Image arrowModeDisplayBackground;

    [SerializeField]
    private Renderer[] RenderersToPaintInPenColor;



    [SerializeField]
    private SteamVR_Action_Boolean actionForModeSwitch;
    [SerializeField]
    private SteamVR_Action_Boolean actionForModeSwitchBack;



    private Rigidbody _rigidbody;
    private AudioClipHelper audioHelper;

    public Color[] smallColorArray { get; private set; }
    public Color[] largeColorArray { get; private set; }

    private bool hasDrawnInLastFrame = false;
    private Vector2 lastTextureSpaceCoordinate;

    private IEnumerator hideExplanationVideoRoutine;



    protected override void Init()
    {
        //penColor = penTip.material.color;
        //penColorArray = Enumerable.Repeat(penColor, penSize * penSize).ToArray();
        _rigidbody = GetComponent<Rigidbody>();
        interactable = GetComponent<Interactable>();
        audioHelper = GetComponent<AudioClipHelper>();

        lastPenMode = penMode;
        SetColor(penColor);

        explanationVideoPlayer.gameObject.SetActive(false);
    }

    protected override void OnAttach()
    {
        _rigidbody.isKinematic = false;
        TryTriggeringHapticPulse(1000);
    }

    protected override void OnDetach()
    {
        _rigidbody.isKinematic = true;
        TryTriggeringHapticPulse(1000);
    }



    public void SetColor(Color color)
    {
        penColor = color;
        
        if (penColor.r > 0.99f && penColor.g > 0.99f && penColor.b > 0.99f)
        {
            if(penMode != PenMode.ERASER)
                lastPenMode = penMode;
            SetMode(PenMode.ERASER, false);
        }
        else if (penMode == PenMode.ERASER)
        {
            SetMode(lastPenMode, false);
        }
        
        UpdateModeDisplay();
        smallColorArray = InitializeBrushTexture(penSize, penColor);
        largeColorArray = InitializeBrushTexture(eraserSize, penColor);

        foreach (Renderer renderer in RenderersToPaintInPenColor)
            renderer.material.color = color;

        byte[] colorBytes = new byte[4];
        colorBytes[0] = (byte)(penColor.r * 255f);
        colorBytes[1] = (byte)(penColor.g * 255f);
        colorBytes[2] = (byte)(penColor.b * 255f);
        colorBytes[3] = (byte)(penColor.a * 255f);
        penColorAsInteger = BitConverter.ToInt32(colorBytes, 0);
    }

    public static Color[] InitializeBrushTexture(int size, Color color)
    {
        Color[] colorArray = new Color[size * size];
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                float alpha = Vector2.Distance(new Vector2(x, y), Vector2.one * (float)size / 2f);
                alpha = alpha / ((float)size / 2f);
                alpha = 1f - alpha;
                alpha = Mathf.Max(0, alpha);
                alpha = Mathf.Pow(alpha, 2f / size);

                colorArray[y * size + x] = new Color(color.r, color.g, color.b, color.a * alpha);
            }
        }
        return colorArray;
    }

    private void SetMode(PenMode mode, bool triggerUpdate = true)
    {
        penMode = mode;
        audioHelper.Play("mode change");

        if(triggerUpdate)
            UpdateModeDisplay();
    }

    private void UpdateModeDisplay()
    {
        Color darkerPenColor;
        {
            Color.RGBToHSV(penColor, out float h, out float s, out float v);
            darkerPenColor = Color.HSVToRGB(h, s, v * 0.5f);
        }
        explanationVideoImage.material.color = darkerPenColor;

        freehandModeDisplayBackground.color = new Color(0.15f, 0.15f, 0.15f, 1f);
        moduleModeDisplayBackground.color = new Color(0.15f, 0.15f, 0.15f, 1f);
        arrowModeDisplayBackground.color = new Color(0.15f, 0.15f, 0.15f, 1f);

        freehandModeDisplayBackground.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        moduleModeDisplayBackground.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        arrowModeDisplayBackground.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1f);

        explanationVideoPlayer.gameObject.SetActive(true);
        if (penMode == PenMode.FREEHAND)
        {
            freehandModeDisplayBackground.color = new Color(0.8f, 0.8f, 0.8f, 1f);
            freehandModeDisplayBackground.transform.GetChild(0).GetComponent<Image>().color = darkerPenColor;

            explanationVideoPlayer.Stop();
            explanationVideoPlayer.clip = freehandExplanationVideo;
            explanationVideoPlayer.Play();
        }
        else if (penMode == PenMode.MODULE)
        {
            moduleModeDisplayBackground.color = new Color(0.8f, 0.8f, 0.8f, 1f);
            moduleModeDisplayBackground.transform.GetChild(0).GetComponent<Image>().color = darkerPenColor;

            explanationVideoPlayer.Stop();
            explanationVideoPlayer.clip = moduleExplanationVideo;
            explanationVideoPlayer.Play();
        }
        else if (penMode == PenMode.ARROW)
        {
            arrowModeDisplayBackground.color = new Color(0.8f, 0.8f, 0.8f, 1f);
            arrowModeDisplayBackground.transform.GetChild(0).GetComponent<Image>().color = darkerPenColor;

            explanationVideoPlayer.Stop();
            explanationVideoPlayer.clip = arrowExplanationVideo;
            explanationVideoPlayer.Play();
        }
        else
        {
            explanationVideoPlayer.gameObject.SetActive(false);
        }

        if (hideExplanationVideoRoutine != null)
        {
            StopCoroutine(hideExplanationVideoRoutine);
            hideExplanationVideoRoutine = null;
        }
        hideExplanationVideoRoutine = hideExplanationVideo();
        StartCoroutine(hideExplanationVideoRoutine);
    }

    private IEnumerator hideExplanationVideo()
    {
        yield return new WaitForSeconds(10);
        explanationVideoPlayer.gameObject.SetActive(false);
    }



    private void TryTriggeringHapticPulse(ushort duration)
    {
        try
        {
            hand.TriggerHapticPulse(duration);
        }
        catch (Exception) { }
    }



    void Update()
    {
        if (!attached)
            return;

        try
        {
            if (actionForModeSwitch.GetStateDown(hand.handType))
            {
                if (penMode == PenMode.FREEHAND)
                    SetMode(PenMode.MODULE);
                else if (penMode == PenMode.MODULE)
                    SetMode(PenMode.ARROW);
                else if (penMode == PenMode.ARROW)
                    SetMode(PenMode.FREEHAND);
            }
            else if (actionForModeSwitchBack.GetStateDown(hand.handType))
            {
                if (penMode == PenMode.FREEHAND)
                    SetMode(PenMode.ARROW);
                else if (penMode == PenMode.MODULE)
                    SetMode(PenMode.FREEHAND);
                else if (penMode == PenMode.ARROW)
                    SetMode(PenMode.MODULE);
            }
        }
        catch(Exception) { }

        if (Physics.Raycast(raycastOriginTransform.position - raycastOriginTransform.forward * penHeight, raycastOriginTransform.forward, out RaycastHit hit, penHeight * 2f)) {
            if(hit.collider.gameObject.TryGetComponent(out WhiteboardHandler boardHandler))
            {
                // We hit the whiteboard!
                int penDrawArea;
                if (penMode == PenMode.ERASER)
                    penDrawArea = eraserSize;
                else
                    penDrawArea = penSize;

                Vector2 textureSpaceCoordinate = hit.textureCoord * boardHandler.textureSize; // - Vector2.one * (penDrawArea / 2f);
                float penPressure;
                if (usePenPressure)
                    penPressure = 1f - Mathf.Min(1, Mathf.Max(0, (hit.distance - penHeight) / penHeight));
                else
                    penPressure = 1;
                //Debug.Log("registered pen at " + textureSpaceCoordinate.x + ", " + textureSpaceCoordinate.y + " with pressure " + penPressure);

                if (boardHandler.isSafeToDraw(textureSpaceCoordinate, penDrawArea))
                {
                    if (!hasDrawnInLastFrame)
                    {
                        audioHelper.Play("drawing");
                        audioHelper.SetVolume("drawing", 1);
                        if (_rigidbody != null)
                            _rigidbody.freezeRotation = true;
                    }

                    if (hasDrawnInLastFrame && Vector2.Distance(textureSpaceCoordinate, lastTextureSpaceCoordinate) > 0.5f)
                    {
                        penTipMaterialChanger.SetDrawing(true);
                        if (hand != null)
                        {
                            TryTriggeringHapticPulse((ushort)(penPressure * /*Time.deltaTime*/ 500f));
                            //hand.TriggerHapticPulse(200);
                        }
                        audioHelper.SetVolume("drawing", penPressure);

                        //if (penMode == PenMode.ERASER)
                        //    boardHandler.Erase(textureSpaceCoordinate + Vector2.one * penDrawArea / 2, eraserSize);

                        //if (penMode == PenMode.MODULE)
                        //    boardHandler.AddModulePoint(textureSpaceCoordinate + Vector2.one * penDrawArea / 2, penColor);

                        //if (penMode == PenMode.ARROW)
                        //    boardHandler.AddArrowPoint(textureSpaceCoordinate + Vector2.one * penDrawArea / 2, penColor);

                        SafelySetPixels(boardHandler, textureSpaceCoordinate, penPressure, penDrawArea);
                    }

                    lastTextureSpaceCoordinate = textureSpaceCoordinate;
                    hasDrawnInLastFrame = true;
                    return;
                }
            }
        }

        if (_rigidbody != null)
            _rigidbody.freezeRotation = false;
        penTipMaterialChanger.SetDrawing(false);
        hasDrawnInLastFrame = false;
        audioHelper.Stop("drawing");

        DetachIfShakingHand();
    }

    private void SafelySetPixels(WhiteboardHandler boardHandler, Vector2 textureSpaceCoordinate, float opacity, int penDrawArea)
    {
        if(penMode == PenMode.ERASER)
            boardHandler.AddPaintViaOperation(penMode, textureSpaceCoordinate, 1, penDrawArea, largeColorArray, penColor);
        else
            boardHandler.AddPaintViaOperation(penMode, textureSpaceCoordinate, opacity, penDrawArea, smallColorArray, penColor);

        float interpolationStepSize = INTERPOLATION_PRECISION / Mathf.Max(1 / penDrawArea, Vector2.Distance(textureSpaceCoordinate, lastTextureSpaceCoordinate));
        for (float f = interpolationStepSize; f < 1f; f += interpolationStepSize)
        {
            Vector2 intermediateCoordinate = Vector2.Lerp(lastTextureSpaceCoordinate, textureSpaceCoordinate, f);
            if (penMode == PenMode.ERASER)
                boardHandler.AddPaintViaOperation(penMode, intermediateCoordinate, 1, penDrawArea, largeColorArray, penColor);
            else
                boardHandler.AddPaintViaOperation(penMode, intermediateCoordinate, opacity, penDrawArea, smallColorArray, penColor);
        }

        boardHandler.ApplyDrawnChanges();
    }

}
