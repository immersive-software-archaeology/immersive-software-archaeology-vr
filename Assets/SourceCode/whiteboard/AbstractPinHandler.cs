using AfGD;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public abstract class AbstractPinHandler : MonoBehaviour
{
    public int id { private set; get; }

    [SerializeField]
    protected Transform modelRoot;
    [SerializeField]
    private MaterialChanger colorRing;

    [SerializeField]
    private Color DefaultColor = Color.white;
    [SerializeField]
    private Color AttachedToHandValidColor = Color.green;
    [SerializeField]
    private Color AttachedToHandInvalidColor = Color.red;
    [SerializeField]

    public Vector2 TextureSpaceCoordinateOnDrawingBoard { get; private set; }
    public Vector3 LocalSpacePositionOnDrawingBoard { get; private set; }

    protected HandHoverHighlighter OutlineHighlighter;
    public WhiteboardHandler parentBoard { get; private set; }

    private IEnumerator validityCheck;
    private IEnumerator destroyRoutine;
    private IEnumerator blinkRoutine;



    protected void Initialize(WhiteboardHandler parentWhiteboard, int id, Vector3 localPinPosition, Vector2 textureSpaceCoordinateOnDrawingBoard)
    {
        parentBoard = parentWhiteboard;

        gameObject.SetActive(true);
        transform.parent = parentWhiteboard.transform;
        transform.localScale = Vector3.one;

        UpdatePosition(localPinPosition, textureSpaceCoordinateOnDrawingBoard);

        // set id
        this.id = id;
        {
            //byte[] whiteboardIdBytes = BitConverter.GetBytes(parentBoard.id);
            //if (whiteboardIdBytes[1] != 0 || whiteboardIdBytes[2] != 0 || whiteboardIdBytes[3] != 0)
            //    Debug.LogError("whiteboard id exceeds maximum value of 255!");

            //byte[] elementIdBytes = BitConverter.GetBytes(modelElement.id);
            //if (elementIdBytes[3] != 0)
            //    Debug.LogError("Pinned element id exceeds maximum value of 2^24!");

            //byte[] pinIdBytes = new byte[4];
            //pinIdBytes[0] = whiteboardIdBytes[0];
            //pinIdBytes[1] = elementIdBytes[0];
            //pinIdBytes[2] = elementIdBytes[1];
            //pinIdBytes[3] = elementIdBytes[2];

            //id = BitConverter.ToInt32(pinIdBytes, 0);
        }
    }

    public virtual void Destroy()
    {
        StopAllCoroutines();
        Destroy(gameObject);
    }



    public void ResetPositionAndColor()
    {
        UpdatePosition(LocalSpacePositionOnDrawingBoard, TextureSpaceCoordinateOnDrawingBoard);
        colorRing.SetEmission(DefaultColor);
    }

    public void UpdatePosition(Vector3 localSpacePosition, Vector2 textureSpacePosition, bool playSound = true)
    {
        if(playSound)
            GetComponent<AudioClipHelper>().Play("pinned to board");

        LocalSpacePositionOnDrawingBoard = localSpacePosition;
        TextureSpaceCoordinateOnDrawingBoard = textureSpacePosition;

        transform.localPosition = LocalSpacePositionOnDrawingBoard;
        transform.rotation = parentBoard.transform.rotation;

        ISAUIInteractionManager.INSTANCE.PositionChanged(this);
    }



    public void OnHandMove()
    {
        ISAUIInteractionManager.INSTANCE.PositionChanged(this);
    }

    public void OnHandAttach()
    {
        if (destroyRoutine != null)
        {
            StopCoroutine(destroyRoutine);
            destroyRoutine = null;
        }

        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = true;

        validityCheck = loopValidityCheck();
        StartCoroutine(validityCheck);
    }

    private IEnumerator loopValidityCheck()
    {
        bool inWarnMode = false;
        while (true)
        {
            for (int i = 0; i < 4; i++)
            {
                if (FindTargetWhiteboard(out _, out _, out _))
                {
                    if (inWarnMode)
                    {
                        inWarnMode = false;
                        GetComponent<AudioClipHelper>().Stop("warning");
                    }
                    BlinkForSeconds(AttachedToHandValidColor * 2f, 0.1f);
                }
                else
                {
                    if (!inWarnMode)
                    {
                        inWarnMode = true;
                        GetComponent<AudioClipHelper>().Play("warning");
                    }

                    if (i == 0)
                        BlinkForSeconds(AttachedToHandInvalidColor * 2f, 0.1f);
                }

                yield return new WaitForSeconds(0.05f);
            }
        }
    }

    public virtual void OnHandDetach()
    {
        if (validityCheck != null)
        {
            StopCoroutine(validityCheck);
            validityCheck = null;
        }

        if (FindTargetWhiteboard(out WhiteboardHandler boardHandler, out Vector3 worldSpaceCoordinate, out Vector2 textureSpaceCoordinate))
        {
            if (boardHandler.Equals(parentBoard))
            {
                boardHandler.MovePin(this, worldSpaceCoordinate, textureSpaceCoordinate);
            }
            else
            {
                parentBoard.RemovePin(this);
                if (this is SoftwareElementPinHandler)
                    boardHandler.AddSoftwarePin(((SoftwareElementPinHandler)this).modelElement, worldSpaceCoordinate, textureSpaceCoordinate);
                else
                    // TODO
                    // TODO
                    // TODO
                    // TODO
                    // TODO
                    // TODO
                    // TODO
                    // TODO
                    // TODO
                    // TODO
                    throw new NotImplementedException();
            }
        }
        else
        {
            parentBoard.RemovePin(this);
            GetComponent<AudioClipHelper>().Stop("warning");
        }
    }

    private bool FindTargetWhiteboard(out WhiteboardHandler boardHandler, out Vector3 worldSpaceCoordinate, out Vector2 textureSpaceCoordinate)
    {
        textureSpaceCoordinate = Vector2.zero;
        worldSpaceCoordinate = Vector2.zero;
        boardHandler = null;

        foreach (RaycastHit hit in Physics.RaycastAll(transform.position - transform.up * 0.1f, transform.up, 0.2f))
        {
            if (hit.collider.gameObject.TryGetComponent(out boardHandler))
            {
                // is a drawing board!
                textureSpaceCoordinate = hit.textureCoord * boardHandler.textureSize;
                worldSpaceCoordinate = hit.point;
                return true;
            }
        }
        return false;
    }

    private IEnumerator ApplyGravityAndWaitForDestroy()
    {
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().isKinematic = false;

        GetComponent<AudioClipHelper>().SetVolume("warning", 0.1f);

        yield return new WaitForSeconds(30);
        Destroy();
    }



    protected void BlinkForSeconds(Color color, float duration, float delay = 0)
    {
        if (blinkRoutine != null)
            StopCoroutine(blinkRoutine);
        blinkRoutine = changeColorRoutine(color, duration, delay);
        StartCoroutine(blinkRoutine);
    }

    private IEnumerator changeColorRoutine(Color color, float duration, float delay)
    {
        if (duration == 0)
        {
            colorRing.SetEmission(DefaultColor);
            yield break;
        }

        yield return new WaitForSeconds(delay);
        colorRing.SetEmission(color);
        yield return new WaitForSeconds(duration);
        colorRing.SetEmission(DefaultColor);
    }
}
