﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WhiteboardControlCanvasManager : MonoBehaviour
{

    [SerializeField]
    private GameObject undoStackEntryDummy;

    [SerializeField]
    private Button undoButton;
    [SerializeField]
    private Button redoButton;

    [SerializeField]
    private Button duplicateButton;
    [SerializeField]
    private Button enlargeButton;

    [SerializeField]
    private Button clearPinsButton;
    [SerializeField]
    private Button clearPaintButton;

    [SerializeField]
    private ActiveToggleable confirmationDialog;
    [SerializeField]
    private TMP_Text confirmationDialogText;
    [SerializeField]
    private Button confirmationDialogYesButton;

    private WhiteboardHandler board;



    public void Initialize(WhiteboardHandler board)
    {
        this.board = board;
        undoStackEntryDummy.SetActive(false);

        //GetComponent<Canvas>().worldCamera = SceneManager.INSTANCE.GetActivePointer().eventCamera;
        SceneManager.INSTANCE.RegisterCanvas(GetComponent<Canvas>());
        Evaluate();
    }



    public void Evaluate()
    {
        undoButton.interactable = board.operationExecuter.CanUndo();
        redoButton.interactable = board.operationExecuter.CanRedo();

        clearPinsButton.interactable = board.ContainsPins();
        clearPaintButton.interactable = board.ContainsPaint;

        enlargeButton.interactable = board.CurrentSize <= 3;

        int indexOfDummy = undoStackEntryDummy.transform.GetSiblingIndex();
        for (int i = indexOfDummy + 1; i < undoStackEntryDummy.transform.parent.childCount; i++)
            Destroy(undoStackEntryDummy.transform.parent.GetChild(i).gameObject);

        List<AbstractOperationVR> ops = board.operationExecuter.GetCopyOfUndoStack();
        for (int i = ops.Count - 1; i >= 0; i--)
        {
            GameObject newEntryObject = Instantiate(undoStackEntryDummy, undoStackEntryDummy.transform.parent, true);
            newEntryObject.SetActive(true);
            newEntryObject.transform.localPosition = new Vector3(newEntryObject.transform.localPosition.x, newEntryObject.transform.localPosition.y - i * 20, newEntryObject.transform.localPosition.z);

            Color fontColor = new Color(0.15f, 0.15f, 0.15f);
            Color iconColor = new Color(0.15f, 0.15f, 0.15f);
            if (ops[i] is AbstractDrawOperationVR)
            {
                Color.RGBToHSV(((AbstractDrawOperationVR)ops[i]).color, out float h, out _, out _);
                fontColor = Color.HSVToRGB(h, 0.5f, 0.15f);
                iconColor = fontColor;
            }

            if (board.operationExecuter.GetPositionInUndoStack() == i)
            {
                fontColor.a = 1;
                iconColor.a = 1;
            }
            else
            {
                iconColor.a = 0.1f;
                if (board.operationExecuter.GetPositionInUndoStack() > i)
                    fontColor.a = 1;
                else
                    fontColor.a = 0.5f;
            }

            newEntryObject.transform.GetChild(0).GetComponent<TMP_Text>().color = fontColor;
            newEntryObject.transform.GetChild(1).GetComponent<Image>().color = iconColor;
            newEntryObject.transform.GetChild(0).GetComponent<TMP_Text>().text = ops[i].ToString();

            object hoverEventElementToRegisterFor = ops[i].GetHoverEventElementToRegister();
            if (hoverEventElementToRegisterFor != null)
            {
                EventTrigger newEntryEventTrigger = newEntryObject.GetComponent<EventTrigger>();
                {
                    EventTrigger.Entry newEntry = new EventTrigger.Entry();
                    newEntry.eventID = EventTriggerType.PointerEnter;
                    newEntry.callback.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverStart(hoverEventElementToRegisterFor); });
                    newEntryEventTrigger.triggers.Add(newEntry);
                }
                {
                    EventTrigger.Entry newEntry = new EventTrigger.Entry();
                    newEntry.eventID = EventTriggerType.PointerExit;
                    newEntry.callback.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverEnd(hoverEventElementToRegisterFor); });
                    newEntryEventTrigger.triggers.Add(newEntry);
                }
            }
        }
    }



    public void DeleteBoard()
    {
        StopAllCoroutines();
        StartCoroutine(InitializeConfirmationDialog("Delete this whiteboard entirely?\n(Cannot be undone)"));
    }



    private IEnumerator InitializeConfirmationDialog(string confirmMessage)
    {
        confirmationDialog.Hide();
        confirmationDialogYesButton.interactable = false;
        confirmationDialogText.text = confirmMessage;

        yield return new WaitForSeconds(0.2f);
        confirmationDialog.Show();

        yield return new WaitForSeconds(2);
        confirmationDialogYesButton.interactable = true;
    }

    public void ConfirmationYes()
    {
        confirmationDialog.Hide();
        StopAllCoroutines();
        board.Destroy();
    }

    public void ConfirmationNo()
    {
        confirmationDialog.Hide();
    }

}