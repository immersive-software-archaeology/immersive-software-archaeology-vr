﻿using ISA.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Canvas))]
public abstract class AbstractWhiteboardElementCanvasHandler : MonoBehaviour, ISAActivatableElementListener, ISAHoverableElementListener, WhiteboardUpdateListener
{

    [SerializeField]
    protected Sprite checkedSprite;
    [SerializeField]
    protected Sprite uncheckedSprite;

    [SerializeField]
    protected TMP_Text numberOfIncomingRelationsText;
    [SerializeField]
    protected TMP_Text numberOfOutgoingRelationsText;

    [SerializeField]
    protected Transform incomingRelationsCanvas;
    [SerializeField]
    protected Transform outgoingRelationsCanvas;

    [SerializeField]
    protected Button incomingRelationEntryDummy;
    [SerializeField]
    protected Button outgoingRelationEntryDummy;

    protected WhiteboardHandler board;
    protected bool listOfRelatedElementsIsUpdated = false;
    protected Dictionary<AbstractVisualElement3D, List<GameObject>> relatedElementToEntryObjectMap = new Dictionary<AbstractVisualElement3D, List<GameObject>>();

    public Color color = new Color(0.35f, 0.35f, 0.35f, 1);



    public void Initialize(WhiteboardHandler board)
    {
        this.board = board;
        this.board.AddUpdateListener(this);

        //GetComponent<Canvas>().worldCamera = SceneManager.INSTANCE.GetActivePointer().eventCamera;
        SceneManager.INSTANCE.RegisterCanvas(GetComponent<Canvas>());

        incomingRelationEntryDummy.GetComponent<Image>().color = color;
        outgoingRelationEntryDummy.GetComponent<Image>().color = color;

        incomingRelationEntryDummy.gameObject.SetActive(false);
        outgoingRelationEntryDummy.gameObject.SetActive(false);
    }



    public IEnumerator Show()
    {
        gameObject.SetActive(true);
        if (listOfRelatedElementsIsUpdated)
            yield break;

        listOfRelatedElementsIsUpdated = true;
        yield return initializeRelations();
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }



    protected abstract IEnumerator initializeRelations();

    protected HashSet<SpaceStation3D> createSetOfContainingStations(List<AbstractVisualElement3D> elements, AbstractVisualElement3D self = null)
    {
        HashSet<SpaceStation3D> containingStations = new HashSet<SpaceStation3D>();
        foreach (AbstractVisualElement3D element in elements)
        {
            SpaceStation3D targetStation;

            if (element is SpaceStation3D)
                targetStation = (SpaceStation3D)element;
            else if (element is SpaceStationAntenna3D)
                targetStation = (SpaceStation3D)((SpaceStationAntenna3D)element).directParent;
            else if (element is SpaceStationBlock3D)
                targetStation = (SpaceStation3D)((SpaceStationBlock3D)element).directParent;
            else throw new System.Exception("Unexpected kind of reference target: " + element.GetType().Name);

            while (targetStation.directParent is SpaceStation3D)
                targetStation = (SpaceStation3D)targetStation.directParent;

            if (self != null && self.Equals(targetStation))
                continue;

            containingStations.Add(targetStation);
        }

        return containingStations;
    }

    protected IEnumerator instantiateRelationEntriesForStations(HashSet<SpaceStation3D> stations, Button dummy)
    {
        for (int i = dummy.transform.parent.childCount - 1; i >= 0; i--)
        {
            Transform child = dummy.transform.parent.GetChild(i);
            if (child.Equals(dummy.transform))
                break;
            Destroy(child.gameObject);
        }

        int elementCount = 0;
        foreach (SpaceStation3D station in stations)
        {
            GameObject newEntryObject = Instantiate(dummy.gameObject, dummy.transform.parent, true);
            RectTransform rectTransform = newEntryObject.GetComponent<RectTransform>();
            Button newEntryButton = newEntryObject.GetComponent<Button>();
            EventTrigger newEntryEventTrigger = newEntryObject.GetComponent<EventTrigger>();
            TMP_Text newEntryLabel = newEntryObject.transform.GetChild(0).GetComponent<TMP_Text>();

            newEntryObject.SetActive(true);
            rectTransform.localScale = Vector3.one;
            rectTransform.localRotation = Quaternion.identity;
            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y - 25 * elementCount, rectTransform.localPosition.z);

            newEntryLabel.text = station.simpleName;

            {
                EventTrigger.Entry newEntry = new EventTrigger.Entry();
                newEntry.eventID = EventTriggerType.PointerEnter;
                newEntry.callback.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverStart(station); });
                newEntryEventTrigger.triggers.Add(newEntry);
            }
            {
                EventTrigger.Entry newEntry = new EventTrigger.Entry();
                newEntry.eventID = EventTriggerType.PointerExit;
                newEntry.callback.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverEnd(station); });
                newEntryEventTrigger.triggers.Add(newEntry);
            }

            newEntryButton.onClick.AddListener(delegate { AttachPinnedElementToHand(station); });

            if (!relatedElementToEntryObjectMap.ContainsKey(station))
                relatedElementToEntryObjectMap.Add(station, new List<GameObject>());
            relatedElementToEntryObjectMap[station].Add(newEntryObject);

            UpdatePinsOnBoardState();

            ISAUIInteractionManager.INSTANCE.RegisterHoverListener(station, this);

            elementCount++;
            if (elementCount % 2 == 1)
                yield return null;
        }
    }

    private void AttachPinnedElementToHand(SpaceStation3D station)
    {
        Hand emptyHand;
        if (Player.instance.rightHand.AttachedObjects.Count == 0)
            emptyHand = Player.instance.rightHand;
        else if (Player.instance.leftHand.AttachedObjects.Count == 0)
            emptyHand = Player.instance.leftHand;
        else
        {
            Player.instance.leftHand.TriggerHapticPulse(0.5f, 1, 1);
            Player.instance.rightHand.TriggerHapticPulse(0.5f, 1, 1);

            string hint = "Make space in either one\nof your hands so that\nyou can grab the element\nyou clicked on!";
            Player.instance.leftHand.ShowGrabHint(hint);
            StartCoroutine(waitUntilHideGrabHint(Player.instance.leftHand, 5));
            Player.instance.rightHand.ShowGrabHint(hint);
            StartCoroutine(waitUntilHideGrabHint(Player.instance.rightHand, 5));
            return;
        }

        GrabTypes grabbingType = GrabTypes.None;
        if (emptyHand.IsGrabbingWithType(GrabTypes.Pinch))
            grabbingType = GrabTypes.Pinch;
        else if (emptyHand.IsGrabbingWithType(GrabTypes.Grip))
            grabbingType = GrabTypes.Grip;
        else if (emptyHand.IsGrabbingWithType(GrabTypes.Trigger))
            grabbingType = GrabTypes.Trigger;

        if (grabbingType != GrabTypes.None)
        {
            station.ChangeVisibility(true);
            emptyHand.AttachObject(station.gameObject, grabbingType, station.GetComponent<Throwable>().attachmentFlags);
            emptyHand.TriggerHapticPulse(0.5f, 2, 1);
            emptyHand.HideGrabHint();
        }
        else
        {
            emptyHand.ShowGrabHint("Grab your controller while\nclicking on the entry to\nspawn the resp. classifier\nin your hand!");
            StartCoroutine(waitUntilHideGrabHint(emptyHand, 5));
        }
    }

    private IEnumerator waitUntilHideGrabHint(Hand hand, float duration)
    {
        yield return new WaitForSeconds(duration);
        ControllerButtonHints.HideTextHint(hand, SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabGrip"));
        hand.HideGrabHint();
    }

    protected void UpdatePinsOnBoardState()
    {
        foreach(AbstractVisualElement3D element in relatedElementToEntryObjectMap.Keys)
        {
            bool elementContained = board.ContainsPinForElement(element);
            List<GameObject> mappedListEntryObjects = relatedElementToEntryObjectMap[element];
            foreach (GameObject o in mappedListEntryObjects)
            {
                if (o == null)
                {
                    Debug.LogWarning("Updating pins in " + this.GetType() + ": a mapped entry got deleted, will remove...");
                }
                else if (elementContained)
                {
                    o.transform.GetChild(0).GetComponent<TMP_Text>().color = new Color(1, 1, 1, 1);
                    o.transform.GetChild(1).GetComponent<Image>().sprite = checkedSprite;
                }
                else
                {
                    o.transform.GetChild(0).GetComponent<TMP_Text>().color = new Color(1, 1, 1, 0.5f);
                    o.transform.GetChild(1).GetComponent<Image>().sprite = uncheckedSprite;
                }
            }
        }
    }



    public virtual void Destroy()
    {
        board.RemoveUpdateListener(this);
        ISAUIInteractionManager.INSTANCE.RemoveActivationListener(this);
        ISAUIInteractionManager.INSTANCE.RemoveHoverListener(this);
        relatedElementToEntryObjectMap.Clear();
    }

    public virtual void NotifyOnElementActiveStateChanged(object e, bool isActive, Vector3 position, Vector3 rotation)
    {
        // nothing to do here
    }

    public virtual void NotifyOnElementHoverStateChanged(object e, bool isHovered)
    {
        if (!(e is AbstractVisualElement3D))
            return;

        if(relatedElementToEntryObjectMap.ContainsKey((AbstractVisualElement3D)e))
        {
            List<GameObject> mappedListEntryObjects = relatedElementToEntryObjectMap[(AbstractVisualElement3D)e];
            foreach (GameObject o in mappedListEntryObjects)
            {
                if (o == null)
                    Debug.LogWarning("Received hover update for " + e + " in " + this.GetType() + ": a mapped entry got deleted, will remove...");
                else if (isHovered)
                    o.GetComponent<Image>().color = new Color(color.r / 2, color.g / 2, color.b / 2, 1);
                else
                    o.GetComponent<Image>().color = color;
            }
        }
    }

    public virtual void NotifyOnBoardUpdate()
    {
        UpdatePinsOnBoardState();
    }
}