using ISA.Modelling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Interactable))]
public class WhiteboardSpawnBehavior : MonoBehaviour, FallbackCameraGUIExtension
{
    private Transform parent;
    private new Rigidbody rigidbody;

    [SerializeField]
    private ActiveToggleable previewToggleable;



    void OnEnable()
    {
        ISAFallbackCameraController.RegisterListener(this);
        rigidbody = GetComponent<Rigidbody>();
        parent = transform.parent;
    }



    private void OnHandHoverBegin(Hand hand)
    {
        hand.ShowGrabHint();
    }

    private void OnHandHoverEnd(Hand hand)
    {
        hand.HideGrabHint();
    }

    public void OnAttachToHand()
    {
        previewToggleable.Show();
    }

    public void Reset(bool detachFromHand)
    {
        if(detachFromHand)
        {
            foreach (Hand hand in Player.instance.hands)
            {
                if (hand.ObjectIsAttached(gameObject))
                {
                    hand.DetachObject(gameObject, true);
                    break;
                }
            }
        }

        transform.parent = parent;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        rigidbody.isKinematic = true;

        previewToggleable.Hide();
    }



    public void SpawnNewWhiteboard()
    {
        Vector3 worldPosition = transform.position + transform.TransformVector(previewToggleable.transform.localPosition);
        //Vector3 worldRotation = (transform.rotation * Quaternion.FromToRotation(Vector3.forward, Vector3.up)).eulerAngles;
        Vector3 worldRotation = transform.rotation.eulerAngles;
        Reset(false);

        if (ModelLoader.instance == null || ModelLoader.instance.VisualizationModel == null)
            return;
        if (!SceneManager.INSTANCE.finishedLoading)
            return;

        WhiteboardSynchronizer.SendSpawnMessage(worldPosition, worldRotation);
    }



    public void DrawGUI()
    {
        if (ModelLoader.instance == null || ModelLoader.instance.VisualizationModel == null)
            return;
        if (!SceneManager.INSTANCE.finishedLoading)
            return;

        if (GUI.Button(new Rect(Screen.width / 2f - 100, Screen.height - 10 - 30 * 2, 200, 25), "Spawn Whiteboard"))
            SpawnNewWhiteboard();
    }

}
