using ISA.Modelling;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;




public interface WhiteboardUpdateListener
{
    public void NotifyOnBoardUpdate();
}

public interface WhiteboardPointerEventListener
{
    public void NotifyOnHoverStart(Vector3 worldPosition, Vector2 uvPosition);
    public void NotifyOnHoverEnd();
    public void NotifyOnHoverUpdate(Vector3 worldPosition, Vector2 uvPosition);
    public void NotifyOnTriggerPressed(Vector3 worldPosition, Vector2 uvPosition);
    public void NotifyOnTriggerReleased();
}

[RequireComponent(typeof(Renderer))]
public class WhiteboardHandler : MonoBehaviour
{
    public int id;

    public GameObject AudioPinPrefab;
    public GameObject SpaceStationPinPrefab;
    public GameObject ModulePinPrefab;

    public GameObject ModuleCanvasPrefab;
    public GameObject PicturePrefab;

    public Material ShapeGlowMaterial;

    public Vector2 textureSize = new Vector2(1024, 1024);
    public Color initialColor = Color.white;
    public float DelayUntilApplication = 1f;
    public float MinDistanceBetweenPinCenters = 0.025f;

    public float MaxDistanceToContinueShapes = 20f;

    public PenHandler pen;
    public float MaxDistanceBetweenModulePolygonCoordinatesInTextureSpace = 10f;
    public bool autoDrawArrowHead;

    public PrecisionGrabbingTarget precisionGrabbingTarget;
    public WhiteboardControlCanvasManager controlCanvas;

    private Texture2D texture;
    [HideInInspector]
    public bool ContainsPaint;
    [HideInInspector]
    public int CurrentSize;

    public List<AbstractPinHandler> visiblePins = new List<AbstractPinHandler>();
    public List<AbstractPinHandler> invisiblePins = new List<AbstractPinHandler>();

    public List<HandheldCameraPhoto> visiblePictures = new List<HandheldCameraPhoto>();
    public List<HandheldCameraPhoto> invisiblePictures = new List<HandheldCameraPhoto>();

    public int OperationStackSize = 16;

    public List<ModulePolygon> modules = new List<ModulePolygon>();
    public List<ArrowPolygon> arrows = new List<ArrowPolygon>();

    public Dictionary<ModulePolygon, ModulePolygonCanvas> moduleCanvases = new Dictionary<ModulePolygon, ModulePolygonCanvas>();

    private IEnumerator boardUpdateRoutine;

    private HashSet<WhiteboardUpdateListener> updateListeners = new HashSet<WhiteboardUpdateListener>();
    private HashSet<WhiteboardPointerEventListener> pointerListeners = new HashSet<WhiteboardPointerEventListener>();

    private WhiteboardSynchronizer synchronizer;
    public WhiteboardOperationExecuter operationExecuter { get; private set; }

    public bool AddUpdateListener(WhiteboardUpdateListener newListener) { return updateListeners.Add(newListener); }
    public bool RemoveUpdateListener(WhiteboardUpdateListener listener) { return updateListeners.Remove(listener); }

    public bool AddPointerEventListener(WhiteboardPointerEventListener newListener) { return pointerListeners.Add(newListener); }
    public bool RemovePointerEventListener(WhiteboardPointerEventListener listener) { return pointerListeners.Remove(listener); }



    void Awake()
    {
        operationExecuter = new WhiteboardOperationExecuter(OperationStackSize);

        if (texture == null)
            ResetToUniformTexture(initialColor);

        controlCanvas.Initialize(this);
        controlCanvas.Evaluate();

        synchronizer = new WhiteboardSynchronizer(this);

        IEnumerator updateHoverRoutine = UpdateHoverPositionRoutine();
        GetComponent<ISAUIInteractable>().onHoverStart.AddListener( delegate {
            RaycastHit hit = SceneManager.INSTANCE.GetActivePointer().hit;
            foreach (WhiteboardPointerEventListener listener in pointerListeners)
                listener.NotifyOnHoverStart(hit.point, hit.textureCoord);
            StopCoroutine(updateHoverRoutine);
            StartCoroutine(updateHoverRoutine);
        } );
        GetComponent<ISAUIInteractable>().onHoverEnd.AddListener(delegate {
            StopCoroutine(updateHoverRoutine);
            foreach (WhiteboardPointerEventListener listener in pointerListeners)
                listener.NotifyOnHoverEnd();
        });
        GetComponent<ISAUIInteractable>().onTriggerPressed.AddListener(delegate {
            RaycastHit hit = SceneManager.INSTANCE.GetActivePointer().hit;
            foreach (WhiteboardPointerEventListener listener in pointerListeners)
                listener.NotifyOnTriggerPressed(hit.point, hit.textureCoord);
        });
        GetComponent<ISAUIInteractable>().onTriggerReleased.AddListener(delegate {
            foreach (WhiteboardPointerEventListener listener in pointerListeners)
                listener.NotifyOnTriggerReleased();
        });

        GetComponent<AudioClipHelper>().Play("created");
    }

    private IEnumerator UpdateHoverPositionRoutine()
    {
        while(true)
        {
            yield return null;
            ISAVRPointer activePointer = SceneManager.INSTANCE.GetActivePointer();
            if (activePointer == null)
                continue;

            RaycastHit hit = activePointer.hit;
            foreach (WhiteboardPointerEventListener listener in pointerListeners)
                if(listener != null)
                    listener.NotifyOnHoverUpdate(hit.point, hit.textureCoord);
        }
    }



    public Transform GetRoot()
    {
        return transform.parent.parent;
    }

    public void SetTransform(Vector3 worldspacePosition, Vector3 worldspaceRotation)
    {
        Transform root = GetRoot();
        root.position = worldspacePosition;

        root.rotation = Quaternion.identity;
        Quaternion rotationQuaternion = Quaternion.Euler(worldspaceRotation); // * Quaternion.FromToRotation(Vector3.forward, Vector3.down);
        foreach (Transform t in root)
        {
            //if(t.TryGetComponent(out Rigidbody rb))
            //{
            //    rb.MovePosition(worldspacePosition);
            //    rb.MoveRotation(rotationQuaternion);
            //}
            //else
            //{
                t.localPosition = Vector3.zero;
                t.localRotation = rotationQuaternion;
            //}
        }
    }



    private void ResetToUniformTexture(Color color)
    {
        texture = new Texture2D((int)textureSize.x, (int)textureSize.y);
        texture.SetPixels(Enumerable.Repeat(color, (int)textureSize.x * (int)textureSize.y).ToArray());
        texture.Apply();

        SetRendererTexture(texture);

        ContainsPaint = false;
    }

    public Texture2D GetCopyOfTexture()
    {
        if (texture == null)
            ResetToUniformTexture(initialColor);
        return Instantiate(texture);
    }

    public void SetRendererTexture(Texture2D newTexture)
    {
        texture = newTexture;
        textureSize = new Vector2(texture.width, texture.height);
        GetComponent<Renderer>().material.mainTexture = newTexture;
    }

    public Color GetPixel(int x, int y)
    {
        return texture.GetPixel(x, y);
    }

    public void ApplyDrawnChanges()
    {
        texture.Apply();
        ContainsPaint = true;
    }



    public void Duplicate()
    {
        Vector3 clonePosition = transform.position - transform.up * 0.2f + transform.forward * 0.05f;
        Vector3 cloneRotation = precisionGrabbingTarget.transform.rotation.eulerAngles;
        synchronizer.SendDuplicateMessage(clonePosition, cloneRotation);
    }

    public void Destroy()
    {
        synchronizer.SendDestroyMessage();
    }



    public void Enlarge()
    {
        synchronizer.SendEnlargeMessage();
    }

    public void ClearPins()
    {
        synchronizer.SendPinsRemoveAllMessage();
    }

    public void ClearPaint()
    {
        synchronizer.SendClearPaintMessage();
    }



    public void UndoLastAction()
    {
        //if (CanUndoLastAction())
            synchronizer.SendUndoMessage();
    }

    public void RedoLastAction()
    {
        //if(CanRedoLastAction())
            synchronizer.SendRedoMessage();
    }



    public void UpdatePinsAndControlCanvas()
    {
        if (boardUpdateRoutine != null)
            StopCoroutine(boardUpdateRoutine);
        boardUpdateRoutine = DoUpdatePinsAndControlCanvas();
        StartCoroutine(boardUpdateRoutine);
    }

    private IEnumerator DoUpdatePinsAndControlCanvas()
    {
        controlCanvas.Evaluate();

        for (int i = 0; i < modules.Count; i++)
        {
            ModulePolygon module = modules[i];
            module.ResetPinContainment();
            foreach (AbstractPinHandler pin in visiblePins)
            {
                if (!(pin is SoftwareElementPinHandler))
                    continue;

                if (module.Contains(pin.TextureSpaceCoordinateOnDrawingBoard))
                {
                    ((SoftwareElementPinHandler)pin).AssignToModule(module);
                    module.AddPin((SoftwareElementPinHandler)pin);
                    //Debug.Log("Pin \"" + pin.name + "\" contained in module " + modulePolygons.IndexOf(module));
                }
            }
            yield return null;
        }

        yield return null;

        foreach (WhiteboardUpdateListener listener in updateListeners)
            listener.NotifyOnBoardUpdate();

        for (int i = 0; i < visiblePins.Count; i++)
        {
            if (visiblePins[i] is SoftwareElementPinHandler)
                ((SoftwareElementPinHandler)visiblePins[i]).UpdateRelationships();
            yield return null;
        }

        boardUpdateRoutine = null;

        yield return new WaitForSeconds(0.5f);
        controlCanvas.Evaluate();
    }



    public void AddAudioPin(MicrophoneHandler mic, Vector3 worldSpaceCoordinate, Vector2 textureSpaceCoordinate)
    {
        synchronizer.SendAudioPinAddMessage(mic, worldSpaceCoordinate, textureSpaceCoordinate);
    }

    public void AddSoftwarePin(AbstractVisualElement3D elementToMap, Vector3 worldSpaceCoordinate, Vector2 textureSpaceCoordinate)
    {
        AbstractPinHandler alreadyPresentPin = GetPinForElement(elementToMap);
        if (alreadyPresentPin == null)
            synchronizer.SendSoftwarePinAddMessage(elementToMap, worldSpaceCoordinate, textureSpaceCoordinate);
        else
            MovePin(alreadyPresentPin, worldSpaceCoordinate, textureSpaceCoordinate);
    }

    public void MovePin(AbstractPinHandler pin, Vector3 worldSpaceCoordinate, Vector2 textureSpaceCoordinate)
    {
        synchronizer.SendPinMoveMessage(pin, worldSpaceCoordinate, textureSpaceCoordinate);
    }

    public void RemovePin(AbstractPinHandler pin)
    {
        synchronizer.SendPinRemoveMessage(pin);
    }

    public void SplitPin(SoftwareElementPinHandler pin)
    {
        synchronizer.SendPinSplitMessage(pin);
    }

    public void HidePin(AbstractPinHandler pin)
    {
        visiblePins.Remove(pin);
        invisiblePins.Add(pin);

        pin.gameObject.SetActive(false);
        if (pin is SoftwareElementPinHandler)
            ((SoftwareElementPinHandler)pin).modelElement.UnregisterContainingDrawingboard(this);
    }

    public void UnhidePin(AbstractPinHandler pin)
    {
        visiblePins.Add(pin);
        invisiblePins.Remove(pin);

        pin.gameObject.SetActive(true);
        if (pin is SoftwareElementPinHandler)
            ((SoftwareElementPinHandler)pin).modelElement.RegisterContainingDrawingboard(this);
    }

    public List<AbstractPinHandler> GetAttachedPins()
    {
        return new List<AbstractPinHandler>(visiblePins);
    }

    public bool ContainsPins()
    {
        return visiblePins.Count > 0;
    }

    public bool ContainsPinForElement(AbstractVisualElement3D element)
    {
        return GetPinForElement(element) != null;
    }

    public AbstractPinHandler GetPinForElement(AbstractVisualElement3D element)
    {
        if (element == null)
            throw new ArgumentNullException("Cannot get pin for null");

        foreach (AbstractPinHandler pin in visiblePins)
            if(pin is SoftwareElementPinHandler)
                if (((SoftwareElementPinHandler) pin).modelElement.Equals(element))
                    return pin;
        return null;
    }

    public AbstractPinHandler GetPinWithId(int id)
    {
        if (id <= 0)
            throw new ArgumentNullException("Cannot get pin with id=" + id);

        foreach (AbstractPinHandler pin in visiblePins)
            if (pin.id == id)
                return pin;
        return null;
    }

    public Color GetColorForRelationBetweenPins(SoftwareElementPinHandler startPin, SoftwareElementPinHandler endPin)
    {
        foreach (ArrowPolygon arrow in arrows)
        {
            if (arrow.startModule.Contains(startPin) && arrow.endModule.Contains(endPin))
            {
                Color c = arrow.color;
                c.a = 0.8f;
                return c;
            }
        }
        Color defaultColor = new Color(0, 0, 0, 0.6f);
        return defaultColor;
    }

    public bool isPinPositionValid(Vector3 worldSpacePosition)
    {
        foreach (AbstractPinHandler pin in visiblePins)
            if (Vector3.Distance(pin.transform.position, worldSpacePosition) < MinDistanceBetweenPinCenters)
                return false;
        return true;
    }

    public bool isAreaValidForPins(Vector3 worldSpaceCenter, float radius, List<AbstractPinHandler> pinsToIgnore, List<Vector3> positionsToAdditionallyTakeIntoAccount)
    {
        // Shoot rays to the edges of the circle to see if it is entirely on the drawing board surface
        Vector3[] edgeRayOrigins = new Vector3[4];
        edgeRayOrigins[0] = worldSpaceCenter - transform.up * 0.1f + transform.right * radius + transform.forward * radius;
        edgeRayOrigins[1] = worldSpaceCenter - transform.up * 0.1f + transform.right * radius - transform.forward * radius;
        edgeRayOrigins[2] = worldSpaceCenter - transform.up * 0.1f - transform.right * radius + transform.forward * radius;
        edgeRayOrigins[3] = worldSpaceCenter - transform.up * 0.1f - transform.right * radius - transform.forward * radius;

        foreach(Vector3 edgeRayOrigin in edgeRayOrigins)
        {
            bool hitDrawingSurface = false;
            foreach (RaycastHit hit in Physics.RaycastAll(edgeRayOrigin, transform.up, 0.2f))
            {
                if (hit.collider.gameObject.TryGetComponent<WhiteboardHandler>(out WhiteboardHandler boardHandler))
                {
                    if (!boardHandler.Equals(this))
                        continue;

                    hitDrawingSurface = true;
                    break;
                }
            }
            if (!hitDrawingSurface)
                return false;
        }

        foreach (AbstractPinHandler pin in visiblePins)
            if (!pinsToIgnore.Contains(pin))
                if (Vector3.Distance(pin.transform.position, worldSpaceCenter) < radius + MinDistanceBetweenPinCenters)
                    return false;

        if(positionsToAdditionallyTakeIntoAccount != null)
            foreach (Vector3 additionalCheck in positionsToAdditionallyTakeIntoAccount)
                if (Vector3.Distance(additionalCheck, worldSpaceCenter) < radius + MinDistanceBetweenPinCenters)
                    return false;

        return true;
    }



    public void AddPhoto(HandheldCameraPhoto photo, Vector2 textureSpacePosition, Vector3 worldSpacePosition, Vector3 worldSpaceForwardDirection, Vector3 worldSpaceUpwardDirection)
    {
        if (photo.containingWhiteboard == null)
            synchronizer.SendPhotoAddMessage(photo, textureSpacePosition, worldSpacePosition, worldSpaceForwardDirection, worldSpaceUpwardDirection);
        else
            synchronizer.SendPhotoMoveMessage(photo, textureSpacePosition, worldSpacePosition, worldSpaceForwardDirection, worldSpaceUpwardDirection);
    }

    public void MovePhoto(HandheldCameraPhoto photo, Vector2 textureSpacePosition, Vector3 worldSpacePosition, Vector3 worldSpaceForwardDirection, Vector3 worldSpaceUpwardDirection)
    {
        synchronizer.SendPhotoMoveMessage(photo, textureSpacePosition, worldSpacePosition, worldSpaceForwardDirection, worldSpaceUpwardDirection);
    }

    public void RemovePhoto(HandheldCameraPhoto photo, bool playDissolveAnimation)
    {
        synchronizer.SendPhotoRemoveMessage(photo, playDissolveAnimation);
    }

    public void HidePhoto(HandheldCameraPhoto picture)
    {
        visiblePictures.Remove(picture);
        invisiblePictures.Add(picture);

        picture.gameObject.SetActive(false);
    }

    public void UnhidePhoto(HandheldCameraPhoto picture)
    {
        visiblePictures.Add(picture);
        invisiblePictures.Remove(picture);

        picture.gameObject.SetActive(true);
    }

    public HandheldCameraPhoto GetPhotoWithId(int id)
    {
        if (id <= 0)
            throw new ArgumentNullException("Cannot get pictures with id=" + id);

        foreach (HandheldCameraPhoto pic in visiblePictures)
            if (pic.id == id)
                return pic;
        return null;
    }



    private IEnumerator currentlyActiveDrawDelayRoutine = null;
    private List<List<Vector2>> registeredPointsOnBoard = new List<List<Vector2>>();
    private int lastDrawnFrame = 0;

    // Save the synchronized state of the whiteboard in this variable
    //      > once user finished drawing, revert to this state so that operations are consistent with other clients once synchronized
    //      > if this texture is set, wait with applying operations
    private Texture2D preDrawTexture = null;

    public void AddPaintViaOperation(PenMode penMode, Vector2 textureSpaceCoordinate, float opacity, int penDrawArea, Color[] colorArray, Color color)
    {
        if (!isSafeToDraw(textureSpaceCoordinate, penDrawArea))
            return;

        if (preDrawTexture == null)
            preDrawTexture = GetCopyOfTexture();

        // create one list for each continuous lines
        if (Time.frameCount-lastDrawnFrame > 2)
            registeredPointsOnBoard.Add(new List<Vector2>());
        lastDrawnFrame = Time.frameCount;

        registeredPointsOnBoard[registeredPointsOnBoard.Count - 1].Add(textureSpaceCoordinate);// + Vector2.one * penDrawArea / 2f);

        AddPaintDirectly(textureSpaceCoordinate, opacity, penDrawArea, colorArray);

        if(currentlyActiveDrawDelayRoutine != null)
            StopCoroutine(currentlyActiveDrawDelayRoutine);

        currentlyActiveDrawDelayRoutine = DelayedPaintApplication(penMode, penDrawArea, color, DelayUntilApplication);
        StartCoroutine(currentlyActiveDrawDelayRoutine);
    }

    private IEnumerator DelayedPaintApplication(PenMode penMode, int penDrawArea, Color color, float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);

        List<List<Vector2>> registeredPointsOnBoardReduced = new List<List<Vector2>>();
        foreach (List<Vector2> pointsOnLine in registeredPointsOnBoard)
        {
            if (pointsOnLine.Count == 0)
                continue;

            List<Vector2> reducedList = new List<Vector2>();
            reducedList.Add(pointsOnLine[0]);

            int lastIndex = 0;
            for (int i = 1; i < pointsOnLine.Count-1; i++)
            {
                if (Vector2.Distance(pointsOnLine[i], pointsOnLine[lastIndex]) > 2f)
                {
                    reducedList.Add(pointsOnLine[i]);
                    lastIndex = i;
                }
            }

            if (lastIndex != pointsOnLine.Count - 1 && reducedList.Count > 1)
                reducedList.Add(pointsOnLine[pointsOnLine.Count - 1]);

            registeredPointsOnBoardReduced.Add(reducedList);
        }

        synchronizer.SendDrawOperation(penMode, penDrawArea, color, registeredPointsOnBoardReduced, preDrawTexture);
        registeredPointsOnBoard.Clear();

        SetRendererTexture(preDrawTexture);
        preDrawTexture = null;
    }

    public bool IsCurrentlyDrawnOn()
    {
        return preDrawTexture != null;
    }

    public void AddPaintDirectly(Vector2 textureSpaceCoordinate, float opacity, int penDrawArea, Color[] colorArray)
    {
        for (int x = 0; x < penDrawArea; x++)
        {
            for (int y = 0; y < penDrawArea; y++)
            {
                Vector2 pixelCoordinate = new Vector2((int)textureSpaceCoordinate.x + x, (int)textureSpaceCoordinate.y + y);
                pixelCoordinate -= Vector2.one * (penDrawArea / 2f);

                Color penColor = colorArray[y * penDrawArea + x];
                penColor.a *= opacity;
                AddPaintOnPixel(penColor, pixelCoordinate);
            }
        }
    }

    private void AddPaintOnPixel(Color penColor, Vector2 pixelCoordinate)
    {
        if (pixelCoordinate.x >= texture.width)
            return;
        if (pixelCoordinate.y >= texture.height)
            return;

        Color oldColor = texture.GetPixel((int)pixelCoordinate.x, (int)pixelCoordinate.y);
        Color newColor = new Color(penColor.r, penColor.g, penColor.b) * penColor.a + new Color(oldColor.r, oldColor.g, oldColor.b) * (1f - penColor.a);
        texture.SetPixel((int)pixelCoordinate.x, (int)pixelCoordinate.y, newColor);
    }

    public bool isSafeToDraw(Vector2 textureSpaceCoordinate, int margin = 0)
    {
        if ((int)textureSpaceCoordinate.x < margin)
            return false;
        if ((int)textureSpaceCoordinate.y < margin)
            return false;

        if ((int)textureSpaceCoordinate.x > textureSize.x - margin)
            return false;
        if ((int)textureSpaceCoordinate.y > textureSize.y - margin)
            return false;

        return true;
    }



    public void SpawnErrorNotification(string title, string message, Vector3 localSpacePosition, float duration = 10)
    {
        Debug.LogError(title + ": " + message);

        GameObject errorCanvasObject = Instantiate(Resources.Load("Models/DrawingBoard/UI/Drawing Operation Error Message") as GameObject);
        errorCanvasObject.transform.SetParent(transform);
        errorCanvasObject.transform.localPosition = localSpacePosition + 0.01f * Vector3.down;
        errorCanvasObject.transform.localRotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);

        DrawingOperationErrorMessage handler = errorCanvasObject.GetComponent<DrawingOperationErrorMessage>();
        handler.SetTitle(title);
        handler.SetMessage(message);

        StartCoroutine(DelayedDestroy(handler, duration));
    }

    private IEnumerator DelayedDestroy(DrawingOperationErrorMessage handler, float delay)
    {
        float stepSize = 0.1f;
        for (float f = 0; f < delay; f += stepSize)
        {
            yield return new WaitForSeconds(stepSize);
            if (handler.gameObject == null)
                yield break;

            handler.SetAlpha(1 - Mathf.Pow(Mathf.Min(1, f / delay), 4));
        }

        Destroy(handler.gameObject);
    }
}
