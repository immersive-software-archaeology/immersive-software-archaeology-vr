﻿using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WhiteboardShape
{
    protected WhiteboardHandler parentBoard;

    protected List<List<Vector2>> registeredDrawnLinePoints;
    protected List<Vector2> flatListOfPoints;

    private float dissolveInterpolationPrecision = 1;

    protected Color highlightColor;
    public Color color { protected set; get; }

    private GameObject spawnedHighlightGameObject;

    protected DrawingBoardShapeHighlightModes currentHighlightMode;



    public WhiteboardShape(WhiteboardHandler parentBoard, List<List<Vector2>> registeredDrawnLinePoints, List<Vector2> flatListOfPoints, Color color)
    {
        this.parentBoard = parentBoard;
        this.registeredDrawnLinePoints = registeredDrawnLinePoints;
        this.flatListOfPoints = flatListOfPoints;
        this.color = color;

        Color.RGBToHSV(color, out float h, out float s, out _);
        highlightColor = Color.HSVToRGB(h, Mathf.Min(0.5f, s), 1) * 1.5f;
    }



    public int GetPointCount()
    {
        return flatListOfPoints.Count;
    }

    public List<Vector2> GetCopyOfPointsOnBoard()
    {
        return new List<Vector2>(flatListOfPoints);
    }

    public List<List<Vector2>> GetCopyOfOriginalDrawingCoordinates()
    {
        return new List<List<Vector2>>(registeredDrawnLinePoints);
    }

    public bool HasPolygonPointsInArea(List<Vector2> points, float radius)
    {
        foreach (Vector2 point in points)
            foreach (Vector2 pointOnBoard in flatListOfPoints)
                if (Vector2.Distance(pointOnBoard, point) <= radius)
                    return true;
        return false;
    }

    public void RemovePoints(List<Vector2> points, float radius)
    {
        foreach (Vector2 pointToRemove in points)
            RemovePoints(pointToRemove, radius);
    }

    public void RemovePoints(Vector2 pointToRemove, float radius)
    {
        for (int i = 0; i < flatListOfPoints.Count; i++)
        {
            Vector2 point = flatListOfPoints[i];
            if (Vector2.Distance(pointToRemove, point) < radius)
                flatListOfPoints.RemoveAt(i--);
        }
    }

    public void insertPoints(List<Vector2> newPoint, Vector2 predecessorInPolygon)
    {
        flatListOfPoints.InsertRange(flatListOfPoints.IndexOf(predecessorInPolygon), newPoint);
    }

    public void replacePoint(int index, Vector2 newPosition)
    {
        flatListOfPoints[index] = newPosition;
    }

    public float getSmallestDistanceToCoordinates(Vector2 point, out Vector2 closestPoint)
    {
        closestPoint = flatListOfPoints[0];
        float minDist = float.MaxValue;
        foreach (Vector2 c in flatListOfPoints)
        {
            float dist = Vector2.Distance(point, c);
            if (dist < minDist)
            {
                closestPoint = c;
                minDist = dist;
            }
        }
        return minDist;
    }

    public Vector2 getCenter()
    {
        if (flatListOfPoints.Count == 0)
            return Vector2.one / 2f;

        Vector2 center = Vector2.zero;
        foreach (Vector2 c in flatListOfPoints)
            center += c;
        return center / flatListOfPoints.Count;
    }

    public float getBoundingCircleRadius()
    {
        return getBoundingCircleRadius(getCenter());
    }

    public float getBoundingCircleRadius(Vector2 center)
    {
        float radius = 0;
        foreach (Vector2 c in flatListOfPoints)
        {
            float dist = Vector2.Distance(center, c);
            if (dist > radius)
                radius = dist;
        }
        return radius;
    }

    public float getLowestX()
    {
        float result = float.MaxValue;
        foreach (Vector2 c in flatListOfPoints)
            if (c.x < result)
                result = c.x;
        return result;
    }

    public abstract bool isValid();



    public void Dissolve(int dissolveSize)
    {
        Color[] dissolveColorArray = InitializeBrushTexture(dissolveSize);
        for (int i=0; i<flatListOfPoints.Count-1; i++)
        {
            Vector2 start = flatListOfPoints[i] - Vector2.one * (dissolveSize / 2f);
            Vector2 end = flatListOfPoints[i + 1] - Vector2.one * (dissolveSize / 2f);

            float interpolationStepSize = dissolveInterpolationPrecision / Mathf.Max(1 / dissolveSize, Vector2.Distance(start, end));
            for (float f = 0; f < 1f; f += interpolationStepSize)
            {
                Vector2 coordinate = Vector2.Lerp(start, end, f);
                parentBoard.AddPaintDirectly(coordinate, 1, dissolveSize, dissolveColorArray);
            }
        }
        parentBoard.ApplyDrawnChanges();
    }

    private Color[] InitializeBrushTexture(int dissolveSize)
    {
        Color[] dissolveColorArray = new Color[dissolveSize * dissolveSize];
        for (int x = 0; x < dissolveSize; x++)
        {
            for (int y = 0; y < dissolveSize; y++)
            {
                float alpha = Vector2.Distance(new Vector2(x, y), Vector2.one * (float)dissolveSize / 2f);
                alpha = alpha / ((float)dissolveSize / 2f);
                alpha = 1f - alpha;
                alpha = Mathf.Max(0, alpha);
                alpha = Mathf.Pow(alpha, 0.1f);

                dissolveColorArray[y * dissolveSize + x] = new Color(1, 1, 1, alpha);
            }
        }
        return dissolveColorArray;
    }



    public void SpawnHighlightRoutine(DrawingBoardShapeHighlightModes mode, float delay, float duration)
    {
        if (highlightRoutine != null)
            parentBoard.StopCoroutine(highlightRoutine);
        highlightRoutine = parentBoard.StartCoroutine(HighlightRoutine(mode, delay, duration));
    }

    Coroutine highlightRoutine;
    private IEnumerator HighlightRoutine(DrawingBoardShapeHighlightModes mode, float delay, float duration)
    {
        yield return new WaitForSeconds(delay);
        Highlight(mode);
        yield return new WaitForSeconds(duration);
        Unhighlight(true);
    }

    protected virtual bool Highlight(DrawingBoardShapeHighlightModes mode)
    {
        if (currentHighlightMode == DrawingBoardShapeHighlightModes.SPAWN)
            return false;

        if (spawnedHighlightGameObject != null)
            return false;

        currentHighlightMode = mode;

        spawnedHighlightGameObject = new GameObject("Highlight");
        spawnedHighlightGameObject.transform.parent = parentBoard.transform.parent;
        spawnedHighlightGameObject.transform.localPosition = Vector3.zero;
        spawnedHighlightGameObject.transform.localScale = Vector3.one;
        spawnedHighlightGameObject.transform.localRotation = Quaternion.identity;

        Material matCopy = GameObject.Instantiate(parentBoard.ShapeGlowMaterial);
        matCopy.SetColor("_Color", highlightColor);
        matCopy.SetColor("_BaseColor", highlightColor);
        matCopy.SetColor("Base Color", highlightColor);
        matCopy.SetColor("Color", highlightColor);

        LineRenderer lineRenderer = spawnedHighlightGameObject.AddComponent<LineRenderer>();
        lineRenderer.material = matCopy;
        lineRenderer.useWorldSpace = false;
        //lineRenderer.startColor = highlightColor;
        //lineRenderer.endColor = highlightColor;
        lineRenderer.widthMultiplier = 0.001f;
        SetLineRendererPositions(lineRenderer);

        return true;
    }

    protected abstract void SetLineRendererPositions(LineRenderer r);

    protected virtual bool Unhighlight(bool overrideChecks = false)
    {
        if(!overrideChecks)
            if (currentHighlightMode == DrawingBoardShapeHighlightModes.SPAWN)
                return false;
        currentHighlightMode = DrawingBoardShapeHighlightModes.NONE;

        if (spawnedHighlightGameObject == null)
            return false;

        GameObject.Destroy(spawnedHighlightGameObject);
        spawnedHighlightGameObject = null;
        return true;
    }
}

public enum DrawingBoardShapeHighlightModes
{
    NONE, SPAWN, HOVER
}