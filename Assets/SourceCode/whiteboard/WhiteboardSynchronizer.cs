using ISA.Modelling;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;
using ISA.Modelling.Whiteboard;
using ISA.Modelling.Clustering;
using ISA.Modelling.Multiplayer;

public class WhiteboardSynchronizer
{
    private static WhiteboardHandler lastUsedBoard;

    private static Coroutine whiteboardUpdateRoutine;
    private static Coroutine operationApplicationRoutine;

    private static long latestOperationOnServerTimestamp = 0;
    private static long latestFetchedOperationTimestamp = 0;
    private static long latestQueuedUpdateTimestamp = 0;

    private static List<AbstractWhiteboardEvent> eventQueue = new List<AbstractWhiteboardEvent>();



    public static void UpdateLatestServerSideWhiteboardEventTimestamp(long newTimestamp)
    {
        if (newTimestamp > latestOperationOnServerTimestamp)
            latestOperationOnServerTimestamp = newTimestamp;
    }

    public static void StartWhiteboardUpdates()
    {
        if (whiteboardUpdateRoutine != null)
            SceneManager.INSTANCE.StopCoroutine(whiteboardUpdateRoutine);
        if (operationApplicationRoutine != null)
            SceneManager.INSTANCE.StopCoroutine(operationApplicationRoutine);

//#if !UNITY_EDITOR
//        SceneManager.INSTANCE.showTutorialWhiteboards = true;
//#endif
//        if (SceneManager.INSTANCE.showTutorialWhiteboards)
//        {
//            _ = HTTPRequestHandler.INSTANCE.SendRequest("whiteboard/tutorial/", null,
//                delegate(string response)
//                {
//                    HistoryUpdateLoadedSuccess(response);
//                    whiteboardUpdateRoutine = SceneManager.INSTANCE.StartCoroutine(WhiteboardsUpdateRoutine());
//                    operationApplicationRoutine = SceneManager.INSTANCE.StartCoroutine(ApplySubmittedChanges());
//                }, HistoryUpdateLoadError);
//        }
//        else
//        {
            whiteboardUpdateRoutine = SceneManager.INSTANCE.StartCoroutine(WhiteboardsUpdateRoutine());
            operationApplicationRoutine = SceneManager.INSTANCE.StartCoroutine(ApplySubmittedChanges());
        //}
    }

    public static void StopWhiteboardUpdates()
    {
        if (whiteboardUpdateRoutine != null)
            SceneManager.INSTANCE.StopCoroutine(whiteboardUpdateRoutine);
        if (operationApplicationRoutine != null)
            SceneManager.INSTANCE.StopCoroutine(operationApplicationRoutine);

        latestOperationOnServerTimestamp = 0;
        latestFetchedOperationTimestamp = 0;
        latestQueuedUpdateTimestamp = 0;
        lastUsedBoard = null;
        eventQueue.Clear();
    }

    private static IEnumerator WhiteboardsUpdateRoutine()
    {
        while (ModelLoader.instance.VisualizationModel == null)
            yield return null;

        while (true)
        {
            yield return null;

            //Debug.Log("latestServerSideOperationTimestamp (" + latestOperationOnServerTimestamp + ") > latestFetchedUpdateTimestamp (" + latestFetchedOperationTimestamp + ") = " + (latestOperationOnServerTimestamp > latestFetchedOperationTimestamp));
            if (latestOperationOnServerTimestamp > latestFetchedOperationTimestamp)
            {
                //Debug.Log("Fetching Whiteboard Update!");
                try
                {
                    _ = HTTPRequestHandler.INSTANCE.SendRequest("whiteboard/fetch/?systemName=" + ModelLoader.instance.VisualizationModel.name + "&latestUpdateTimestamp=" + latestFetchedOperationTimestamp,
                        null, HistoryUpdateLoadedSuccess, HistoryUpdateLoadError);
                    latestFetchedOperationTimestamp = latestOperationOnServerTimestamp;
                }
                catch(Exception e)
                {
                    Debug.LogError(e);
                }
            }
        }
    }

    private static void HistoryUpdateLoadedSuccess(string response)
    {
        long latestTimestamp = 0;

        WhiteboardHistory submittedInformation = ModelLoader.ParseXMLCodeToObject<WhiteboardHistory>(response);
        foreach (AbstractWhiteboardEvent eventEcore in submittedInformation.eventLog ?? new AbstractWhiteboardEvent[0])
        {
            if (eventEcore.timestamp <= latestQueuedUpdateTimestamp)
                continue;
            if (eventEcore.timestamp > latestTimestamp)
                latestTimestamp = eventEcore.timestamp;

            eventQueue.Add(eventEcore);
        }

        if (latestTimestamp > latestQueuedUpdateTimestamp)
            latestQueuedUpdateTimestamp = latestTimestamp;
    }

    private static void HistoryUpdateLoadError(string response)
    {
        Debug.LogError(response);
    }

    private static IEnumerator ApplySubmittedChanges()
    {
        while (true)
        {
            while (true)
            {
                yield return null;

                if (!SceneManager.INSTANCE.finishedLoading)
                    continue;

                if (eventQueue.Count == 0)
                    continue;

                bool currentlyDrawing = false;
                foreach (WhiteboardHandler boardInScene in SceneManager.INSTANCE.GetWhiteboards())
                    if (boardInScene.IsCurrentlyDrawnOn())
                        currentlyDrawing = true;
                if (!currentlyDrawing)
                    break;
            }

            AbstractWhiteboardEvent nextQueuedEventEcore = getNextOperationFromStack();
            AbstractWhiteboardEventVR nextQueuedEventVR;

            if (nextQueuedEventEcore is AbstractOperation)
            {
                if(nextQueuedEventEcore is BoardEnlargeOperation)
                    nextQueuedEventVR = new BoardEnlargeOperationVR((BoardEnlargeOperation)nextQueuedEventEcore);

                else if (nextQueuedEventEcore is DrawFreehandOperation)
                    nextQueuedEventVR = new DrawFreehandOperationVR((DrawFreehandOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is DrawModuleOperation)
                    nextQueuedEventVR = new DrawModuleOperationVR((DrawModuleOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is DrawArrowOperation)
                    nextQueuedEventVR = new DrawArrowOperationVR((DrawArrowOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PaintEraseOperation)
                    nextQueuedEventVR = new PaintEraseOperationVR((PaintEraseOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PaintClearAllOperation)
                    nextQueuedEventVR = new PaintClearAllOperationVR((PaintClearAllOperation)nextQueuedEventEcore);

                else if (nextQueuedEventEcore is PinSoftwareAddOperation)
                    nextQueuedEventVR = new PinSoftwareAddOperationVR((PinSoftwareAddOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PinAudioAddOperation)
                    nextQueuedEventVR = new PinAudioAddOperationVR((PinAudioAddOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PinMoveOperation)
                    nextQueuedEventVR = new PinMoveOperationVR((PinMoveOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PinRemoveOperation)
                    nextQueuedEventVR = new PinRemoveOperationVR((PinRemoveOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PinSplitOperation)
                    nextQueuedEventVR = new PinSplitOperationVR((PinSplitOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PinsRemoveAllOperation)
                    nextQueuedEventVR = new PinsRemoveAllOperationVR((PinsRemoveAllOperation)nextQueuedEventEcore);

                else if (nextQueuedEventEcore is PhotoAddOperation)
                    nextQueuedEventVR = new PhotoAddOperationVR((PhotoAddOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PhotoMoveOperation)
                    nextQueuedEventVR = new PhotoMoveOperationVR((PhotoMoveOperation)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is PhotoRemoveOperation)
                    nextQueuedEventVR = new PhotoRemoveOperationVR((PhotoRemoveOperation)nextQueuedEventEcore);

                else throw new ArgumentException("Unknown kind of operation: " + nextQueuedEventEcore);

                WhiteboardHandler board = nextQueuedEventVR.getWhiteboardVR();
                yield return board.operationExecuter.ExecuteRoutine((AbstractOperationVR)nextQueuedEventVR, true);
            }
            else if (nextQueuedEventEcore is UndoEvent)
            {
                WhiteboardHandler board = SceneManager.INSTANCE.GetWhiteboard(nextQueuedEventEcore.whiteboardId);
                yield return board.operationExecuter.UndoRoutine(true);
            }
            else if (nextQueuedEventEcore is RedoEvent)
            {
                WhiteboardHandler board = SceneManager.INSTANCE.GetWhiteboard(nextQueuedEventEcore.whiteboardId);
                yield return board.operationExecuter.RedoRoutine(true);
            }
            else
            {
                if (nextQueuedEventEcore is BoardSpawnEvent)
                    nextQueuedEventVR = new BoardSpawnEventVR((BoardSpawnEvent)nextQueuedEventEcore);
                else if (nextQueuedEventEcore is BoardDestroyEvent)
                    nextQueuedEventVR = new BoardDestroyEventVR((BoardDestroyEvent)nextQueuedEventEcore);
                //throw new InvalidOperationException("Board duplication events should be handled server side by translating them to a series of operations as executed on the original board.");
                else throw new ArgumentException("Unknown kind of event: " + nextQueuedEventEcore);

                yield return nextQueuedEventVR.ExecuteRoutine(true);
            }

            if(nextQueuedEventEcore.confirmationRequired)
            {
                if(nextQueuedEventEcore.submittingClientId == SceneManager.INSTANCE.udpClient.id)
                {
                    WhiteboardHandler board = SceneManager.INSTANCE.GetWhiteboard(nextQueuedEventEcore.whiteboardId);
                    string path = "whiteboard/update-bitmap/?systemName=" + ModelLoader.instance.VisualizationModel.name + "&operationTimestamp=" + nextQueuedEventEcore.timestamp + "&whiteboardId=" + board.id;
                    string bitmapBase64 = Convert.ToBase64String(board.GetCopyOfTexture().EncodeToPNG());

                    _ = HTTPRequestHandler.INSTANCE.SendRequest(path, bitmapBase64, null,
                        delegate (string message) {
                            Debug.LogError("Something went wrong while propagating the whiteboard bitmap back: " + message);
                        });
                }
            }
        }
    }

    private static AbstractWhiteboardEvent getNextOperationFromStack()
    {
        AbstractWhiteboardEvent youngestEvent = null;
        foreach(AbstractWhiteboardEvent thisEvent in eventQueue)
            if(youngestEvent == null || thisEvent.timestamp < youngestEvent.timestamp)
                youngestEvent = thisEvent;

        eventQueue.Remove(youngestEvent);
        return youngestEvent;
    }



    private WhiteboardHandler board;

    public WhiteboardSynchronizer(WhiteboardHandler board)
    {
        this.board = board;
    }



    public static void SendSpawnMessage(Vector3 worldPosition, Vector3 worldRotation)
    {
        BoardSpawnEvent ecoreEvent = new BoardSpawnEvent();
        ecoreEvent.whiteboardId = 0; // will be set server-side
        ecoreEvent.worldSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(worldPosition);
        ecoreEvent.worldSpaceRotation = EcoreUtil.ConvertToEcoreWhiteboard(worldRotation);
        SubmitEvent(null, ecoreEvent);
    }

    public void SendDestroyMessage()
    {
        BoardDestroyEvent ecoreEvent = new BoardDestroyEvent();
        ecoreEvent.whiteboardId = board.id;
        SubmitEvent(board, ecoreEvent);
    }



    public void SendUndoMessage()
    {
        UndoEvent ecoreEvent = new UndoEvent();
        ecoreEvent.whiteboardId = board.id;
        SubmitEvent(board, ecoreEvent);
    }

    public void SendRedoMessage()
    {
        RedoEvent ecoreEvent = new RedoEvent();
        ecoreEvent.whiteboardId = board.id;
        SubmitEvent(board, ecoreEvent);
    }



    public void SendDuplicateMessage(Vector3 worldPosition, Vector3 worldRotation)
    {
        BoardDuplicateEvent ecoreEvent = new BoardDuplicateEvent();
        ecoreEvent.whiteboardId = board.id;
        ecoreEvent.worldSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(worldPosition);
        ecoreEvent.worldSpaceRotation = EcoreUtil.ConvertToEcoreWhiteboard(worldRotation);
        SubmitEvent(board, ecoreEvent);
    }



    public void SendAudioPinAddMessage(MicrophoneHandler mic, Vector3 worldSpaceCoordinate, Vector2 textureSpaceCoordinate)
    {
        PinAudioAddOperation ecoreOperation = new PinAudioAddOperation();
        ecoreOperation.whiteboardId = board.id;
        ecoreOperation.pinId = 0; // will be set server-side

        // include the header here, just use the SavWav stuff, that should do :-)
        //ecoreOperation.soundWaveBase64 = Convert.ToBase64String(AudioUtil.ConvertTo16BitArray(mic.lastRecordedClip));
        ecoreOperation.soundWaveBase64 = Convert.ToBase64String(SavWav.GetWav(mic.lastRecordedClip, out _, true));
        ecoreOperation.soundWaveFrequency = mic.lastRecordedClip.frequency;

        ecoreOperation.localSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformPoint(worldSpaceCoordinate));
        ecoreOperation.textureSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(textureSpaceCoordinate);

        SubmitEvent(board, ecoreOperation);
    }

    public void SendSoftwarePinAddMessage(AbstractVisualElement3D elementToMap, Vector3 worldSpaceCoordinate, Vector2 textureSpaceCoordinate)
    {
        PinSoftwareAddOperation ecoreOperation = new PinSoftwareAddOperation();
        ecoreOperation.whiteboardId = board.id;
        ecoreOperation.pinId = 0; // will be set server-side
        ecoreOperation.pinnedElementName = elementToMap.qualifiedName;

        if (elementToMap is TransparentCapsule3D)
            ecoreOperation.color = EcoreUtil.ConvertToEcoreWhiteboard(((TransparentCapsule3D)elementToMap).WhiteboardColor);
        else
            ecoreOperation.color = EcoreUtil.ConvertToEcoreWhiteboard(elementToMap.GetParentCapsule().WhiteboardColor);

        ecoreOperation.localSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformPoint(worldSpaceCoordinate));
        ecoreOperation.textureSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(textureSpaceCoordinate);

        SubmitEvent(board, ecoreOperation);
    }

    public void SendPinMoveMessage(AbstractPinHandler pin, Vector3 worldSpaceCoordinate, Vector2 textureSpaceCoordinate)
    {
        PinMoveOperation ecoreOperation = new PinMoveOperation();
        ecoreOperation.whiteboardId = board.id;
        ecoreOperation.pinId = pin.id;
        if(pin is SoftwareElementPinHandler)
            ecoreOperation.pinnedElementName = ((SoftwareElementPinHandler)pin).modelElement.qualifiedName;

        ecoreOperation.localSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformPoint(worldSpaceCoordinate));
        ecoreOperation.textureSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(textureSpaceCoordinate);

        SubmitEvent(board, ecoreOperation);
    }

    public void SendPinRemoveMessage(AbstractPinHandler pin)
    {
        PinRemoveOperation ecoreOperation = new PinRemoveOperation();
        ecoreOperation.whiteboardId = board.id;
        ecoreOperation.pinId = pin.id;
        if (pin is SoftwareElementPinHandler)
            ecoreOperation.pinnedElementName = ((SoftwareElementPinHandler)pin).modelElement.qualifiedName;

        SubmitEvent(board, ecoreOperation);
    }

    public void SendPinsRemoveAllMessage()
    {
        PinsRemoveAllOperation clearOperation = new PinsRemoveAllOperation();
        clearOperation.whiteboardId = board.id;

        List<AbstractOperation> subOperations = new List<AbstractOperation>();
        foreach (AbstractPinHandler pin in board.GetAttachedPins())
        {
            PinRemoveOperation subOperation = new PinRemoveOperation();
            subOperation.whiteboardId = board.id;
            subOperation.pinId = pin.id;
            if(pin is SoftwareElementPinHandler)
                subOperation.pinnedElementName = ((SoftwareElementPinHandler) pin).modelElement.qualifiedName;

            subOperations.Add(subOperation);
        }
        clearOperation.subOperations = subOperations.ToArray();

        SubmitEvent(board, clearOperation);
    }

    public void SendPinSplitMessage(SoftwareElementPinHandler pin)
    {
        if (!((TransparentCapsule3D)pin.modelElement is TransparentCapsule3D))
            return;

        _ = HTTPRequestHandler.INSTANCE.SendRequest("cluster/?systemName=" + ModelLoader.instance.VisualizationModel.name + "&packageName=" + pin.modelElement.qualifiedName, null,
        delegate (string xmlCode) {
            ISASimpleCluster clusteringResult = ModelLoader.ParseXMLCodeToObject<ISASimpleCluster>(xmlCode);

            PinSplitOperation splitOperation = new PinSplitOperation();
            splitOperation.whiteboardId = board.id;
            splitOperation.pinId = pin.id;
            splitOperation.pinnedElementName = pin.modelElement.qualifiedName;

            splitOperation.subOperations = PinSplitOperationVR.Prepare(pin, clusteringResult);
            if (splitOperation.subOperations != null)
                SubmitEvent(board, splitOperation);
            else
                Debug.Log("Something went wrong while preparing a pin split operation");
        },
        delegate(string message) {
            Debug.Log(message);
        });
    }



    public void SendEnlargeMessage()
    {
        BoardEnlargeOperation ecoreOperation = new BoardEnlargeOperation();
        ecoreOperation.whiteboardId = board.id;
        SubmitEvent(board, ecoreOperation);
    }

    public void SendClearPaintMessage()
    {
        PaintClearAllOperation ecoreOperation = new PaintClearAllOperation();
        ecoreOperation.whiteboardId = board.id;
        ecoreOperation.color = EcoreUtil.ConvertToEcoreWhiteboard(Color.white);
        SubmitEvent(board, ecoreOperation);
    }

    public void SendDrawOperation(PenMode penMode, int penSize, Color color, List<List<Vector2>> registeredPointsOnBoard, Texture2D preDrawTexture)
    {
        AbstractDrawOperation ecoreOperation;

        if (penMode == PenMode.FREEHAND)
            ecoreOperation = new DrawFreehandOperation();
        else if (penMode == PenMode.MODULE)
            ecoreOperation = new DrawModuleOperation();
        else if (penMode == PenMode.ARROW)
            ecoreOperation = new DrawArrowOperation();
        else if (penMode == PenMode.ERASER)
            ecoreOperation = new PaintEraseOperation();
        else
            throw new InvalidOperationException("Unknown operation type");

        if (penMode == PenMode.ARROW)
            ((DrawArrowOperation)ecoreOperation).autoDrawArrowHead = true;

        ecoreOperation.whiteboardId = board.id;
        ecoreOperation.penSize = penSize;
        ecoreOperation.drawnLines = EcoreUtil.convertToEcoreWhiteboard(registeredPointsOnBoard);
        ecoreOperation.color = EcoreUtil.ConvertToEcoreWhiteboard(color);

        if (penMode == PenMode.FREEHAND)
            SubmitEvent(board, (DrawFreehandOperation)ecoreOperation);
        else if (penMode == PenMode.MODULE)
            SubmitEvent(board, (DrawModuleOperation)ecoreOperation);
        else if (penMode == PenMode.ARROW)
            SubmitEvent(board, (DrawArrowOperation)ecoreOperation);
        else if (penMode == PenMode.ERASER)
            SubmitEvent(board, (PaintEraseOperation)ecoreOperation);
    }



    public void SendPhotoAddMessage(HandheldCameraPhoto photo, Vector2 textureSpacePosition, Vector3 worldSpacePosition, Vector3 worldSpaceForwardDirection, Vector3 worldSpaceUpwardDirection)
    {
        PhotoAddOperation ecoreOperation = new PhotoAddOperation();
        ecoreOperation.photoId = 0; // will be set server side
        ecoreOperation.bitmapBase64 = Convert.ToBase64String(photo.photoTexture.EncodeToPNG());
        ecoreOperation.whiteboardId = board.id;

        ecoreOperation.textureSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(textureSpacePosition);

        ecoreOperation.localSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformPoint(worldSpacePosition));
        ecoreOperation.localSpaceForwardDirection = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformDirection(worldSpaceForwardDirection));
        ecoreOperation.localSpaceUpwardDirection = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformDirection(worldSpaceUpwardDirection));

        ecoreOperation.cameraPositionWhenPhotoWasTaken = EcoreUtil.ConvertToEcoreWhiteboard(photo.cameraPosition);
        ecoreOperation.cameraRotationWhenPhotoWasTaken = EcoreUtil.ConvertToEcoreWhiteboard(photo.cameraRotation);
        ecoreOperation.systemPositionWhenPhotoWasTaken = EcoreUtil.ConvertToEcoreWhiteboard(photo.systemPositionToRestore);
        ecoreOperation.systemSizeWhenPhotoWasTaken = photo.systemSizeToRestore;
        ecoreOperation.relationsVisibleWhenPhotoWasTaken = photo.relationsToRestore;
        ecoreOperation.openCapsulesElementQualifiedNamesWhenPhotoWasTaken = photo.openCapsuleElementNames;

        SubmitEvent(board, ecoreOperation);
    }

    public void SendPhotoMoveMessage(HandheldCameraPhoto photo, Vector2 textureSpacePosition, Vector3 worldSpacePosition, Vector3 worldSpaceForwardDirection, Vector3 worldSpaceUpwardDirection)
    {
        PhotoMoveOperation ecoreOperation = new PhotoMoveOperation();
        ecoreOperation.photoId = photo.id;
        ecoreOperation.whiteboardId = board.id;

        ecoreOperation.textureSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(textureSpacePosition);

        ecoreOperation.localSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformPoint(worldSpacePosition));
        ecoreOperation.localSpaceForwardDirection = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformDirection(worldSpaceForwardDirection));
        ecoreOperation.localSpaceUpwardDirection = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformDirection(worldSpaceUpwardDirection));

        SubmitEvent(board, ecoreOperation);
    }

    public void SendPhotoRemoveMessage(HandheldCameraPhoto photo, bool playDissolveAnimation)
    {
        PhotoRemoveOperation ecoreOperation = new PhotoRemoveOperation();
        ecoreOperation.photoId = photo.id;
        ecoreOperation.whiteboardId = board.id;
        ecoreOperation.playDissolveAnimation = playDissolveAnimation; // not used

        SubmitEvent(board, ecoreOperation);
    }



    private static void SubmitEvent<T>(WhiteboardHandler board, T ecoreOperation) where T : AbstractWhiteboardEvent
    {
        ecoreOperation.submittingClientId = SceneManager.INSTANCE.udpClient.id;
        lastUsedBoard = board;

        string operationAsString = ModelLoader.encodeModelAsXML(ecoreOperation);

        if (ModelLoader.instance.VisualizationModel != null)
            _ = HTTPRequestHandler.INSTANCE.SendRequest("whiteboard/push/?systemName=" + ModelLoader.instance.VisualizationModel.name, operationAsString, SubmissionSuccess, SubmissionFail);
        else
            SubmissionFail("Cannot perform whiteboard operations without entering a subject system.");
    }

    private static void SubmissionSuccess(string response)
    {
        //Debug.Log("Event/Operation submitted successfully: " + response);
    }

    private static void SubmissionFail(string response)
    {
        string title = "Operation Was Not Applied";
        string message = "Your last operation was <b>rejected by the server</b>. It replies with:\n\n" + response;
        if (lastUsedBoard != null)
            lastUsedBoard.SpawnErrorNotification(title, message, Vector3.zero, 10);
        else
            Debug.LogError(message);
    }

}