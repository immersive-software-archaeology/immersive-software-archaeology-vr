﻿using ISA.Modelling.Multiplayer;
using ISA.Modelling.Visualization;
using ISA.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Canvas))]
public class PinInfoCanvasHandler : AbstractWhiteboardElementCanvasHandler
{

    [SerializeField]
    private Sprite highlightedSprite;
    [SerializeField]
    private Sprite unhighlightedSprite;
    [SerializeField]
    private Sprite classSprite;
    [SerializeField]
    private Sprite interfaceSprite;
    [SerializeField]
    private Sprite enumSprite;
    [SerializeField]
    private Sprite recordSprite;
    [SerializeField]
    private Sprite packageSprite;

    [SerializeField]
    private Button splitIntoComponentsButton;
    [SerializeField]
    private Button openVisualizationUIButton;

    [SerializeField]
    private Image iconImage;
    [SerializeField]
    private TMP_Text titleText;
    [SerializeField]
    private TMP_Text numberOfModulesText;
    [SerializeField]
    private TMP_Text numberOfClassifiersText;

    private SoftwareElementPinHandler pin;



    public void Initialize(WhiteboardHandler board, SoftwareElementPinHandler pin)
    {
        base.Initialize(board);

        this.pin = pin;

        ISAUIInteractionManager.INSTANCE.RegisterActivationListener(pin.modelElement, this);
        NotifyOnElementActiveStateChanged(pin.modelElement, ISAUIInteractionManager.INSTANCE.isActive(pin.modelElement), Vector3.zero, Vector3.zero);

        splitIntoComponentsButton.onClick.AddListener(delegate {
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(pin, false);
            Hide();
            board.SplitPin(pin);
        });
        openVisualizationUIButton.onClick.AddListener(delegate { ISAUIInteractionManager.INSTANCE.SubmitActiveStateToggleEvent(pin.modelElement); });

        if(pin.modelElement is TransparentCapsule3D)
        {
            iconImage.sprite = packageSprite;
        }
        else if (pin.modelElement is SpaceStation3D)
        {
            switch(((SpaceStation3D)pin.modelElement).ecoreModel.type)
            {
                case ISASpaceStationType.CLASS:
                    iconImage.sprite = classSprite;
                    break;
                case ISASpaceStationType.INTERFACE:
                    iconImage.sprite = interfaceSprite;
                    break;
                case ISASpaceStationType.ENUM:
                    iconImage.sprite = enumSprite;
                    break;
                case ISASpaceStationType.RECORD:
                    iconImage.sprite = recordSprite;
                    break;
                default:
                    Debug.LogError("Unrecognized kind of model element: " + ((SpaceStation3D)pin.modelElement).ecoreModel.type);
                    break;
            }
        }
        else Debug.LogError("Unhandled kind of model element: " + pin.modelElement);

        titleText.text = NamePlateManager.FormatName(pin.modelElement.qualifiedName);
        if(pin.modelElement is TransparentCapsule3D)
        {
            numberOfModulesText.text = ((TransparentCapsule3D)pin.modelElement).containedSubCapsules.Count.ToString();
            numberOfClassifiersText.text = ((TransparentCapsule3D)pin.modelElement).directContainment.containedSpaceStations.Count.ToString();
        }
    }



    protected override IEnumerator initializeRelations()
    {
        //Vector3 fallbackPinPosition = pin.transform.position - pin.transform.up * 0.1f;
        relatedElementToEntryObjectMap.Clear();

        {
            List<AbstractVisualElement3D> incomingRelationOriginElements = new List<AbstractVisualElement3D>();

            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetIncomingRelationships(RelationType.TYPE_REFERENCE))
                incomingRelationOriginElements.Add(relation.globalOriginElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetIncomingRelationships(RelationType.METHOD_CALL))
                incomingRelationOriginElements.Add(relation.globalOriginElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetIncomingRelationships(RelationType.FIELD_ACCESS))
                incomingRelationOriginElements.Add(relation.globalOriginElement);
            yield return null;

            HashSet<SpaceStation3D> incomingRelationOriginStations = createSetOfContainingStations(incomingRelationOriginElements, pin.modelElement);
            yield return null;

            yield return instantiateRelationEntriesForStations(incomingRelationOriginStations, incomingRelationEntryDummy);

            numberOfIncomingRelationsText.text = incomingRelationOriginStations.Count.ToString();
        }
        {
            List<AbstractVisualElement3D> outgoingRelationTargetElements = new List<AbstractVisualElement3D>();

            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetOutgoingRelationships(RelationType.TYPE_REFERENCE))
                outgoingRelationTargetElements.Add(relation.globalTargetElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetOutgoingRelationships(RelationType.METHOD_CALL))
                outgoingRelationTargetElements.Add(relation.globalTargetElement);
            yield return null;
            foreach (RelationManager.RelationInfo relation in pin.modelElement.GetOutgoingRelationships(RelationType.FIELD_ACCESS))
                outgoingRelationTargetElements.Add(relation.globalTargetElement);
            yield return null;

            HashSet<SpaceStation3D> outgoingRelationTargetStations = createSetOfContainingStations(outgoingRelationTargetElements, pin.modelElement);
            yield return null;

            yield return instantiateRelationEntriesForStations(outgoingRelationTargetStations, outgoingRelationEntryDummy);

            numberOfOutgoingRelationsText.text = outgoingRelationTargetStations.Count.ToString();
        }
    }



    public override void NotifyOnElementActiveStateChanged(object e, bool isActive, Vector3 position, Vector3 rotation)
    {
        if (!pin.modelElement.Equals(e))
            return;

        if(isActive)
        {
            openVisualizationUIButton.GetComponent<Image>().color = Color.HSVToRGB(25f / 255f, 0.75f, 1);
            openVisualizationUIButton.transform.GetChild(0).GetComponent<TMP_Text>().text = "Close UI";
            openVisualizationUIButton.transform.GetChild(1).GetComponent<Image>().sprite = highlightedSprite;
        }
        else
        {
            openVisualizationUIButton.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            openVisualizationUIButton.transform.GetChild(0).GetComponent<TMP_Text>().text = "Open UI";
            openVisualizationUIButton.transform.GetChild(1).GetComponent<Image>().sprite = unhighlightedSprite;
        }
    }
}