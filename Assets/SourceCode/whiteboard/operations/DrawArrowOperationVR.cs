﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DrawArrowOperationVR : AbstractDrawOperationVR
{

    private ArrowPolygon arrow;
    private bool autoDrawArrowHead;



    public DrawArrowOperationVR(DrawArrowOperation ev) : base(ev)
    {
        this.autoDrawArrowHead = ev.autoDrawArrowHead;
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        yield return base.ExecuteRoutine(false);

        arrow = CreateArrow(board, color, registeredDrawnLinePoints, flatListOfPoints, out ModulePolygon closestModuleToStart, out ModulePolygon closestModuleToEnd);

        if (autoDrawArrowHead)
        {
            DrawArrowHead();
            yield return null;
        }

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();

        board.GetComponent<AudioClipHelper>().Play("new arrow");

        closestModuleToStart.SpawnHighlightRoutine(DrawingBoardShapeHighlightModes.SPAWN, 0, 0.5f);
        arrow.SpawnHighlightRoutine(DrawingBoardShapeHighlightModes.SPAWN, 0.5f, 0.5f);
        closestModuleToEnd.SpawnHighlightRoutine(DrawingBoardShapeHighlightModes.SPAWN, 1f, 0.5f);

        //Debug.Log("New arrow from module " + modulePolygons.IndexOf(closestModuleToStart) + " to module " + modulePolygons.IndexOf(closestModuleToEnd));
    }

    public static ArrowPolygon CreateArrow(WhiteboardHandler board, Color color, List<List<Vector2>> registeredDrawnLinePoints, List<Vector2> flatListOfPoints, out ModulePolygon closestModuleToStart, out ModulePolygon closestModuleToEnd)
    {
        Vector2 startPoint = flatListOfPoints[0];
        Vector2 endPoint = flatListOfPoints[flatListOfPoints.Count - 1];

        float smallestDistanceToStart = float.MaxValue;
        float smallestDistanceToEnd = float.MaxValue;

        closestModuleToStart = null;
        closestModuleToEnd = null;
        foreach (ModulePolygon module in board.modules)
        {
            float distToStart = module.getSmallestDistanceToCoordinates(startPoint, out _);
            if (distToStart < smallestDistanceToStart)
            {
                smallestDistanceToStart = distToStart;
                closestModuleToStart = module;
            }
            float distToEnd = module.getSmallestDistanceToCoordinates(endPoint, out _);
            if (distToEnd < smallestDistanceToEnd)
            {
                smallestDistanceToEnd = distToEnd;
                closestModuleToEnd = module;
            }
        }

        if (closestModuleToEnd == null || closestModuleToStart == null)
        {
            board.GetComponent<AudioClipHelper>().Play("fail");
            return null;
        }

        ArrowPolygon arrow = new ArrowPolygon(board, closestModuleToStart, closestModuleToEnd, color, registeredDrawnLinePoints, flatListOfPoints);
        AddArrowToBoard(board, arrow);
        return arrow;
    }

    public static void AddArrowToBoard(WhiteboardHandler board, ArrowPolygon arrow)
    {
        board.arrows.Add(arrow);
    }

    public static void RemoveArrowFromBoard(WhiteboardHandler board, ArrowPolygon arrow)
    {
        //arrow.Unhighlight();
        board.arrows.Remove(arrow);
    }

    private void DrawArrowHead()
    {
        Vector2 endPoint = flatListOfPoints[flatListOfPoints.Count - 1];
        Vector2 referencePoint = Vector2.one * -1;
        float traveledDistance = 0;
        for (int i = flatListOfPoints.Count - 2; i >= 0; i--)
        {
            traveledDistance += Vector2.Distance(flatListOfPoints[i + 1], flatListOfPoints[i]);
            if (traveledDistance > 100)
            {
                referencePoint = flatListOfPoints[i + 1];
                break;
            }
        }
        if (referencePoint.x < 0)
            referencePoint = flatListOfPoints[0];

        Vector2 endDirection = (referencePoint - endPoint).normalized;
        Vector2 normalofEndDirection = new Vector2(-endDirection.y, endDirection.x);

        Vector2 pointAlongReferencePointLine = endPoint + endDirection * 20f;
        Vector2 arrowTipEnd1 = pointAlongReferencePointLine + normalofEndDirection * 20f;
        Vector2 arrowTipEnd2 = pointAlongReferencePointLine - normalofEndDirection * 20f;

        float interpolationStepSize = PenHandler.INTERPOLATION_PRECISION / Mathf.Max(1 / penSize, Vector2.Distance(endPoint, arrowTipEnd1));
        if (interpolationStepSize == 0)
            return;

        for (float f = interpolationStepSize; f < 1f; f += interpolationStepSize)
        {
            Vector2 intermediateCoordinate1 = Vector2.Lerp(endPoint, arrowTipEnd1, f);
            board.AddPaintDirectly(intermediateCoordinate1, 1, (int)penSize, colorArray);

            Vector2 intermediateCoordinate2 = Vector2.Lerp(endPoint, arrowTipEnd2, f);
            board.AddPaintDirectly(intermediateCoordinate2, 1, (int)penSize, colorArray);
        }

        board.ApplyDrawnChanges();
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return base.ExecuteRoutine(false);
        AddArrowToBoard(board, arrow);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        yield return base.UndoRoutine(false);
        RemoveArrowFromBoard(board, arrow);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override object GetHoverEventElementToRegister()
    {
        return arrow;
    }



    protected override string GetName()
    {
        return "Arrow Drawn";
    }
}