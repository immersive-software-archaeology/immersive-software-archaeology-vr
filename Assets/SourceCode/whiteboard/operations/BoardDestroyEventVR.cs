﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class BoardDestroyEventVR : AbstractWhiteboardEventVR
{

    public BoardDestroyEventVR(BoardDestroyEvent ev) : base(ev)
    {
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        board.SetTransform(Vector3.down * 10, Quaternion.identity.eulerAngles);

        foreach (AbstractPinHandler pin in board.visiblePins)
        {
            pin.Destroy();
            yield return null;
        }
        foreach (AbstractPinHandler pin in board.invisiblePins)
        {
            pin.Destroy();
            yield return null;
        }
        foreach (ModulePolygonCanvas canvas in new List<ModulePolygonCanvas>(board.moduleCanvases.Values))
            canvas.Destroy();

        foreach (Hand h in Player.instance.hands)
            h.DetachObject(board.pen.gameObject, true);

        SceneManager.INSTANCE.UnregisterWhiteboard(board);
        UnityEngine.Object.Destroy(board.GetRoot().gameObject);
    }

}