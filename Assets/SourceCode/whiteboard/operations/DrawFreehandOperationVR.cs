﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DrawFreehandOperationVR : AbstractDrawOperationVR
{

    public DrawFreehandOperationVR(DrawFreehandOperation ev) : base(ev)
    {
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return ExecuteRoutine(updateOnFinish);
    }



    public override object GetHoverEventElementToRegister()
    {
        return null;
    }

    protected override string GetName()
    {
        return "Freehand Drawn";
    }

}