﻿using ISA.Modelling.Whiteboard;
using System.Collections;
using UnityEngine;

public class PhotoRemoveOperationVR : AbstractPhotoOperationVR
{

    private bool playDissolveAnimation;

    public PhotoRemoveOperationVR(PhotoRemoveOperation ev) : base(ev)
    {
        playDissolveAnimation = ev.playDissolveAnimation;
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        board.HidePhoto(photo);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return ExecuteRoutine(updateOnFinish);
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        board.UnhidePhoto(photo);

        photo.ResetPosition();

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    protected override string GetName()
    {
        return "Photo Removed";
    }
}