﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteboardOperationExecuter
{

    private int stackSize;
    private List<AbstractOperationVR> undoStack;
    private int positionInUndoStack;



    public WhiteboardOperationExecuter(int stackSize)
    {
        this.stackSize = stackSize;
        this.undoStack = new List<AbstractOperationVR>(stackSize);
        this.positionInUndoStack = -1;
    }



    public IEnumerator ExecuteRoutine(AbstractOperationVR op, bool updateOnFinish)
    {
        yield return op.ExecuteRoutine(updateOnFinish);

        for (int i = undoStack.Count - 1; i > positionInUndoStack; i--)
        {
            //undoStack[i].FreeRoutine();
            undoStack.RemoveAt(i);
        }

        if (undoStack.Count == stackSize)
        {
            //undoStack[0].FreeRoutine();
            undoStack.RemoveAt(0);
            positionInUndoStack--;
        }

        undoStack.Add(op);
        positionInUndoStack++;
    }



    public bool CanRedo()
    {
        return positionInUndoStack < undoStack.Count - 1;
    }

    public IEnumerator RedoRoutine(bool updateOnFinish)
    {
        if (!CanRedo())
            yield break;

        AbstractOperationVR op = undoStack[++positionInUndoStack];
        op.getWhiteboardVR().GetComponent<AudioClipHelper>().Play("redo");
        yield return op.RedoRoutine(updateOnFinish);
    }



    public bool CanUndo()
    {
        return positionInUndoStack >= 0;
    }

    public IEnumerator UndoRoutine(bool updateOnFinish)
    {
        if (!CanUndo())
            yield break;

        AbstractOperationVR op = undoStack[positionInUndoStack--];
        op.getWhiteboardVR().GetComponent<AudioClipHelper>().Play("undo");
        yield return op.UndoRoutine(updateOnFinish);
    }



    public List<AbstractOperationVR> GetCopyOfUndoStack()
    {
        return new List<AbstractOperationVR>(undoStack);
    }

    public int GetPositionInUndoStack()
    {
        return positionInUndoStack;
    }

    public override string ToString()
    {
        String result = "Operation Executer " + this.GetHashCode() + " - size=" + undoStack.Count + ":\n";
        for (int i = 0; i < undoStack.Count; i++)
        {
            if (i == positionInUndoStack)
                result += " > ";
            else
                result += "   ";
            result += undoStack[i].GetType() + "\n";
        }
        return result;
    }

}