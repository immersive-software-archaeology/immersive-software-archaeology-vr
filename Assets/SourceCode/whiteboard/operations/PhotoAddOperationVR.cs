﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using UnityEngine;

public class PhotoAddOperationVR : AbstractPhotoOperationVR
{

    protected string bitmapBase64;

    protected Vector3 cameraPositionToRestore;
    protected Vector3 cameraRotationToRestore;

    protected Vector3 systemPositionToRestore;
    protected float systemSizeToRestore;

    protected VisibleRelation[] relationsToRestore;
    protected string[] openCapsuleElementNames;



    public PhotoAddOperationVR(PhotoAddOperation ev) : base(ev)
    {
        bitmapBase64 = ev.bitmapBase64;

        cameraPositionToRestore = EcoreUtil.ConvertToUnity(ev.cameraPositionWhenPhotoWasTaken);
        cameraRotationToRestore = EcoreUtil.ConvertToUnity(ev.cameraRotationWhenPhotoWasTaken);

        systemPositionToRestore = EcoreUtil.ConvertToUnity(ev.systemPositionWhenPhotoWasTaken);
        systemSizeToRestore = ev.systemSizeWhenPhotoWasTaken;

        relationsToRestore = ev.relationsVisibleWhenPhotoWasTaken;
        openCapsuleElementNames = ev.openCapsulesElementQualifiedNamesWhenPhotoWasTaken;
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        byte[] pngBytes = Convert.FromBase64String(bitmapBase64);
        Texture2D texture = new Texture2D(2, 2);
        texture.LoadImage(pngBytes);

        yield return null;

        //if (ev.clientId == SceneManager.INSTANCE.syncClient.id)
        //{
        //    // this client shot a photo
        //    SceneManager.INSTANCE.handheldCamera.SpawnPhoto(texture);
        //}
        //else
        //{
        //    // another client shot a photo
        //    Collaborator otherClient = SceneManager.INSTANCE.syncClient.GetCollaborator((byte)ev.clientId);
        //    otherClient.SpawnPhoto(texture);
        //}

        GameObject photoGameObject = UnityEngine.Object.Instantiate(board.PicturePrefab);
        photoGameObject.transform.parent = board.transform;

        photo = photoGameObject.GetComponent<HandheldCameraPhoto>();
        photo.Initialize(texture, photoId);
        photo.SetRestoreSystemData(cameraPositionToRestore, cameraRotationToRestore, systemPositionToRestore, systemSizeToRestore, relationsToRestore, openCapsuleElementNames);
        photo.UpdatePosition(board, localSpacePosition, localSpaceRotation);

        board.UnhidePhoto(photo);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        board.UnhidePhoto(photo);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        board.HidePhoto(photo);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    protected override string GetName()
    {
        return "Photo Added";
    }

}