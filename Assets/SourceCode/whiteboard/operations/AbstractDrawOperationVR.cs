﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public abstract class AbstractDrawOperationVR : AbstractOperationVR
{

    protected List<List<Vector2>> registeredDrawnLinePoints;
    protected List<Vector2> flatListOfPoints;

    protected Texture2D preTexture;

    public Color color { get; protected set; }
    protected Color[] colorArray;

    protected int penSize;



    public AbstractDrawOperationVR(AbstractDrawOperation ev) : base(ev)
    {
        penSize = ev.penSize;
        if (penSize == 0)
            penSize = 5;

        registeredDrawnLinePoints = EcoreUtil.ConvertToUnity(ev.drawnLines);
        flatListOfPoints = new List<Vector2>();
        foreach (List<Vector2> lineList in registeredDrawnLinePoints)
            flatListOfPoints.AddRange(lineList);

        color = EcoreUtil.ConvertToUnity(ev.color);
        colorArray = PenHandler.InitializeBrushTexture(penSize, color);

        if(board != null)
            preTexture = board.GetCopyOfTexture();
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        foreach (List<Vector2> linePoints in registeredDrawnLinePoints)
        {
            if(linePoints.Count == 1)
            {
                board.AddPaintDirectly(linePoints[0], 1, penSize, colorArray);
                continue;
            }

            for (int i = 0; i < linePoints.Count - 1; i++)
            {
                Vector2 thisPoint = linePoints[i];
                Vector2 nextPoint = linePoints[i + 1];

                float dist = Vector2.Distance(thisPoint, nextPoint);
                if (dist <= float.Epsilon)
                    continue;

                float interpolationStepSize = PenHandler.INTERPOLATION_PRECISION / Mathf.Max(1 / penSize, dist);
                if (interpolationStepSize <= float.Epsilon)
                    continue;

                for (float f = 0; f < 1f; f += interpolationStepSize)
                {
                    Vector2 intermediateCoordinate1 = Vector2.Lerp(thisPoint, nextPoint, f);
                    board.AddPaintDirectly(intermediateCoordinate1, 1, penSize, colorArray);
                }
            }
        }
        board.ApplyDrawnChanges();

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        board.SetRendererTexture(UnityEngine.Object.Instantiate(preTexture));

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }

}