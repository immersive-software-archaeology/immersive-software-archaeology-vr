﻿using ISA.Modelling.Whiteboard;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinsRemoveAllOperationVR : AbstractCompoundOperationVR
{

    public PinsRemoveAllOperationVR(PinsRemoveAllOperation ev) : base(ev)
    {
    }

    //public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    //{
    //    List<PinHandler> copy = new List<PinHandler>(board.GetAttachedPins());
    //    foreach (PinHandler pin in copy)
    //    {
    //        PinRemoveOperation ecoreOp = new PinRemoveOperation();
    //        ecoreOp.whiteboardID = board.ID;
    //        ecoreOp.pinnedElementName = pin.modelElement.QualifiedName;

    //        PinRemoveOperationVR removeOp = new PinRemoveOperationVR(ecoreOp);
    //        subOperationsVR.Add(removeOp);
    //        yield return removeOp.ExecuteRoutine(false);
    //    }

    //    if (updateOnFinish)
    //        board.UpdatePinsAndControlCanvas();
    //}



    public override object GetHoverEventElementToRegister()
    {
        return null;
    }



    protected override string GetName()
    {
        return "All Pins Removed";
    }

}