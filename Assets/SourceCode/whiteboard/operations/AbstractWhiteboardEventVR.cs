﻿

using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractWhiteboardEventVR : System.IComparable
{

    public long serverSideTimestamp { get; private set; }
    protected WhiteboardHandler board;



    protected AbstractWhiteboardEventVR(AbstractWhiteboardEvent ev)
    {
        board = SceneManager.INSTANCE.GetWhiteboard(ev.whiteboardId);
        serverSideTimestamp = ev.timestamp;
    }



    public WhiteboardHandler getWhiteboardVR()
    {
        return board;
    }


    public int CompareTo(object obj)
    {
        if (!(obj is AbstractWhiteboardEventVR))
            return 0;
        return serverSideTimestamp.CompareTo(((AbstractWhiteboardEventVR)obj).serverSideTimestamp);
    }



    public abstract IEnumerator ExecuteRoutine(bool updateOnFinish);

}