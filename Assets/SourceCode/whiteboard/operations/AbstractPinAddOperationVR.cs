﻿using ISA.Modelling.Whiteboard;
using ISA.util;
using System;
using System.Collections;
using UnityEngine;

public abstract class AbstractPinAddOperationVR : AbstractPinOperationVR
{

    protected AbstractPinAddOperationVR(AbstractPinOperation ev) : base(ev)
    {
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        board.UnhidePin(pin);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        board.HidePin(pin);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }
}