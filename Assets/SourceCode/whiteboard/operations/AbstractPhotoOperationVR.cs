﻿using ISA.Modelling.Whiteboard;
using System.Collections;
using UnityEngine;

public abstract class AbstractPhotoOperationVR : AbstractOperationVR
{

    protected HandheldCameraPhoto photo;

    protected int photoId;

    protected Vector3 localSpacePosition;
    protected Quaternion localSpaceRotation;



    protected AbstractPhotoOperationVR(AbstractPhotoOperation ev) : base(ev)
    {
        photoId = ev.photoId;

        photo = board.GetPhotoWithId(photoId);

        localSpacePosition = EcoreUtil.ConvertToUnity(ev.localSpacePosition);

        Vector3 localSpaceForward = EcoreUtil.ConvertToUnity(ev.localSpaceForwardDirection);
        Vector3 localSpaceUp = EcoreUtil.ConvertToUnity(ev.localSpaceUpwardDirection);
        localSpaceRotation = Quaternion.LookRotation(localSpaceForward, localSpaceUp);
    }



    public override object GetHoverEventElementToRegister()
    {
        return photo;
    }
}