﻿using ISA.Modelling.Whiteboard;
using ISA.util;
using System;
using System.Collections;
using UnityEngine;

public abstract class AbstractPinOperationVR : AbstractOperationVR
{

    protected AbstractPinHandler pin;
    protected AbstractVisualElement3D pinnedElement3D;
    protected int pinId;

    protected Vector3 localSpacePosition;
    protected Vector2 textureSpacePosition;

    protected AbstractPinOperationVR(AbstractPinOperation ev) : base(ev)
    {
        pinId = ev.pinId;
        if(ev.pinnedElementName != null)
        {
            pinnedElement3D = SceneManager.INSTANCE.elementResolver.FindElementByQualifiedName(ev.pinnedElementName);
            pin = board.GetPinForElement(pinnedElement3D);
        }
        else
        {
            pin = board.GetPinWithId(pinId);
        }

        localSpacePosition = EcoreUtil.ConvertToUnity(ev.localSpacePosition);
        textureSpacePosition = EcoreUtil.ConvertToUnity(ev.textureSpacePosition);
    }

    public override object GetHoverEventElementToRegister()
    {
        if(pin is SoftwareElementPinHandler)
            return ((SoftwareElementPinHandler) pin).modelElement;
        else
            return null;
    }

}