﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class PaintEraseOperationVR : AbstractDrawOperationVR
{
    protected List<ModulePolygon> originalModules = new List<ModulePolygon>();
    protected List<ModulePolygon> modifiedModules = new List<ModulePolygon>();

    protected List<ModulePolygonCanvas> originalModuleCanvases = new List<ModulePolygonCanvas>();
    protected List<ModulePolygonCanvas> modifiedModuleCanvases = new List<ModulePolygonCanvas>();

    protected List<ArrowPolygon> originalArrows = new List<ArrowPolygon>();
    protected List<ArrowPolygon> modifiedArrows = new List<ArrowPolygon>();



    public PaintEraseOperationVR(PaintEraseOperation ev) : base(ev)
    {
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        yield return base.ExecuteRoutine(false);

        foreach (ModulePolygon originalModule in new List<ModulePolygon>(board.modules))
        {
            if (originalModule.HasPolygonPointsInArea(flatListOfPoints, (float)penSize / 2f))
            {
                ReplaceModule(originalModule);
                yield return null;
            }
        }
        foreach (ArrowPolygon originalArrow in new List<ArrowPolygon>(board.arrows))
        {
            if (originalArrow.HasPolygonPointsInArea(flatListOfPoints, (float)penSize / 2f))
            {
                ReplaceArrow(originalArrow);
                yield return null;
            }
        }

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }

    private void ReplaceModule(ModulePolygon originalModule)
    {
        ModulePolygonCanvas originalModuleCanvas = board.moduleCanvases[originalModule];

        originalModules.Add(originalModule);
        originalModuleCanvases.Add(originalModuleCanvas);

        // hide the original module, is essentially the content of the ModuleDrawOperation.Undo() method
        {
            DrawModuleOperationVR.RemoveModuleFromBoard(board, originalModule, originalModuleCanvas);
        }

        GetReducedPoints(originalModule, out List<Vector2> reducedFlatListOfPoints, out List<List<Vector2>> reducedRegisteredDrawnLinePoints);
        if (reducedFlatListOfPoints.Count <= Mathf.Min(4, originalModule.GetPointCount() / 3f))
        {
            // removed a significant amount of the previously present points: delete module entirely.
            originalModule.Dissolve(penSize);

            // Add blankets so that we can later linearly go through the list and
            // check for them to see that modules were deleted and not replaced
            modifiedModules.Add(null);
            modifiedModuleCanvases.Add(null);
        }
        else
        {
            ModulePolygon module = DrawModuleOperationVR.CreateModule(board, originalModule.color, reducedRegisteredDrawnLinePoints, reducedFlatListOfPoints);
            ModulePolygonCanvas canvas = DrawModuleOperationVR.CreateModuleCanvas(board, module);

            modifiedModules.Add(module);
            modifiedModuleCanvases.Add(canvas);
        }
    }

    private void ReplaceArrow(ArrowPolygon originalArrow)
    {
        originalArrows.Add(originalArrow);

        // hide the original arrow, is essentially the content of the ArrowDrawOperation.Undo() method
        {
            DrawArrowOperationVR.RemoveArrowFromBoard(board, originalArrow);
        }

        GetReducedPoints(originalArrow, out List<Vector2> reducedFlatListOfPoints, out List<List<Vector2>> reducedRegisteredDrawnLinePoints);
        if (reducedFlatListOfPoints.Count <= Mathf.Min(4, originalArrow.GetPointCount() / 3f))
        {
            // removed a significant amount of the previously present points: delete arrow entirely.
            originalArrow.Dissolve(penSize);
            modifiedArrows.Add(null);
        }
        else
        {
            ArrowPolygon arrow = DrawArrowOperationVR.CreateArrow(board, originalArrow.color, reducedRegisteredDrawnLinePoints, reducedFlatListOfPoints, out _, out _);
            modifiedArrows.Add(arrow);
        }
    }

    private void GetReducedPoints(WhiteboardShape polygonShape, out List<Vector2> reducedFlatListOfPoints, out List<List<Vector2>> reducedRegisteredDrawnLinePoints)
    {
        reducedFlatListOfPoints = polygonShape.GetCopyOfPointsOnBoard();
        reducedRegisteredDrawnLinePoints = polygonShape.GetCopyOfOriginalDrawingCoordinates();
        foreach (Vector2 pointToRemove in flatListOfPoints)
        {
            for (int i = 0; i < reducedFlatListOfPoints.Count; i++)
            {
                if (Vector2.Distance(pointToRemove, reducedFlatListOfPoints[i]) <= (float)penSize / 2f)
                    reducedFlatListOfPoints.RemoveAt(i--);
            }
            for (int i = 0; i < reducedRegisteredDrawnLinePoints.Count; i++)
            {
                List<Vector2> pointsInLine = reducedRegisteredDrawnLinePoints[i];
                for (int j = 0; j < pointsInLine.Count; j++)
                {
                    if (Vector2.Distance(pointToRemove, pointsInLine[j]) <= (float)penSize / 2f)
                        pointsInLine.RemoveAt(j--);
                }
                if (pointsInLine.Count <= 1)
                    reducedRegisteredDrawnLinePoints.RemoveAt(i--);
            }
        }
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return base.ExecuteRoutine(false);

        for (int i = 0; i < originalModules.Count; i++)
        {
            DrawModuleOperationVR.RemoveModuleFromBoard(board, originalModules[i], originalModuleCanvases[i]);
            if (modifiedModules[i] != null && modifiedModuleCanvases[i] != null)
                DrawModuleOperationVR.AddModuleToBoard(board, modifiedModules[i], modifiedModuleCanvases[i]);
        }

        for (int i = 0; i < originalArrows.Count; i++)
        {
            DrawArrowOperationVR.RemoveArrowFromBoard(board, originalArrows[i]);
            if (modifiedArrows[i] != null)
                DrawArrowOperationVR.AddArrowToBoard(board, modifiedArrows[i]);
        }

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        yield return base.UndoRoutine(updateOnFinish);

        for (int i = 0; i < originalModules.Count; i++)
        {
            DrawModuleOperationVR.AddModuleToBoard(board, originalModules[i], originalModuleCanvases[i]);
            if (modifiedModules[i] != null && modifiedModuleCanvases[i] != null)
                DrawModuleOperationVR.RemoveModuleFromBoard(board, modifiedModules[i], modifiedModuleCanvases[i]);
        }

        for (int i = 0; i < originalArrows.Count; i++)
        {
            DrawArrowOperationVR.AddArrowToBoard(board, originalArrows[i]);
            if (modifiedArrows[i] != null)
                DrawArrowOperationVR.RemoveArrowFromBoard(board, modifiedArrows[i]);
        }

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override object GetHoverEventElementToRegister()
    {
        return null;
    }



    protected override string GetName()
    {
        return "Erased Paint";
    }

}