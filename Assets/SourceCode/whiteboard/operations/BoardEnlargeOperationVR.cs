﻿using ISA.Modelling.Whiteboard;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class BoardEnlargeOperationVR : AbstractDrawOperationVR
{

    protected Texture2D postTexture;

    protected float preRatio;
    protected float postRatio;

    protected Transform drawingAreaTransform;

    public BoardEnlargeOperationVR(BoardEnlargeOperation ev) : base(ev)
    {
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        // temporarily unset the texture...
        board.GetComponent<Renderer>().material.mainTexture = null;

        foreach (StretchWhenWhiteboardEnlarges elementToEnlarge in board.GetRoot().GetComponentsInChildren<StretchWhenWhiteboardEnlarges>())
        {
            Mesh meshToStretch = elementToEnlarge.GetComponent<MeshFilter>().mesh;
            if (elementToEnlarge.TryGetComponent<WhiteboardHandler>(out _))
            {
                drawingAreaTransform = elementToEnlarge.transform;
                preRatio = calculateRatio(meshToStretch.vertices);
            }

            updateVertices(meshToStretch, true);
            updateColliders(elementToEnlarge.transform);
        }

        yield return null;

        Mesh mesh = drawingAreaTransform.GetComponent<MeshFilter>().mesh;
        postRatio = calculateRatio(mesh.vertices);
        updateUVs(mesh, true);

        // Update all drawn shapes on the board
        // This needs to be done _before_ updating the texture!
        foreach (ModulePolygon module in board.modules)
            updateDrawingBoardShapePosition(module, true);
        foreach (ArrowPolygon arrow in board.arrows)
            updateDrawingBoardShapePosition(arrow, true);

        foreach (AbstractPinHandler pin in board.GetAttachedPins())
            updatePinPosition(pin, true);

        yield return null;

        // Update the texture
        {
            int oldTextureHeight = (int)board.textureSize.y;
            Vector2 newTextureSize = new Vector2(board.textureSize.x, board.textureSize.x * postRatio);
            int textureHeightDiff = (int)newTextureSize.y - oldTextureHeight;

            //Debug.Log("ratio = " + ratio + ", " + "original ratio = " + originalRatio + ", " + "texture height diff = " + textureHeightDiff);

            postTexture = new Texture2D((int)newTextureSize.x, (int)newTextureSize.y);
            for (int y = 0; y < (int)newTextureSize.y; y++)
            {
                if (y > textureHeightDiff / 2 && y < oldTextureHeight + textureHeightDiff / 2)
                {
                    // paint original drawing
                    for (int x = 0; x < (int)newTextureSize.x; x++)
                        postTexture.SetPixel(x, y, board.GetPixel(x, y - textureHeightDiff / 2));
                }
                else
                {
                    // paint in white
                    for (int x = 0; x < (int)newTextureSize.x; x++)
                        postTexture.SetPixel(x, y, Color.white);
                }

                if (y % 200 == 0)
                    yield return null;
            }
            postTexture.Apply();

            board.SetRendererTexture(postTexture);
        }

        board.CurrentSize += 1;

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }

    private float calculateRatio(Vector3[] vertices)
    {
        Vector2 extremeX = new Vector2(float.MaxValue, float.MinValue);
        Vector2 extremeY = extremeX;
        Vector2 extremeZ = extremeX;

        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i].x < extremeX.x)
                extremeX.x = vertices[i].x;
            if (vertices[i].y < extremeY.x)
                extremeY.x = vertices[i].y;
            if (vertices[i].z < extremeZ.x)
                extremeZ.x = vertices[i].z;

            if (vertices[i].x > extremeX.y)
                extremeX.y = vertices[i].x;
            if (vertices[i].y > extremeY.y)
                extremeY.y = vertices[i].y;
            if (vertices[i].z > extremeZ.y)
                extremeZ.y = vertices[i].z;
        }

        Vector3 diff = new Vector3((extremeX.y - extremeX.x), (extremeY.y - extremeY.x), (extremeZ.y - extremeZ.x));
        return diff.x / diff.z;
    }

    private void updateVertices(Mesh mesh, bool enlarge)
    {
        Vector3[] vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i].x < 0)
                vertices[i] = vertices[i] + (enlarge ? Vector3.left : Vector3.right) * 0.1f;
            else if (vertices[i].x > 0)
                vertices[i] = vertices[i] + (enlarge ? Vector3.right : Vector3.left) * 0.1f;
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }

    private void updateUVs(Mesh mesh, bool enlarge)
    {
        Vector2[] uvs = mesh.uv;
        for (int i = 0; i < uvs.Length; i++)
        {
            float currentGap;
            if (uvs[i].y > 0.5f)
                currentGap = 1f - uvs[i].y;
            else
                currentGap = uvs[i].y;

            float postGap;
            if (enlarge)
                postGap = currentGap * preRatio / postRatio;
            else
                postGap = currentGap / preRatio * postRatio;

            if (uvs[i].y > 0.5f)
                uvs[i] = new Vector2(uvs[i].x, 1f - postGap);
            else
                uvs[i] = new Vector2(uvs[i].x, postGap);
        }
        mesh.uv = uvs;
    }

    private void updateDrawingBoardShapePosition(WhiteboardShape shape, bool enlarge)
    {
        float preTextureSize = board.textureSize.y;
        float postTextureSize;
        if (enlarge)
            postTextureSize = board.textureSize.y / preRatio * postRatio;
        else
            postTextureSize = board.textureSize.y * preRatio / postRatio;

        List<Vector2> pointsCopy = shape.GetCopyOfPointsOnBoard();
        for (int i = 0; i < pointsCopy.Count; i++)
        {
            float preGap = pointsCopy[i].y - preTextureSize / 2f;
            shape.replacePoint(i, new Vector2(pointsCopy[i].x, preGap + postTextureSize / 2f));
        }
    }

    private void updatePinPosition(AbstractPinHandler pin, bool enlarge)
    {
        float preTextureSize = board.textureSize.y;
        float postTextureSize;
        if (enlarge)
            postTextureSize = board.textureSize.y / preRatio * postRatio;
        else
            postTextureSize = board.textureSize.y * preRatio / postRatio;

        float preGap = pin.TextureSpaceCoordinateOnDrawingBoard.y - preTextureSize / 2f;

        Vector2 TextureSpaceCoordinateOnDrawingBoard = new Vector2(pin.TextureSpaceCoordinateOnDrawingBoard.x, preGap + postTextureSize / 2f);
        pin.UpdatePosition(pin.LocalSpacePositionOnDrawingBoard, TextureSpaceCoordinateOnDrawingBoard, false);
    }

    private void updateColliders(Transform elementToEnlarge)
    {
        MeshCollider oldCollider = elementToEnlarge.GetComponent<MeshCollider>();
        //MeshCollider newCollider = elementToEnlarge.gameObject.AddComponent<MeshCollider>();

        MeshCollider colliderToSetup = oldCollider;
        if (colliderToSetup == null)
            return;

        colliderToSetup.sharedMesh = elementToEnlarge.GetComponent<MeshFilter>().sharedMesh;
        if(!elementToEnlarge.TryGetComponent<WhiteboardHandler>(out _))
        {
            colliderToSetup.cookingOptions = MeshColliderCookingOptions.CookForFasterSimulation | MeshColliderCookingOptions.EnableMeshCleaning | MeshColliderCookingOptions.WeldColocatedVertices;
            colliderToSetup.convex = true;
        }
        //UnityEngine.Object.DestroyImmediate(oldCollider);
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        foreach (ModulePolygon module in board.modules)
            updateDrawingBoardShapePosition(module, false);
        foreach (ArrowPolygon arrow in board.arrows)
            updateDrawingBoardShapePosition(arrow, false);

        foreach (AbstractPinHandler pin in board.GetAttachedPins())
            updatePinPosition(pin, false);

        yield return base.UndoRoutine(updateOnFinish);

        foreach (StretchWhenWhiteboardEnlarges elementToEnlarge in board.GetRoot().GetComponentsInChildren<StretchWhenWhiteboardEnlarges>())
        {
            updateVertices(elementToEnlarge.GetComponent<MeshFilter>().mesh, false);
            updateColliders(elementToEnlarge.transform);
        }
        updateUVs(drawingAreaTransform.GetComponent<MeshFilter>().mesh, false);

        board.CurrentSize -= 1;

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        foreach (ModulePolygon module in board.modules)
            updateDrawingBoardShapePosition(module, true);
        foreach (ArrowPolygon arrow in board.arrows)
            updateDrawingBoardShapePosition(arrow, true);

        foreach (AbstractPinHandler pin in board.GetAttachedPins())
            updatePinPosition(pin, true);

        board.SetRendererTexture(UnityEngine.Object.Instantiate(postTexture));

        foreach (StretchWhenWhiteboardEnlarges elementToEnlarge in board.GetRoot().GetComponentsInChildren<StretchWhenWhiteboardEnlarges>())
        {
            updateVertices(elementToEnlarge.GetComponent<MeshFilter>().mesh, true);
            updateColliders(elementToEnlarge.transform);
        }
        updateUVs(drawingAreaTransform.GetComponent<MeshFilter>().mesh, true);

        board.CurrentSize += 1;

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override object GetHoverEventElementToRegister()
    {
        return null;
    }

    protected override string GetName()
    {
        return "Board Enlarged";
    }
}