﻿using ISA.Modelling.Whiteboard;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractCompoundOperationVR : AbstractOperationVR
{

    protected List<AbstractOperationVR> subOperationsVR = new List<AbstractOperationVR>();



    public AbstractCompoundOperationVR(AbstractCompoundOperation ev) : base(ev)
    {
        foreach(AbstractOperation subOp in ev.subOperations)
        {
            AbstractOperationVR newOperationVR;
            if (subOp is PinSoftwareAddOperation)
                newOperationVR = new PinSoftwareAddOperationVR((PinSoftwareAddOperation)subOp);
            else if (subOp is PinMoveOperation)
                newOperationVR = new PinMoveOperationVR((PinMoveOperation)subOp);
            else if (subOp is PinRemoveOperation)
                newOperationVR = new PinRemoveOperationVR((PinRemoveOperation)subOp);
            else throw new System.InvalidOperationException("Operations of type " + subOp.GetType().Name + " are not yet supported for execution in the context of a compound operation.");

            subOperationsVR.Add(newOperationVR);
        }
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        foreach (AbstractOperationVR operation in subOperationsVR)
            yield return operation.ExecuteRoutine(false);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        for (int i = 0; i < subOperationsVR.Count; i++)
            yield return subOperationsVR[i].RedoRoutine(false);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        for (int i = subOperationsVR.Count - 1; i >= 0; i--)
            yield return subOperationsVR[i].UndoRoutine(false);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }

}