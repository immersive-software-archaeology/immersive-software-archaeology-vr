﻿using ISA.Modelling;
using ISA.util;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using System.Net.Http;
using System;
using HullDelaunayVoronoi.Delaunay;
using HullDelaunayVoronoi.Primitives;
using ISA.Modelling.Whiteboard;
using ISA.Modelling.Clustering;

public class PinSplitOperationVR : AbstractCompoundOperationVR
{

    protected string pinnedElementName;
    protected SoftwareElementPinHandler pin;

    protected List<List<string>> clusters;



    public PinSplitOperationVR(PinSplitOperation ev) : base(ev)
    {
        pinnedElementName = ev.pinnedElementName;
        AbstractVisualElement3D pinnedElement3D = SceneManager.INSTANCE.elementResolver.FindElementByQualifiedName(ev.pinnedElementName);
        
        AbstractPinHandler pin = board.GetPinForElement(pinnedElement3D);
        if (pin is SoftwareElementPinHandler)
            this.pin = (SoftwareElementPinHandler)pin;
        else
            throw new Exception("Pin of type SoftwareElementPinHandler expected but found pin of type " + pin.GetType().Name);
    }



    public override object GetHoverEventElementToRegister()
    {
        return pin.modelElement;
    }



    protected override string GetName()
    {
        return "Pin Split Up: " + pin.modelElement.simpleName + " <color=#00000044>[" + pin.modelElement.qualifiedName + "]</color>";
    }



    public static AbstractOperation[] Prepare(SoftwareElementPinHandler pin, ISASimpleCluster clusteringResult)
    {
        List<List<string>> flatListOfClusters = new List<List<string>>();
        extractClusterStructure(flatListOfClusters, clusteringResult);

        WhiteboardHandler board = pin.parentBoard;
        TransparentCapsule3D capsule = (TransparentCapsule3D)pin.modelElement;

        // add capsules contained in the to-be split capsule
        foreach (TransparentCapsule3D subCapsule in capsule.containedSubCapsules)
        {
            List<string> subCapsuleSingletonList = new List<string>();
            subCapsuleSingletonList.Add(subCapsule.qualifiedName);
            flatListOfClusters.Add(subCapsuleSingletonList);
        }

        int numberOfPins = 0;
        int numberOfPinSlots = 0;

        foreach (List<string> cluster in flatListOfClusters)
        {
            numberOfPins += cluster.Count;
            numberOfPinSlots += cluster.Count + 1;
        }
        if (flatListOfClusters.Count == 1)
            numberOfPinSlots -= 1;

        float radius = LayoutUtil.CalculateRadiusOfCircleBasedOnDistanceBetweenVertices(numberOfPinSlots, board.MinDistanceBetweenPinCenters);

        string[] classifierNames = new string[numberOfPins];
        Vector2[] localPositionsOfPins = new Vector2[numberOfPins];

        int currentIndexInList = 0;
        int currentIndexInCircle = 0;
        foreach (List<string> cluster in flatListOfClusters)
        {
            foreach (string classifierName in cluster)
            {
                classifierNames[currentIndexInList] = classifierName;
                localPositionsOfPins[currentIndexInList] = LayoutUtil.CalculateVertexPositionInUnitCircle(currentIndexInCircle, numberOfPinSlots) * radius;

                currentIndexInList++;
                currentIndexInCircle++;
            }
            currentIndexInCircle++;
        }

        // Add pins for the content of the split pin with previously calculated positions
        float radiusOfCluster = radius + board.MinDistanceBetweenPinCenters;
        Vector3 layoutCircleCenterWorldPosition = board.transform.TransformPoint(pin.LocalSpacePositionOnDrawingBoard);
        {
            int attempts = 0;
            while (!board.isAreaValidForPins(layoutCircleCenterWorldPosition, radiusOfCluster, new List<AbstractPinHandler>() { pin }, null))
            {
                // position is invalid
                if (attempts++ > 100)
                {
                    Debug.LogError("Could not find a suitable place for the split pin structure");
                    board.SpawnErrorNotification("Not enough space",
                        "Could not find a location on the board to spawn pins for containment.",
                        pin.LocalSpacePositionOnDrawingBoard);
                    return null;
                }

                // Try to find an alternative position
                Vector2 randomOffset = RandomFactory.INSTANCE.getRandomVector2(board.MinDistanceBetweenPinCenters, board.MinDistanceBetweenPinCenters + (float)attempts * board.MinDistanceBetweenPinCenters / 10f);
                layoutCircleCenterWorldPosition = board.transform.TransformPoint(pin.LocalSpacePositionOnDrawingBoard) + board.transform.right * randomOffset.x + board.transform.forward * randomOffset.y;
            }
        }

        List<AbstractOperation> subOperations = new List<AbstractOperation>();

        // Remove the original pin
        {
            PinRemoveOperation removeOperation = new PinRemoveOperation();
            removeOperation.whiteboardId = board.id;
            removeOperation.pinId = pin.id;
            removeOperation.pinnedElementName = pin.modelElement.qualifiedName;
            subOperations.Add(removeOperation);
        }

        List<Vector3> allToBeSpawnedPinWorldPositions = new List<Vector3>();

        // Get a list of all elements contained in the to be split pin
        List<AbstractVisualElement3D> containedElements = new List<AbstractVisualElement3D>();
        foreach (SpaceStation3D containedStation in capsule.directContainment.containedSpaceStations)
            containedElements.Add(containedStation);
        foreach (TransparentCapsule3D child in capsule.containedSubCapsules)
            containedElements.Add(child);

        for (int j = 0; j < classifierNames.Length; j++)
        {
            AbstractVisualElement3D element = getElementByName(containedElements, classifierNames[j]);

            Vector3 pinWorldSpacePositionWithoutCircleOffset = board.transform.TransformPoint(new Vector3(localPositionsOfPins[j].x, 0, localPositionsOfPins[j].y) / board.transform.localScale.x);
            Vector3 worldSpacePinPosition = pinWorldSpacePositionWithoutCircleOffset + layoutCircleCenterWorldPosition - board.transform.position;
            allToBeSpawnedPinWorldPositions.Add(worldSpacePinPosition);

            Vector2 textureSpaceCoordinate = rayCastTextureSpaceCoordinate(board, worldSpacePinPosition);

            AbstractPinHandler pinOnBoard = board.GetPinForElement(element);
            AbstractPinOperation subOperation;
            if (pinOnBoard == null)
            {
                subOperation = new PinSoftwareAddOperation();
                subOperation.pinId = 0; // will be set server-side
            }
            else
            {
                subOperation = new PinMoveOperation();
                subOperation.pinId = pinOnBoard.id;
            }

            subOperation.whiteboardId = board.id;
            subOperation.pinnedElementName = classifierNames[j];
            subOperation.localSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(board.transform.InverseTransformPoint(worldSpacePinPosition));
            subOperation.textureSpacePosition = EcoreUtil.ConvertToEcoreWhiteboard(textureSpaceCoordinate);
            subOperation.color = EcoreUtil.ConvertToEcoreWhiteboard(element.GetParentCapsule().WhiteboardColor);

            subOperations.Add(subOperation);
        }

        return subOperations.ToArray();
    }

    private static void extractClusterStructure(List<List<string>> flatListOfClusters, ISASimpleCluster cluster)
    {
        if (cluster.elementNames != null)
        {
            List<string> classifiers = new List<string>();
            classifiers.AddRange(cluster.elementNames);
            flatListOfClusters.Add(classifiers);
        }

        if (cluster.subClusters != null)
            foreach (ISASimpleCluster subCluster in cluster.subClusters)
                extractClusterStructure(flatListOfClusters, subCluster);
    }

    private static AbstractVisualElement3D getElementByName(List<AbstractVisualElement3D> containedElements, string name)
    {
        foreach (AbstractVisualElement3D element in containedElements)
            if (element.qualifiedName.Equals(name))
                return element;
        throw new NullReferenceException("Element \"" + name + "\" not found in list of cluster elements.");
    }

    private static Vector2 rayCastTextureSpaceCoordinate(WhiteboardHandler board, Vector3 worldSpacePositionOnDrawingboard)
    {
        Vector3 rayOrigin = worldSpacePositionOnDrawingboard - board.transform.up * 0.1f;
        VisualDebugger.DrawPoint(rayOrigin, Vector3.one * 0.01f, Color.blue);
        Debug.DrawLine(rayOrigin, rayOrigin + board.transform.up * 0.2f, Color.red, float.MaxValue);

        foreach (RaycastHit hit in Physics.RaycastAll(rayOrigin, board.transform.up, 0.2f))
        {
            if (hit.collider.gameObject.TryGetComponent<WhiteboardHandler>(out WhiteboardHandler boardHandler))
            {
                if (!boardHandler.Equals(board))
                    continue;

                //Vector2 result = hit.textureCoord * board.textureSize;
                //Debug.Log("Found a position on the whiteboard:  ("
                //    + worldSpacePositionOnDrawingboard.x + ", "
                //    + worldSpacePositionOnDrawingboard.y + ", "
                //    + worldSpacePositionOnDrawingboard.z + ")" +
                //    "-> (" + result.x + ", " + result.y + ")");
                return hit.textureCoord * board.textureSize;
            }
        }
        throw new NullReferenceException("Cannot find a texture space coordinate for whiteboard world space coordinate ("
            + worldSpacePositionOnDrawingboard.x + ", "
            + worldSpacePositionOnDrawingboard.y + ", "
            + worldSpacePositionOnDrawingboard.z + ")");
    }

}


