﻿using ISA.Modelling.Whiteboard;
using ISA.util;
using System;
using System.Collections;
using UnityEngine;

public class PinMoveOperationVR : AbstractPinOperationVR
{

    protected Vector3 preLocalSpacePosition;
    protected Vector2 preTextureSpacePosition;



    public PinMoveOperationVR(PinMoveOperation ev) : base(ev)
    {
        if (pin == null)
            Debug.LogError("Cannot move pin because it was not found on the whiteboard. name=" + ev.pinnedElementName);
        preLocalSpacePosition = pin.LocalSpacePositionOnDrawingBoard;
        preTextureSpacePosition = pin.TextureSpaceCoordinateOnDrawingBoard;
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        //RemovePinOperation removeOperation = new RemovePinOperation(board, pin);
        //yield return removeOperation.execute(false);

        //AddPinOperation addOperation = new AddPinOperation(board, pin.modelElement, worldSpacePosition, textureSpacePosition, validatePosition);
        //yield return addOperation.execute(updateOnFinish);

        pin.UpdatePosition(localSpacePosition, textureSpacePosition);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();

        yield break;
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return ExecuteRoutine(updateOnFinish);
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        pin.UpdatePosition(preLocalSpacePosition, preTextureSpacePosition);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    protected override string GetName()
    {
        if (pin is SoftwareElementPinHandler)
            return "Pin Moved: " + ((SoftwareElementPinHandler)pin).modelElement.simpleName + " <color=#00000066>[" + ((SoftwareElementPinHandler)pin).modelElement.qualifiedName + "]</color>";
        else if (pin is AudioPinHandler)
            return "Pin Moved (Audio)";
        else
            throw new Exception("Unexpected type of pin: " + pin.GetType().Name);
    }

}