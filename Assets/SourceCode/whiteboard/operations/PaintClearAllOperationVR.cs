﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class PaintClearAllOperationVR : AbstractDrawOperationVR
{

    protected Dictionary<ModulePolygon, ModulePolygonCanvas> preModuleCanvases;
    protected List<ModulePolygon> preModules;
    protected List<ArrowPolygon> preArrows;
    protected bool preDidContainPaint;

    public PaintClearAllOperationVR(PaintClearAllOperation ev) : base(ev)
    {
    }

    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        board.GetComponent<AudioClipHelper>().Play("clear paint");

        yield return null;

        preDidContainPaint = board.ContainsPaint;
        {
            Texture2D texture = new Texture2D((int)board.textureSize.x, (int)board.textureSize.y);
            for (int y = 0; y < (int)board.textureSize.y; y++)
            {
                for (int x = 0; x < (int)board.textureSize.x; x++)
                    texture.SetPixel(x, y, color);

                if (y % 50 == 0)
                    yield return null;
            }
            texture.Apply();

            board.SetRendererTexture(texture);
            board.ContainsPaint = false;
        }

        yield return null;

        foreach (ModulePolygonCanvas canvas in board.moduleCanvases.Values)
        {
            canvas.Hide();
            board.RemovePointerEventListener(canvas);
        }

        preModuleCanvases = new Dictionary<ModulePolygon, ModulePolygonCanvas>(board.moduleCanvases);
        preModules = new List<ModulePolygon>(board.modules);
        preArrows = new List<ArrowPolygon>(board.arrows);

        board.moduleCanvases = new Dictionary<ModulePolygon, ModulePolygonCanvas>();
        board.modules = new List<ModulePolygon>();
        board.arrows = new List<ArrowPolygon>();

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return ExecuteRoutine(updateOnFinish);
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        yield return base.UndoRoutine(updateOnFinish);

        board.ContainsPaint = preDidContainPaint;

        board.moduleCanvases = preModuleCanvases;
        board.modules = preModules;
        board.arrows = preArrows;

        foreach (ModulePolygonCanvas canvas in board.moduleCanvases.Values)
            board.AddPointerEventListener(canvas);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    public override object GetHoverEventElementToRegister()
    {
        return null;
    }



    protected override string GetName()
    {
        return "Paint Cleared";
    }

}