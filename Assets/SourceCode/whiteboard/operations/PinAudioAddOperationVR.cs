﻿using ISA.Modelling.Whiteboard;
using ISA.util;
using System;
using System.Collections;
using UnityEngine;

public class PinAudioAddOperationVR : AbstractPinAddOperationVR
{

    private AudioClip clip;

    public PinAudioAddOperationVR(PinAudioAddOperation ev) : base(ev)
    {
        byte[] soundWaveBytes = Convert.FromBase64String(ev.soundWaveBase64);
        clip = AudioUtil.BytesTo16BitAudioClip(soundWaveBytes, ev.soundWaveFrequency);
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        GameObject newPin = UnityEngine.Object.Instantiate(board.AudioPinPrefab);

        pin = newPin.GetComponentInChildren<AudioPinHandler>();
        ((AudioPinHandler) pin).Initialize(board, clip, pinId, localSpacePosition, textureSpacePosition);
        pin.GetComponent<AudioClipHelper>().Play("pinned to board");

        board.UnhidePin(pin);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    protected override string GetName()
    {
        return "Pin Added: Audio " + TimeUtil.FormatSecondsInDigitalClockStyle(clip.length, true);
    }
}