﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DrawModuleOperationVR : AbstractDrawOperationVR
{

    protected ModulePolygon module;
    protected ModulePolygonCanvas canvas;

    protected Vector2 pointInModuleToAddTo;



    public DrawModuleOperationVR(DrawModuleOperation ev) : base(ev)
    {
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        yield return base.ExecuteRoutine(false);

        // close the polygon
        if (module == null)
            flatListOfPoints.Add(flatListOfPoints[0]);

        if (module == null)
        {
            module = CreateModule(board, color, registeredDrawnLinePoints, flatListOfPoints);
            canvas = CreateModuleCanvas(board, module);
        }
        else
        {
            module.insertPoints(flatListOfPoints, pointInModuleToAddTo);
            canvas = board.moduleCanvases[module];
        }

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();

        board.GetComponent<AudioClipHelper>().Play("new module");

        module.SpawnHighlightRoutine(DrawingBoardShapeHighlightModes.SPAWN, 0, 0.5f);
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return base.ExecuteRoutine(false);
        AddModuleToBoard(board, module, canvas);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }

    public static ModulePolygon CreateModule(WhiteboardHandler board, Color color, List<List<Vector2>> registeredDrawnLinePoints, List<Vector2> flatListOfPoints)
    {
        ModulePolygon module = new ModulePolygon(board, registeredDrawnLinePoints, flatListOfPoints, color);
        board.modules.Add(module);
        return module;
    }

    public static ModulePolygonCanvas CreateModuleCanvas(WhiteboardHandler board, ModulePolygon module)
    {
        GameObject newCanvasObject = UnityEngine.Object.Instantiate<GameObject>(board.ModuleCanvasPrefab, board.transform.parent);
        ModulePolygonCanvas canvas = newCanvasObject.GetComponent<ModulePolygonCanvas>();
        canvas.Initialize(board, module);
        board.moduleCanvases.Add(module, canvas);
        return canvas;
    }

    public static void AddModuleToBoard(WhiteboardHandler board, ModulePolygon module, ModulePolygonCanvas canvas)
    {
        board.modules.Add(module);
        board.moduleCanvases.Add(module, canvas);
        board.AddPointerEventListener(canvas);
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        yield return base.UndoRoutine(false);
        RemoveModuleFromBoard(board, module, canvas);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }

    public static void RemoveModuleFromBoard(WhiteboardHandler board, ModulePolygon module, ModulePolygonCanvas canvas)
    {
        //module.Unhighlight();
        canvas.Hide();
        board.modules.Remove(module);
        board.moduleCanvases.Remove(module);
        board.RemovePointerEventListener(canvas);
    }


    public override object GetHoverEventElementToRegister()
    {
        return module;
    }



    protected override string GetName()
    {
        return "Module Drawn";
    }

}