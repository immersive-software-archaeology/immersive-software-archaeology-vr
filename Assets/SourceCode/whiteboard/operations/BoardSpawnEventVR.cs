﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardSpawnEventVR : AbstractWhiteboardEventVR
{

    private int id;
    private Vector3 worldSpacePosition;
    private Vector3 worldSpaceRotation;

    public BoardSpawnEventVR(BoardSpawnEvent ev) : base(ev)
    {
        id = ev.whiteboardId;
        worldSpacePosition = EcoreUtil.ConvertToUnity(ev.worldSpacePosition);
        worldSpaceRotation = EcoreUtil.ConvertToUnity(ev.worldSpaceRotation);
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        GameObject newWhiteBoardObject = UnityEngine.Object.Instantiate(Resources.Load("Models/DrawingBoard/DrawingBoard") as GameObject);

        newWhiteBoardObject.SetActive(true);
        board = newWhiteBoardObject.GetComponentInChildren<WhiteboardHandler>();
        board.id = id;
        board.SetTransform(worldSpacePosition, worldSpaceRotation);

        SceneManager.INSTANCE.RegisterNewWhiteboard(board);
        yield break;
    }

    //private static IEnumerator setBoardSizeTextureAndPins()
    //{
    //    for (int i = 0; i < size; i++)
    //    {
    //        BoardEnlargeOperationVR op = new BoardEnlargeOperationVR(newBoard, newBoard.initialColor);
    //        yield return newBoard.operationExecuter.PerformExecuteRoutine(op, false);
    //    }
    //    yield return null;

    //    byte[] pngBytes = Convert.FromBase64String(whiteboardMessage.bitmapAsBase64PNG);
    //    Texture2D texture = new Texture2D(2, 2);
    //    texture.LoadImage(pngBytes);
    //    newBoard.SetRendererTexture(texture);

    //    foreach (Pin pin in whiteboardMessage.pins)
    //    {
    //        MultiViewElement3D visualElement = SceneManager.INSTANCE.elementResolver.findElementByQualifiedName(pin.elementQualifiedName);
    //        PinAddOperationVR pinAddOp = new PinAddOperationVR(newBoard, visualElement, convertToUnity(pin.worldSpacePosition), convertToUnity(pin.textureSpacePosition));
    //        newBoard.StartCoroutine(pinAddOp.ExecuteRoutine(false));
    //    }
    //    newBoard.UpdateBoard();
    //}

} 