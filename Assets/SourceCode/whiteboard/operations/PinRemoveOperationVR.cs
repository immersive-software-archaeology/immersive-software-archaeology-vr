﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using UnityEngine;

public class PinRemoveOperationVR : AbstractPinOperationVR
{

    public PinRemoveOperationVR(PinRemoveOperation ev) : base(ev)
    {
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        board.HidePin(pin);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return ExecuteRoutine(updateOnFinish);
    }



    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        board.UnhidePin(pin);

        pin.ResetPositionAndColor();

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }



    public override object GetHoverEventElementToRegister()
    {
        if (pin is SoftwareElementPinHandler)
            return ((SoftwareElementPinHandler)pin).modelElement;
        else
            return null;
    }



    protected override string GetName()
    {
        if (pin is SoftwareElementPinHandler)
            return "Pin Removed: " + ((SoftwareElementPinHandler)pin).modelElement.simpleName + " <color=#00000066>[" + ((SoftwareElementPinHandler)pin).modelElement.qualifiedName + "]</color>";
        else if (pin is AudioPinHandler)
            return "Pin Removed (Audio)";
        else
            throw new Exception("Unexpected type of pin: " + pin.GetType().Name);
    }

}