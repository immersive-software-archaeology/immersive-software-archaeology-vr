﻿using ISA.Modelling.Whiteboard;
using ISA.util;
using System;
using System.Collections;
using UnityEngine;

public class PinSoftwareAddOperationVR : AbstractPinAddOperationVR
{

    public PinSoftwareAddOperationVR(PinSoftwareAddOperation ev) : base(ev)
    {
    }



    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        AbstractPinHandler existingPin = board.GetPinForElement(pinnedElement3D);
        if (existingPin != null)
        {
            Debug.Log("A pin for " + pinnedElement3D.simpleName + " already existed on this board. Aborting exection of add operation. Consider executing a move operation instead!");
            board.UnhidePin(existingPin);
            yield break;
        }

        GameObject newPin;
        if (pinnedElement3D is TransparentCapsule3D)
            newPin = UnityEngine.Object.Instantiate(board.ModulePinPrefab);
        else if (pinnedElement3D is SpaceStation3D)
            newPin = UnityEngine.Object.Instantiate(board.SpaceStationPinPrefab);
        else
            throw new ArgumentException("Unexpected kind of attached element: " + pinnedElement3D.GetType().Name);

        pin = newPin.GetComponentInChildren<SoftwareElementPinHandler>();
        ((SoftwareElementPinHandler) pin).Initialize(board, pinnedElement3D, pinId, localSpacePosition, textureSpacePosition);
        pin.GetComponent<AudioClipHelper>().Play("pinned to board");

        board.UnhidePin(pin);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
    }



    protected override string GetName()
    {
        return "Pin Added: " + pinnedElement3D.simpleName + " <color=#00000066>[" + pinnedElement3D.qualifiedName + "]</color>";
    }
}