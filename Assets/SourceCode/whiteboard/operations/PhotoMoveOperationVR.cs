﻿using ISA.Modelling.Whiteboard;
using System.Collections;
using UnityEngine;

public class PhotoMoveOperationVR : AbstractPhotoOperationVR
{
    protected Vector3 preLocalSpacePosition;
    protected Quaternion preLocalSpaceRotation;

    public PhotoMoveOperationVR(PhotoMoveOperation ev) : base(ev)
    {
        if (photo == null)
            Debug.LogError("Cannot move photo because it was not found on the whiteboard. id=" + ev.photoId);
        preLocalSpacePosition = photo.localSpacePositionOnDrawingBoard;
        preLocalSpaceRotation = photo.localSpaceRotationOnDrawingBoard;
    }

    public override IEnumerator ExecuteRoutine(bool updateOnFinish)
    {
        photo.UpdatePosition(board, localSpacePosition, localSpaceRotation);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }

    public override IEnumerator RedoRoutine(bool updateOnFinish)
    {
        yield return ExecuteRoutine(updateOnFinish);
    }

    public override IEnumerator UndoRoutine(bool updateOnFinish)
    {
        photo.UpdatePosition(board, preLocalSpacePosition, preLocalSpaceRotation);

        if (updateOnFinish)
            board.UpdatePinsAndControlCanvas();
        yield break;
    }

    protected override string GetName()
    {
        return "Photo Moved";
    }
}