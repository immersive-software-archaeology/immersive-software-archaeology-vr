﻿using ISA.Modelling.Whiteboard;
using System;
using System.Collections;
using UnityEngine;

public abstract class AbstractOperationVR : AbstractWhiteboardEventVR
{

    protected AbstractOperationVR(AbstractWhiteboardEvent ev) : base(ev)
    {
    }



    public abstract IEnumerator UndoRoutine(bool updateOnFinish);

    public abstract IEnumerator RedoRoutine(bool updateOnFinish);

    //public abstract IEnumerator FreeRoutine();



    public abstract object GetHoverEventElementToRegister();



    protected abstract string GetName();



    public override string ToString()
    {
        return "<color=#00000066>[" + TimeUtil.UnixMillisecondsToString(serverSideTimestamp) + "]</color> " + GetName();
    }

}