using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class SlideWithUserPositionManager : MonoBehaviour
{
    [SerializeField]
    private float boardScaleFactor = 0.5f;

    [SerializeField]
    private WhiteboardHandler board;

    [SerializeField]
    private SlideableElement[] elements;

    private float centerToOptimalPoint;
    private float lastUpdatedCenterToOptimalPoint;



    void Start()
    {
        foreach(SlideableElement e in elements)
        {
            e.initialLocalPosition = e.tranform.localPosition;
        }

        StartCoroutine(positionCalculation());
        StartCoroutine(positionUpdate());
    }

    private IEnumerator positionCalculation()
    {
        while (true)
        {
            Vector3 boardPosition2 = new Vector3(transform.position.x, 0, transform.position.z);
            Vector3 playerPosition2 = new Vector3(Player.instance.transform.position.x, 0, Player.instance.transform.position.z);

            float boardScale = board.CurrentSize * boardScaleFactor;

            Vector3 boardStart = boardPosition2 - transform.right * boardScale;
            Vector3 boardEnd = boardPosition2 + transform.right * boardScale;

            yield return null;

            Vector3 optimalPoint = ClosestPointOnLine(boardStart, boardEnd, playerPosition2);
            centerToOptimalPoint = Vector3.Distance(boardPosition2, optimalPoint) * Mathf.Sign(Vector3.Dot(transform.right, optimalPoint - boardPosition2));

            yield return null;
        }
    }

    private IEnumerator positionUpdate()
    {
        while (true)
        {
            if(Mathf.Abs(lastUpdatedCenterToOptimalPoint - centerToOptimalPoint) > 0.1f)
            {
                lastUpdatedCenterToOptimalPoint = centerToOptimalPoint;
                foreach (SlideableElement e in elements)
                    e.tranform.localPosition = e.initialLocalPosition + Vector3.right * centerToOptimalPoint * e.slidingFactor;
            }
            yield return null;
        }
    }



    private Vector3 ClosestPointOnLine(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 startToPoint = point - lineStart;
        Vector3 startToEndNormalized = (lineEnd - lineStart).normalized;

        float distance = Vector3.Distance(lineStart, lineEnd);
        float projectionDistance = Vector3.Dot(startToEndNormalized, startToPoint);

        if (projectionDistance <= 0)
            return lineStart;

        if (projectionDistance >= distance)
            return lineEnd;

        return lineStart + startToEndNormalized * projectionDistance;
    }

}

[System.Serializable]
public class SlideableElement
{
    public Transform tranform;
    
    [HideInInspector]
    public Vector3 initialLocalPosition;

    public float slidingFactor = 1;
}