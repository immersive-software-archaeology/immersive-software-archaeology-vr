using AfGD;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class SoftwareElementPinHandler : AbstractPinHandler, ISAHoverableElementListener, ISAActivatableElementListener, ISAPositionableElementListener
{
    [SerializeField]
    private NamePlateManager namePlate;
    [SerializeField]
    private PinInfoCanvasHandler infoCanvas;

    [SerializeField]
    private LineRenderer dummyLine;
    private float blinkDuration = 0.5f;

    public AbstractVisualElement3D modelElement { private set; get; }

    private Dictionary<SoftwareElementPinHandler, LineRenderer> relatedPinToLineRenderersCacheMap = new Dictionary<SoftwareElementPinHandler, LineRenderer>();
    private List<ModulePolygon> containingModulePolyon = new List<ModulePolygon>();



    public void Initialize(WhiteboardHandler parentWhiteboard, AbstractVisualElement3D modelElement, int id, Vector3 localPinPosition, Vector2 textureSpaceCoordinateOnDrawingBoard)
    {
        gameObject.name = "Software Element Pin for " + modelElement.qualifiedName;
        Initialize(parentWhiteboard, id, localPinPosition, textureSpaceCoordinateOnDrawingBoard);

        this.modelElement = modelElement;

        dummyLine.gameObject.SetActive(false);
        dummyLine.positionCount = 10;
        dummyLine.receiveShadows = false;
        dummyLine.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        dummyLine.startColor = Color.black;
        dummyLine.endColor = Color.black;

        Color color;
        if (modelElement is SpaceStation3D)
        {
            namePlate.SetClassifierName(modelElement.simpleName);
            CopyPublicStructure((SpaceStation3D) modelElement, modelRoot);
            color = ((SpaceStation3D)modelElement).GetParentCapsule().WhiteboardColor;
        }
        else if (modelElement is TransparentCapsule3D)
        {
            namePlate.SetPackageName(modelElement.GetParentCapsule(), modelElement.qualifiedName);
            color = ((TransparentCapsule3D)modelElement).WhiteboardColor;
            // this code changes the material for the transaprent capsule. That is not prominent enough. Instead, we are now changing the color of the pin itself
            //modelRoot.GetChild(1).GetComponent<MeshRenderer>().material = Instantiate(modelRoot.GetChild(1).GetComponent<MeshRenderer>().material);
            //modelRoot.GetChild(1).GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", ((TransparentCapsule3D) modelElement).WhiteboardColor);
        }
        else throw new ArgumentException("Pins can only be initialized for stations and capsules. You initialized it with " + modelElement.GetType().Name);

        // adjust pin color
        {
            Material material = Instantiate(modelRoot.GetComponent<MeshRenderer>().materials[0]);
            material.SetColor("_BaseColor", color);
            for(int i = 0; i < modelRoot.GetComponent<MeshRenderer>().materials.Length; i++)
            {
                modelRoot.GetComponent<MeshRenderer>().materials[i] = material;
                modelRoot.GetComponent<MeshRenderer>().materials[i].SetColor("_BaseColor", color);
            }
        }

        ISAUIInteractable interactable = modelRoot.GetComponentInChildren<ISAUIInteractable>();
        //if(attachedElement3D is SpaceStation3D)
        {
            interactable.onHoverStart.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverStart(modelElement); });
            interactable.onHoverEnd.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverEnd(modelElement); });
            interactable.onTriggerPressed.AddListener(delegate { ISAUIInteractionManager.INSTANCE.SubmitActiveStateToggleEvent(this); });

            ISAUIInteractionManager.INSTANCE.RegisterHoverListener(modelElement, this);
            ISAUIInteractionManager.INSTANCE.RegisterActivationListener(this, this);
        }

        OutlineHighlighter = GetComponent<HandHoverHighlighter>();
        OutlineHighlighter.notifyOnHoverBegin.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverStart(modelElement); });
        OutlineHighlighter.notifyOnHoverEnd.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverEnd(modelElement); });

        infoCanvas.Initialize(parentWhiteboard, this);

        ElementUpdater.instance.AddPinNamePlate(namePlate);
    }

    private void CopyPublicStructure(SpaceStation3D station, Transform parentTransform)
    {
        Transform root = parentTransform.Find("StructureHook");
        //Material material = null;

        foreach (SpaceStationBlock3D originalBlock in station.Blocks)
        {
            if (!originalBlock.isPublic)
                continue;

            Transform originalMesh = originalBlock.transform.Find("Mesh");

            float scale = 0.01f;

            GameObject copiedBlock = Instantiate(originalMesh.gameObject);
            copiedBlock.transform.parent = root;
            copiedBlock.transform.localScale = scale * originalMesh.localScale;
            copiedBlock.transform.localRotation = Quaternion.identity;
            copiedBlock.transform.localPosition = scale * originalBlock.gameObject.transform.localPosition;

            // this code changes the material for the copied structure. Instead, we are now changing the color of the pin itself
            //if(material == null)
            //{
            //    material = Instantiate(copiedBlock.GetComponent<MeshRenderer>().material);
            //    material.SetColor("_BaseColor", station.GetParentCapsule().WhiteboardColor);
            //}
            //copiedBlock.GetComponent<MeshRenderer>().material = material;

            foreach (Component comp in copiedBlock.GetComponents<Component>())
            {
                if (comp is Transform)
                    continue;
                if (comp is MeshFilter)
                    continue;

                if (comp is Renderer)
                    ((Renderer)comp).materials = new Material[] { ((Renderer)comp).materials[0] };
                else
                    Destroy(comp);
            }
        }
    }

    public override void Destroy()
    {
        ISAUIInteractionManager.INSTANCE.RemoveHoverListener(this);
        ISAUIInteractionManager.INSTANCE.RemoveActivationListener(this);
        ISAUIInteractionManager.INSTANCE.RemovePositioningListener(this);

        modelElement.UnregisterContainingDrawingboard(parentBoard);
        ElementUpdater.instance.RemovePinNamePlate(namePlate);

        infoCanvas.Destroy();

        base.Destroy();
    }



    public void UpdateRelationships(int depth = 0)
    {
        // remove relations to deleted pins and clear from cache map
        foreach(SoftwareElementPinHandler cachedPinHandler in new List<SoftwareElementPinHandler>(relatedPinToLineRenderersCacheMap.Keys))
        {
            if (/*cachedHandler == null || cachedHandler.gameObject == null ||*/ !parentBoard.GetAttachedPins().Contains(cachedPinHandler))
            {
                LineRenderer renderer = relatedPinToLineRenderersCacheMap[cachedPinHandler];
                if(renderer != null)
                    renderer.gameObject.SetActive(false);
                relatedPinToLineRenderersCacheMap.Remove(cachedPinHandler);
            }
        }

        foreach (AbstractPinHandler otherPin in parentBoard.GetAttachedPins())
        {
            if (!(otherPin is SoftwareElementPinHandler))
                continue;
            if (otherPin.Equals(this))
                continue;

            SoftwareElementPinHandler otherSoftwarePin = (SoftwareElementPinHandler)otherPin;

            if (depth < 1 && otherSoftwarePin.HasRelationTo(this))
                otherSoftwarePin.UpdateRelationships(depth+1);

            if (!relatedPinToLineRenderersCacheMap.TryGetValue(otherSoftwarePin, out LineRenderer lineRenderer))
            {
                float weight = modelElement.GetRelationWeightTo(otherSoftwarePin.modelElement, true);
                if (weight == 0)
                {
                    // Create empty entry to mark that we calculated the weight for this element,
                    // but it was 0 (meaning there is no connection)
                    relatedPinToLineRenderersCacheMap.Add(otherSoftwarePin, null);
                    continue;
                }

                if (dummyLine == null)
                    // The pin was deleted while some mechanism is still trying to update it!
                    return;

                GameObject newLineObject = Instantiate(dummyLine.gameObject, dummyLine.transform.parent);
                newLineObject.SetActive(true);

                lineRenderer = newLineObject.GetComponent<LineRenderer>();
                lineRenderer.widthMultiplier = Mathf.Min(0.05f, (0.25f + weight / 500f) / 50f);

                relatedPinToLineRenderersCacheMap.Add(otherSoftwarePin, lineRenderer);

                ISAUIInteractionManager.INSTANCE.RegisterPositioningListener(this, this);
                ISAUIInteractionManager.INSTANCE.RegisterPositioningListener(otherSoftwarePin, this);
            }

            if (lineRenderer == null)
                // This pin has no relation to our current pin, move to next
                continue;

            if (otherSoftwarePin.gameObject == null)
                // This can happen when during the update loop, elements get permanently deleted,
                // e.g., a big board starts its long-running deletion routine during which it
                // removed one pin after another while this loop is unaware and still updating.
                continue;

            if (!otherSoftwarePin.gameObject.activeInHierarchy)
            {
                lineRenderer.gameObject.SetActive(false);
                continue;
            }
            lineRenderer.gameObject.SetActive(true);

            Color lineColor = parentBoard.GetColorForRelationBetweenPins(this, otherSoftwarePin);
            if(ISAUIInteractionManager.INSTANCE.isHovered(this.modelElement) || ISAUIInteractionManager.INSTANCE.isHovered(otherSoftwarePin.modelElement)
                || ISAUIInteractionManager.INSTANCE.isActive(this) || ISAUIInteractionManager.INSTANCE.isActive(otherSoftwarePin))
                lineColor.a = 1f;

            lineRenderer.startColor = lineColor;
            lineRenderer.endColor = lineColor;
        }

        NotifyOnElementPositionChanged(this);
        return;
    }

    public bool HasRelationTo(SoftwareElementPinHandler pinHandler)
    {
        return relatedPinToLineRenderersCacheMap.ContainsKey(pinHandler);
    }

    public void NotifyOnElementPositionChanged(object e)
    {
        foreach(SoftwareElementPinHandler relatedPin in relatedPinToLineRenderersCacheMap.Keys)
        {
            //if (relatedPin == null || relatedPin.gameObject == null)
            //    continue;

            LineRenderer renderer = relatedPinToLineRenderersCacheMap[relatedPin];
            if (renderer == null || !renderer.gameObject.activeInHierarchy)
                continue;

            Vector3 thisBezierHandle = -transform.up + 0.1f * Vector3.Cross(-transform.up, (relatedPin.transform.position - transform.position).normalized);
            Vector3 otherBezierHandle = -relatedPin.transform.up + 0.1f * Vector3.Cross((transform.position - relatedPin.transform.position).normalized, -relatedPin.transform.up);

            CurveSegment curve = CurveUtil.CalculateCurve(transform.position, relatedPin.transform.position, thisBezierHandle, otherBezierHandle, 0.05f);
            float curveLength = CurveUtil.CalculateCurveLength(curve, renderer.positionCount);
            renderer.material.SetFloat("_Tiling", (1f / renderer.widthMultiplier) * curveLength);

            CurveUtil.RenderCurve(renderer, curve);
        }
    }



    public override void OnHandDetach()
    {
        ISAUIInteractionManager.INSTANCE.HoverEnd(modelElement);
        base.OnHandDetach();
    }



    public void AssignToModule(ModulePolygon module)
    {
        if (containingModulePolyon.Contains(module))
            return;
        containingModulePolyon.Add(module);

        //playParticleAnimation(AssignedToModuleColor);

        BlinkForSeconds(module.color * 1f, blinkDuration);
    }

    public void HighlightArrow(Color color, bool partOfStartModule)
    {
        float delay = partOfStartModule ? 0f : blinkDuration * 1.5f;
        BlinkForSeconds(color * 1f, blinkDuration, delay);
    }

    public void HighlightModuleContainment(Color color)
    {
        BlinkForSeconds(color * 1f, blinkDuration);
    }



    public void NotifyOnElementHoverStateChanged(object e, bool isHovered)
    {
        OutlineHighlighter.SetHighlighted(isHovered);
        UpdateRelationships();

        if (isHovered)
            namePlate.AddVisibilityToken(NamePlateVisibilityToken.HOVERED);
        else
            namePlate.RemoveVisibilityToken(NamePlateVisibilityToken.HOVERED);
    }

    public void NotifyOnElementActiveStateChanged(object e, bool isActive, Vector3 position, Vector3 rotation)
    {
        OutlineHighlighter.SetActive(isActive);

        if (isActive)
        {
            namePlate.AddVisibilityToken(NamePlateVisibilityToken.ACTIVE);
            SceneManager.INSTANCE.StartCoroutine(infoCanvas.Show());
        }
        else
        {
            namePlate.RemoveVisibilityToken(NamePlateVisibilityToken.ACTIVE);
            infoCanvas.Hide();
        }
    }
}
