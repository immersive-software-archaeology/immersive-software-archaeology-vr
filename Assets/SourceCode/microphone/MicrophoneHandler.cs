using RengeGames.HealthBars;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class MicrophoneHandler : PermanentAttachable
{
    private float currentRecordingStartTimestamp = -1;
    private string lastUsedRecordingDeviceName;
    private int recordingSegmentLengthSeconds = 10;

    [SerializeField]
    private TMP_Text infoText;
    [SerializeField]
    private MeshRenderer ringRenderer;
    [SerializeField]
    private Transform deviceIndexIndicator;

    [SerializeField]
    private RadialSegmentedHealthBar recordingProgressBar;

    [SerializeField]
    private SteamVR_Action_Boolean actionNextAudioDevice;
    [SerializeField]
    private SteamVR_Action_Boolean actionPreviousAudioDevice;
    [SerializeField]
    private SteamVR_Action_Boolean actionForRecording;

    private Coroutine recordingRoutine;
    public AudioClip lastRecordedClip { get; private set; }
    private WhiteboardAttachableElementHandler whiteboardAttachHandler;

    [SerializeField]
    private GameObject pinPreview;



    protected override void Init()
    {
        PrintAudioInputDevices();

        DetermineAudioDevice(out _, out _);
        EndRecording();

        whiteboardAttachHandler = GetComponent<WhiteboardAttachableElementHandler>();
        whiteboardAttachHandler.enabled = false;
        whiteboardAttachHandler.onAttachment.AddListener(delegate {
            pinPreview.SetActive(false);
            whiteboardAttachHandler.HideIndicator();
            whiteboardAttachHandler.enabled = false;
        });

        pinPreview.SetActive(false);
    }

    protected override void OnAttach()
    {
        // temporarily put the microphone into the scene directly so that when users pass it form one hand
        // to another, it is not located in the potentially inactive menu (which messes with routines and
        // the Valve interaction system, e.g., hand poses will then not be reset)
        transform.parent = null;
    }

    protected override void OnDetach()
    {
        EndRecording();
    }



    private void PrintAudioInputDevices()
    {
        string deviceMsg = "Available Recording Devices:\n";
        foreach (string thisDevice in Microphone.devices)
        {
            bool isRecording = Microphone.IsRecording(thisDevice);
            int pos = Microphone.GetPosition(thisDevice);
            Microphone.GetDeviceCaps(thisDevice, out int minFreq, out int maxFreq);
            deviceMsg += "\t> Mic: " + thisDevice + " [recording=" + isRecording + ", position=" + pos + ", " + minFreq + " <= freq <= " + maxFreq + "]\n";
        }
        Debug.Log(deviceMsg);
    }



    void Update()
    {
        if (!attached)
            return;

        try
        {
            if(Time.time - attachTime > 0.5f)
            {
                if (actionNextAudioDevice.GetStateDown(hand.handType))
                    SwitchMicrophone(1);
                if (actionPreviousAudioDevice.GetStateDown(hand.handType))
                    SwitchMicrophone(-1);

                if (actionForRecording.GetStateDown(hand.handType))
                    ToggleRecording();
            }
        }
        catch (Exception e) {
            Debug.LogError(e);
        }

        DetachIfShakingHand();
    }



    private float lastToggle;

    public void ToggleRecording()
    {
        if (Time.time - lastToggle < 0.2f)
            return;
        lastToggle = Time.time;

        if (currentRecordingStartTimestamp == -1)
            StartRecording();
        else
            EndRecording();
    }

    private void StartRecording()
    {
        currentRecordingStartTimestamp = Time.time;

        if (recordingRoutine != null)
            StopCoroutine(recordingRoutine);
        recordingRoutine = SceneManager.INSTANCE.StartCoroutine(RecordingProcedureDynamic());
    }

    private void EndRecording()
    {
        currentRecordingStartTimestamp = -1;
    }

    public void SwitchMicrophone(int microphoneIndexIncrement)
    {
        if(currentRecordingStartTimestamp == -1)
            DetermineAudioDevice(out _, out _, microphoneIndexIncrement);
    }



    private List<float> allSamples;
    private IEnumerator RecordingProcedureDynamic()
    {
        string recordingDevice;
        int freq;

        try
        {
            DetermineAudioDevice(out recordingDevice, out freq);
        }
        catch (Exception e)
        {
            Debug.Log(e);
            yield break;
        }

        //recordingProgressBar.gameObject.SetActive(true);
        //recordingProgressBar.SetPercent(0);
        ringRenderer.material.SetColor("_EmissionColor", Color.white * 5f);

        allSamples = new List<float>();
        while (currentRecordingStartTimestamp >= 0)
            yield return AddSamplesToRecording(recordingDevice, freq);

        float[] sampleArray = allSamples.ToArray();
        AudioUtil.Normalize(sampleArray);

        //recordingProgressBar.gameObject.SetActive(false);
        ringRenderer.material.SetColor("_EmissionColor", Color.black);

        lastRecordedClip = AudioClip.Create("Microphone Recording", allSamples.Count, 1, freq, false);
        lastRecordedClip.SetData(sampleArray, 0);
        //Debug.Log("Recording ended!" +
        //    "\nlength: " + TimeUtil.FormatSecondsInDigitalClockStyle(lastRecordedClip.length, true) +
        //    "\nraw size: " + lastRecordedClip.samples + " samples" +
        //    "\ndevice: " + recordingDevice);

        DetermineAudioDevice(out _, out _);

        //SavWav.Save(((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds() + ".wav", lastRecordedClip, false);

        pinPreview.SetActive(true);
        whiteboardAttachHandler.enabled = true;
        whiteboardAttachHandler.OnAttachedToHand();
    }

    private IEnumerator AddSamplesToRecording(string recordingDevice, int freq)
    {
        AudioClip tempClip = Microphone.Start(recordingDevice, true, recordingSegmentLengthSeconds + 1, freq);
        for (int i = 0; i < recordingSegmentLengthSeconds * 10; i++)
        {
            if (currentRecordingStartTimestamp == -1)
                break;
            infoText.text = TimeUtil.FormatSecondsInDigitalClockStyle(Time.time - currentRecordingStartTimestamp, false);
            yield return new WaitForSeconds(0.1f);
        }
        Microphone.End(recordingDevice);

        float[] tempSamplesUncut = new float[tempClip.samples];
        tempClip.GetData(tempSamplesUncut, 0);

        int startIndex = 0;
        int endIndex = tempClip.samples - 1;
        {
            for (int i = 0; i < tempClip.samples; i++)
            {
                if (tempSamplesUncut[i] == 0)
                    continue;

                startIndex = i;
                break;
            }

            for (int i = tempClip.samples - 1; i >= 0; i--)
            {
                if (tempSamplesUncut[i] == 0)
                    continue;

                endIndex = i;
                break;
            }
        }

        float[] tempSamplesCut = new float[endIndex - startIndex + 1];
        for (int i = startIndex; i <= endIndex; i++)
            tempSamplesCut[i - startIndex] = tempSamplesUncut[i];

        List<float> newSamples = new List<float>();
        newSamples.AddRange(allSamples);
        newSamples.AddRange(tempSamplesCut);
        allSamples = newSamples;

        //float sampleSum = 0;
        //for (var i = 0; i < tempSamplesCut.Length; i++)
        //    sampleSum += Mathf.Abs(tempSamplesCut[i]);

        //float volume = Mathf.Max(0, Mathf.Min(1, Mathf.Pow(sampleSum / (float)tempSamplesCut.Length, 0.2f)));
        //recordingProgressBar.SetPercent(volume);
    }



    private void DetermineAudioDevice(out string recordingDevice, out int freq, int microphoneIndexIncrement = 0)
    {
        if (Microphone.devices.Length == 0)
        {
            infoText.text = "No audio input devices found!";
            throw new Exception(infoText.text);
        }

        int currentIndex = -1;
        if (lastUsedRecordingDeviceName == null)
        {
            // intially, just take the first available device...
            currentIndex = 0;
            recordingDevice = Microphone.devices[currentIndex];
            Microphone.GetDeviceCaps(recordingDevice, out freq, out _);
        }
        else
        {
            // Try to find the same device again.
            for (int i = 0; i < Microphone.devices.Length; i++)
                if(Microphone.devices[i].Equals(lastUsedRecordingDeviceName))
                    currentIndex = i;

            if(currentIndex == -1)
            {
                // Previously used device not available anymore...
                // Reset to first device in list...
                currentIndex = 0;
                recordingDevice = Microphone.devices[currentIndex];
                Microphone.GetDeviceCaps(recordingDevice, out freq, out _);
            }
            else
            {
                // The previously used device was found.
                // Either pick it or switch to the next (depending on the switchToNext paramter)
                currentIndex += microphoneIndexIncrement;
                if (currentIndex < 0)
                    currentIndex = Microphone.devices.Length - 1;
                if (currentIndex >= Microphone.devices.Length)
                    currentIndex = 0;

                recordingDevice = Microphone.devices[currentIndex];
                Microphone.GetDeviceCaps(recordingDevice, out freq, out _);
            }
        }

        lastUsedRecordingDeviceName = recordingDevice;
        infoText.text = lastUsedRecordingDeviceName;
        for (int i = 1; i < deviceIndexIndicator.childCount; i++)
            deviceIndexIndicator.GetChild(i).gameObject.SetActive(false);

        for (int i = 0; i < Microphone.devices.Length; i++)
        {
            Transform thisBar;
            if (i >= deviceIndexIndicator.childCount)
                thisBar = Instantiate(deviceIndexIndicator.GetChild(0), deviceIndexIndicator);
            else
                thisBar = deviceIndexIndicator.GetChild(i);

            thisBar.gameObject.SetActive(true);
            thisBar.GetComponent<RectTransform>().sizeDelta = new Vector2(120 / Microphone.devices.Length, 0);
            thisBar.GetComponent<RectTransform>().anchoredPosition = new Vector2(120 / Microphone.devices.Length / 2 + i * 120 / Microphone.devices.Length, 0);

            thisBar.GetComponent<Image>().color = new Color(1, 1, 1, (i == currentIndex) ? 1f : 0.1f);
        }
    }

}
