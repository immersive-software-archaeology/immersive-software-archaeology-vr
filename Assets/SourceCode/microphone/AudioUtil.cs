﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class AudioUtil
{

    private static readonly int WAVEFRONT_HEADER_SIZE = 44;

    public static byte[] GetSoundWaveHeader(AudioClip clip)
    {
        byte[] headerBytes = new byte[WAVEFRONT_HEADER_SIZE];
        //SavWav.WriteHeader(headerBytes, clip, (uint)(clip.samples * 2 + WAVEFRONT_HEADER_SIZE), (uint)clip.samples);
        return headerBytes;
    }



    public static void Normalize(float[] samples)
    {
        float maxValue = 0;
        foreach(float sample in samples)
        {
            float value = Mathf.Abs(sample);
            if (value > maxValue)
                maxValue = value;
        }
        for (int i = 0; i < samples.Length; i++)
            samples[i] /= maxValue;
    }



    public static byte[] ConvertTo16BitArray(AudioClip clip)
    {
        float[] samples = new float[clip.samples];
        clip.GetData(samples, 0);

        return ConvertTo16BitArray(samples);
    }

    public static byte[] ConvertTo16BitArray(float[] samplesFloat32)
    {
        MemoryStream stream = new MemoryStream();
        BinaryWriter writer = new BinaryWriter(stream);

        foreach (float sample in samplesFloat32)
        {
            // Convert from overly precice 32 bit float to 16 bit ints
            // multiply with 2^16 / 2 - 1
            short sampleInt16 = (short)(sample * 32767);
            writer.Write(sampleInt16);
        }

        return stream.ToArray();
    }



    public static byte[] ConvertTo8BitArray(AudioClip clip)
    {
        float[] samples = new float[clip.samples];
        clip.GetData(samples, 0);

        return ConvertTo8BitArray(samples);
    }

    public static byte[] ConvertTo8BitArray(float[] samplesFloat32)
    {
        MemoryStream stream = new MemoryStream();
        BinaryWriter writer = new BinaryWriter(stream);

        foreach (float sample in samplesFloat32)
        {
            // Convert from overly precice 32 bit float to 16 bit ints
            // multiply with 2^8 / 2 - 1
            byte sampleInt8 = (byte)(sample * 127);
            writer.Write(sampleInt8);
        }

        return stream.ToArray();
    }



    public static AudioClip BytesTo16BitAudioClip(byte[] bytes, int frequency)
    {
        float[] samples = BytesTo16BitSamples(bytes, WAVEFRONT_HEADER_SIZE);

        AudioClip clip = AudioClip.Create("Audio Clip", samples.Length, 1, frequency, false);
        clip.SetData(samples, 0);

        return clip;
    }

    public static float[] BytesTo16BitSamples(byte[] bytes, int offset)
    {
        // two bytes (16 bit) in the input array will be converted into one float (32 bit)
        // thus: size factor from byte to int16 = 1 / 2
        short[] samplesInt16 = new short[(bytes.Length - offset) / 2];

        // BlockCopy(Array src, int srcOffset, Array dst, int dstOffset, int count);
        Buffer.BlockCopy(bytes, offset, samplesInt16, 0, bytes.Length - offset);

        // now translate from 16 bit ints to overly precice 32 bit floats, that is how Unity wants it
        float[] samplesFloat32 = new float[samplesInt16.Length];
        for (int i = offset / 2; i < samplesInt16.Length; i++)
            samplesFloat32[i] = samplesInt16[i] / 32767f;

        return samplesFloat32;
    }



    public static AudioClip ConvertFrom8BitArray(byte[] bytes, int frequency)
    {
        float[] samples = ConvertFrom8BitArray(bytes);

        AudioClip clip = AudioClip.Create("Audio Clip", samples.Length, 1, frequency, false);
        clip.SetData(samples, 0);

        return clip;
    }

    public static float[] ConvertFrom8BitArray(byte[] bytes)
    {
        // one bytes (8 bit) in the input array will be converted into one float (32 bit)
        // thus: size factor from byte to int8 = 1 / 1, no need to transform

        // now translate from 16 bit ints to overly precice 32 bit floats, that is how Unity wants it
        float[] samplesFloat32 = new float[bytes.Length];
        for (int i = 0; i < bytes.Length; i++)
            samplesFloat32[i] = bytes[i] / 127;

        return samplesFloat32;
    }

}
