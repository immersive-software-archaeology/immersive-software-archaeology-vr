﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using System;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class HTTPRequestHandler
{

    public static readonly HTTPRequestHandler INSTANCE = new HTTPRequestHandler();

    private HTTPRequestHandler() {
        ip = "127.0.0.1";
        port = "9005";
    }



    private readonly HttpClient HTTP_CLIENT = new HttpClient();

    public string ip { get; private set; }
    public string port { get; private set; }



    public bool TryChangeServerAddress(string newAddress)
    {
        string[] ipPortSplit = newAddress.Split(':');
        int port = 9005;
        if (ipPortSplit.Length == 2)
        {
            if (!int.TryParse(ipPortSplit[1], out port))
                return false;
            if (port < 0)
                return false;
            if (port >= Math.Pow(2, 16))
                return false;
        }
        else if (ipPortSplit.Length != 1)
            return false;

        string[] ipByteSplit = ipPortSplit[0].Split('.');
        if (ipByteSplit.Length != 4)
            return false;

        for (int i = 0; i < 4; i++)
        {
            if (!int.TryParse(ipByteSplit[i], out int ipSegment))
                return false;

            if (ipSegment < 0)
                return false;
            if (ipSegment >= Math.Pow(2, 8))
                return false;
        }

        ip = ipPortSplit[0];
        this.port = port.ToString();

        Debug.Log("Changing server address: " + newAddress);
        PlayerPrefs.SetString("LAST_SERVER_ADDRESS", newAddress);
        return true;
    }



    public async Task SendRequest(string path, string message = null, Action<string> callBackSuccess = null, Action<string> callBackFail = null)
    {
        string fullPath = "http://" + ip + ":" + port + "/" + path;

        try
        {
            HttpResponseMessage responseMessage;
            if (message != null)
            {
                StringContent messageContent = new StringContent(message);
                responseMessage = await HTTP_CLIENT.PostAsync(fullPath, messageContent);
            }
            else
            {
                responseMessage = await HTTP_CLIENT.GetAsync(fullPath);
            }

            if (responseMessage.IsSuccessStatusCode)
            {
                if (callBackSuccess != null)
                    callBackSuccess(await responseMessage.Content.ReadAsStringAsync());
            }
            else
            {
                callBackFail("Server replied with status code " + responseMessage.StatusCode + "\n" + await responseMessage.Content.ReadAsStringAsync());
            }
        }
        catch(Exception e)
        {
            //Debug.LogError("HTTP Request failed: Connection could not be established: " + e.Message);
            if (callBackFail != null)
                callBackFail(e.Message + ":\n" + e.StackTrace);
            return;
        }
    }



    public void SubmitEvent<T>(T ecoreOperation, Action<string> callBackSuccess = null, Action<string> callBackFail = null) where T : AbstractEvent
    {
        ecoreOperation.clientId = SceneManager.INSTANCE.udpClient.id;
        ecoreOperation.systemName = ModelLoader.instance.VisualizationModel.name;
        ecoreOperation.timestamp = 0; // will be set server-side

        string operationAsString = ModelLoader.encodeModelAsXML(ecoreOperation);

        if (ModelLoader.instance.VisualizationModel != null)
            _ = SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name, operationAsString, callBackSuccess, callBackFail);
        else
            callBackFail("Cannot perform whiteboard operation/event without entering a subject system.");
    }

}



[System.Serializable]
public class SocketInfoMessage
{
    public SocketInfoMessageEntry[] entries;
}

[System.Serializable]
public class SocketInfoMessageEntry
{
    public string name;
    public int port;
}