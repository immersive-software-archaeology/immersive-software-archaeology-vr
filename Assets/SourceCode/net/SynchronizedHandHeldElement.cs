﻿using ISA.Modelling.Clientinfo;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public class SynchronizedHandHeldElement : MonoBehaviour
{

    private static HashSet<SynchronizedHandHeldElement> selfMaintainedElementList = new HashSet<SynchronizedHandHeldElement>();

    [SerializeField]
    [Tooltip("Manually set an id for the synchronization of this object. Is overriden when setting the synchronized element field beneath.")]
    private int id = 0;

    [SerializeField]
    [Tooltip("Optional, can be used to set the id to be synchronized with the server when picking up this object based on a referenced script (e.g., handler scripts for capsules, whiteboards, pens, ...).")]
    private MonoBehaviour synchronizedElement;

    [SerializeField]
    private HandPose pose; // { get; private set; }



    private void Awake()
    {
        AssignId();
    }

    private void AssignId()
    {
        if (synchronizedElement is AbstractVisualElement3D)
        {
            id = ((AbstractVisualElement3D)synchronizedElement).id;
        }
        else if (synchronizedElement is WhiteboardHandler)
        {
            id = ((WhiteboardHandler)synchronizedElement).id;
        }
        else if (synchronizedElement is AbstractPinHandler)
        {
            id = ((AbstractPinHandler)synchronizedElement).id;
        }
        else if (synchronizedElement is PenHandler)
        {
            id = 0; // not necessary here, look into GetId() method
        }
        else if (synchronizedElement is SystemPreviewManager)
        {
            id = ((SystemPreviewManager)synchronizedElement).index;
        }
        else if (synchronizedElement is SoftwareElementInfoCanvas)
        {
            AbstractVisualElement3D softwareElement = ((SoftwareElementInfoCanvas)synchronizedElement).currentlyHighlightedElement;
            if (softwareElement != null)
                id = softwareElement.id;
            else
                id = 0;
        }
        else if (synchronizedElement is HandheldCameraPhoto)
        {
            id = ((HandheldCameraPhoto)synchronizedElement).id;
        }
        else if(id > 0)
        {
            selfMaintainedElementList.Add(this);
        }
    }

    public static SynchronizedHandHeldElement GetSelfMaintainedElement(int id)
    {
        foreach (SynchronizedHandHeldElement el in selfMaintainedElementList)
            if (el.id == id)
                return el;
        return null;
    }



    public byte GetHandPose()
    {
        return (byte)pose;
    }

    public int GetId()
    {
        if (synchronizedElement is PenHandler)
            return ((PenHandler)synchronizedElement).penColorAsInteger;
        if (synchronizedElement is MicrophoneHandler || synchronizedElement is HandheldCamera)
            // No need to check
            return id;

        // Try to get an id one more time, then return the result
        if (id == 0)
            AssignId();

        if (id <= 0)
        {
            if (synchronizedElement == null)
                Debug.LogError("Synchronized element " + gameObject.name + " has id=" + id + "...");
            else
                Debug.LogError("Synchronized element " + gameObject.name + " [synchronizedElement=" + synchronizedElement.gameObject.name + "] has id=" + id + "...");
        }

        return id;
    }
}