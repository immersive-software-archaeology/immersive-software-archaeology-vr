﻿using ISA.Modelling.Clientinfo;
using System.Collections;
using UnityEngine;

public class CollaboratorHandData : MonoBehaviour
{
    [Tooltip("The hand pose this object represents.")]
    public HandPose pose;
}