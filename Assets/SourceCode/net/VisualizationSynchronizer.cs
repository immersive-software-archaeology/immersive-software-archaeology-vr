﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class VisualizationSynchronizer : MonoBehaviour
{

    public static VisualizationSynchronizer instance;

    private RelationManager relationManager;

    private long currentTimestamp;
    private List<AbstractEvent> eventQueue;

    private Dictionary<AbstractVisualElement3D, HashSet<VisualizationSynchronizerListener>> listeners;

    private Dictionary<AbstractEvent, int> unavailableStateChangeEventsMap;



    void Start()
    {
        instance = this;
        Reset();
    }

    public void Reset()
    {
        StopAllCoroutines();
        eventQueue = new List<AbstractEvent>();
        listeners = new Dictionary<AbstractVisualElement3D, HashSet<VisualizationSynchronizerListener>>();
        unavailableStateChangeEventsMap = new Dictionary<AbstractEvent, int>();
        currentTimestamp = 0;

        relationManager = new RelationManager();
        relationManager.Init();
    }



    public void RegisterListener(AbstractVisualElement3D elementToRegisterFor, VisualizationSynchronizerListener newListener)
    {
        if(!listeners.TryGetValue(elementToRegisterFor, out HashSet<VisualizationSynchronizerListener> newListenerList))
        {
            newListenerList = new HashSet<VisualizationSynchronizerListener>();
            listeners.Add(elementToRegisterFor, newListenerList);
        }
        newListenerList.Add(newListener);
    }



    public void SendClearAllRequest()
    {
        if (ModelLoader.instance.VisualizationModel == null)
            return;

        ClearAllRelationsEvent e = new ClearAllRelationsEvent();
        e.systemName = ModelLoader.instance.VisualizationModel.name;
        e.clientId = SceneManager.INSTANCE.udpClient.id;

        string operationAsString = ModelLoader.encodeModelAsXML(e);
        _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name,
            operationAsString, null, delegate (string r) { Debug.LogError(r); });
    }



    // NOT running in main thread!
    public void UpdateLatestServerSideEventTimestamp(long latestServerSideTimestamp)
    {
        if (latestServerSideTimestamp <= currentTimestamp)
            return;

        //Debug.Log("Fetching Visualization Update!");
        try
        {
            _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/fetch/?systemName=" + ModelLoader.instance.VisualizationModel.name + "&latestUpdateTimestamp=" + currentTimestamp,
                null, SubmissionSuccess, delegate(string response) { Debug.LogError(response); });
            currentTimestamp = latestServerSideTimestamp;
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void SubmissionSuccess(string response)
    {
        EventLog submittedInformation = ModelLoader.ParseXMLCodeToObject<EventLog>(response);
        if (submittedInformation.log == null)
            return;

        foreach (AbstractEvent eventEcore in submittedInformation.log)
        {
            if(eventEcore != null)
                eventQueue.Add(eventEcore);
        }
    }



    void FixedUpdate()
    {
        if (eventQueue.Count == 0)
            return;

        // Get the next event from the queue & apply it safely in the main thread!
        AbstractEvent eventEcore = eventQueue[0];

        if (eventEcore is ShowRelationEvent)
        {
            ShowRelationEvent showEvent = (ShowRelationEvent)eventEcore;
            AbstractVisualElement3D element = SceneManager.INSTANCE.elementResolver.FindElementByQualifiedName(showEvent.elementName);
            bool forward = showEvent.direction == Direction.FORWARD;

            if (element != null)
            {
                relationManager.ShowRelations(element, showEvent.relationType, forward);
                NotifyListeners(eventEcore, element);
            }
            else Debug.LogError("Element was null, operation could not be executed: " + eventEcore.GetType().Name);
        }
        else if (eventEcore is ClearOneRelationEvent)
        {
            ClearOneRelationEvent hideEvent = (ClearOneRelationEvent)eventEcore;
            AbstractVisualElement3D element = SceneManager.INSTANCE.elementResolver.FindElementByQualifiedName(hideEvent.elementName);
            bool forward = hideEvent.direction == Direction.FORWARD;

            if (element != null)
            {
                relationManager.HideRelationsForElement(element, hideEvent.relationType, forward);
                NotifyListeners(eventEcore, element);
            }
            else Debug.LogError("Element was null, operation could not be executed: " + eventEcore.GetType().Name);
        }
        else if (eventEcore is ClearRelationsEvent)
        {
            AbstractVisualElement3D element = SceneManager.INSTANCE.elementResolver.FindElementByQualifiedName(((ClearRelationsEvent)eventEcore).elementName);
            if (element != null)
            {
                relationManager.HideAllRelationsForElement(element);
                NotifyListeners(eventEcore, element);
            }
            else Debug.LogError("Element was null, operation could not be executed: " + eventEcore.GetType().Name);
        }
        else if (eventEcore is ClearAllRelationsEvent)
        {
            List<AbstractVisualElement3D> elements = relationManager.GetAllElementsWithDisplayedRelationships();

            relationManager.HideAllRelations();

            foreach(AbstractVisualElement3D element in elements)
                NotifyListeners(eventEcore, element);
        }
        else if (eventEcore is ChangeElementActiveStateEvent)
        {
            ChangeElementActiveStateEvent ev = (ChangeElementActiveStateEvent)eventEcore;
            object element;
            if (ev.elementType == ElementType.VISUAL_ELEMENT)
                element = SceneManager.INSTANCE.elementResolver.FindElementById(ev.elementId);
            else if (ev.elementType == ElementType.PIN)
                element = SceneManager.INSTANCE.elementResolver.FindPinById(ev.elementId);
            else
                element = null;

            //if(ev.newState)
            //    Debug.Log(Time.frameCount + ": Looking for " + ev.elementType + " with id=" + ev.elementId + " while setting active to " + ev.newState + " yielded " + element);

            if (element != null)
            {
                unavailableStateChangeEventsMap.Remove(eventEcore);

                Vector3 position = EcoreUtil.ConvertToUnity(ev.position);
                Vector3 rotation = EcoreUtil.ConvertToUnity(ev.rotation);

                ISAUIInteractionManager.INSTANCE.PerformActiveStateChangeLocally(element, ev.newState, position, rotation);
            }
            else
            {
                ShuffleToEndOfEventQueue(eventEcore);
            }
        }
        else if (eventEcore is SystemMoveEvent)
        {
            SystemMoveEvent ev = (SystemMoveEvent)eventEcore;
            SceneManager.INSTANCE.RootTransform.position = EcoreUtil.ConvertToUnity(ev.position);
            SceneManager.INSTANCE.RootTransform.rotation = Quaternion.Euler(EcoreUtil.ConvertToUnity(ev.rotation));
            SceneManager.INSTANCE.RootTransform.localScale = ev.scale * Vector3.one;
            SceneManager.INSTANCE.PositionOrScaleChanged();
        }
        else if (eventEcore is ChangeNameplateVisibilityEvent)
        {
            ChangeNameplateVisibilityEvent ev = (ChangeNameplateVisibilityEvent)eventEcore;
            SceneManager.INSTANCE.ApplyNamePlateVisibility(ev.visible);
        }
        else if (eventEcore is SphereOpenCloseEvent)
        {
            SphereOpenCloseEvent ev = (SphereOpenCloseEvent)eventEcore;
            AbstractVisualElement3D element = SceneManager.INSTANCE.elementResolver.FindElementByQualifiedName(ev.elementName);
            if (element is TransparentCapsule3D)
            {
                if (ev.nowOpen)
                    ((TransparentCapsule3D)element).OpenLocally();
                else
                    ((TransparentCapsule3D)element).CloseLocally();
            }
            else if (element == null)
                Debug.LogError("Expected a capsule for opening, but no element was found for " + ev.elementName);
            else
                Debug.LogError("Unexpected kind of element: expected a capsule for opening, found a " + element.GetType().Name + " for " + ev.elementName);
        }
        else if (eventEcore is AudioEvent)
        {
            AudioEvent ev = (AudioEvent)eventEcore;
            AbstractPinHandler pin = SceneManager.INSTANCE.elementResolver.FindPinById(ev.pinId);
            if(pin != null)
            {
                if (pin is AudioPinHandler)
                    ((AudioPinHandler)pin).TogglePlayback(ev.play, ev.playbackTime, ev.timestamp);
                else
                    Debug.LogError("Unexpected kind of pin: expected an audio pin, found a " + pin.GetType().Name + " for pin id=" + ev.pinId);
            }
            else
            {
                ShuffleToEndOfEventQueue(eventEcore);
            }
        }
        else throw new InvalidOperationException("Unknown kind of operation: " + eventEcore.GetType().Name);

        eventQueue.RemoveAt(0);
    }

    private void ShuffleToEndOfEventQueue(AbstractEvent eventEcore)
    {
        // necessary because we are mixing whiteboard events and visualization events here...
        // This could use a refactoring!
        // Problem: pins might not be available at the time this operation is tried to be executed
        // -> they might also simply not exist anymore in which case we should ignore this operation!

        //Debug.LogWarning("Element active state cannot be changed, because it could not (yet?) be resolved: id=" + ev.elementId + ", type=" + ev.elementType);

        // not the most efficient, but working solution:
        //     sort this event to the end of the event queue and let other events execute first (unless that was done 1000 times already)
        if (!unavailableStateChangeEventsMap.TryGetValue(eventEcore, out int trials))
        {
            unavailableStateChangeEventsMap.Add(eventEcore, 0);
            eventQueue.Add(eventEcore);
        }
        else if (trials < 5 * 60 * 50) // try for roughly 5 minutes
        {
            unavailableStateChangeEventsMap.Remove(eventEcore);
            unavailableStateChangeEventsMap.Add(eventEcore, trials + 1);
            eventQueue.Add(eventEcore);
        }
        else
        {
            unavailableStateChangeEventsMap.Remove(eventEcore);
        }
    }

    private void NotifyListeners(AbstractEvent eventEcore, AbstractVisualElement3D element)
    {
        if (listeners.TryGetValue(element, out HashSet<VisualizationSynchronizerListener> listenerList))
            foreach (VisualizationSynchronizerListener listener in listenerList)
                listener.NotifyOnSynchronizedEvent(eventEcore, element);
    }



    public bool IsRelationCurrentlyShown(AbstractVisualElement3D selectedElement, RelationType type, bool forward)
    {
        return relationManager.IsRelationCurrentlyShown(selectedElement, type, forward);
    }

    public void ExecuteOnCurrentlyShownRelations(Action<AbstractVisualElement3D, RelationType, bool> action)
    {
        foreach(AbstractVisualElement3D element in relationManager.GetAllElementsWithDisplayedRelationships())
            ExecuteOnCurrentlyShownRelations(element, action);
    }

    public void ExecuteOnCurrentlyShownRelations(AbstractVisualElement3D element, Action<AbstractVisualElement3D, RelationType, bool> action)
    {
        relationManager.ExecuteOnCurrentlyShownRelations(element, action);
    }



    public void ShowRelationSegmentsInCapsule(TransparentCapsule3D capsule)
    {
        relationManager.ChangeRelationSegmentVisibilityInCapsule(capsule, true);
    }

    public void HideRelationSegmentsInCapsule(TransparentCapsule3D capsule)
    {
        relationManager.ChangeRelationSegmentVisibilityInCapsule(capsule, false);
    }

}

public interface VisualizationSynchronizerListener
{
    public void NotifyOnSynchronizedEvent(AbstractEvent ev, AbstractVisualElement3D el);
}