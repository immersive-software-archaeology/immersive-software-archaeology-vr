﻿using System;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Force.Crc32;
using System.Threading;

public class DatagramReader
{
    private byte[] datagram;
    private int currentIndex;

    public DatagramReader(byte[] datagram)
    {
        this.datagram = datagram;
        currentIndex = 0;
    }

    public byte readByte()
    {
        return datagram[currentIndex++];
    }

    public long readLong()
    {
        byte[] bytes = new byte[8];
        for (int i = 0; i < bytes.Length; i++)
            bytes[i] = readByte();

        if (BitConverter.IsLittleEndian)
            changeEndianess(bytes);
        return BitConverter.ToInt64(bytes);
    }

    public int readInt()
    {
        byte[] bytes = new byte[4];
        for (int i = 0; i < bytes.Length; i++)
            bytes[i] = readByte();

        if (BitConverter.IsLittleEndian)
            changeEndianess(bytes);
        return BitConverter.ToInt32(bytes);
    }

    public float readFloat()
    {
        byte[] bytes = new byte[4];
        for (int i = 0; i < bytes.Length; i++)
            bytes[i] = readByte();

        if (BitConverter.IsLittleEndian)
            changeEndianess(bytes);
        return BitConverter.ToSingle(bytes);
    }

    public Vector3 readVector3()
    {
        float x = readFloat();
        float y = readFloat();
        float z = readFloat();
        return new Vector3(x, y, z);
    }

    public string readString(int length)
    {
        byte[] bytes = new byte[length];
        for (int i = 0; i < bytes.Length; i++)
            bytes[i] = readByte();
        return Encoding.ASCII.GetString(bytes);
    }

    private void changeEndianess(byte[] data)
    {
        Array.Reverse(data);
    }
}