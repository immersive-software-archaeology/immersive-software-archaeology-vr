﻿using System;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Force.Crc32;
using System.Threading;
using ISA.util;
using System.Collections.Generic;
using ISA.Modelling;
using ISA.UI;
using ISA.Modelling.Clientinfo;
using static Valve.VR.InteractionSystem.Hand;
using System.Linq;
using ISA.VR;

public class ISAUdpClient
{
    private enum PacketType
    {
        FAILURE = (byte)0,
        OUTDATED = (byte)1,
        CONNECT = (byte)8,
        DISCONNECT = (byte)16,
        TRANSFORM_UPDATE = (byte)32
    }

    private readonly int PACKET_SIZE = 512;

    private readonly int TIMEOUT = 5000;
    private readonly int UPDATE_INTERVAL = 1000/50;

    private bool routinesStarted = false;

    public byte id { get; private set; }
    private int port;
    private bool connected;
    private long lastServerTimestamp;
    private long lastValidInteractionWithServer;
    private Hand handThatMovesSystemCurrently;
    Dictionary<Hand, SoftwareElementInfoCanvas> currentlyScrollingHands = new Dictionary<Hand, SoftwareElementInfoCanvas>();

    private IPEndPoint endPoint;
    private UdpClient udpClient;

    private Dictionary<byte, Collaborator> connectedCollaborators;
    private HashSet<byte> collaboratorsToRemove;
    private HashSet<byte> collaboratorsToAdd;



    public Collaborator GetCollaborator(byte id)
    {
        if (connectedCollaborators == null)
            return null;

        if (!connectedCollaborators.ContainsKey(id))
            return null;
        if (collaboratorsToRemove.Contains(id))
            return null;
        return connectedCollaborators[id];
    }



    public void Init()
    {
        udpClient = new UdpClient();
        connected = false;
        port = -1;
        lastServerTimestamp = 0;
        lastValidInteractionWithServer = 0;
        handThatMovesSystemCurrently = null;

        collaboratorsToAdd = new HashSet<byte>();
        collaboratorsToRemove = new HashSet<byte>();
        if (connectedCollaborators == null)
            connectedCollaborators = new Dictionary<byte, Collaborator>();
        else
            foreach (byte colaboratorId in connectedCollaborators.Keys)
                collaboratorsToRemove.Add(colaboratorId);

        _ = HTTPRequestHandler.INSTANCE.SendRequest("sockets/list", null, ProcessSocketInfo);
    }

    public void Disconnect()
    {
        connected = false;
        if (udpClient != null && udpClient.Client.Connected)
            SendDisconnectPacket();
    }

    private void ProcessSocketInfo(string socketInfoAsJSON)
    {
        SocketInfoMessage message = JsonUtility.FromJson<SocketInfoMessage>(socketInfoAsJSON);
        foreach (SocketInfoMessageEntry entry in message.entries)
            if (entry.name.Equals("Synchronization Socket"))
                port = entry.port;

        if (port == -1)
        {
            Debug.LogError("Could not find the socket on the server.");
            return;
        }

        Debug.Log("Initializing UDP client on port " + port);
        endPoint = new IPEndPoint(IPAddress.Parse(HTTPRequestHandler.INSTANCE.ip), port);

        if(!routinesStarted)
        {
            Debug.Log("Starting network routines!");
            routinesStarted = true;
            _ = SendPacketsContinuously();
            _ = ReceivePacketsContinuously();
            SceneManager.INSTANCE.StartCoroutine(AddAndRemoveCollaborators());
        }
    }



    private async Task SendPacketsContinuously()
    {
        CancellationToken cancelToken = ThreadingUtility.QuitToken;

        while(true)
        {
            try
            {
                cancelToken.ThrowIfCancellationRequested();
                await Task.Delay(UPDATE_INTERVAL);

                if (ModelLoader.instance.VisualizationModel == null)
                    continue;

                long currentTime = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();
                if (currentTime - lastValidInteractionWithServer >= TIMEOUT)
                {
                    // timeout!
                    if(connected)
                    {
                        // marking as disconnected and resetting visualization...
                        Debug.LogWarning(Time.frameCount + ": timeout: marking as disconnected and resetting visualization...");

                        //TableManager.INSTANCE.LoadSystem(ModelLoader.instance.SelectedEntry);
                        connected = false;
                        udpClient = null;
                        TableManager.INSTANCE.Reset();
                    }
                    else if(udpClient != null)
                    {
                        // resetting timeout timer and trying to connect...
                        Debug.Log(Time.frameCount + ": not connected! resetting timeout timer and trying to connect...");

                        lastValidInteractionWithServer = currentTime;

                        udpClient.Connect(endPoint);
                        udpClient.Client.SendTimeout = TIMEOUT;
                        udpClient.Client.ReceiveTimeout = TIMEOUT;

                        Disconnect();
                        SendConnectPacket();
                    }
                }
                else if(connected)
                {
                    SendPositionUpdate();
                }
            }
            catch (System.OperationCanceledException)
            {
                return;
            }
            catch (Exception e)
            {
                Debug.LogWarning("Something went wrong when starting to send data: "+ e.GetType().Name +"\n\t> " + e.Message + "\n" + e.StackTrace);
            }
        }
    }

    private void SendConnectPacket()
    {
        DatagramWriter writer = new DatagramWriter(PACKET_SIZE);
        writer.write((byte)PacketType.CONNECT);
        writer.write((byte)ModelLoader.instance.VisualizationModel.name.Length);
        writer.write(ModelLoader.instance.VisualizationModel.name);

        byte[] datagram = writer.getDatagram();
        udpClient.BeginSend(datagram, datagram.Length, new AsyncCallback(SendCallback), "Connect Packet");
    }

    private void SendDisconnectPacket()
    {
        DatagramWriter writer = new DatagramWriter(PACKET_SIZE);
        writer.write((byte)PacketType.DISCONNECT);
        writer.write(id);

        byte[] datagram = writer.getDatagram();
        udpClient.BeginSend(datagram, datagram.Length, new AsyncCallback(SendCallback), "Disconnect Packet");
    }

    private byte updatePackageIndexSent;
    private Dictionary<byte, long> packageIndexToTimestamp = new Dictionary<byte, long>();
    private void SendPositionUpdate()
    {
        DatagramWriter writer = new DatagramWriter(PACKET_SIZE);
        writer.write((byte)PacketType.TRANSFORM_UPDATE);
        writer.write(id);

        long timestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();
        writer.write(timestamp);

        writer.write(updatePackageIndexSent);
        if(packageIndexToTimestamp.ContainsKey(updatePackageIndexSent))
            packageIndexToTimestamp.Remove(updatePackageIndexSent);
        packageIndexToTimestamp.Add(updatePackageIndexSent++, ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds());

        int fps = (int)Mathf.Round(1f / Time.deltaTime);
        writer.write((byte)Math.Min(255, fps));

        Vector3 headPos = Vector3.zero;
        Vector3 headRot = Vector3.zero;
        if (Player.instance.headCollider != null)
        {
            headPos = Player.instance.headCollider.transform.position;
            headRot = Player.instance.headCollider.transform.rotation.eulerAngles;
        }
        {
            writer.write(headPos.x);
            writer.write(headPos.y);
            writer.write(headPos.z);

            writer.write(headRot.x);
            writer.write(headRot.y);
            writer.write(headRot.z);
        }

        if(Player.instance.rightHand == null || !Player.instance.rightHand.isActive)
        {
            Hand fallBackHand = null;
            foreach(Hand hand in Player.instance.hands)
                if (hand != Player.instance.leftHand && hand != Player.instance.rightHand)
                    fallBackHand = hand;
            writeHandInfo(Player.instance.leftHand, writer);
            writeHandInfo(fallBackHand, writer);
        }
        else
        {
            writeHandInfo(Player.instance.leftHand, writer);
            writeHandInfo(Player.instance.rightHand, writer);
        }

        byte[] datagram = writer.getDatagram();
        udpClient.BeginSend(datagram, datagram.Length, new AsyncCallback(SendCallback), "Position Update Packet");
    }

    private void SendCallback(IAsyncResult res)
    {
        try
        {
            int result = udpClient.EndSend(res);
        }
        catch(Exception e)
        {
            Debug.LogWarning("Something went wrong when ending with sending data:\n\t> " + e.Message + "\n" + e.StackTrace);
        }
    }



    private void writeHandInfo(Hand hand, DatagramWriter writer)
    {
        byte handPose = 0;
        Vector3 handPosition = Vector3.zero;
        Vector3 handRotation = Vector3.zero;
        Vector3 indexFingerTipPosition = Vector3.zero;

        int heldObjectId = 0;
        Vector3 heldObjectPosition = Vector3.zero;
        Vector3 heldObjectRotation = Vector3.zero;

        if (hand != null)
        {
            GetHandPose(hand, out handPose, out heldObjectId, out heldObjectPosition, out heldObjectRotation);
            handPosition = hand.transform.position;
            handRotation = hand.transform.rotation.eulerAngles;

            Transform indexFingerTipTransform = ISAVRPointer.FindIndexFingerTip(hand.transform);
            if(indexFingerTipTransform != null)
                indexFingerTipPosition = indexFingerTipTransform.position;
        }

        writer.write(handPose);

        writer.write(handPosition.x);
        writer.write(handPosition.y);
        writer.write(handPosition.z);

        writer.write(handRotation.x);
        writer.write(handRotation.y);
        writer.write(handRotation.z);

        writer.write(indexFingerTipPosition.x);
        writer.write(indexFingerTipPosition.y);
        writer.write(indexFingerTipPosition.z);

        writer.write(heldObjectId);

        writer.write(heldObjectPosition.x);
        writer.write(heldObjectPosition.y);
        writer.write(heldObjectPosition.z);

        writer.write(heldObjectRotation.x);
        writer.write(heldObjectRotation.y);
        writer.write(heldObjectRotation.z);
    }

    public void RegisterSystemMovement(Hand movingHand)
    {
        handThatMovesSystemCurrently = movingHand;
    }

    public void UnregisterSystemMovement()
    {
        handThatMovesSystemCurrently = null;
    }

    public void RegisterCanvasScroll(Hand scrollingHand, SoftwareElementInfoCanvas softwareElementInfoCanvas)
    {
        if (scrollingHand == null)
            return;
        if(!currentlyScrollingHands.ContainsKey(scrollingHand))
            currentlyScrollingHands.Add(scrollingHand, softwareElementInfoCanvas);
    }

    private void GetHandPose(Hand hand, out byte pose, out int heldObjectId, out Vector3 heldObjectPosition, out Vector3 heldObjectRotation)
    {
        if(handThatMovesSystemCurrently != null && hand.Equals(handThatMovesSystemCurrently))
        {
            pose = (byte)HandPose.SYSTEM;
            heldObjectId = (int) Mathf.Round(SceneManager.INSTANCE.RootTransform.localScale.x * 100000f);
            heldObjectPosition = SceneManager.INSTANCE.RootTransform.position;
            heldObjectRotation = SceneManager.INSTANCE.RootTransform.rotation.eulerAngles;
        }
        else if (hand.AttachedObjects.Count == 0)
        {
            if (currentlyScrollingHands.TryGetValue(hand, out SoftwareElementInfoCanvas canvas))
            {
                pose = (byte)HandPose.UI_SOFTWARE_CANVAS_SCROLL;
                heldObjectId = canvas.currentlyHighlightedElement.id;
                heldObjectPosition = canvas.GetScrollPosition();
                heldObjectRotation = Vector3.zero;

                currentlyScrollingHands.Remove(hand);
            }
            else
            {
                pose = GetHandPoseFromActions(hand);
                heldObjectId = 0;
                heldObjectPosition = Vector3.zero;
                heldObjectRotation = Vector3.zero;
            }
        }
        else
        {
            GameObject attachedObject = null;
            SynchronizedHandHeldElement syncInfo = null;

            foreach (AttachedObject obj in hand.AttachedObjects)
            {
                attachedObject = obj.attachedObject;
                if (attachedObject.TryGetComponent(out syncInfo))
                    break;
            }

            heldObjectPosition = attachedObject.transform.position;
            heldObjectRotation = attachedObject.transform.rotation.eulerAngles;

            if (syncInfo == null)
            {
                pose = GetHandPoseFromActions(hand);
                heldObjectId = 0;
            }
            else
            {
                pose = syncInfo.GetHandPose();
                heldObjectId = syncInfo.GetId();
            }
        }
    }

    private byte GetHandPoseFromActions(Hand hand)
    {
        if (hand.IsGrabbingWithType(GrabTypes.Grip))
        {
            if (hand.IsGrabbingWithType(GrabTypes.Pinch))
                return (byte)HandPose.FIST;
            if (!hand.IsGrabbingWithType(GrabTypes.Pinch))
                return (byte)HandPose.POINTING;
        }
        return (byte)HandPose.DEFAULT;
    }



    private async Task ReceivePacketsContinuously()
    {
        CancellationToken cancelToken = ThreadingUtility.QuitToken;

        while (true)
        {
            try
            {
                cancelToken.ThrowIfCancellationRequested();
                await Task.Delay(UPDATE_INTERVAL);

                if(udpClient != null && udpClient.Client.Connected)
                    udpClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);
            }
            catch (System.OperationCanceledException)
            {
                return;
            }
            catch (Exception e)
            {
                Debug.LogWarning("Something went wrong when starting to receive data:\n\t> " + e.Message + "\n" + e.StackTrace);
            }
        }
    }

    private void ReceiveCallback(IAsyncResult res)
    {
        try
        {
            byte[] receivedData = udpClient.EndReceive(res, ref endPoint);

            DatagramReader reader = new DatagramReader(receivedData);
            byte messageType = reader.readByte();

            if (messageType == (byte)PacketType.FAILURE)
            {
                byte messageLength = reader.readByte();
                string message = reader.readString(messageLength);
                throw new Exception("Received error packet [" + messageType + "]: " + message);
            }
            else if (messageType == (byte)PacketType.OUTDATED)
            {
                // an outdated packet gets discarded.
                // Do nothing.
            }
            else
            {
                lastValidInteractionWithServer = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();

                if (messageType == (byte)PacketType.CONNECT)
                {
                    ReceiveConnectPacket(reader);
                }
                else if (messageType == (byte)PacketType.DISCONNECT)
                {
                    ReceiveDisconnectPacket(reader);
                }
                else if (messageType == (byte)PacketType.TRANSFORM_UPDATE)
                {
                    ReceivePositionUpdatePacket(reader);
                }
                else throw new Exception("Unsupported kind of packet: " + messageType);
            }
        }
        catch(Exception e)
        {
            Debug.LogWarning("Something went wrong when ending with receiving data:\n\t> " + e.Message + "\n" + e.StackTrace);
        }
    }

    private void ReceiveConnectPacket(DatagramReader reader)
    {
        id = reader.readByte();
        connected = true;
        Debug.Log("Successfully connected to the server. Player ID = " + id);
    }

    private void ReceiveDisconnectPacket(DatagramReader reader)
    {
        byte disconnectedId = reader.readByte();
        if(disconnectedId == id)
        {
            connected = false;
            Debug.Log("Successfully disconnected from the server.");
        }
        else
        {
            Debug.Log("Successfully disconnected old session (id=" + disconnectedId + ").");
        }
    }

    private void ReceivePositionUpdatePacket(DatagramReader reader)
    {
        long serverTimestamp = reader.readLong();
        if (lastServerTimestamp >= serverTimestamp)
            return;
        lastServerTimestamp = serverTimestamp;

        byte packageNumber = reader.readByte();
        HandleTimeCalculations(packageNumber);

        long latestServerSideEventTimestamp = reader.readLong();
        VisualizationSynchronizer.instance.UpdateLatestServerSideEventTimestamp(latestServerSideEventTimestamp);

        long latestServerSideWhiteboardOperationTimestamp = reader.readLong();
        WhiteboardSynchronizer.UpdateLatestServerSideWhiteboardEventTimestamp(latestServerSideWhiteboardOperationTimestamp);

        byte numberOfClients = reader.readByte();

        List<byte> identifiedCollaboratorsToRemove = new List<byte>(connectedCollaborators.Keys);

        for (int i = 0; i < numberOfClients; i++)
        {
            byte clientID = reader.readByte();
            if (!connectedCollaborators.TryGetValue(clientID, out Collaborator collaborator))
            {
                // First time data from a new collaborator is transmitted
                collaboratorsToAdd.Add(clientID);
                continue;
            }
            else
            {
                // This collaborator is still active
                identifiedCollaboratorsToRemove.Remove(clientID);
            }

            Vector3 headPos = reader.readVector3();
            Vector3 headRot = reader.readVector3();
            collaborator.UpdateHead(headPos, headRot);

            {
                byte lHandPose = reader.readByte();
                Vector3 lHandPos = reader.readVector3();
                Vector3 lHandRot = reader.readVector3();
                Vector3 lHandIndexFingerTipPosition = reader.readVector3();

                int lHandHeldObjectId = reader.readInt();
                Vector3 lHandHeldObjectPos = reader.readVector3();
                Vector3 lHandHeldObjectRot = reader.readVector3();

                collaborator.UpdateLeftHand(lHandPose, lHandPos, lHandRot, lHandIndexFingerTipPosition, lHandHeldObjectId, lHandHeldObjectPos, lHandHeldObjectRot);
            }
            {
                byte rHandPose = reader.readByte();
                Vector3 rHandPos = reader.readVector3();
                Vector3 rHandRot = reader.readVector3();
                Vector3 rHandIndexFingerTipPosition = reader.readVector3();

                int rHandHeldObjectId = reader.readInt();
                Vector3 rHandHeldObjectPos = reader.readVector3();
                Vector3 rHandHeldObjectRot = reader.readVector3();

                collaborator.UpdateRightHand(rHandPose, rHandPos, rHandRot, rHandIndexFingerTipPosition, rHandHeldObjectId, rHandHeldObjectPos, rHandHeldObjectRot);
            }
        }

        foreach (byte collaboratorID in identifiedCollaboratorsToRemove)
            collaboratorsToRemove.Add(collaboratorID);
    }

    private List<int> lastRTTs = new List<int>();
    private void HandleTimeCalculations(byte packageNumber)
    {
        if (!packageIndexToTimestamp.ContainsKey(packageNumber))
            return;

        long rtt = lastValidInteractionWithServer - packageIndexToTimestamp[packageNumber];
        packageIndexToTimestamp.Remove(packageNumber);

        lastRTTs.Add((int)rtt);
        if (lastRTTs.Count > 10)
            lastRTTs.RemoveAt(0);

        double averageRTT = Queryable.Average(lastRTTs.AsQueryable());
        TimeUtil.RegisterServerRTT(averageRTT);

        long serverTimeNow = lastServerTimestamp + (long)Math.Round(averageRTT);
        long timeDeltaToServer = lastValidInteractionWithServer - serverTimeNow;
        TimeUtil.RegisterTimeDeltaToServer(timeDeltaToServer);

        //Debug.Log("RECEIVED! packageNumber = " + packageNumber + " | packageIndexToTimestamp.Count (package loss) = " + packageIndexToTimestamp.Count + " | lastRTTs.Count = " + lastRTTs.Count + " | RTT = " + averageRTT + " | averageRTT = " + averageRTT + " | timeDeltaToServer = " + timeDeltaToServer);
    }



    private IEnumerator AddAndRemoveCollaborators()
    {
        while (true)
        {
            yield return null;

            foreach (byte collaboratorID in collaboratorsToRemove)
            {
                if(connectedCollaborators.TryGetValue(collaboratorID, out Collaborator collaboratorToRemove))
                    UnityEngine.Object.Destroy(collaboratorToRemove.gameObject);
                connectedCollaborators.Remove(collaboratorID);
            }
            collaboratorsToRemove.Clear();

            foreach (byte collaboratorID in collaboratorsToAdd)
            {
                GameObject newCollaboratorObject = UnityEngine.Object.Instantiate(Resources.Load("Models/Multiplayer/Collaborator") as GameObject);
                Collaborator newCollaborator = newCollaboratorObject.GetComponent<Collaborator>();
                newCollaborator.Init(collaboratorID);
                connectedCollaborators.Add(collaboratorID, newCollaborator);
            }
            collaboratorsToAdd.Clear();
        }
    }



    private void printBytesAsBits(byte[] bytes)
    {
        String result = "";
        for (int i = 0; i < bytes.Length; i++)
            result += Convert.ToString(bytes[i], 2).PadLeft(8, '0') + " ";
        Debug.Log(result);
    }

    private bool isValidPacket(byte[] datagram)
    {
        byte[] resortedDatagram = new byte[datagram.Length];

        for (int i = 0; i < 4; i++)
            resortedDatagram[datagram.Length - 4 + i] = datagram[i];

        for (int i = 4; i < resortedDatagram.Length; i++)
            resortedDatagram[i - 4] = datagram[i];

        return Crc32Algorithm.IsValidWithCrcAtEnd(resortedDatagram);
    }

}