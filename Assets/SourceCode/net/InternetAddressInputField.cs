using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(ISAInputField))]
public class InternetAddressInputField : MonoBehaviour, FallbackCameraGUIExtension
{

    public BeveledUIMesh backgroundMeshGenerator;
    private string address;



    void Start()
    {
        address = PlayerPrefs.GetString("LAST_SERVER_ADDRESS", "127.0.0.1:9005");
        // Will "transitively" invoke the ChangeServerAddress method below
        ISAInputField inputField = GetComponent<ISAInputField>();
        inputField.SetText(address);

        ISAFallbackCameraController.RegisterListener(this);
        backgroundMeshGenerator.GenerateMesh().GetComponent<ISAUIInteractable>().onTriggerReleased.AddListener(delegate { inputField.OpenKeyboard(); });
    }

    public void ChangeServerAddress(string newAddress)
    {
        HTTPRequestHandler.INSTANCE.TryChangeServerAddress(newAddress);
    }

    public void DrawGUI()
    {
        GUI.Label(new Rect(Screen.width - 430, 10, 230, 20),
            "Eclipse Back-End Server Address:");

        string previousAddress = address;
        address = GUI.TextField(new Rect(Screen.width - 210, 10, 200, 20), address);

        if (!previousAddress.Equals(address))
        {
            GetComponent<ISAInputField>().SetText(address);
            ChangeServerAddress(address);
        }
    }
}
