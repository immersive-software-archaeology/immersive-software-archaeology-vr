using ISA.Modelling.Clientinfo;
using ISA.UI;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Collaborator : MonoBehaviour
{

    private class ExtendedHandInfo
    {
        public Transform handTransform;
        public Dictionary<HandPose, GameObject> handPoseObjects;

        public HandInfo currentHandData;
        public HandInfo previousHandData;

        public GameObject currentHeldGameObject;

        public Action ExecuteWhileHeld;
        public Action ExecuteOnDetach;

        public ExtendedHandInfo(Transform handTransform, HandInfo handData)
        {
            this.handTransform = handTransform;
            this.handPoseObjects = new Dictionary<HandPose, GameObject>();
            foreach (Transform child in handTransform)
            {
                if (child.TryGetComponent(out CollaboratorHandData poseInfo))
                {
                    if (handPoseObjects.ContainsKey(poseInfo.pose))
                        Debug.LogWarning("A hand pose is registered twice in the collaborator model for pose=" + poseInfo.pose);
                    handPoseObjects.Add(poseInfo.pose, child.gameObject);
                }
                child.gameObject.SetActive(false);
            }

            this.currentHandData = handData;

            this.previousHandData = CreateEmptyClientInfo();
            this.previousHandData.pose = HandPose.FIST;
            this.previousHandData.heldObject.id = -1;
        }
    }



    public bool printDebugStatements = false;

    public float interpolationSpeed = 10f;

    public Transform headTransform;
    public Transform lHandTransform;
    public Transform rHandTransform;

    public float markerScaleRatio = 20f;
    public Renderer markerRenderer;
    public Renderer[] renderersToPaintInPenColor;

    private ExtendedHandInfo lHandInfoContainer;
    private ExtendedHandInfo rHandInfoContainer;

    private ClientInfo clientInfo;
    private Color color;



    public void Init(byte id)
    {
        clientInfo = new ClientInfo();
        clientInfo.id = (sbyte)id;
        clientInfo.head = new HeadInfo();
        {
            clientInfo.head.position = new ISA.Modelling.Clientinfo.ISAVector3();
            clientInfo.head.rotation = new ISA.Modelling.Clientinfo.ISAVector3();
        }
        clientInfo.leftHand = CreateEmptyClientInfo();
        clientInfo.rightHand = CreateEmptyClientInfo();

        lHandInfoContainer = new ExtendedHandInfo(lHandTransform, clientInfo.leftHand);
        rHandInfoContainer = new ExtendedHandInfo(rHandTransform, clientInfo.rightHand);
    }

    public void UpdateColor(byte hueByte)
    {
        float hueFloat = ((float)(hueByte + 145) / 255f) % 1f;
        Color newColor = Color.HSVToRGB(hueFloat, 0.7f, 0.7f);

        if (newColor == color)
            return;

        color = newColor;
        markerRenderer.material.color = color;
    }

    private static HandInfo CreateEmptyClientInfo()
    {
        HandInfo emptyInfo = new HandInfo();
        {
            emptyInfo.pose = HandPose.DEFAULT;
            emptyInfo.position = new ISA.Modelling.Clientinfo.ISAVector3();
            emptyInfo.rotation = new ISA.Modelling.Clientinfo.ISAVector3();
            emptyInfo.indexFingerTipPosition = new ISA.Modelling.Clientinfo.ISAVector3();
            emptyInfo.heldObject = new HandHeldObjectInfo();
            {
                emptyInfo.heldObject.id = 0;
                emptyInfo.heldObject.position = new ISA.Modelling.Clientinfo.ISAVector3();
                emptyInfo.heldObject.rotation = new ISA.Modelling.Clientinfo.ISAVector3();
            }
        }
        return emptyInfo;
    }



    public void UpdateHead(Vector3 pos, Vector3 rot)
    {
        UpdateVector(clientInfo.head.position, pos);
        UpdateVector(clientInfo.head.rotation, rot);
    }

    public void UpdateLeftHand(byte pose, Vector3 pos, Vector3 rot, Vector3 indexFingerTipPosition, int heldObjectId, Vector3 heldObjectPos, Vector3 heldObjectRot)
    {
        UpdateHand(clientInfo.leftHand, pose, pos, rot, indexFingerTipPosition, heldObjectId, heldObjectPos, heldObjectRot);
    }

    public void UpdateRightHand(byte pose, Vector3 pos, Vector3 rot, Vector3 indexFingerTipPosition, int heldObjectId, Vector3 heldObjectPos, Vector3 heldObjectRot)
    {
        UpdateHand(clientInfo.rightHand, pose, pos, rot, indexFingerTipPosition, heldObjectId, heldObjectPos, heldObjectRot);
    }

    private void UpdateHand(HandInfo handInfo, byte pose, Vector3 pos, Vector3 rot, Vector3 indexFingerTipPosition, int heldObjectId, Vector3 heldObjectPos, Vector3 heldObjectRot)
    {
        handInfo.pose = (HandPose)pose;
        UpdateVector(handInfo.position, pos);
        UpdateVector(handInfo.rotation, rot);
        UpdateVector(handInfo.indexFingerTipPosition, indexFingerTipPosition);

        handInfo.heldObject.id = heldObjectId;
        UpdateVector(handInfo.heldObject.position, heldObjectPos);
        UpdateVector(handInfo.heldObject.rotation, heldObjectRot);
    }

    private void UpdateVector(ISA.Modelling.Clientinfo.ISAVector3 isaVector, Vector3 unityVector)
    {
        isaVector.x = unityVector.x;
        isaVector.y = unityVector.y;
        isaVector.z = unityVector.z;
    }



    //public void SpawnPhoto(Texture2D texture)
    //{
    //    ExtendedHandInfo relevantContainer;
    //    if (clientInfo.leftHand.pose == HandPose.CAMERA)
    //        relevantContainer = lHandInfoContainer;
    //    else if (clientInfo.rightHand.pose == HandPose.CAMERA)
    //        relevantContainer = rHandInfoContainer;
    //    else
    //        return;

    //    // Assuming the camera object is the second child of its parent hand:
    //    relevantContainer.handPoseObjects[HandPose.CAMERA].transform.GetChild(1).GetComponent<HandheldCamera>().SpawnPhoto(texture);
    //}



    void FixedUpdate()
    {
        try
        {
            float t = Mathf.Min(1f, interpolationSpeed * Time.deltaTime);

            Transform markerParent = markerRenderer.transform.parent;
            markerParent.position = Vector3.Lerp(markerParent.position, EcoreUtil.ConvertToUnity(clientInfo.head.position), t);
            markerParent.localScale = Vector3.Lerp(markerParent.localScale, Vector3.one * (0.8f + Vector3.Distance(Player.instance.headCollider.transform.position, markerParent.position) / markerScaleRatio), t);

            //VisualDebugger.DrawPoint(headPos, Vector3.one * 0.2f, Color.blue, 0.5f);
            headTransform.position = Vector3.Lerp(headTransform.position, EcoreUtil.ConvertToUnity(clientInfo.head.position), t);
            headTransform.rotation = Quaternion.Lerp(headTransform.rotation, Quaternion.Euler(EcoreUtil.ConvertToUnity(clientInfo.head.rotation)), t);

            //VisualDebugger.DrawPoint(lHandPos, Vector3.one * 0.2f, Color.red, 0.5f);
            ApplyHandpose(lHandInfoContainer, t);

            //VisualDebugger.DrawPoint(rHandPos, Vector3.one * 0.2f, Color.green, 0.5f);
            ApplyHandpose(rHandInfoContainer, t);
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void ApplyHandpose(ExtendedHandInfo dataContainer, float t)
    {
        dataContainer.handTransform.position = Vector3.Lerp(dataContainer.handTransform.position, EcoreUtil.ConvertToUnity(dataContainer.currentHandData.position), t);
        dataContainer.handTransform.rotation = Quaternion.Lerp(dataContainer.handTransform.rotation, Quaternion.Euler(EcoreUtil.ConvertToUnity(dataContainer.currentHandData.rotation)), t);

        if (HandPoseChanged(dataContainer.currentHandData, dataContainer.previousHandData))
        {
            dataContainer.handPoseObjects[dataContainer.previousHandData.pose].SetActive(false);
            dataContainer.handPoseObjects[dataContainer.currentHandData.pose].SetActive(true);

            dataContainer.previousHandData.pose = dataContainer.currentHandData.pose;
        }

        if(HandHeldObjectChanged(dataContainer.currentHandData, dataContainer.previousHandData))
        {
            if (dataContainer.ExecuteOnDetach != null)
                dataContainer.ExecuteOnDetach();

            if (dataContainer.currentHandData.pose == HandPose.PREVIEW)
                GrabSystemPreview(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.WHITEBOARD)
                GrabWhiteboard(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.WHITEBOARD_SPAWN)
                GrabWhiteboardSpawner(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.PIN)
                GrabPin(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.PEN)
                GrabPen(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.CAPSULE || dataContainer.currentHandData.pose == HandPose.STATION)
                GrabSoftwareElement(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.UI_SOFTWARE_CANVAS)
                GrabSoftwareCanvas(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.CAMERA)
                GrabCamera(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.PICTURE)
                GrabPhoto(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.SYSTEM)
                GrabSystem(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.MICROPHONE)
                GrabMicrophone(dataContainer);

            else if (dataContainer.currentHandData.pose == HandPose.UI_SOFTWARE_CANVAS_SCROLL)
                ScrollOnCanvas(dataContainer);

            else if (dataContainer.currentHandData.heldObject.id > 0)
                GrabSelfMaintanedElement(dataContainer);

            else
                DetachElements(dataContainer);

            dataContainer.previousHandData.heldObject.id = dataContainer.currentHandData.heldObject.id;
        }
        else
        {
            if (dataContainer.ExecuteWhileHeld != null)
                dataContainer.ExecuteWhileHeld();
        }

        if (dataContainer.currentHeldGameObject != null)
        {
            dataContainer.currentHeldGameObject.transform.position = Vector3.Lerp(dataContainer.currentHeldGameObject.transform.position, EcoreUtil.ConvertToUnity(dataContainer.currentHandData.heldObject.position), t);
            dataContainer.currentHeldGameObject.transform.rotation = Quaternion.Lerp(dataContainer.currentHeldGameObject.transform.rotation, Quaternion.Euler(EcoreUtil.ConvertToUnity(dataContainer.currentHandData.heldObject.rotation)), t);
        }
    }

    private bool HandPoseChanged(HandInfo currentHandData, HandInfo previousHandData)
    {
        return currentHandData.pose != previousHandData.pose;
    }

    private bool HandHeldObjectChanged(HandInfo currentHandData, HandInfo previousHandData)
    {
        return HandPoseChanged(currentHandData, previousHandData)
            || currentHandData.heldObject.id != previousHandData.heldObject.id;
    }



    private void GrabSystemPreview(ExtendedHandInfo dataContainer)
    {
        SystemPreviewManager preview = TableManager.INSTANCE.GetPreviewManager(dataContainer.currentHandData.heldObject.id);
        if(printDebugStatements)
            Debug.Log("Put system preview (id=" + preview.index + ", name=" + preview.name + ") in collabotor (id=" + clientInfo.id + ") hand.");

        Rigidbody rb = preview.gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = true;

        dataContainer.currentHeldGameObject = preview.gameObject;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = delegate { rb.isKinematic = false; };
    }

    private void GrabWhiteboard(ExtendedHandInfo dataContainer)
    {
        WhiteboardHandler whiteboard = SceneManager.INSTANCE.GetWhiteboard(dataContainer.currentHandData.heldObject.id);
        if (whiteboard == null)
        {
            Debug.Log("Collaborator grabbed a whiteboard (id=" + dataContainer.currentHandData.heldObject.id + ") that does not exist (probably not loaded yet).");
            return;
        }

        if (printDebugStatements)
            Debug.Log("Put whiteboard (id=" + whiteboard.id + ") in collabotor (id=" + clientInfo.id + ") hand.");

        PrecisionGrabbingTarget precisionGrabbing = whiteboard.precisionGrabbingTarget;
        precisionGrabbing.OnPickup();

        dataContainer.currentHeldGameObject = precisionGrabbing.gameObject;
        dataContainer.ExecuteWhileHeld = delegate { precisionGrabbing.OnHandHeldUpdate(); };
        dataContainer.ExecuteOnDetach = delegate { precisionGrabbing.OnDetach(); };
    }

    private void GrabWhiteboardSpawner(ExtendedHandInfo dataContainer)
    {
        if (printDebugStatements)
            Debug.Log("Put whiteboard spawner in collabotor (id=" + clientInfo.id + ") hand.");

        dataContainer.currentHeldGameObject = null;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = null;
    }

    private void GrabPin(ExtendedHandInfo dataContainer)
    {
        if (printDebugStatements)
            Debug.Log("Put pin (id=" + dataContainer.currentHandData.heldObject.id + ") in collabotor (id=" + clientInfo.id + ") hand.");

        // Tricksing around with bytes to store the IDs of both the whiteboard AND the pinned element in one integer
        // This is obsolete now.
        {
            //byte[] pinIdBytes = BitConverter.GetBytes(dataContainer.currentHandData.heldObject.id);

            //byte[] whiteboardIdBytes = new byte[4];
            //whiteboardIdBytes[0] = pinIdBytes[0];
            //int whiteboardId = BitConverter.ToInt32(whiteboardIdBytes, 0);
            //WhiteboardHandler whiteboard = SceneManager.INSTANCE.GetWhiteboard(whiteboardId);

            //byte[] elementIdBytes = new byte[4];
            //elementIdBytes[0] = pinIdBytes[1];
            //elementIdBytes[1] = pinIdBytes[2];
            //elementIdBytes[2] = pinIdBytes[3];
            //int elementId = BitConverter.ToInt32(elementIdBytes, 0);
            //MultiViewElement3D element = SceneManager.INSTANCE.elementResolver.FindElementById(elementId);
        }

        AbstractPinHandler pin = SceneManager.INSTANCE.elementResolver.FindPinById(dataContainer.currentHandData.heldObject.id);

        dataContainer.currentHeldGameObject = pin.gameObject;
        dataContainer.ExecuteWhileHeld = delegate { ISAUIInteractionManager.INSTANCE.PositionChanged(pin); };
        dataContainer.ExecuteOnDetach = delegate { /* TODO: force an update of the whiteboard in case a UDP message for a pin movement overwrites a freshly fetched HTTP pin-move??? */ };
    }

    private void GrabPen(ExtendedHandInfo dataContainer)
    {
        //byte R = (byte)((dataContainer.currentHandData.heldObject.id >> 16) & 0xFF);
        //byte G = (byte)((dataContainer.currentHandData.heldObject.id >> 8) & 0xFF);
        //byte B = (byte)((dataContainer.currentHandData.heldObject.id) & 0xFF);

        byte[] colorBytes = BitConverter.GetBytes(dataContainer.currentHandData.heldObject.id);
        byte R = colorBytes[0];
        byte G = colorBytes[1];
        byte B = colorBytes[2];
        byte A = colorBytes[3];
        Color32 color = new Color32(R, G, B, A);

        foreach (Renderer renderer in renderersToPaintInPenColor)
            renderer.material.color = color;
        if (printDebugStatements)
            Debug.Log("Put pen in collabotor (id=" + clientInfo.id + ") hand. Color=" + color.r + ", " + color.g + ", " + color.b);

        dataContainer.currentHeldGameObject = null;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = null;
    }

    private void GrabSoftwareElement(ExtendedHandInfo dataContainer)
    {
        AbstractVisualElement3D element = SceneManager.INSTANCE.elementResolver.FindElementById(dataContainer.currentHandData.heldObject.id);
        if (printDebugStatements)
            Debug.Log("Put software element (id=" + dataContainer.currentHandData.heldObject.id + ", name=" + element.name + ") in collabotor (id=" + clientInfo.id + ") hand.");

        PositionResetter resetter = element.handHoverHighlighter.gameObject.GetComponent<PositionResetter>();
        resetter.StopAnimation();

        //if (element is SpaceStation3D)
        //{
        //    if(dataContainer == lHandInfoContainer)
        //        ((SpaceStation3D)element).OnHandHeldStart(rHandInfoContainer.handPoseObjects[dataContainer.currentHandData.pose].transform);
        //    else
        //        ((SpaceStation3D)element).OnHandHeldStart(lHandInfoContainer.handPoseObjects[dataContainer.currentHandData.pose].transform);
        //}

        dataContainer.currentHeldGameObject = resetter.gameObject;
        dataContainer.ExecuteWhileHeld = delegate {
            ISAUIInteractionManager.INSTANCE.PositionChanged(element);
            if (element is SpaceStation3D)
            {
                if (dataContainer == lHandInfoContainer)
                    ((SpaceStation3D)element).OnHandHeldUpdate(EcoreUtil.ConvertToUnity(rHandInfoContainer.currentHandData.indexFingerTipPosition));
                else
                    ((SpaceStation3D)element).OnHandHeldUpdate(EcoreUtil.ConvertToUnity(lHandInfoContainer.currentHandData.indexFingerTipPosition));
            }
        };
        dataContainer.ExecuteOnDetach = delegate {
            resetter.ResetToRememberedPositionAnimated();
            if (element is SpaceStation3D)
                if(((SpaceStation3D)element).locallyHoveringIndexFingerTip == null)
                    // Only reset this station if it is not currently grabbed by the local user
                    ((SpaceStation3D)element).OnHandHeldEnd();
        };
    }

    private void GrabSoftwareCanvas(ExtendedHandInfo dataContainer)
    {
        SoftwareElementInfoCanvas canvas = SceneManager.INSTANCE.elementResolver.FindCanvasForElementWithId(dataContainer.currentHandData.heldObject.id);
        if (printDebugStatements)
            Debug.Log("Put information canvas for software element (id=" + dataContainer.currentHandData.heldObject.id + ") in collabotor (id=" + clientInfo.id + ") hand.");
        if (canvas == null)
            Debug.Log("Canvas is NULL for software element (id=" + dataContainer.currentHandData.heldObject.id + "), happened for collabotor with id=" + clientInfo.id + ".");

        dataContainer.currentHeldGameObject = canvas.gameObject;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = null;
    }

    private void ScrollOnCanvas(ExtendedHandInfo dataContainer)
    {
        SoftwareElementInfoCanvas canvas = SceneManager.INSTANCE.elementResolver.FindCanvasForElementWithId(dataContainer.currentHandData.heldObject.id);
        if (printDebugStatements)
            Debug.Log("Scrolling on information canvas for software element (id=" + dataContainer.currentHandData.heldObject.id + ") in collabotor (id=" + clientInfo.id + ") hand.");

        dataContainer.currentHeldGameObject = null;
        dataContainer.ExecuteWhileHeld = delegate {
            canvas.PerformScrollViaNetwork(EcoreUtil.ConvertToUnity(dataContainer.currentHandData.heldObject.position));
        };
        dataContainer.ExecuteOnDetach = null;
    }

    private void GrabCamera(ExtendedHandInfo dataContainer)
    {
        if (printDebugStatements)
            Debug.Log("Put camera in collabotor (id=" + clientInfo.id + ") hand.");

        dataContainer.currentHeldGameObject = null;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = null;
    }

    private void GrabPhoto(ExtendedHandInfo dataContainer)
    {
        if (printDebugStatements)
            Debug.Log("Put photo (id=" + dataContainer.currentHandData.heldObject.id + ") in collabotor (id=" + clientInfo.id + ") hand.");

        int photoId = dataContainer.currentHandData.heldObject.id;
        if(photoId > 0)
        {
            // move actual photo
            HandheldCameraPhoto photo = SceneManager.INSTANCE.elementResolver.FindPhotoById(photoId);
            dataContainer.currentHeldGameObject = photo.gameObject;
            dataContainer.handPoseObjects[HandPose.PICTURE].transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            // move placeholder
            dataContainer.currentHeldGameObject = null;
            dataContainer.handPoseObjects[HandPose.PICTURE].transform.GetChild(1).gameObject.SetActive(true);
        }

        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = delegate { /* TODO: force an update of the whiteboard in case a UDP message for a photo movement overwrites a freshly fetched HTTP photo-move??? */ };
    }

    private void GrabMicrophone(ExtendedHandInfo dataContainer)
    {
        if (printDebugStatements)
            Debug.Log("Put microphone in collabotor (id=" + clientInfo.id + ") hand.");

        dataContainer.currentHeldGameObject = null;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = null;
    }

    private void GrabSystem(ExtendedHandInfo dataContainer)
    {
        if (printDebugStatements)
            Debug.Log("Put system in collabotor (id=" + clientInfo.id + ") hand.");

        dataContainer.currentHeldGameObject = SceneManager.INSTANCE.RootTransform.gameObject;
        dataContainer.ExecuteWhileHeld = delegate {
            float t = Mathf.Min(1f, interpolationSpeed * Time.deltaTime);
            SceneManager.INSTANCE.RootTransform.localScale = Vector3.Lerp(SceneManager.INSTANCE.RootTransform.localScale, (float)dataContainer.currentHandData.heldObject.id / 100000f * Vector3.one, t);
            SceneManager.INSTANCE.PositionOrScaleChanged();
        };
        dataContainer.ExecuteOnDetach = null;
    }

    private void GrabSelfMaintanedElement(ExtendedHandInfo dataContainer)
    {
        SynchronizedHandHeldElement element = SynchronizedHandHeldElement.GetSelfMaintainedElement(dataContainer.currentHandData.heldObject.id);
        if (printDebugStatements)
            Debug.Log("Put self-maintained element (id=" + dataContainer.currentHandData.heldObject.id +", name=" +
            element.name + ") in collabotor (id=" +
            clientInfo.id + ") hand.");

        dataContainer.currentHeldGameObject = element.gameObject;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = null;
        if (dataContainer.currentHeldGameObject.gameObject.TryGetComponent(out Rigidbody rb))
        {
            if (!rb.isKinematic)
            {
                rb.isKinematic = true;
                dataContainer.ExecuteOnDetach = delegate { rb.isKinematic = false; };
            }
        }
    }

    private void DetachElements(ExtendedHandInfo dataContainer)
    {
        if (printDebugStatements)
            Debug.Log("Clear Hand Attachment.");

        dataContainer.currentHeldGameObject = null;
        dataContainer.ExecuteWhileHeld = null;
        dataContainer.ExecuteOnDetach = null;
    }
}


