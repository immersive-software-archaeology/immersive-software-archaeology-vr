﻿using System;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Force.Crc32;
using System.Threading;

public class DatagramWriter
{
    private byte[] datagram;
    private int currentIndex;

    public DatagramWriter(int packetSize)
    {
        datagram = new byte[packetSize];
        currentIndex = 0;
    }

    public byte[] getDatagram()
    {
        return datagram;
    }



    public void write(byte b)
    {
        datagram[currentIndex++] = b;
    }

    public void write(byte[] bytes)
    {
        if (BitConverter.IsLittleEndian)
            changeEndianess(bytes);

        foreach (byte b in bytes)
            write(b);
    }

    public void write(long l)
    {
        write(BitConverter.GetBytes(l));
    }

    public void write(int i)
    {
        write(BitConverter.GetBytes(i));
    }

    public void write(float f)
    {
        write(BitConverter.GetBytes(f));
    }

    public void write(string name)
    {
        byte[] bytes = Encoding.ASCII.GetBytes(name);
        foreach (byte b in bytes)
            write(b);
    }

    private void changeEndianess(byte[] data)
    {
        Array.Reverse(data);
    }
}