﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;



public class SystemScaler : MonoBehaviour
{

    public AudioClipHelper audioHelper;

    public List<Transform> ToShowWhenInMoveMode;
    public List<Transform> ToShowWhenInScaleRotateMode;

    public SteamVR_Input_Sources PrimaryHandInputSource;
    public SteamVR_Input_Sources SecondaryHandInputSource;
    public SteamVR_Action_Boolean TargetAction;

    public LineRenderer BaseLineRenderer;
    public LineRenderer ScaleLineRenderer;
    //public Canvas ScaleCanvas;
    //public RectTransform slider;
    //public RectTransform startMarker;

    public bool RelativeScaling = true;
    public bool CopyRotationFromHand = false;



    private Hand movingHand;
    private SteamVR_Input_Sources movingInputSource;
    private Hand scalingHand;
    private SteamVR_Input_Sources scalingInputSource;

    private Vector3 previousMovingHandCursorPosition;

    private Quaternion previousRotation;
    private Vector3 previousMovingHandLookDirection;

    private float startScale;
    private float previousScale;
    private float pivotHandDistance;

    //private bool showHints = true;



    public void Start()
    {
        BaseLineRenderer.gameObject.SetActive(false);
        ScaleLineRenderer.gameObject.SetActive(false);
        //ScaleCanvas.gameObject.SetActive(false);
        foreach (Transform t in ToShowWhenInScaleRotateMode)
            t.gameObject.SetActive(false);
        Reset();
    }

    public void Reset()
    {
        movingHand = null;
        scalingHand = null;
    }



    public void Update()
    {
        if (ModelLoader.instance == null || ModelLoader.instance.VisualizationModel == null)
            return;
        if (!SceneManager.INSTANCE.finishedLoading)
            return;
        if (Player.instance.leftHand == null || Player.instance.rightHand == null)
            return;

        if (movingHand != null && movingHand.AttachedObjects.Count > 0)
            UnregisterMoveActive();
        if (scalingHand != null && scalingHand.AttachedObjects.Count > 0)
            UnregisterScaleRotateActive();

        if (movingHand == null)
        {
            if (TargetAction.GetStateDown(SteamVR_Input_Sources.LeftHand))
                RegisterMoveActive(Player.instance.leftHand, SteamVR_Input_Sources.LeftHand, SteamVR_Input_Sources.RightHand);
            else if (TargetAction.GetStateDown(SteamVR_Input_Sources.RightHand))
                RegisterMoveActive(Player.instance.rightHand, SteamVR_Input_Sources.RightHand, SteamVR_Input_Sources.LeftHand);
        }
        // not using 'else' so that the user can theoretically trigger the actions on both hands at the same time
        if (movingHand != null)
        {
            // currently moving
            if (TargetAction.GetStateUp(movingInputSource) || movingHand.AttachedObjects.Count > 0)
            {
                UnregisterMoveActive();
                UnregisterScaleRotateActive();
            }
            else if (scalingHand == null)
            {
                if (TargetAction.GetStateDown(scalingInputSource))
                    RegisterScaleRotateActive(movingHand.otherHand);
            }
        }
        if (scalingHand != null)
        {
            // currently scaling
            if (TargetAction.GetStateUp(scalingInputSource) || scalingHand.AttachedObjects.Count > 0)
                UnregisterScaleRotateActive();
        }

        if (movingHand != null)
        {
            HandleMove();
            if (scalingHand != null)
            {
                //handleRotation();
                HandleScaling();
                UpdateScaleLine();
            }
            SceneManager.INSTANCE.PositionOrScaleChanged();
            SceneManager.INSTANCE.udpClient.RegisterSystemMovement(movingHand);
        }
    }



    private void RegisterMoveActive(Hand movingHand, SteamVR_Input_Sources movingInputSource, SteamVR_Input_Sources scalingInputSource)
    {
        //if (showHints)
        //{
        //    ControllerButtonHints.ShowTextHint(movingHand.otherHand, TargetAction, "Use the trigger on this hand to scale and rotate the visualization!\nThe gizmo attached to the other hand serves as pivot center for the scaling and rotation.", true);
        //    ControllerButtonHints.HideTextHint(movingHand, TargetAction);
        //}

        if (movingHand.AttachedObjects.Count > 0)
            return;

        this.movingHand = movingHand;
        SceneManager.INSTANCE.DisableInteraction(movingHand, this);

        this.movingInputSource = movingInputSource;
        this.scalingInputSource = scalingInputSource;

        previousMovingHandCursorPosition = movingHand.objectAttachmentPoint.transform.position;
        previousMovingHandLookDirection = movingHand.objectAttachmentPoint.transform.position - movingHand.otherHand.objectAttachmentPoint.transform.position;
    }

    private void RegisterScaleRotateActive(Hand scalingHand)
    {
        //if (showHints)
        //{
        //    ControllerButtonHints.HideTextHint(scalingHand, TargetAction);
        //    showHints = false;
        //}

        if (scalingHand.AttachedObjects.Count > 0)
            return;

        this.scalingHand = scalingHand;
        SceneManager.INSTANCE.DisableInteraction(scalingHand, this);

        foreach (Transform t in ToShowWhenInScaleRotateMode)
            t.gameObject.SetActive(true);

        //{
        //    // Position the overview within its parent sothat the rotation will follow the secondary hand's pivot point
        //    Vector3 originalSystemPosition = SceneManager.INSTANCE.RootTransform.parent.position;
        //    SceneManager.INSTANCE.RootTransform.parent.position = movingHand.objectAttachmentPoint.transform.position;
        //    SceneManager.INSTANCE.RootTransform.transform.position = originalSystemPosition;
        //}

        BaseLineRenderer.gameObject.SetActive(true);
        ScaleLineRenderer.gameObject.SetActive(true);

        startScale = SceneManager.INSTANCE.RootTransform.localScale.x;
        pivotHandDistance = Vector3.Distance(movingHand.objectAttachmentPoint.transform.position, scalingHand.objectAttachmentPoint.transform.position);
        previousRotation = Quaternion.identity;
    }

    private void UnregisterMoveActive()
    {
        if (movingHand == null)
            return;

        SceneManager.INSTANCE.udpClient.UnregisterSystemMovement();

        SceneManager.INSTANCE.EnableInteraction(movingHand, this);
        movingHand = null;

        SystemMoveEvent ev = new SystemMoveEvent();
        ev.systemName = ModelLoader.instance.VisualizationModel.name;
        ev.clientId = SceneManager.INSTANCE.udpClient.id;
        ev.position = EcoreUtil.ConvertToEcoreMultiplayer(SceneManager.INSTANCE.RootTransform.position);
        ev.rotation = EcoreUtil.ConvertToEcoreMultiplayer(SceneManager.INSTANCE.RootTransform.rotation.eulerAngles);
        ev.scale = SceneManager.INSTANCE.RootTransform.localScale.x;

        string operationAsString = ModelLoader.encodeModelAsXML(ev);
        _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name,
            operationAsString, null, delegate (string r) { Debug.LogError(r); });
    }

    private void UnregisterScaleRotateActive()
    {
        if (scalingHand == null)
            return;

        SceneManager.INSTANCE.EnableInteraction(scalingHand, this);
        scalingHand = null;

        foreach (Transform t in ToShowWhenInScaleRotateMode)
            t.gameObject.SetActive(false);

        //{
        //    // Reposition the overview to its original position, with local position 0
        //    SceneManager.INSTANCE.RootTransform.parent.position += SceneManager.INSTANCE.RootTransform.transform.position - SceneManager.INSTANCE.RootTransform.parent.position;
        //    SceneManager.INSTANCE.RootTransform.transform.localPosition = Vector3.zero;
        //}

        BaseLineRenderer.gameObject.SetActive(false);
        ScaleLineRenderer.gameObject.SetActive(false);
    }



    private void HandleMove()
    {
        SceneManager.INSTANCE.RootTransform.position += movingHand.objectAttachmentPoint.transform.position - previousMovingHandCursorPosition;
        float y = Mathf.Max(SceneManager.INSTANCE.minPositionY, Mathf.Min(SceneManager.INSTANCE.maxPositionY, SceneManager.INSTANCE.RootTransform.position.y));
        SceneManager.INSTANCE.RootTransform.position = new Vector3(SceneManager.INSTANCE.RootTransform.position.x, y, SceneManager.INSTANCE.RootTransform.position.z);

        previousMovingHandCursorPosition = movingHand.objectAttachmentPoint.transform.position;
    }

    private void HandleRotation()
    {
        if (!CopyRotationFromHand)
        {
            Vector3 currentLookDirection = movingHand.objectAttachmentPoint.transform.position - scalingHand.objectAttachmentPoint.transform.position;
            Quaternion someRotation = Quaternion.FromToRotation(previousMovingHandLookDirection, currentLookDirection);

            if (previousRotation != Quaternion.identity)
            {
                Quaternion thisRotation = Quaternion.identity * Quaternion.Inverse(someRotation);
                Quaternion oldRotation = Quaternion.identity * Quaternion.Inverse(previousRotation);
                Quaternion deltaRotation = Quaternion.Inverse(thisRotation) * (oldRotation);
                SceneManager.INSTANCE.RootTransform.rotation = deltaRotation * SceneManager.INSTANCE.RootTransform.rotation;
            }

            previousRotation = someRotation;
        }
        else
        {
            if (previousRotation != Quaternion.identity)
            {
                Quaternion thisRotation = Quaternion.identity * Quaternion.Inverse(scalingHand.transform.rotation);
                Quaternion oldRotation = Quaternion.identity * Quaternion.Inverse(previousRotation);
                Quaternion deltaRotation = Quaternion.Inverse(thisRotation) * (oldRotation);
                SceneManager.INSTANCE.RootTransform.rotation = deltaRotation * SceneManager.INSTANCE.RootTransform.rotation;
            }

            previousRotation = scalingHand.transform.rotation;
        }
    }

    private void HandleScaling()
    {
        float currentHandDistance = Vector3.Distance(movingHand.objectAttachmentPoint.transform.position, scalingHand.objectAttachmentPoint.transform.position);
        float scale = startScale * currentHandDistance / pivotHandDistance;
        if (float.IsNaN(scale) || float.IsInfinity(scale))
            return;
        else if (scale <= SceneManager.INSTANCE.minScaleFactor)
            scale = SceneManager.INSTANCE.minScaleFactor;
        else if (scale >= SceneManager.INSTANCE.maxScaleFactor)
            scale = SceneManager.INSTANCE.maxScaleFactor;

        if (previousScale != scale && scale == SceneManager.INSTANCE.maxScaleFactor)
        {
            audioHelper.transform.position = 0.5f * (movingHand.objectAttachmentPoint.transform.position + scalingHand.objectAttachmentPoint.transform.position);
            audioHelper.Play("Scale End Reached");
        }
        previousScale = scale;

        if (RelativeScaling)
        {
            // Get the pivot point position relative to the solar system at the time point where scaling is triggered
            Vector3 scalingPivot = movingHand.objectAttachmentPoint.transform.position;
            // Set the y value to the value of the model: we do not want to do the scaling relative to the y coordinate!
            scalingPivot.y = SceneManager.INSTANCE.RootTransform.position.y;

            // diff between object pivot to desired pivot/origin
            Vector3 diff = SceneManager.INSTANCE.RootTransform.position - scalingPivot;

            float relativeScaleFactor = scale / SceneManager.INSTANCE.RootTransform.localScale.x;

            // calc final position
            Vector3 finalPosition = scalingPivot + diff * relativeScaleFactor;

            // perform the translation
            SceneManager.INSTANCE.RootTransform.position = finalPosition;
        }

        // perform the scale
        SceneManager.INSTANCE.RootTransform.localScale = scale * Vector3.one;
    }



    private void UpdateScaleLine()
    {
        Vector3 position1 = movingHand.objectAttachmentPoint.transform.position;
        Vector3 position2 = scalingHand.objectAttachmentPoint.transform.position;

        Vector3 direction = (position2 - position1);
        if (direction.magnitude < 0.04f)
        {
            BaseLineRenderer.enabled = false;
            ScaleLineRenderer.enabled = false;
            return;
        }

        position1 += direction.normalized * 0.02f;
        position2 -= direction.normalized * 0.02f;

        BaseLineRenderer.enabled = true;
        BaseLineRenderer.SetPosition(0, position1);
        BaseLineRenderer.SetPosition(1, position2);

        float currentRelativeScale = (SceneManager.INSTANCE.RootTransform.localScale.x - SceneManager.INSTANCE.minScaleFactor) / (SceneManager.INSTANCE.maxScaleFactor - SceneManager.INSTANCE.minScaleFactor);
        float startRelativeScale = (startScale - SceneManager.INSTANCE.minScaleFactor) / (SceneManager.INSTANCE.maxScaleFactor - SceneManager.INSTANCE.minScaleFactor);

        ScaleLineRenderer.enabled = true;
        ScaleLineRenderer.SetPosition(0, position1);
        ScaleLineRenderer.SetPosition(1, position1 + currentRelativeScale * (position2 - position1));
    }
}
