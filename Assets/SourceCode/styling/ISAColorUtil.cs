﻿using ISA.Modelling.Multiplayer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ISAColorUtil
{

    public static Color GetColorForRelationType(RelationType relationType)
    {
        //if (relationType == RelationType.INHERITANCE)
        //    return Color.HSVToRGB(30f / 360f, 0.5f, 1f);
        if (relationType == RelationType.TYPE_REFERENCE)
            return Color.HSVToRGB(50f / 360f, 0.5f, 1f);
        if (relationType == RelationType.METHOD_CALL)
            return Color.HSVToRGB(240f / 360f, 0.5f, 1f);
        if (relationType == RelationType.FIELD_ACCESS)
            return Color.HSVToRGB(170f / 360f, 0.5f, 1f);
        throw new Exception("Unexpected relation type: " + relationType);
    }

    //public static Color GetColorForRelation(RelationLineSegment relationManager)
    //{
    //    HashSet<RelationType> differentRelationTypes = relationManager.GetContainedRelationTypes();

    //    if (differentRelationTypes.Count == 0)
    //        return Color.magenta;

    //    if (differentRelationTypes.Count != 1)
    //        return AdjustColorToRelationWeight(new Color(1, 1, 1), relationManager.totalWeight);

    //    foreach (RelationType type in differentRelationTypes)
    //        // Return the first entry type, there is only one entry anyways
    //        return AdjustColorToRelationWeight(GetColorForRelationType(type), relationManager.totalWeight);

    //    throw new Exception("This can impossibly happen");
    //}

    public static Color AdjustColorToRelationWeight(Color color, float weight)
    {
        //color.a = 1f;
        color.a = 1f - Mathf.Min(0.75f, weight / 50f);
        return color;
    }

}