﻿using System.Collections;
using UnityEngine;
using ISA.Modelling;
using System;

public class ModelStatistics
{

    public static int GetLargestSystemSize()
    {
        int largestSize = 0;
        foreach(ModelStorageMessageEntry entry in ModelLoader.instance.Entries)
        {
            if (entry.numberOfClassifiers > largestSize)
                largestSize = (int)entry.numberOfClassifiers;
        }
        return largestSize;
    }

    public static float GetRelativeSizeInContextOfAllAvailableModels(ModelStorageMessageEntry entry)
    {
        int maxSize = GetLargestSystemSize();
        return (float)entry.numberOfClassifiers / (float)maxSize;
    }
}