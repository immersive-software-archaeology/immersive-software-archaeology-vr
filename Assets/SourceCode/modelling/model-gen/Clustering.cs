﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:2.0.50727.9151
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// Dieser Quellcode wurde automatisch generiert von xsd, Version=2.0.50727.3038.
// 


namespace ISA.Modelling.Clustering
{
/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/clustering")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/clustering", IsNullable=false)]
public partial class ISASimpleCluster {
    
    private string[] elementNamesField;
    
    private ISASimpleCluster[] subClustersField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("elementNames", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    public string[] elementNames {
        get {
            return this.elementNamesField;
        }
        set {
            this.elementNamesField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("subClusters", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASimpleCluster[] subClusters {
        get {
            return this.subClustersField;
        }
        set {
            this.subClustersField = value;
        }
    }
}
}