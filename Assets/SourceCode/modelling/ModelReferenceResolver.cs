using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using ISA.Modelling.Visualization;


namespace ISA.Modelling
{

    public class ModelReferenceResolver
    {
        private ISAVisualizationModel model;

        public ModelReferenceResolver(ISAVisualizationModel model)
        {
            this.model = model;
        }

        // example Reference: "#//@capsules.0/@subCapsules.0/@subCapsules.0/@subCapsules.0/@subCapsules.0/@subCapsules.0/@classifierCuboids.0/@nestedCuboids.1"
        public T resolveReferenceByEcoreString<T>(string ecoreReference)
        {
            object currentElement = model;
            foreach (string segment in ecoreReference.Split('/'))
            {
                if (!segment.StartsWith("@"))
                    continue;

                string fieldName;
                int targetIndex;
                if (segment.Contains("."))
                {
                    fieldName = segment.Split('.')[0].Substring(1);
                    int.TryParse(segment.Split('.')[1], out targetIndex);
                }
                else
                {
                    fieldName = segment.Substring(1);
                    targetIndex = -1;
                }

                Type type = currentElement.GetType();

                PropertyInfo info = type.GetProperty(fieldName);
                if (info == null)
                    throw new Exception("Reference Resolver: Member name \"" + fieldName + "\" supplied is neither a field nor property of type " + type.FullName);

                object field = info.GetValue(currentElement);

                if(targetIndex == -1)
                {
                    // Handle regular ecore references (i.e., not lists / standard fields)
                    currentElement = field;
                }
                else
                {
                    // Handle ecore lists
                    if (!field.GetType().IsArray)
                        throw new Exception("Reference Resolver: Member \"" + fieldName + "\" in type " + type.FullName + " is not an array!");

                    object[] fieldAsArray = (object[])field;
                    currentElement = fieldAsArray[targetIndex];
                }
            }

            if(currentElement is T)
                return (T) currentElement;
            else
                throw new Exception("Reference Resolver: Resolved target is not of type " + typeof(T).Name + ": " + ecoreReference);
        }
    }

}
