﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using ISA.Modelling.Whiteboard;
using ISA.Modelling.Visualization;

namespace ISA.Modelling
{
    class ModelLoader
    {

        public static readonly ModelLoader instance = new ModelLoader();

        private ModelLoader() { }



        public ModelStorageMessageEntry[] Entries;
        public ModelStorageMessageEntry SelectedEntry;

        public ISASoftwareSystemQualityModel QualityModel{ get; private set; }
        public ISAVisualizationModel VisualizationModel { get; private set; }



        public void SetEntries(string response)
        {
            if (response == null)
            {
                Entries = new ModelStorageMessageEntry[0];
            }
            else
            {
                ModelStorageMessage message = JsonUtility.FromJson<ModelStorageMessage>(response);
                Entries = message.entries;
            }
        }

        public void SetSelectedEntry(ModelStorageMessageEntry selectedEntry)
        {
            VisualizationModel = null;
            QualityModel = null;
            SelectedEntry = selectedEntry;
        }



        public void ParseQualityModel(string xmlCodeForModel)
        {
            QualityModel = ParseXMLCodeToObject<ISASoftwareSystemQualityModel>(xmlCodeForModel);
        }

        public void ParseVisualizationModel(string xmlCodeForModel)
        {
            //fullFilePath = "C:\\Eclipse\\2020-09\\workspaces\\ISA-runtime\\.metadata\\.plugins\\dk.itu.cs.isa.storage\\models\\123.xml";
            VisualizationModel = ParseXMLCodeToObject<ISAVisualizationModel>(xmlCodeForModel);
        }



        public static T ParseXMLCodeToObject<T>(string xmlCode)
        {
            T returnObject = default;

            if (string.IsNullOrEmpty(xmlCode))
                return returnObject;

            try
            {
                StringReader stringReader = new StringReader(xmlCode);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(stringReader);
                stringReader.Close();
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
            return returnObject;
        }

        public static T ParseXMLFileToObject<T>(string xmlFileName)
        {
            T returnObject = default;

            if (string.IsNullOrEmpty(xmlFileName))
                return returnObject;

            try
            {
                StreamReader xmlStream = new StreamReader(xmlFileName);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
                xmlStream.Close();
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
            return returnObject;
        }

        public static string encodeModelAsXML<T>(T model)
        {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(writer, model);
            writer.Close();

            return writer.ToString();
        }
    }



    [System.Serializable]
    public class ModelStorageMessage
    {
        public string baseFolderLocation;
        public string modelStorageXmlLocation;

        public ModelStorageMessageEntry[] entries;
    }

    [System.Serializable]
    public class ModelStorageMessageEntry
    {
        public string systemName;
        public int numberOfClassifiers;

        public string structureModel;
        public string qualityModel;
        public string visualizationModel;
    }
}