using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SlicingResult
{

    public int localWeight;
    public string element;
    public string type;
    public SlicingResult[] linkedElements;

}
