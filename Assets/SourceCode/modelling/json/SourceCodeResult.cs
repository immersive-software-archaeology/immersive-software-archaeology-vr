using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SourceCodeMemberArray
{
    public SourceCodeMemberEntry[] entries;
}

[System.Serializable]
public class SourceCodeMemberEntry
{
    public SourceCodeMemberEntry[] entries;
    public string memberQualifiedName;
    public string code;
}
