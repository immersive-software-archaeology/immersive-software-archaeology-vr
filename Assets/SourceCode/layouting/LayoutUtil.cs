using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ISA.util
{

    public class LayoutUtil
    {

        public static void LayoutElementsWithEvenMargins<T>(List<T> elements, Transform parent, float marginBetweenElements, float centerRadius) where T : BoundInteractableMultiViewElement3D
        {
            float[] elementDiameters = new float[elements.Count];
            float largestContainedRadius = 0;

            for (int i = 0; i < elements.Count; i++)
            {
                float radius = elements[i].BoundingVolume.radius;
                if (radius > largestContainedRadius)
                    largestContainedRadius = radius;

                elementDiameters[i] = radius * 2f;
            }

            CalculateVertexPositionsInCircleWithEvenMargins(elementDiameters, marginBetweenElements, centerRadius, largestContainedRadius, out Vector2[] elementPositions, out float layoutCircleRadius);

            for (int i = 0; i < elements.Count; i++)
            {
                // Position the element
                //elements[i].gameObject.SetActive(true);
                elements[i].gameObject.transform.parent = parent;
                elements[i].gameObject.transform.localPosition = new Vector3(elementPositions[i].x, 0, elementPositions[i].y);

                // Also position the bounding volume respectively - but don't change its radius!
                elements[i].BoundingVolume = new Sphere(elements[i].gameObject.transform.position, elements[i].BoundingVolume.radius);
            }
        }

        public static void CalculateVertexPositionsInCircleWithEvenMargins(float[] elementDiameters, float marginBetweenElements, float centerRadius, float largestContainedRadius, out Vector2[] elementLocalPositions, out float layoutCircleRadius)
        {
            float minimumLayoutCircleRadius = centerRadius + largestContainedRadius + marginBetweenElements;
            if (elementDiameters.Length <= 1)
            {
                elementLocalPositions = new Vector2[elementDiameters.Length];
                layoutCircleRadius = minimumLayoutCircleRadius;

                if (elementDiameters.Length == 1)
                    elementLocalPositions[0] = Vector2.up * layoutCircleRadius;

                return;
            }

            for (int i = 0; i < elementDiameters.Length; i++)
                elementDiameters[i] += marginBetweenElements;

            float totalLength = 0;
            foreach (float pointSize in elementDiameters)
                totalLength += pointSize;

            elementLocalPositions = new Vector2[elementDiameters.Length];

            float previousAngleEndInDegrees = 0f;
            for (int i = 0; i < elementDiameters.Length; i++)
            {
                float segmentWidthInDegrees = elementDiameters[i] / totalLength * 360f;
                float segmentCenterInDegrees = previousAngleEndInDegrees + segmentWidthInDegrees / 2f;

                elementLocalPositions[i] = CalculateVertexPositionInUnitCircle(segmentCenterInDegrees);

                previousAngleEndInDegrees += segmentWidthInDegrees;
            }

            // at this point, the vertices are spread out on a unit sphere

            float minRadius = 0;
            foreach (float pointSize in elementDiameters)
                if (pointSize / 2f > minRadius)
                    minRadius = pointSize / 2f;

            Vector2 p0 = elementLocalPositions[0];
            Vector2 p1 = elementLocalPositions[1];
            float exampleDistanceInUnitCircle = Vector2.Distance(p0, p1);

            float exampleDesiredDistance = elementDiameters[0] / 2f + elementDiameters[1] / 2f; // + marginBetweenPoints;
            layoutCircleRadius = exampleDesiredDistance / exampleDistanceInUnitCircle;

            if (layoutCircleRadius < minimumLayoutCircleRadius)
                layoutCircleRadius *= minimumLayoutCircleRadius / layoutCircleRadius;

            for (int i = 0; i < elementLocalPositions.Length; i++)
            {
                elementLocalPositions[i] *= layoutCircleRadius;
            }

            // descrease the circle radius to compact further...
            while(true)
            {
                float smallestDistanceToNeighbor = float.MaxValue;
                float smallestDistanceToCenter = float.MaxValue;
                for (int i = 0; i < elementLocalPositions.Length; i++)
                {
                    Vector2 pos1 = elementLocalPositions[i];
                    Vector2 pos2 = elementLocalPositions[(i + 1) % elementLocalPositions.Length];

                    float distToNeighbor = Vector2.Distance(pos1, pos2) - elementDiameters[i] / 2f - elementDiameters[(i + 1) % elementLocalPositions.Length] / 2f;
                    if (distToNeighbor < smallestDistanceToNeighbor)
                        smallestDistanceToNeighbor = distToNeighbor;

                    float distToCenter = pos1.magnitude - elementDiameters[i] / 2f - centerRadius;
                    if (distToCenter < smallestDistanceToCenter)
                        smallestDistanceToCenter = distToCenter;
                }
                if (smallestDistanceToNeighbor < marginBetweenElements || (centerRadius > 0f && smallestDistanceToCenter < marginBetweenElements))
                {
                    for (int i = 0; i < elementLocalPositions.Length; i++)
                        elementLocalPositions[i] /= 0.99f;
                    break;
                }
                
                for (int i = 0; i < elementLocalPositions.Length; i++)
                    elementLocalPositions[i] *= 0.99f;
            }
        }



        public static Vector2 CalculateVertexPositionInCircleWithEvenDistances(int vertexIndex, int circleResolution, float distanceBetweenVertices)
        {
            float radius = CalculateRadiusOfCircleBasedOnDistanceBetweenVertices(circleResolution, distanceBetweenVertices);
            return CalculateVertexPositionInUnitCircle(vertexIndex, circleResolution) * radius;
        }

        public static float CalculateRadiusOfCircleBasedOnDistanceBetweenVertices(int circleResolution, float distanceBetweenPoints)
        {
            if (circleResolution <= 1)
                return 0f;

            Vector2 p0 = CalculateVertexPositionInUnitCircle(0, circleResolution);
            Vector2 p1 = CalculateVertexPositionInUnitCircle(1, circleResolution);
            float distanceInUnitCircle = Vector2.Distance(p0, p1);

            //return Mathf.Sqrt(distanceBetweenPoints / distanceInUnitCircle);
            //return distanceInUnitCircle * Mathf.Pow(distanceBetweenPoints, 2);
            return distanceBetweenPoints / distanceInUnitCircle;
        }



        public static Vector2 CalculateVertexPositionInUnitCircle(int vertexIndex, int circleResolution)
        {
            float angle = (float)vertexIndex * 360f / (float)circleResolution;
            return CalculateVertexPositionInUnitCircle(angle);
        }

        public static Vector2 CalculateVertexPositionInUnitCircle(float angle)
        {
            return new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
        }

    }

}
