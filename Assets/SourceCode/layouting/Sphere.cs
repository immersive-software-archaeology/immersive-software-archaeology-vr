﻿using System;
using System.Collections.Generic;
using UnityEngine;

public struct Sphere
{

    public bool initialized { get; private set; }

    private Vector3 _center;
    private float _radius;

    public Vector3 center
    {
        get
        {
            return _center;
        }
        set
        {
            initialized = true;
            _center = value;
        }
    }

    public float radius
    {
        get
        {
            return _radius;
        }
        set
        {
            initialized = true;
            _radius = value;
        }
    }



    public Sphere(Vector3 center, float radius) : this()
    {
        this.center = center;
        this.radius = radius;
    }

    public Sphere(Sphere sphere) : this()
    {
        this.center = sphere.center;
        this.radius = sphere.radius;
    }
}