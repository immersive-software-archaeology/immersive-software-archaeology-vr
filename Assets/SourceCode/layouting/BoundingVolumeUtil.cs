﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BoundingVolumeUtil
{

    //private static readonly float EPSILON = 0.00001f;



    public static Sphere CalculateBoundingSphereRitter(List<Sphere> spheresToContain)
    {
        if (spheresToContain == null || spheresToContain.Count < 2)
            throw new Exception("Cannot calculate bounding sphere for empty list of containment");

        // Pick a point x from P, search a point y in P, which has the largest distance from x
        Sphere x = spheresToContain[0];
        Sphere y = getFurthestSphere(spheresToContain, x);

        // Search a point z in P, which has the largest distance from y.
        Sphere z = getFurthestSphere(spheresToContain, y);

        // Set up an initial ball B, with its centre as the midpoint of y and z, the radius as half of the distance between y and z
        Sphere boundingSphere = calculateBoundingSphere(y, z);

        int maxTrials = spheresToContain.Count * 2;
        for (int i = 0; i < maxTrials; i++)
        {
            // If all points in P are within ball B, then we get a bounding sphere
            if (!canFindOutlier(spheresToContain, boundingSphere, out Sphere outlier))
                break;

            // Otherwise, let p be a point outside the ball, construct a new ball covering both point p and previous ball.
            boundingSphere = calculateBoundingSphere(boundingSphere, outlier);

            // Repeat this step until all points are covered
            if (i == maxTrials - 1)
                Debug.LogWarning("Maximum tries for bounding sphere reached!");
        }
        return boundingSphere;
    }

    private static Sphere getFurthestSphere(List<Sphere> spheres, Sphere sphereToCompare)
    {
        Sphere furthestSphere = spheres[0];
        float largestDistance = 0;
        for (int i = 0; i < spheres.Count; i++)
        {
            float distance = getDistanceOfMostFurthestPointsOnSpheres(sphereToCompare, spheres[i]);
            if (distance > largestDistance)
            {
                furthestSphere = spheres[i];
                largestDistance = distance;
            }
        }
        return furthestSphere;
    }

    public static Sphere calculateBoundingSphere(Sphere containedSphere1, Sphere containedSphere2)
    {
        getFurthestPointsOnSpheres(containedSphere1, containedSphere2, out Vector3 point1, out Vector3 point2);

        Vector3 center = (point1 + point2) / 2f;
        float radius = Vector3.Distance(center, point1);
        return new Sphere(center, radius);
    }

    private static bool canFindOutlier(List<Sphere> spheresToContain, Sphere volumeToCheck, out Sphere furthestOutlier)
    {
        furthestOutlier = new Sphere();
        float furthestDistance = 0;

        foreach (Sphere sphere in spheresToContain)
        {
            // If this distance becomes positive, it means we have found an outlier!!
            float distanceBetweenSpheres = Vector3.Distance(volumeToCheck.center, sphere.center) - Mathf.Abs(volumeToCheck.radius - sphere.radius);
            if (distanceBetweenSpheres > furthestDistance)
            {
                furthestOutlier = sphere;
                furthestDistance = distanceBetweenSpheres;
            }
        }
        return furthestDistance > 0;
    }



    private static void getFurthestPointsOnSpheres(Sphere sphere1, Sphere sphere2, out Vector3 point1, out Vector3 point2)
    {
        Vector3 from1To2 = (sphere2.center - sphere1.center).normalized;

        point1 = sphere1.center - from1To2 * sphere1.radius;
        point2 = sphere2.center + from1To2 * sphere2.radius;
    }



    public static float getDistanceBetweenSpheres(Sphere sphere1, Sphere sphere2)
    {
        return Vector3.Distance(sphere1.center, sphere2.center) - (sphere1.radius + sphere2.radius);
    }

    public static float getDistanceOfMostFurthestPointsOnSpheres(Sphere sphere1, Sphere sphere2)
    {
        return Vector3.Distance(sphere1.center, sphere2.center) + sphere1.radius + sphere2.radius;
    }
}