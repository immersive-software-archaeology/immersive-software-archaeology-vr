﻿using ISA.util;
using System;
using System.Collections;
using UnityEngine;

public class AudioClipHelper : MonoBehaviour
{

    [SerializeField]
    private ISAAudioSource[] sources;

    private bool muted = false;



    public void Init()
    {
        for (int i = 0; i < sources.Length; i++)
        {
            sources[i].source = gameObject.AddComponent<AudioSource>();

            if(sources[i].clips.Length == 1)
                sources[i].source.clip = sources[i].clips[0];
            sources[i].source.loop = sources[i].loop;
            sources[i].source.volume = sources[i].volume;

            sources[i].source.playOnAwake = false;
            sources[i].source.spatialBlend = 1;
        }
    }

    public void Play(string name)
    {
        Play(name, 0);
    }

    public void Play(string name, float delay)
    {
        if (muted)
            return;

        ISAAudioSource source = getAudioSource(name);
        if (source.clips.Length > 1)
            source.source.clip = source.clips[RandomFactory.INSTANCE.getRandomInt(0, source.clips.Length - 1)];

        if (delay == 0)
            source.source.Play();
        else
            source.source.PlayDelayed(delay);
    }

    public void Stop(string name)
    {
        getAudioSource(name).source.Stop();
    }

    public void SetVolume(string name, float volume)
    {
        ISAAudioSource source = getAudioSource(name);
        source.volume = volume;
        source.source.volume = volume;
    }

    private ISAAudioSource getAudioSource(string name)
    {
        for (int i = 0; i < sources.Length; i++)
        {
            if (sources[i].source == null)
                Init();

            if (sources[i].name.Equals(name))
                return sources[i];
        }

        throw new Exception("Could not find audio source for \"" + name + "\"");
    }

    public void Mute()
    {
        muted = true;
    }

    public void Unmute()
    {
        muted = false;
    }

}

[System.Serializable]
public struct ISAAudioSource
{
    public string name;
    public bool loop;

    [Range(0, 1)]
    public float volume;

    public AudioClip[] clips;

    [HideInInspector]
    public AudioSource source;
}