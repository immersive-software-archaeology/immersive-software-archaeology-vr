using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeveledUIMesh : MonoBehaviour
{

    public GameObject meshPrefab;
    private GameObject meshObject;

    public bool generateMeshCollider = false;
    public Vector2 offset;
    public int layer = 5;

    public Color color = new Color(0.25f, 0.25f, 0.25f);

    public bool registerHoverEvents = false;
    public Color hoveredColor = new Color(0.1f, 0.1f, 0.1f);

    void Start()
    {
        GenerateMesh();
    }

    //void OnValidate()
    //{
    //    GenerateMesh();
    //}

    public GameObject GenerateMesh()
    {
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        if (meshPrefab == null)
            return null;

        meshObject = Instantiate<GameObject>(meshPrefab);
        meshObject.layer = layer;
        meshObject.transform.parent = transform;
        meshObject.transform.localPosition = Vector3.zero;
        meshObject.transform.localRotation = Quaternion.identity;
        meshObject.transform.localScale = Vector3.one;

        if (!meshObject.TryGetComponent(out MeshRenderer renderer))
            renderer = meshObject.AddComponent<MeshRenderer>();

        Material mat = Instantiate(renderer.material);
        mat.SetColor("_BaseColor", color);
        renderer.material = mat;

        if (!meshObject.TryGetComponent(out MeshFilter filter))
            filter = meshObject.AddComponent<MeshFilter>();

        Mesh mesh = filter.mesh;
        Vector3[] vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i].x < 0)
                vertices[i] = vertices[i] + Vector3.left * (offset.x);
            else if (vertices[i].x > 0)
                vertices[i] = vertices[i] + Vector3.right * (offset.x);

            if (vertices[i].y < 0)
                vertices[i] = vertices[i] + Vector3.down * (offset.y);
            else if (vertices[i].y > 0)
                vertices[i] = vertices[i] + Vector3.up * (offset.y);
        }
        mesh.vertices = vertices;
        mesh.RecalculateBounds();

        if (generateMeshCollider)
        {
            if (!meshObject.TryGetComponent(out MeshCollider collider))
                collider = meshObject.AddComponent<MeshCollider>();
            collider.sharedMesh = mesh;
            collider.convex = true;
            collider.cookingOptions = MeshColliderCookingOptions.CookForFasterSimulation | MeshColliderCookingOptions.EnableMeshCleaning | MeshColliderCookingOptions.WeldColocatedVertices;
        }

        if (registerHoverEvents)
        {
            if (!meshObject.TryGetComponent(out ISAUIInteractable interactable))
                interactable = meshObject.AddComponent<ISAUIInteractable>();
            interactable.onHoverStart.AddListener(delegate { renderer.material.SetColor("_BaseColor", hoveredColor); });
            interactable.onHoverEnd.AddListener(delegate { renderer.material.SetColor("_BaseColor", color); });
        }

        return meshObject;
    }

}
