using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.SolarOverview;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using ISA.util;
using ISA.VR;
using Valve.VR.InteractionSystem;

namespace ISA.UI
{

    public enum NamePlateVisibilityToken
    {
        ACTIVE, HOVERED, IN_RANGE, LOCAL_INVISIBLE, GLOBAL_INVISIBLE
    }



    [RequireComponent(typeof(FollowRotationScript))]
    public class NamePlateManager : MonoBehaviour
    {
        public bool defaultVisibility = false;

        public bool setupForUsageViaInteractionSystem = false;
        public float scaleMultiplier = 1f;

        public BeveledUIMesh uiMesh;
        public float beveledMeshStretchingScale = 0.00002f;

        public bool orientToPlayerHead = true;

        public Canvas NamePlateCanvas;
        public TMP_Text NameText;

        public Vector2 padding;

        public bool performOffsets = true;
        public float offsetZ;
        public float offsetY;

        private Image[] images;
        private Color[] imageColors;

        private TMP_Text[] texts;
        private Color[] textColors;

        private HashSet<NamePlateVisibilityToken> activeTokens = new HashSet<NamePlateVisibilityToken>();



        private void Start()
        {
            if (!orientToPlayerHead)
                GetComponent<FollowRotationScript>().enabled = false;
            else
                GetComponent<FollowRotationScript>().TransformToFollow = Player.instance.headCollider.transform;
        }

        private void Update()
        {
            float lossyScaleBalance = scaleMultiplier * 0.0001f / transform.lossyScale.x;
            if(!float.IsNaN(lossyScaleBalance) && !float.IsInfinity(lossyScaleBalance))
                NamePlateCanvas.transform.localScale = lossyScaleBalance * Math.Max(1, Vector3.Distance(transform.position, Player.instance.headCollider.transform.position)) * Vector3.one;

            if (performOffsets)
            {
                NamePlateCanvas.transform.localPosition = Vector3.back * offsetZ + Vector3.up * offsetY;
                //transform.localPosition = Vector3.up * offsetY * transform.localScale.y;
            }
        }



        public bool AddVisibilityToken(NamePlateVisibilityToken token)
        {
            int tokenCount = activeTokens.Count;
            activeTokens.Add(token);
            if (isActiveAndEnabled && activeTokens.Count == tokenCount)
                // nothing changed
                return false;

            //StopAllCoroutines();
            SetColorsAndMesh();
            return true;
        }

        public bool RemoveVisibilityToken(NamePlateVisibilityToken token)
        {
            bool changed = activeTokens.Remove(token);
            if (!changed)
                return false;

            SetColorsAndMesh();
            return true;

            //if (isActiveAndEnabled && activeTokens.Count == 0)
            //{
            //    StartCoroutine(fadeout());
            //    return true;
            //}
            //return false;
        }

        public string PrintActiveTokens()
        {
            return String.Join("\n", activeTokens);
        }

        private void SetColorsAndMesh()
        {
            if(images == null)
            {
                images = transform.GetComponentsInChildren<Image>();
                imageColors = new Color[images.Length];
                for (int i = 0; i < images.Length; i++)
                    imageColors[i] = images[i].color;

                texts = transform.GetComponentsInChildren<TMP_Text>();
                textColors = new Color[texts.Length];
                for (int i = 0; i < texts.Length; i++)
                    textColors[i] = texts[i].color;
            }

            if (activeTokens.Contains(NamePlateVisibilityToken.GLOBAL_INVISIBLE) || activeTokens.Contains(NamePlateVisibilityToken.LOCAL_INVISIBLE))
            {
                gameObject.SetActive(false);
            }
            else if (defaultVisibility || activeTokens.Contains(NamePlateVisibilityToken.HOVERED) || activeTokens.Contains(NamePlateVisibilityToken.ACTIVE))
            {
                // display with full opacity
                gameObject.SetActive(true);
                for (int i = 0; i < images.Length; i++)
                    images[i].color = new Color(imageColors[i].r, imageColors[i].g, imageColors[i].b, imageColors[i].a);
                for (int i = 0; i < texts.Length; i++)
                    texts[i].color = new Color(textColors[i].r, textColors[i].g, textColors[i].b, textColors[i].a);

                uiMesh.gameObject.SetActive(true);
            }
            else if (activeTokens.Contains(NamePlateVisibilityToken.IN_RANGE))
            {
                // display with reduced opacity
                gameObject.SetActive(true);
                for (int i = 0; i < images.Length; i++)
                    images[i].color = new Color(imageColors[i].r, imageColors[i].g, imageColors[i].b, imageColors[i].a * 0.1f);
                for (int i = 0; i < texts.Length; i++)
                    texts[i].color = new Color(textColors[i].r, textColors[i].g, textColors[i].b, textColors[i].a * 0.25f);

                uiMesh.gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(false);
            }
            Update();
        }

        private IEnumerator fadeout()
        {
            float startTime = Time.time;
            float duration = 0.5f;

            uiMesh.gameObject.SetActive(false);
            yield return null;

            while (Time.time - startTime < duration)
            {
                float progress = 1f - (Time.time - startTime) / duration;

                for (int i = 0; i < images.Length; i++)
                    images[i].color = new Color(imageColors[i].r, imageColors[i].g, imageColors[i].b, images[i].color.a * progress);

                for (int i = 0; i < texts.Length; i++)
                    texts[i].color = new Color(textColors[i].r, textColors[i].g, textColors[i].b, texts[i].color.a * progress);

                yield return null;
            }

            gameObject.SetActive(false);
        }



        public void SetPackageName(TransparentCapsule3D parent, string name)
        {
            if (parent == null)
            {
                SetName(FormatName(name));
            }
            else
            {
                string uniquePart = name.Substring(parent.qualifiedName.Length);
                SetName(FormatName(uniquePart));
            }
        }

        public void SetPackageName(string name)
        {
            SetName(FormatName(name));
        }

        public void SetClassifierName(string name)
        {
            SetName(FormatName(name));
        }

        public static string FormatName(string name)
        {
            string formattedName;

            string[] segmentsPointSeparated = name.Split(".");
            string[] segmentsSlashSeparated = segmentsPointSeparated[0].Split("/");

            if (segmentsSlashSeparated.Length > 1 && segmentsPointSeparated.Length == 1)
            {
                formattedName = "<color=#fff3>";
                for (int i = 0; i < segmentsSlashSeparated.Length - 1; i++)
                    formattedName += segmentsSlashSeparated[i] + "/";
                formattedName += "</color>";
                formattedName += "<color=#ffff><b>" + segmentsSlashSeparated[segmentsSlashSeparated.Length - 1] + "</b></color>";
                return formattedName;
            }

            if (segmentsSlashSeparated.Length > 1 && segmentsPointSeparated.Length > 1)
            {
                formattedName = "<color=#fff9>";
                for (int i = 0; i < segmentsSlashSeparated.Length - 1; i++)
                    formattedName += segmentsSlashSeparated[i] + "/";
                formattedName += "</color>";

                formattedName += "<color=#fff3>";
                formattedName += segmentsSlashSeparated[segmentsSlashSeparated.Length - 1] + ".";
                for (int i = 1; i < segmentsPointSeparated.Length - 1; i++)
                    formattedName += segmentsPointSeparated[i] + ".";
                formattedName += "</color>";
            }
            else
            {
                formattedName = "<color=#fff3>";
                for (int i = 0; i < segmentsPointSeparated.Length - 1; i++)
                    formattedName += segmentsPointSeparated[i] + ".";
                formattedName += "</color>";
            }

            formattedName += "<color=#ffff><b>" + segmentsPointSeparated[segmentsPointSeparated.Length-1] + "</b></color>";
            return formattedName;
        }

        public void SetMemberName(string name)
        {
            string formattedName = "";

            // include package part if present
            string[] segments = name.Split(";", 2);
            if(segments.Length == 2)
            {
                formattedName += "<color=#fff3>" + segments[0] + ".</color>";
                name = segments[1];
            }

            // always include simple name
            segments = name.Split("(", 2);
            formattedName += "<color=#ffff><b>" + segments[0] + "</b></color>";

            // include parameters in paranthesis if present
            if (segments.Length == 2)
                formattedName += "<color=#fff3>(" + segments[1] + "</color>";

            SetName(formattedName);
        }

        public void SetName(string name)
        {
            NameText.text = name;

            RectTransform t = NamePlateCanvas.GetComponent<RectTransform>();
            t.sizeDelta = new Vector2(scaleMultiplier * (NameText.preferredWidth + padding.x), t.sizeDelta.y);

            uiMesh.offset = new Vector2(scaleMultiplier * (beveledMeshStretchingScale * (NameText.preferredWidth + padding.x) /*- 0.005f*/), uiMesh.offset.y);
            uiMesh.GenerateMesh();
        }
    }
}
