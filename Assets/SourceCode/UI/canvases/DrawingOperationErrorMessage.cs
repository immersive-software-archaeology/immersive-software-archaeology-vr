using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DrawingOperationErrorMessage : MonoBehaviour
{

    public TMP_Text titleText;
    public TMP_Text messageText;

    public void SetTitle(string title)
    {
        titleText.SetText(title);
    }

    public void SetMessage(string title)
    {
        messageText.SetText(title);
        //messageText.preferredWidth

        RectTransform t = GetComponent<RectTransform>();
        t.sizeDelta = new Vector2(t.sizeDelta.x, messageText.preferredHeight + 100);
    }

    public void SetAlpha(float a)
    {
        foreach(Transform t in transform)
        {
            if (t.TryGetComponent(out Image image))
                image.color = new Color(image.color.r, image.color.g, image.color.b, a);
            if (t.TryGetComponent(out TMP_Text text))
                text.color = new Color(text.color.r, text.color.g, text.color.b, a);
        }
    }

}
