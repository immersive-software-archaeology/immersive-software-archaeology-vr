﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RelationshipCanvas : MonoBehaviour, VisualizationSynchronizerListener
{

    [SerializeField]
    private Button typeBackButton;
    [SerializeField]
    private Button methodBackButton;
    [SerializeField]
    private Button fieldBackButton;
    [SerializeField]
    private Button typeForwButton;
    [SerializeField]
    private Button methodForwButton;
    [SerializeField]
    private Button fieldForwButton;

    [SerializeField]
    private Transform imageRoot;

    private static readonly string PATH_TO_ICONS = "UI/Icons/Relationships/";
    private static Dictionary<string, Sprite> spriteMap = new Dictionary<string, Sprite>();

    [SerializeField]
    private ActiveToggleable clearButtonToggleable;
    private AbstractVisualElement3D selectedElement;

    private HashSet<GameObject> createdArrows = new HashSet<GameObject>();
    private Dictionary<Image, Color> tintedArrowsToOriginalColor = new Dictionary<Image, Color>();



    public static void InitiallyLoadSpritesFromDisk()
    {
        if (spriteMap.Count > 0)
            return;

        string[] elementTypes = new string[] { "antenna", "block", "capsule", "station" };
        string[] directions = new string[] { "in", "out" };
        string[] levels = new string[] { "fields", "methods", "types" };

        foreach (string elementType in elementTypes)
        {
            // load base image
            {
                string fullPath = PATH_TO_ICONS + elementType;
                spriteMap.Add(fullPath, loadSpriteFromDisk(fullPath));
            }

            foreach (string direction in directions)
            {
                foreach (string level in levels)
                {
                    string fullPath = PATH_TO_ICONS + elementType + "-" + direction + "-" + level;
                    spriteMap.Add(fullPath, loadSpriteFromDisk(fullPath));
                }
            }
        }
    }

    private static Sprite loadSpriteFromDisk(string fullPath)
    {
        Texture2D texture = Resources.Load(fullPath) as Texture2D;
        return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
    }



    public void SendToggleTypeReferencesRequest(bool forward)
    {
        SendToggleRequest(forward, RelationType.TYPE_REFERENCE);
    }

    public void SendToggleMethodCallsRequest(bool forward)
    {
        SendToggleRequest(forward, RelationType.METHOD_CALL);
    }

    public void SendToggleFieldAccessesRequest(bool forward)
    {
        SendToggleRequest(forward, RelationType.FIELD_ACCESS);
    }

    private void SendToggleRequest(bool forward, RelationType type)
    {
        if (VisualizationSynchronizer.instance.IsRelationCurrentlyShown(selectedElement, type, forward))
            SendHideRequest(forward, type);
        else
            SendShowRequest(forward, type);
    }



    private void SendShowRequest(bool forward, RelationType type)
    {
        ShowRelationEvent e = new ShowRelationEvent();
        e.systemName = ModelLoader.instance.VisualizationModel.name;
        e.clientId = SceneManager.INSTANCE.udpClient.id;
        e.elementName = selectedElement.qualifiedName;
        e.relationType = type;
        e.direction = forward ? Direction.FORWARD : Direction.BACKWARD;

        string operationAsString = ModelLoader.encodeModelAsXML(e);
        _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name,
            operationAsString, null, delegate (string r) { Debug.LogError(r); });
    }

    private void SendHideRequest(bool forward, RelationType type)
    {
        ClearOneRelationEvent e = new ClearOneRelationEvent();
        e.systemName = ModelLoader.instance.VisualizationModel.name;
        e.clientId = SceneManager.INSTANCE.udpClient.id;
        e.elementName = selectedElement.qualifiedName;
        e.relationType = type;
        e.direction = forward ? Direction.FORWARD : Direction.BACKWARD;

        string operationAsString = ModelLoader.encodeModelAsXML(e);
        _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name,
            operationAsString, null, delegate (string r) { Debug.LogError(r); });
    }

    public void SendClearRequest()
    {
        ClearRelationsEvent e = new ClearRelationsEvent();
        e.systemName = ModelLoader.instance.VisualizationModel.name;
        e.clientId = SceneManager.INSTANCE.udpClient.id;
        e.elementName = selectedElement.qualifiedName;

        string operationAsString = ModelLoader.encodeModelAsXML(e);
        _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name,
            operationAsString, null, delegate (string r) { Debug.LogError(r); });
    }



    public void NotifyOnSynchronizedEvent(AbstractEvent ev, AbstractVisualElement3D element)
    {
        if (element == selectedElement)
            Setup(selectedElement);
    }



    public void Setup(AbstractVisualElement3D element)
    {
        selectedElement = element;
        VisualizationSynchronizer.instance.RegisterListener(element, this);

        SetButtonStates();

        ResetImagesDefaultState();
        ResetSideArrowsToDefaultState();

        VisualizationSynchronizer.instance.ExecuteOnCurrentlyShownRelations(element,
            delegate (AbstractVisualElement3D element, RelationType relationType, bool forward)
            {
                ChangeButtons(relationType, forward, 1);
                UpdateImages(relationType, forward);
            });
    }



    private void UpdateImages(RelationType relationType, bool forward)
    {
        string level;
        if (relationType == RelationType.TYPE_REFERENCE)
            level = "types";
        else if (relationType == RelationType.METHOD_CALL)
            level = "methods";
        else if (relationType == RelationType.FIELD_ACCESS)
            level = "fields";
        else throw new Exception("Unexpected kind of relation type: " + relationType);

        Image imageToChange = imageRoot.transform.Find((forward ? "out" : "in") + "-" + level).GetComponent<Image>();
        imageToChange.color = ISAColorUtil.GetColorForRelationType(relationType);
    }

    private void ResetImagesDefaultState()
    {
        string elementType;
        if (selectedElement is TransparentCapsule3D)
            elementType = "capsule";
        else if (selectedElement is SpaceStation3D)
            elementType = "station";
        else if (selectedElement is SpaceStationBlock3D)
            elementType = "block";
        else if (selectedElement is SpaceStationAntenna3D)
            elementType = "antenna";
        else throw new Exception("Unexpected kind of element: " + selectedElement.GetType().Name);

        // Set base
        {
            string fullPath = PATH_TO_ICONS + elementType;
            //Debug.Log(fullPath + " > " + (spriteMap[fullPath] == null));
            imageRoot.Find("Base").GetComponent<Image>().sprite = spriteMap[fullPath];
        }
        foreach (Transform imageTransform in imageRoot)
        {
            string fullPath = PATH_TO_ICONS + elementType + "-" + imageTransform.name;
            if (spriteMap.ContainsKey(fullPath))
            {
                string direction = imageTransform.name.Split("-")[0];
                string relationType = imageTransform.name.Split("-")[1];

                //Debug.Log(fullPath + " > " + (spriteMap[fullPath] == null));
                Image image = imageTransform.GetComponent<Image>();
                image.sprite = spriteMap[fullPath];
                if (direction.Equals("out") && AreForwardRelationsAvailable(selectedElement, stringToRelationType(relationType)))
                    image.color = new Color(1, 1, 1, 0.25f);
                else if (direction.Equals("in") && AreBackwardRelationsAvailable(selectedElement, stringToRelationType(relationType)))
                    image.color = new Color(1, 1, 1, 0.25f);
                else
                    image.color = new Color(1, 1, 1, 0.025f);
            }
        }
    }

    private RelationType stringToRelationType(string relationTypeString)
    {
        if (relationTypeString.Equals("types"))
            return RelationType.TYPE_REFERENCE;
        else if (relationTypeString.Equals("methods"))
            return RelationType.METHOD_CALL;
        else if (relationTypeString.Equals("fields"))
            return RelationType.FIELD_ACCESS;
        else
            throw new Exception();
    }

    private Button GetButton(RelationType relationType, bool forward)
    {
        Button button;
        if (relationType == RelationType.TYPE_REFERENCE)
            button = forward ? typeForwButton : typeBackButton;
        else if (relationType == RelationType.METHOD_CALL)
            button = forward ? methodForwButton : methodBackButton;
        else if (relationType == RelationType.FIELD_ACCESS)
            button = forward ? fieldForwButton : fieldBackButton;
        else throw new Exception("Unexpected type of relation: " + relationType);

        return button;
    }

    private void SetButtonStates()
    {
        // Hide buttons that have no purpose, reset colors

        typeForwButton.gameObject.SetActive(AreForwardRelationsAvailable(selectedElement, RelationType.TYPE_REFERENCE));
        methodForwButton.gameObject.SetActive(AreForwardRelationsAvailable(selectedElement, RelationType.METHOD_CALL));
        fieldForwButton.gameObject.SetActive(AreForwardRelationsAvailable(selectedElement, RelationType.FIELD_ACCESS));
        SetButtonNormalColor(typeForwButton, new Color(1, 1, 1, 0.2f));
        SetButtonNormalColor(methodForwButton, new Color(1, 1, 1, 0.2f));
        SetButtonNormalColor(fieldForwButton, new Color(1, 1, 1, 0.2f));

        typeBackButton.gameObject.SetActive(AreBackwardRelationsAvailable(selectedElement, RelationType.TYPE_REFERENCE));
        methodBackButton.gameObject.SetActive(AreBackwardRelationsAvailable(selectedElement, RelationType.METHOD_CALL));
        fieldBackButton.gameObject.SetActive(AreBackwardRelationsAvailable(selectedElement, RelationType.FIELD_ACCESS));
        SetButtonNormalColor(typeBackButton, new Color(1, 1, 1, 0.2f));
        SetButtonNormalColor(methodBackButton, new Color(1, 1, 1, 0.2f));
        SetButtonNormalColor(fieldBackButton, new Color(1, 1, 1, 0.2f));

        // Disable buttons that have no purpose

        //typeForwButton.interactable = AreForwardRelationsAvailable(selectedElement, RelationType.TYPE_REFERENCE);// && !VisualizationSynchronizer.instance.IsRelationCurrentlyShown(selectedElement, RelationType.TYPE_REFERENCE, true);
        //methodForwButton.interactable = AreForwardRelationsAvailable(selectedElement, RelationType.METHOD_CALL);// && !VisualizationSynchronizer.instance.IsRelationCurrentlyShown(selectedElement, RelationType.METHOD_CALL, true);
        //fieldForwButton.interactable = AreForwardRelationsAvailable(selectedElement, RelationType.FIELD_ACCESS);// && !VisualizationSynchronizer.instance.IsRelationCurrentlyShown(selectedElement, RelationType.FIELD_ACCESS, true);

        //typeBackButton.interactable = AreBackwardRelationsAvailable(selectedElement, RelationType.TYPE_REFERENCE);// && !VisualizationSynchronizer.instance.IsRelationCurrentlyShown(selectedElement, RelationType.TYPE_REFERENCE, false);
        //methodBackButton.interactable = AreBackwardRelationsAvailable(selectedElement, RelationType.METHOD_CALL);// && !VisualizationSynchronizer.instance.IsRelationCurrentlyShown(selectedElement, RelationType.METHOD_CALL, false);
        //fieldBackButton.interactable = AreBackwardRelationsAvailable(selectedElement, RelationType.FIELD_ACCESS);// && !VisualizationSynchronizer.instance.IsRelationCurrentlyShown(selectedElement, RelationType.FIELD_ACCESS, false);
    }

    private void SetButtonNormalColor(Button b, Color c)
    {
        ColorBlock buttonColors = b.colors;
        buttonColors.normalColor = c;
        b.colors = buttonColors;
    }

    private void ChangeButtons(RelationType relationType, bool forward, int state)
    {
        Button button = GetButton(relationType, forward);
        SetButtonNormalColor(button, new Color(1, 1, 1, 1));

        Image originalArrow = button.transform.Find("Arrow").GetComponent<Image>();
        //getButton(relationType, forward).interactable = false;
        if (state == 1)
        {
            tintedArrowsToOriginalColor.Add(originalArrow, originalArrow.color);
            originalArrow.color = Color.white;
        }
        //else
        //{
        //    GameObject newArrow = Instantiate<GameObject>(originalArrow.gameObject, originalArrow.transform.parent);
        //    newArrow.transform.localScale = Vector3.one;
        //    newArrow.GetComponent<RectTransform>().anchoredPosition = new Vector2((-22 + 22 * (state - 1)) * (forward ? 1 : -1), 0);
        //    createdArrows.Add(newArrow);
        //}
    }

    private void ResetSideArrowsToDefaultState()
    {
        foreach (Image arrow in tintedArrowsToOriginalColor.Keys)
            arrow.color = tintedArrowsToOriginalColor[arrow];
        tintedArrowsToOriginalColor.Clear();

        foreach (GameObject arrowObject in createdArrows)
            Destroy(arrowObject);
        createdArrows.Clear();
    }



    private bool AreForwardRelationsAvailable(AbstractVisualElement3D element, RelationType relationType)
    {
        return element.GetOutgoingRelationships(relationType).Count > 0;
    }

    private bool AreBackwardRelationsAvailable(AbstractVisualElement3D element, RelationType relationType)
    {
        return element.GetIncomingRelationships(relationType).Count > 0;
    }

}