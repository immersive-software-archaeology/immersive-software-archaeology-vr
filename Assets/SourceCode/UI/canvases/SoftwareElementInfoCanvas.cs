﻿using AfGD;
using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.Modelling.Visualization;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(ActiveToggleable))]
public class SoftwareElementInfoCanvas : MonoBehaviour, ISAActivatableElementListener, TransparentCapsule3DOpenCloseObserver
{
    [SerializeField]
    private Image titleImageToSet;
    [SerializeField]
    private Sprite imagePrefabPackage;
    [SerializeField]
    private Sprite imagePrefabClass;
    [SerializeField]
    private Sprite imagePrefabEnum;
    [SerializeField]
    private Sprite imagePrefabInterface;
    [SerializeField]
    private Sprite imagePrefabRecord;

    public TMP_Text nameText;
    public TMP_Text qualifiedNameText;

    public ScrollRect scrollRect;
    [SerializeField]
    private TMP_Text sourceCodeText;

    private bool wasOpenedOnceBefore = false;

    private TransparentCapsule3D capsule;
    private SpaceStation3D station;

    [SerializeField]
    private Transform shortBioTranform;
    [SerializeField]
    private RelationshipCanvas relationshipCanvas;
    [SerializeField]
    private Transform codeCanvas;
    [SerializeField]
    private Transform capsuleCanvas;
    [SerializeField]
    private Button openCloseCapsuleButton;

    public AbstractVisualElement3D currentlyHighlightedElement { get; private set; }
    private SourceCodeMemberArray sourceCodeJSON;

    [SerializeField]
    private LineRenderer lineRenderer;
    public int lineRendererResolution = 12;

    private Hand lastScrollingHand;
    private Vector3 lastScrollingPosition;



    void Start()
    {
        lineRenderer.positionCount = lineRendererResolution;
        lineRenderer.receiveShadows = false;
        lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        scrollRect.onValueChanged.AddListener(delegate {
            Vector3 pos = GetScrollPosition();
            if (Mathf.Abs(lastScrollingPosition.y - pos.y) < 0.001f)
                return;
            lastScrollingPosition = pos;

            //Debug.Log("Scrolled on canvas for element with name=" + currentlyHighlightedElement.name + (collaboratorIsCurrentlyScrolling ? " (collaborator scrolling): ":": ") + pos.x + ", " + pos.y);
            if (collaboratorIsCurrentlyScrolling)
                return;

            ISAVRPointer activePointer = SceneManager.INSTANCE.GetActivePointer();
            if (activePointer != null)
                lastScrollingHand = activePointer.hand;
            
            SceneManager.INSTANCE.udpClient.RegisterCanvasScroll(lastScrollingHand, this);
        });
    }

    private Coroutine collaboratorFlagResetRoutine;
    private bool collaboratorIsCurrentlyScrolling = false;
    public void PerformScrollViaNetwork(Vector3 scrollPosition)
    {
        collaboratorIsCurrentlyScrolling = true;
        scrollRect.horizontalScrollbar.value = scrollPosition.x;
        scrollRect.verticalScrollbar.value = scrollPosition.y;

        if (collaboratorFlagResetRoutine != null)
            StopCoroutine(collaboratorFlagResetRoutine);
        collaboratorFlagResetRoutine = StartCoroutine(ResetCollaboratorFlagDelayed());
    }

    private IEnumerator ResetCollaboratorFlagDelayed()
    {
        yield return new WaitForSeconds(0.2f);
        collaboratorIsCurrentlyScrolling = false;
    }

    public Vector3 GetScrollPosition()
    {
        return Vector3.up * scrollRect.verticalScrollbar.value + Vector3.right * scrollRect.horizontalScrollbar.value;
    }



    public void Close()
    {
        if (capsule != null)
        {
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(capsule, false);
        }
        if(station != null)
        {
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(station, false);
            foreach (SpaceStationBlock3D block in station.Blocks)
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(block, false);
            foreach (SpaceStationAntenna3D antenna in station.Antennas)
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(antenna, false);
        }
    }



    public void Initialize(TransparentCapsule3D capsule)
    {
        this.capsule = capsule;
        ISAUIInteractionManager.INSTANCE.RegisterActivationListener(capsule, this);

        nameText.text = capsule.simpleName;
        qualifiedNameText.text = capsule.qualifiedName;
        titleImageToSet.sprite = imagePrefabPackage;

        foreach (Transform childTransform in transform)
            if (childTransform.TryGetComponent(out Canvas c))
                SceneManager.INSTANCE.RegisterCanvas(c);

        BeveledUIMesh meshGenerator = shortBioTranform.Find("Background").GetComponent<BeveledUIMesh>();
        meshGenerator.color = capsule.CanvasColor;
        meshGenerator.GenerateMesh();

        capsule.AddListener(this);
        openCloseCapsuleButton.onClick.AddListener(delegate {
            if(capsule.isOpened)
                capsule.SendCloseRequest();
            else
                capsule.SendOpenRequest();
        });
    }

    public void Initialize(SpaceStation3D station)
    {
        this.station = station;
        RegisterForStation(station);

        nameText.text = station.simpleName;
        qualifiedNameText.text = station.qualifiedName;
        if (station.ecoreModel.type == ISASpaceStationType.CLASS)
            titleImageToSet.sprite = imagePrefabClass;
        else if (station.ecoreModel.type == ISASpaceStationType.ENUM)
            titleImageToSet.sprite = imagePrefabEnum;
        else if (station.ecoreModel.type == ISASpaceStationType.INTERFACE)
            titleImageToSet.sprite = imagePrefabInterface;
        else if (station.ecoreModel.type == ISASpaceStationType.RECORD)
            titleImageToSet.sprite = imagePrefabRecord;
        else
            throw new Exception("Unknown kind of station: " + station.ecoreModel.type);

        BeveledUIMesh meshGenerator = shortBioTranform.Find("Background").GetComponent<BeveledUIMesh>();
        meshGenerator.color = station.GetParentCapsule().CanvasColor;
        meshGenerator.GenerateMesh();

        foreach (Transform childTransform in transform)
            if (childTransform.TryGetComponent(out Canvas c))
                SceneManager.INSTANCE.RegisterCanvas(c);
    }

    private void RegisterForStation(SpaceStation3D s)
    {
        ISAUIInteractionManager.INSTANCE.RegisterActivationListener(s, this);

        foreach (SpaceStationBlock3D block in s.Blocks)
            ISAUIInteractionManager.INSTANCE.RegisterActivationListener(block, this);
        foreach (SpaceStationAntenna3D antenna in s.Antennas)
            ISAUIInteractionManager.INSTANCE.RegisterActivationListener(antenna, this);

        foreach (SpaceStation3D satelliteStation in s.SatelliteStations)
            RegisterForStation(satelliteStation);
    }



    void Update()
    {
        // Draw a line from the canvas to the target
        if(currentlyHighlightedElement != null)
        {
            Transform targetTransform = currentlyHighlightedElement.handHoverHighlighter.transform;
            CurveSegment curve = CurveUtil.CalculateCurve(lineRenderer.transform.position, targetTransform.position, lineRenderer.transform.forward, (lineRenderer.transform.position - targetTransform.position).normalized);
            CurveUtil.RenderCurve(lineRenderer, curve, false);
        }
    }



    public void OnAttachToHand()
    {
    }

    public void OnDetachFromHand()
    {
        //UnityEngine.SceneManagement.SceneManager.MoveGameObjectToScene(gameObject, UnityEngine.SceneManagement.SceneManager.GetActiveScene());

        Vector3 position = transform.position;
        Vector3 rotation = Quaternion.LookRotation(-transform.forward, transform.up).eulerAngles;

        // Send an updated position
        ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(currentlyHighlightedElement, true, position, rotation);
    }



    public void OpenInIDE()
    {
        _ = HTTPRequestHandler.INSTANCE.SendRequest("open/?systemName=" + ModelLoader.instance.VisualizationModel.name + "&classifierQualifiedName=" + station.qualifiedName);
    }



    public void NotifyOnCapsuleOpenCloseEvent(TransparentCapsule3D capsule, bool nowOpen)
    {
        if(nowOpen)
        {
            openCloseCapsuleButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(-50, 0);
            openCloseCapsuleButton.transform.GetChild(0).GetComponent<SpriteToggler>().ChangeSprite(0);
        }
        else
        {
            openCloseCapsuleButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(50, 0);
            openCloseCapsuleButton.transform.GetChild(0).GetComponent<SpriteToggler>().ChangeSprite(1);
        }

    }

    public void NotifyOnElementActiveStateChanged(object element, bool isActive, Vector3 position, Vector3 rotation)
    {
        if (isActive)
        {
            gameObject.SetActive(true);

            currentlyHighlightedElement = (AbstractVisualElement3D)element;

            nameText.text = currentlyHighlightedElement.simpleName;
            qualifiedNameText.text = currentlyHighlightedElement.qualifiedName;

            //if (!wasOpenedOnceBefore) // || Vector3.Distance(transform.position, position) > 2f)
            {
                transform.parent = null;
                transform.localScale = 0.5f * Vector3.one;
                transform.position = position;
                transform.rotation = Quaternion.Euler(rotation);
                transform.rotation = Quaternion.LookRotation(-transform.forward, transform.up);
            }
            Update();

            relationshipCanvas.Setup(currentlyHighlightedElement);
            if (station != null)
            {
                if (!wasOpenedOnceBefore)
                    LoadSourceCode();
                else
                    SetSourceCodeAndStartAnimation();
            }

            wasOpenedOnceBefore = true;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void LoadSourceCode()
    {
        SetText("<color=#FFFFFF66>Loading...</color>");
        _ = HTTPRequestHandler.INSTANCE.SendRequest("code/classifier/?systemName=" + ModelLoader.instance.VisualizationModel.name + "&classifierQualifiedName=" + station.qualifiedName, null, callBackSourceCodeSuccess, callBackSourceCodeFail);
    }

    private void callBackSourceCodeSuccess(string response)
    {
        sourceCodeJSON = JsonUtility.FromJson<SourceCodeMemberArray>(response);
        SetSourceCodeAndStartAnimation();
    }

    private void callBackSourceCodeFail(string response)
    {
        SetText("<color=#FF8D40><b>Loading Failed!</b></color>\n\n" +
            "There was a problem with finding the code for this element.\n\n" +
            "The most likely explanation is that your Eclipse runtime does not contain\n" +
            "the source code of this system in form of Java projects - or maybe their\n" +
            "names do not match with the names specified in the  models loaded in VR.\n\n" +
            "If you want to make sure everything runs smoothly, rerun the automated\n" +
            "system analysis within Eclipse!");
    }



    private void SetText(string text)
    {
        sourceCodeText.SetText(text);
        sourceCodeText.GetComponent<RectTransform>().sizeDelta = new Vector2(sourceCodeText.preferredWidth, sourceCodeText.preferredHeight);
    }

    private Coroutine setTextRoutine;
    private void SetSourceCodeAndStartAnimation()
    {
        if (!gameObject.activeSelf)
            return;
        if (currentlyHighlightedElement is TransparentCapsule3D)
            return;

        //if (currentlyHighlightedElement.Equals(station))
        //    // If the highlighted element is the station itself, don't highlight anything
        //    currentlyHighlightedElement = null;

        if(setTextRoutine != null)
            StopCoroutine(setTextRoutine);
        setTextRoutine = StartCoroutine(SourceCodeAnimation());
    }

    private IEnumerator SourceCodeAnimation()
    {
        if (sourceCodeJSON == null)
        {
            LoadSourceCode();
            yield break;
        }

        string code = RecursivelyGenerateHighlightedCode(sourceCodeJSON.entries, true, out int linebreaksToHighlightMember, out int linebreaksTotal, out bool highlightedMemberReached);

        SetText(code);

        if(highlightedMemberReached)
        {
            // Animate smooth slide
            float targetPosition = Mathf.Min(1f, 1f - ((float)linebreaksToHighlightMember - 3f) / ((float)linebreaksTotal - 27f));
            //float startTime = Time.time;

            //float lastChange = float.MaxValue;
            //while (lastChange > 1f / sourceCodeText.preferredHeight)
            //{
            //    yield return new WaitForEndOfFrame();
            //    float currentScrollValue = scrollRect.verticalScrollbar.value;

            //    float diff = (scrollRect.verticalScrollbar.value - targetPosition);
            //    scrollRect.verticalScrollbar.value -= diff * Time.deltaTime * 2f;

            //    if (Time.time - startTime > 5)
            //        // safety stop after 5 seconds because sometimes the
            //        // calculation is bit weird and then the animation will
            //        // never stop, rendering the source code pane useless...
            //        break;

            //    lastChange = Mathf.Abs(scrollRect.verticalScrollbar.value - currentScrollValue);
            //}

            //yield return new WaitForEndOfFrame();
            scrollRect.verticalScrollbar.value = targetPosition;
        }
    }

    private string RecursivelyGenerateHighlightedCode(SourceCodeMemberEntry[] entries, bool allowTransparency, out int linebreaksToHighlightMember, out int linebreaksTotal, out bool highlightedMemberReached)
    {
        string code = "";

        linebreaksToHighlightMember = 0;
        linebreaksTotal = 0;
        highlightedMemberReached = false;

        foreach (SourceCodeMemberEntry entry in entries)
        {
            string localCode;
            if (entry.entries != null)
            {
                bool thisMemberIsTheHighlightTarget = false;
                if (entry.memberQualifiedName != null && currentlyHighlightedElement != null && entry.memberQualifiedName.Equals(currentlyHighlightedElement.qualifiedName))
                {
                    highlightedMemberReached = true;
                    thisMemberIsTheHighlightTarget = true;
                }

                // is a nested class
                localCode = RecursivelyGenerateHighlightedCode(entry.entries, !thisMemberIsTheHighlightTarget, out int nestedLinebreaksToHighlightMember, out int nestedLinebreaksTotal, out bool nestedHighlightedMemberReached);

                linebreaksTotal += nestedLinebreaksTotal;
                if (nestedHighlightedMemberReached)
                {
                    // Somewhere within the nested structure, we reached the member to highlight
                    highlightedMemberReached = true;
                    linebreaksToHighlightMember += nestedLinebreaksToHighlightMember;
                }
                else
                {
                    //localCode = makeTransparent(localCode);
                    if(!highlightedMemberReached)
                        linebreaksToHighlightMember += nestedLinebreaksTotal;
                }
            }
            else
            {
                // is a field, method, or constructor
                localCode = entry.code;

                if (entry.memberQualifiedName != null && currentlyHighlightedElement != null && entry.memberQualifiedName.Equals(currentlyHighlightedElement.qualifiedName))
                    highlightedMemberReached = true;
                else if (allowTransparency && !currentlyHighlightedElement.Equals(station))
                    localCode = MakeTransparent(localCode);

                if (!highlightedMemberReached)
                    linebreaksToHighlightMember += localCode.Split("\n").Length;
                linebreaksTotal += localCode.Split("\n").Length;
            }

            code += localCode;
        }

        return code;
    }

    private string MakeTransparent(string code)
    {
        string[] segments = code.Split("<color=#");
        string newCode = segments[0];
        
        for(int i=1; i<segments.Length; i++)
        {
            string colorCode = segments[i].Substring(0, 6);
            string remainder = segments[i].Substring(6);

            newCode += "<color=#" + colorCode + "22" + remainder;
        }
        return "<color=#BBBBBB33>" + newCode + "</color>";
    }
}