using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.SolarOverview;
using ISA.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ISAUIInteractionManager
{

    public static ISAUIInteractionManager INSTANCE = new ISAUIInteractionManager();

    private Dictionary<object, List<ISAHoverableElementListener>> hoverListeners;
    private Dictionary<object, List<ISAActivatableElementListener>> activeListeners;
    private Dictionary<object, List<ISAPositionableElementListener>> positionListeners;

    private List<object> hoveredElements = new List<object>();
    private List<object> activeElements = new List<object>();

    private ISAUIInteractionManager()
    {
        hoverListeners = new Dictionary<object, List<ISAHoverableElementListener>>();
        activeListeners = new Dictionary<object, List<ISAActivatableElementListener>>();
        positionListeners = new Dictionary<object, List<ISAPositionableElementListener>>();

        if(SceneManager.INSTANCE != null)
            SceneManager.INSTANCE.StartCoroutine(ActivationEventBatchDeliveryRoutine());
    }

    public void Reset()
    {
        hoverListeners.Clear();
        activeListeners.Clear();
        positionListeners.Clear();
    }




    public void RegisterHoverListener(object elementToReceiveUpdatesFor, ISAHoverableElementListener newListener)
    {
        if (hoverListeners.TryGetValue(elementToReceiveUpdatesFor, out List<ISAHoverableElementListener> existingListeners))
        {
            if(!existingListeners.Contains(newListener))
                existingListeners.Add(newListener);
        }
        else
        {
            List<ISAHoverableElementListener> newListenerList = new List<ISAHoverableElementListener>();
            newListenerList.Add(newListener);
            hoverListeners.Add(elementToReceiveUpdatesFor, newListenerList);
        }
    }

    public void RemoveHoverListener(ISAHoverableElementListener listener)
    {
        foreach (object key in hoverListeners.Keys)
        {
            if (hoverListeners.TryGetValue(key, out List<ISAHoverableElementListener> existingListeners))
            {
                if(existingListeners.Remove(listener))
                {
                    if(existingListeners.Count == 0)
                    {
                        hoverListeners.Remove(key);
                    }
                }
            }
        }
    }

    public void RegisterActivationListener(object elementToReceiveUpdatesFor, ISAActivatableElementListener newListener)
    {
        if (activeListeners.TryGetValue(elementToReceiveUpdatesFor, out List<ISAActivatableElementListener> existingListeners))
        {
            if (!existingListeners.Contains(newListener))
                existingListeners.Add(newListener);
        }
        else
        {
            List<ISAActivatableElementListener> newListenerList = new List<ISAActivatableElementListener>();
            newListenerList.Add(newListener);
            activeListeners.Add(elementToReceiveUpdatesFor, newListenerList);
        }
    }

    public void RemoveActivationListener(ISAActivatableElementListener listener)
    {
        foreach (object key in activeListeners.Keys)
        {
            if (activeListeners.TryGetValue(key, out List<ISAActivatableElementListener> existingListeners))
            {
                if (existingListeners.Remove(listener))
                {
                    if (existingListeners.Count == 0)
                    {
                        hoverListeners.Remove(key);
                    }
                }
            }
        }
    }

    public void RegisterPositioningListener(object elementToReceiveUpdatesFor, ISAPositionableElementListener newListener)
    {
        if (positionListeners.TryGetValue(elementToReceiveUpdatesFor, out List<ISAPositionableElementListener> existingListeners))
        {
            if (!existingListeners.Contains(newListener))
                existingListeners.Add(newListener);
        }
        else
        {
            List<ISAPositionableElementListener> newListenerList = new List<ISAPositionableElementListener>();
            newListenerList.Add(newListener);
            positionListeners.Add(elementToReceiveUpdatesFor, newListenerList);
        }
    }

    public void RemovePositioningListener(ISAPositionableElementListener listener)
    {
        foreach (object key in positionListeners.Keys)
        {
            if (positionListeners.TryGetValue(key, out List<ISAPositionableElementListener> existingListeners))
            {
                if (existingListeners.Remove(listener))
                {
                    if (existingListeners.Count == 0)
                    {
                        hoverListeners.Remove(key);
                    }
                }
            }
        }
    }



    public void HoverStart(object e)
    {
        Hovered(e, true);
    }

    public void HoverEnd(object e)
    {
        Hovered(e, false);
    }

    public void Hovered(object e, bool hoveredNow)
    {
        if (hoveredNow)
        {
            if (!hoveredElements.Contains(e))
                hoveredElements.Add(e);
        }
        else
        {
            hoveredElements.Remove(e);
        }
        PerformHoverNotifications(e, hoveredNow);
    }

    private void PerformHoverNotifications(object e, bool hoveredNow)
    {
        if (e == null)
            return;

        if (hoverListeners.TryGetValue(e, out List<ISAHoverableElementListener> listeners1))
        {
            foreach (ISAHoverableElementListener listener in listeners1)
            {
                listener.NotifyOnElementHoverStateChanged(e, isHovered(e));
            }
        }
    }



    public void SubmitActiveStateToggleEvent(object element)
    {
        if (!isActive(element))
            SubmitActiveStateChangeEvent(element, true);
        else
            SubmitActiveStateChangeEvent(element, false);
    }

    public void SubmitActiveStateChangeEvent(object element, bool setToActive)
    {
        Vector3 playerHeadPosition = Player.instance.headCollider.transform.position;
        Vector3 playerHeadLookDirection = Player.instance.headCollider.transform.forward;

        float distFromHead = 0.5f;
        foreach (RaycastHit hit in Physics.SphereCastAll(new Ray(playerHeadPosition, playerHeadLookDirection), 0.2f, distFromHead, LayerMask.GetMask("UI", "Drawing")))
            distFromHead = Math.Max(distFromHead, hit.distance - 0.05f);
        distFromHead = Math.Max(distFromHead, 0.25f);

        Vector3 position = playerHeadPosition + playerHeadLookDirection * distFromHead;
        Vector3 rotation = Quaternion.LookRotation(-playerHeadLookDirection).eulerAngles;

        SubmitActiveStateChangeEvent(element, setToActive, position, rotation);
    }

    public void SubmitActiveStateChangeEvent(object element, bool setToActive, Vector3 position, Vector3 rotation)
    {
        int elementId;
        ElementType elementType;
        if (element is AbstractVisualElement3D)
        {
            elementId = ((AbstractVisualElement3D)element).id;
            elementType = ElementType.VISUAL_ELEMENT;
        }
        else if (element is SoftwareElementPinHandler)
        {
            elementId = ((SoftwareElementPinHandler)element).id;
            elementType = ElementType.PIN;
        }
        else throw new Exception("Unexpected kind of activated element: " + element.GetType().Name);

        ChangeElementActiveStateEvent ev = new ChangeElementActiveStateEvent();
        ev.systemName = ModelLoader.instance.VisualizationModel.name;
        ev.clientId = SceneManager.INSTANCE.udpClient.id;
        ev.elementId = elementId;
        ev.elementType = elementType;
        ev.newState = setToActive;
        ev.position = EcoreUtil.ConvertToEcoreMultiplayer(position);
        ev.rotation = EcoreUtil.ConvertToEcoreMultiplayer(rotation);

        activationEventQueue.Add(ev);
    }

    private List<ChangeElementActiveStateEvent> activationEventQueue = new List<ChangeElementActiveStateEvent>();

    private IEnumerator ActivationEventBatchDeliveryRoutine()
    {
        while(true)
        {
            yield return new WaitForFixedUpdate();

            if (activationEventQueue.Count == 0)
                continue;
            if (ModelLoader.instance.VisualizationModel == null)
                continue;

            List<ChangeElementActiveStateEvent> activationEventQueueCopy = new List<ChangeElementActiveStateEvent>();
            while (activationEventQueue.Count > 0)
            {
                activationEventQueueCopy.Add(activationEventQueue[0]);
                activationEventQueue.RemoveAt(0);
            }

            {
                EventLog batch = new EventLog();
                batch.log = activationEventQueueCopy.ToArray();

                string operationAsString = ModelLoader.encodeModelAsXML(batch);
                _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name,
                    operationAsString, null, delegate (string r) { Debug.LogError(r); });
            }
        }
    }

    public void PerformActiveStateChangeLocally(object e, bool activeNow, Vector3 position, Vector3 rotation)
    {
        if (e == null)
            return;

        if (activeNow)
        {
            if (!activeElements.Contains(e))
                activeElements.Add(e);
        }
        else
        {
            activeElements.Remove(e);
        }

        if (activeListeners.TryGetValue(e, out List<ISAActivatableElementListener> listeners))
        {
            foreach (ISAActivatableElementListener listener in listeners)
            {
                listener.NotifyOnElementActiveStateChanged(e, activeNow, position, rotation);
            }
        }
    }



    public void PositionChanged(object e)
    {
        PerformPositionNotifications(e);
    }

    private void PerformPositionNotifications(object e)
    {
        if (e == null)
            return;

        if (positionListeners.TryGetValue(e, out List<ISAPositionableElementListener> listeners))
        {
            foreach (ISAPositionableElementListener listener in listeners)
                listener.NotifyOnElementPositionChanged(e);
        }
        if (e is TransparentCapsule3D)
        {
            foreach (TransparentCapsule3D subCapsule in ((TransparentCapsule3D)e).containedSubCapsules)
                PerformPositionNotifications(subCapsule);

            TransparentCapsuleContainment3D containment = ((TransparentCapsule3D)e).directContainment;
            PerformPositionNotifications(containment);
        }
        if (e is TransparentCapsuleContainment3D)
        {
            foreach (SpaceStation3D station in ((TransparentCapsuleContainment3D)e).containedSpaceStations)
                PerformPositionNotifications(station);
        }
        if (e is SpaceStation3D)
        {
            foreach (SpaceStation3D satellite in ((SpaceStation3D)e).SatelliteStations)
                PerformPositionNotifications(satellite);
            foreach (SpaceStationBlock3D block in ((SpaceStation3D)e).Blocks)
                PerformPositionNotifications(block);
            foreach (SpaceStationAntenna3D antenna in ((SpaceStation3D)e).Antennas)
                PerformPositionNotifications(antenna);
        }
    }



    public bool isHovered(object e)
    {
        return hoveredElements.Contains(e);
    }

    public bool isActive(object e)
    {
        return activeElements.Contains(e);
    }

}


public interface ISAHoverableElementListener
{
    public void NotifyOnElementHoverStateChanged(object e, bool isHovered);
}

public interface ISAActivatableElementListener
{
    public void NotifyOnElementActiveStateChanged(object e, bool isActive, Vector3 position, Vector3 rotation);
}

public interface ISAPositionableElementListener
{
    public void NotifyOnElementPositionChanged(object e);
}