using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using ISA.SolarOverview;

namespace ISA.UI
{

    public class SlicingButtonManager : MonoBehaviour
    {
        //public enum SlicingTargetLevel
        //{
        //    BUILDING,
        //    CITY,
        //    PLANET
        //}

        //public TMP_Text ErrorLabel;

        //public int connectionDepthValue;

        //public AbstractInfoCanvasManager containingCanvasManager;

        //public Button _Button;

        //public Image ArrowIcon;
        //public Image LoadIcon;

        //private Color buttonDefaultColor;
        //private Color arrowDefaultColor;

        //private bool currentlyShowsConnections = false;



        //public void Start()
        //{
        //    buttonDefaultColor = _Button.image.color;
        //    arrowDefaultColor = _Button.image.color;

        //    _Button.onClick.AddListener(delegate { ToggleConnections(); });
        //    ErrorLabel.gameObject.SetActive(false);
        //    SetDefaultState();
        //}

        //public void DeleteConnections()
        //{
        //    ConnectionLineLayouter.DeleteConnections(containingCanvasManager.ecoreElement, connectionDepthValue);
        //    SetDefaultState();
        //}

        //private void ToggleConnections()
        //{
        //    if(!currentlyShowsConnections)
        //    {
        //        // make visible
        //        SetLoadingState();
        //        if (containingCanvasManager.TargetLevel.Equals(SlicingTargetLevel.BUILDING))
        //            BuildingConnectionCreator.INSTANCE.SetConnections(containingCanvasManager.ecoreElement, connectionDepthValue, SetColorizedActiveState, SetErrorMessage);
        //        else if (containingCanvasManager.TargetLevel.Equals(SlicingTargetLevel.CITY))
        //            CityConnectionCreator.INSTANCE.SetConnections(containingCanvasManager.ecoreElement, connectionDepthValue, SetColorizedActiveState, SetErrorMessage);
        //        else if (containingCanvasManager.TargetLevel.Equals(SlicingTargetLevel.PLANET))
        //            PlanetConnectionCreator.INSTANCE.SetConnections(containingCanvasManager.ecoreElement, connectionDepthValue, SetColorizedActiveState, SetErrorMessage);
        //    }
        //    else
        //    {
        //        DeleteConnections();
        //    }
        //    currentlyShowsConnections = !currentlyShowsConnections;
        //}

        //private void SetColorizedActiveState()
        //{
        //    LoadIcon.gameObject.SetActive(false);
        //    _Button.interactable = true;

        //    Color color = AbstractConnectionCreator.GetColorForConnectionDepth(connectionDepthValue);
        //    Color.RGBToHSV(color, out float h, out float s, out _);
        //    color = Color.HSVToRGB(h, 0.75f * s, 1f);

        //    _Button.image.color = color;
        //    ArrowIcon.color = color;
        //}

        //private void SetLoadingState()
        //{
        //    _Button.interactable = false;
        //    LoadIcon.gameObject.SetActive(true);
        //    ErrorLabel.gameObject.SetActive(false);

        //    _Button.image.color = new Color(1, 1, 1, 0);
        //}

        //private void SetDefaultState()
        //{
        //    LoadIcon.gameObject.SetActive(false);

        //    _Button.image.color = buttonDefaultColor;
        //    ArrowIcon.color = arrowDefaultColor;

        //    _Button.interactable = true;
        //}

        //private void SetErrorMessage()
        //{
        //    ErrorLabel.gameObject.SetActive(true);
        //    SetDefaultState();
        //}
    }

}
