using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.SolarOverview;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using ISA.Modelling;
using System.Threading.Tasks;
using System.Net.Http;

namespace ISA.UI
{
    public class BuildingInfoCanvasManager : AbstractInfoCanvasManager
    {
        //private static readonly HttpClient client = new HttpClient();

        //public TMP_Text QualifiedNameText;

        //public TMP_Text ConstructorFloorText;
        //public TMP_Text MethodFloorText;

        //private bool sourceCodeRequested = false;
        //public TMP_Text SourceCodeText;

        //public BuildingInCity3D buildingInCity { private set; get; }



        //public void Setup(BuildingOnPlanet3D buildingOnPlanet)
        //{
        //    this.ecoreElement = buildingOnPlanet.ecoreModelBuilding;
        //    //this.targetTransform = buildingOnPlanet.gameObject.transform;

        //    SetupInternal();
        //}

        //public void Setup(BuildingInCity3D buildingInCity)
        //{
        //    this.ecoreElement = buildingInCity.ecoreModel;
        //    //this.targetTransform = buildingInCity.gameObject.transform;
        //    this.buildingInCity = buildingInCity;

        //    SetupInternal();
        //}

        //private void SetupInternal()
        //{
        //    NameText.text = ecoreElement.name;
        //    QualifiedNameText.text = ecoreElement.qualifiedName;

        //    ISAFloor[] floors = ((ISABuilding)ecoreElement).floors ?? new ISAFloor[0];

        //    int numberOfAntennaFloors = 0;
        //    foreach (ISAFloor floor in floors)
        //        if (floor.antennaFloor)
        //            numberOfAntennaFloors++;

        //    ConstructorFloorText.text = "" + numberOfAntennaFloors;
        //    MethodFloorText.text = "" + (floors.Length - numberOfAntennaFloors);

        //    base.Setup();
        //    //SceneHandler.INSTANCE.AddListener(this);
        //}



        //protected override void layoutCanvasNextToElement()
        //{
        //    if (!PlayerLocationTracker.INSTANCE.PlayerIsCurrentlyInSpaceship)
        //    {
        //        if (connectionManager.elementHookTransform == null || buildingInCity == null)
        //            return;

        //        Vector3 buildingPositionWithEyeY = connectionManager.elementHookTransform.position;
        //        buildingPositionWithEyeY.y = SceneHandler.INSTANCE.VRHeadCollider.transform.position.y;
        //        Vector3 headToBuildingOnEyeY = (SceneHandler.INSTANCE.VRHeadCollider.transform.position - buildingPositionWithEyeY);
        //        Vector3 lookDirectionNormal = Vector3.Cross(headToBuildingOnEyeY.normalized, Vector3.up).normalized;

        //        transform.localScale = Vector3.one * Mathf.Max(0.3f, Mathf.Min(10f, headToBuildingOnEyeY.magnitude * 0.75f));
        //        transform.position = buildingPositionWithEyeY
        //            + lookDirectionNormal * Planet3D.OVERSIZED_GENERATION_FACTOR * connectionManager.elementHookTransform.lossyScale.x * buildingInCity.MaxDiameter / SceneHandler.INSTANCE.PlanetSize
        //            + lookDirectionNormal * 0.2f
        //            + Vector3.up * transform.localScale.x / 2f;

        //        Vector3 headToBuilding = transform.position - SceneHandler.INSTANCE.VRHeadCollider.transform.position;
        //        transform.rotation = Quaternion.LookRotation(headToBuilding);
        //        transform.localScale = Vector3.one * Mathf.Max(1f, Mathf.Min(15f, headToBuilding.magnitude * 0.75f));
        //    }
        //    else base.layoutCanvasNextToElement();
        //}

        //protected override void OnSwitchToActive(ISANamedVisualElement e)
        //{
        //    if(!sourceCodeRequested)
        //    {
        //        sourceCodeRequested = true;
        //        _ = SetupSourceCodeText();
        //    }
        //    base.OnSwitchToActive(e);
        //}

        //protected override void OnSwitchToHovered(ISANamedVisualElement e)
        //{
        //    if (!sourceCodeRequested)
        //    {
        //        sourceCodeRequested = true;
        //        _ = SetupSourceCodeText();
        //    }
        //    base.OnSwitchToHovered(e);
        //}

        //private async Task SetupSourceCodeText()
        //{
        //    string request = "http://localhost:9005/code/classifier/" +
        //        "?systemName=" + ModelLoader.instance.VisualizationModel.name +
        //        "&classifierQualifiedName=" + ecoreElement.qualifiedName;
        //    //Debug.Log("Request: " + request);

        //    try
        //    {
        //        string response = await client.GetStringAsync(request);
        //        //Debug.Log(response);
        //        /*SourceCodeMemberArray result = JsonUtility.FromJson<SourceCodeMemberArray>(response);

        //        string quickAndDirtyCompleteCode = "";
        //        foreach(SourceCodeMemberEntry entry in result.entries)
        //        {
        //            quickAndDirtyCompleteCode += entry.code + "\n";
        //        }*/

        //        SourceCodeText.text = response;
        //        SourceCodeText.GetComponent<RectTransform>().sizeDelta = new Vector2(SourceCodeText.preferredWidth + 25, SourceCodeText.preferredHeight + 100);
        //    }
        //    catch (Exception e)
        //    {
        //        Debug.LogWarning(e.Data);
        //        Debug.LogWarning(e.Source);
        //        Debug.LogWarning(e.Message);
        //        SourceCodeText.text = "<color=#FF8D40><b>" + e.Message + "</b></color>\n\n" +
        //            "There was a problem with finding the code for this building.\n\n" +
        //            "The most likely explanation is that your Eclipse runtime does not contain\n" +
        //            "the source code of this system in form of Java projects - or maybe their\n" +
        //            "names do not match with the names specified in the  models loaded in VR.\n\n" +
        //            "If you want to make sure everything runs smoothly, rerun the automated\n" +
        //            "system analysis within Eclipse!";
        //        SourceCodeText.GetComponent<RectTransform>().sizeDelta = new Vector2(SourceCodeText.preferredWidth + 25, SourceCodeText.preferredHeight + 100);
        //        return;
        //    }
        //}



        //public override void NotifyCityChangeTriggered()
        //{
        //    //Debug.LogWarning("Safely deleting the canvas");

        //    //SceneHandler.INSTANCE.RemoveListenerSafely(this);
        //    //ISAUIInteractionSynchronizer.INSTANCE.RemoveListener(this);

        //    //SceneHandler.INSTANCE.RemoveListenerSafely(CanvasToElementConnectionManager);
        //    //PlayerLocationTracker.INSTANCE.RemoveListener(CanvasToElementConnectionManager);

        //    //Destroy(gameObject);
        //}

        ////public override void NotifyCityChangeComplete()
        ////{
        ////}

    }
}
