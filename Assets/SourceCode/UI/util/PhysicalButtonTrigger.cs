using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Button))]
public class PhysicalButtonTrigger : MonoBehaviour
{
    //public float startTime = 2f;
    //public float minTimeDelta = 0.2f;

    //private Button button;
    //private static float lastClickTime = 0;

    //private void Start()
    //{
    //    button = GetComponent<Button>();
    //    GetComponent<Collider>().isTrigger = true;
    //}

    // commented out: no haptic UI interaction for now...
    //void OnTriggerEnter(Collider other)
    //{
    //    if (button != null && other.gameObject.name.StartsWith("finger_index"))
    //    {
    //        if (Time.time > startTime && Time.time - lastClickTime > minTimeDelta)
    //        {
    //            button.onClick.Invoke();
    //            lastClickTime = Time.time;
    //        }
    //    }
    //}
}
