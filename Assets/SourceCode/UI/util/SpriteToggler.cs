using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SpriteToggler : MonoBehaviour
{

    public Color[] colors;
    public Sprite[] sprites;
    private int index = 0;



    public void ToggleSprite()
    {
        ChangeSprite(index + 1);
    }

    public void ChangeSprite(int newIndex)
    {
        index = newIndex;
        if (index >= sprites.Length)
            index = 0;

        Image img = GetComponent<Image>();
        img.sprite = sprites[index];
        
        if(colors.Length - 1 >= index)
            img.color = colors[index];
    }

}
