using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ActiveToggleable))]
public class ConfirmationCanvas : MonoBehaviour
{

    [SerializeField]
    private float buttonEnableDelay = 1f;
    [SerializeField]
    private float autoCloseDelay = 0f;

    [SerializeField]
    private Button[] buttonsToActivate;



    public void Show()
    {
        if (buttonsToActivate != null)
            foreach (Button b in buttonsToActivate)
                b.interactable = false;

        GetComponent<ActiveToggleable>().Show();

        StopAllCoroutines();
        StartCoroutine(EnableButtons());

        if(autoCloseDelay > 0f)
            StartCoroutine(AutoClose());
    }

    private IEnumerator EnableButtons()
    {
        yield return new WaitForSeconds(buttonEnableDelay);
        if (buttonsToActivate != null)
            foreach (Button b in buttonsToActivate)
                b.interactable = true;
    }

    private IEnumerator AutoClose()
    {
        yield return new WaitForSeconds(autoCloseDelay);
        GetComponent<ActiveToggleable>().Hide();
    }

}
