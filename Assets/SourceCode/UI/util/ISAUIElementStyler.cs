using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteAlways]
public class ISAUIElementStyler : MonoBehaviour
{

    public Color DefaultBackgroundColor = Color.HSVToRGB(205f / 360f, 20f / 100f, 75f / 100f);
    public Color DefaultForegroundColor = Color.HSVToRGB(0f, 0f, 1f);
    public float DefaultOpacity = 0.2f;

    public Color ActiveBackgroundColor = Color.HSVToRGB(0f, 0f, 1f);
    public Color ActiveForegroundColor = Color.HSVToRGB(205f / 360f, 45f / 100f, 45f / 100f);
    public float ActiveOpacity = 1f;

    public Graphic[] BackgroundElements;
    public Graphic[] ForegroundElements;



    public void SetDefaultColors()
    {
        if(BackgroundElements != null)
        {
            foreach (Graphic g in BackgroundElements)
            {
                g.color = new Color(DefaultBackgroundColor.r, DefaultBackgroundColor.g, DefaultBackgroundColor.b, DefaultBackgroundColor.a * DefaultOpacity);
            }
        }
        if (BackgroundElements != null)
        {
            foreach (Graphic g in ForegroundElements)
            {
                    g.color = new Color(DefaultForegroundColor.r, DefaultForegroundColor.g, DefaultForegroundColor.b, DefaultForegroundColor.a * DefaultOpacity);
            }
        }
    }

    public void SetActiveColors()
    {
        if (BackgroundElements != null)
        {
            foreach (Graphic g in BackgroundElements)
            {
                g.color = new Color(ActiveBackgroundColor.r, ActiveBackgroundColor.g, ActiveBackgroundColor.b, ActiveBackgroundColor.a * ActiveOpacity);
            }
        }
        if (BackgroundElements != null)
        {
            foreach (Graphic g in ForegroundElements)
            {
                g.color = new Color(ActiveForegroundColor.r, ActiveForegroundColor.g, ActiveForegroundColor.b, ActiveForegroundColor.a * ActiveOpacity);
            }
        }
    }
}
