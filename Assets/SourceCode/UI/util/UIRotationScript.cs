using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRotationScript : MonoBehaviour
{

    public float RotationSpeed = 50;

    public void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            Vector3 rotation = transform.localEulerAngles;
            rotation.z += Time.deltaTime * RotationSpeed;
            transform.localEulerAngles = rotation;
        }
    }

}
