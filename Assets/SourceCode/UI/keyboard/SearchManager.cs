using ISA.SolarOverview;
using ISA.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class SearchManager : MonoBehaviour//, KeyboardListener
{
    //public Button EntryTemplate;

    //public Sprite sunSprite;
    //public Sprite planetSprite;
    //public Sprite continentSprite;
    //public Sprite citySprite;
    //public Sprite buildingSprite;



    //void Start()
    //{
    //    EntryTemplate.gameObject.SetActive(false);
    //    gameObject.SetActive(false);

    //    KeyboardManager.INSTANCE.addListener(this);
    //    GetComponent<Canvas>().worldCamera = MultiViewSceneManager.INSTANCE.VRPointerCamera;
    //}



    //public void keyPressed()
    //{
    //    SearchAndDisplayResults(KeyboardManager.INSTANCE.getText());
    //}

    //public void cleared()
    //{
    //    gameObject.SetActive(false);
    //}

    //public void enterPressed(string searchTerm)
    //{
    //    SearchAndDisplayResults(searchTerm);
    //}



    //private void SearchAndDisplayResults(string searchTerm)
    //{
    //    for (int i = 1; i < EntryTemplate.transform.parent.childCount; i++)
    //    {
    //        GameObject.Destroy(EntryTemplate.transform.parent.GetChild(i).gameObject);
    //    }

    //    if (searchTerm.Length < 3)
    //    {
    //        gameObject.SetActive(false);
    //        return;
    //    }
    //    gameObject.SetActive(true);

    //    Dictionary<ISANamedVisualElement, ISANamedVisualElement> searchResultsToHoverableMap = new Dictionary<ISANamedVisualElement, ISANamedVisualElement>();
    //    performSearch(searchResultsToHoverableMap, searchTerm);

    //    int currentPosition = 0;
    //    foreach (ISANamedVisualElement result in searchResultsToHoverableMap.Keys)
    //    {
    //        Button newEntry = GameObject.Instantiate(EntryTemplate);
    //        newEntry.transform.SetParent(EntryTemplate.transform.parent);
    //        //newEntry.transform.parent = EntryTemplate.transform.parent;
    //        newEntry.transform.localPosition = Vector3.zero;
    //        newEntry.transform.localRotation = Quaternion.identity;
    //        newEntry.transform.localScale = Vector3.one;

    //        currentPosition -= 40;
    //        RectTransform rectTrans = newEntry.GetComponent<RectTransform>();
    //        rectTrans.anchoredPosition = new Vector2(0, currentPosition);
    //        rectTrans.sizeDelta = new Vector2(0, 60);

    //        newEntry.GetComponent<TMP_Text>().text = result.name;
    //        Image iconImage = newEntry.transform.GetChild(0).GetComponent<Image>();
    //        if (result is ISASolarSystem)
    //            iconImage.sprite = sunSprite;
    //        else if (result is ISAPlanet)
    //            iconImage.sprite = planetSprite;
    //        else if (result is ISAContinent)
    //            iconImage.sprite = continentSprite;
    //        else if (result is ISACity)
    //            iconImage.sprite = citySprite;
    //        else if (result is ISABuilding)
    //            iconImage.sprite = buildingSprite;
    //        else
    //            Debug.LogError("Unexpected type of element in tree view: " + result.qualifiedName);

    //        Button infoCanvasButton = newEntry.transform.GetChild(1).GetComponent<Button>();
    //        infoCanvasButton.onClick.AddListener(delegate { ISAUIInteractionSynchronizerOld.INSTANCE.NotifyToggleActive(searchResultsToHoverableMap[result]); });

    //        newEntry.gameObject.SetActive(true);

    //        ISAUIInteractable interactable = newEntry.gameObject.GetComponent<ISAUIInteractable>();
    //        interactable.onHoverStart.AddListener(delegate { ISAUIInteractionSynchronizerOld.INSTANCE.NotifyHoverStart(searchResultsToHoverableMap[result]); });
    //        interactable.onHoverEnd.AddListener(delegate { ISAUIInteractionSynchronizerOld.INSTANCE.NotifyHoverEnd(searchResultsToHoverableMap[result]); });

    //    }

    //    RectTransform parentRectTrabsform = EntryTemplate.transform.parent.GetComponent<RectTransform>();
    //    parentRectTrabsform.sizeDelta = new Vector2(parentRectTrabsform.sizeDelta.x, Mathf.Abs(currentPosition) + 40);
    //}



    //private void performSearch(Dictionary<ISANamedVisualElement, ISANamedVisualElement> searchResultsToHoverableMap, string searchTerm)
    //{
    //    foreach (Planet3D planet in SolarSystemManager.INSTANCE.SolarSystemGenerator.planetDataList)
    //    {
    //        if (PlanetConnectionCreator.INSTANCE.namesMatch(planet.planetEcoreModel, searchTerm, AbstractConnectionCreator.SearchQueryType.CONTAINS_IGNORECASE))
    //        {
    //            //ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverStart
    //            searchResultsToHoverableMap.Add(planet.planetEcoreModel, planet.planetEcoreModel);
    //        }

    //        foreach (Continent3D continent in planet.rootContinents)
    //        {
    //            searchInContinents(searchResultsToHoverableMap, continent, searchTerm);
    //        }
    //    }
    //}

    //private void searchInContinents(Dictionary<ISANamedVisualElement, ISANamedVisualElement> searchResultsToHoverableMap, Continent3D continent, string searchTerm)
    //{
    //    if(continent is CityContinent3D)
    //    {
    //        CityContinent3D cityContinent = (CityContinent3D)continent;

    //        if (CityConnectionCreator.INSTANCE.namesMatch(cityContinent.cityEcoreModel, searchTerm, AbstractConnectionCreator.SearchQueryType.CONTAINS_IGNORECASE))
    //        {
    //            searchResultsToHoverableMap.Add(cityContinent.cityEcoreModel, cityContinent.cityEcoreModel);
    //        }

    //        foreach (ISABuilding building in cityContinent.buildingMap.Keys)
    //        {
    //            if (BuildingConnectionCreator.INSTANCE.namesMatch(building, searchTerm, AbstractConnectionCreator.SearchQueryType.CONTAINS_IGNORECASE))
    //            {
    //                // This returns the containing city as hovered object
    //                //searchResultsToHoverableMap.Add(building, cityContinent.cityEcoreModel);

    //                // This returns the building as hovered object
    //                searchResultsToHoverableMap.Add(building, building);
    //            }
    //        }
    //    }
    //    else if (continent is CompoundContinent3D)
    //    {
    //        foreach (Continent3D subContinent in ((CompoundContinent3D)continent).children)
    //        {
    //            searchInContinents(searchResultsToHoverableMap, subContinent, searchTerm);
    //        }
    //    }
    //}
}
