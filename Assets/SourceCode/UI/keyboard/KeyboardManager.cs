using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class KeyboardManager : MonoBehaviour
{

    public Transform KeyParent;
    private ISAInputField fieldToEdit;



    void Start()
    {
        foreach (Transform keyObject in KeyParent)
        {
            Button keyButton = keyObject.GetComponent<Button>();
            keyButton.onClick.AddListener(delegate { keyPressed(keyObject.name); });
        }

        //GetComponent<Canvas>().worldCamera = SceneManager.INSTANCE.GetActivePointer().eventCamera;
        SceneManager.INSTANCE.RegisterCanvas(GetComponent<Canvas>());
    }

    public void Init(ISAInputField fieldToEdit)
    {
        this.fieldToEdit = fieldToEdit;
    }



    private void keyPressed(string key)
    {
        if (key.Equals("Shift"))
        {
        }
        else if (key.Equals("Enter"))
        {
            fieldToEdit.EnterPressed();
        }
        else if (key.Equals("Remove"))
        {
            if (GetText().Length == 0)
                return;

            SetText(GetText().Substring(0, GetText().Length - 1));
        }
        else if (key.Equals("Clear"))
        {
            SetText("");
        }
        else if (key.Equals("Space"))
        {
            SetText(GetText() + " ");
        }
        else
        {
            SetText(GetText() + key);
        }
    }

    private void SetText(string newText)
    {
        fieldToEdit.SetText(newText);
    }

    private string GetText()
    {
        return fieldToEdit.GetText();
    }
}