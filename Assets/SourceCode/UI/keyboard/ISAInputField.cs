using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.UI;

public class ISAInputField : MonoBehaviour
{
    public TMP_Text textToEdit;

    public Transform hookForKeyboard;

    [SerializeField]
    private UnityEvent onEnterPressed;
    [SerializeField]
    private UnityEvent<string> onTextChanged;

    private KeyboardManager keyboard = null;

    public void OpenKeyboard()
    {
        if (keyboard != null)
            return;

        GameObject keyboardObject = Instantiate(Resources.Load("UI/3D/Keyboard") as GameObject);
        keyboardObject.transform.parent = hookForKeyboard;
        keyboardObject.transform.localScale = Vector3.one;
        keyboardObject.transform.localPosition = Vector3.zero;
        keyboardObject.transform.localRotation = Quaternion.identity;

        keyboard = keyboardObject.GetComponent<KeyboardManager>();
        keyboard.Init(this);
    }

    public void CloseKeyboard()
    {
        GameObject.Destroy(keyboard.gameObject);
        keyboard = null;
    }

    public void SetText(string newText)
    {
        textToEdit.text = newText;
        onTextChanged.Invoke(newText);
    }

    public string GetText()
    {
        return textToEdit.text;
    }

    public void EnterPressed()
    {
        onEnterPressed.Invoke();
    }

}
