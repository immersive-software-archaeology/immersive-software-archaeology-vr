using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class UIDisposalBinManager : MonoBehaviour
{

    public static UIDisposalBinManager INSTANCE;

    public Transform ArrowTransform;

    private List<UIDisposableElement> activatingElements;



    void Start()
    {
        INSTANCE = this;
        gameObject.SetActive(false);
        activatingElements = new List<UIDisposableElement>();
    }

    void Update()
    {
        if (!gameObject.activeInHierarchy)
            return;

        Vector3 forwardOnXZ = new Vector3(Player.instance.headCollider.transform.forward.x, 0, Player.instance.headCollider.transform.forward.z).normalized;
        transform.position = Player.instance.headCollider.transform.position + Vector3.down * 0.5f + forwardOnXZ * 0.1f;
        //transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);

        ArrowTransform.localPosition = new Vector3(0, Mathf.Sin(Time.time), 0);
    }


    public void Show(UIDisposableElement element)
    {
        activatingElements.Add(element);

        gameObject.SetActive(true);
        Update();
    }

    public void Hide(UIDisposableElement element)
    {
        activatingElements.Remove(element);

        if(activatingElements.Count == 0)
            gameObject.SetActive(false);
    }



    public Vector3 GetCenter()
    {
        return transform.position + Vector3.down * GetRadius() * 0.5f;
    }
    public float GetRadius()
    {
        return transform.lossyScale.x;
    }
}
