using ISA.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public class UIDisposableElement : MonoBehaviour
{

    public UnityEvent OnDispose;
    public Graphic[] GraphicsToHueOnHighlight;
    public BeveledUIMesh[] BeveledMeshesToHueOnHighlight;
    public float radius = 0.2f;

    private float originalLocalScale;

    private bool currentlyHighlighted = false;
    private bool currentlyInDisposalBin = false;
    private Dictionary<object, Color> originalColorCache = new Dictionary<object, Color>();



    void Start()
    {
    }

    private void OnDestroy()
    {
        if(UIDisposalBinManager.INSTANCE != null)
            UIDisposalBinManager.INSTANCE.Hide(this);
    }



    private void OnAttachedToHand(Hand hand)
    {
        originalLocalScale = transform.localScale.x;
        UIDisposalBinManager.INSTANCE.Show(this);
    }

    private void OnDetachedFromHand(Hand hand)
    {
        transform.localScale = Vector3.one * originalLocalScale;
        if (currentlyInDisposalBin)
        {
            restoreColor();
            StartCoroutine(Dispose());
        }
        UIDisposalBinManager.INSTANCE.Hide(this);
    }

    private IEnumerator Dispose()
    {
        yield return new WaitForSeconds(0.1f);
        OnDispose.Invoke();
    }



    private void HandAttachedUpdate(Hand hand)
    {
        Vector3 elementWorldPosition = hand.transform.position;
        float distance = Vector3.Distance(elementWorldPosition, UIDisposalBinManager.INSTANCE.GetCenter());
        currentlyInDisposalBin = distance < radius + UIDisposalBinManager.INSTANCE.GetRadius();

        // For some reason, relative scaling does not work in combination with precision grabbing
        // Because the scaling feels weird when not done relative to the grabbing hand, disable it...
        
        //float scale = 1;
        //if(currentlyInDisposalBin)
        //{
        //    scale = distance / (radius + UIDisposalBinManager.INSTANCE.GetRadius());
        //    scale = Mathf.Max(0.25f, Mathf.Pow(scale, 4));
        //}
        //scaleRelatively(originalLocalScale * scale, elementWorldPosition);

        if (currentlyInDisposalBin)
        {
            if(!currentlyHighlighted)
                colorRed();
            currentlyHighlighted = true;
        }
        else
        {
            if(currentlyHighlighted)
                restoreColor();
            currentlyHighlighted = false;
        }
    }

    private void scaleRelatively(float scale, Vector3 pivotPoint)
    {
        Vector3 pivotToAnchor = transform.position - pivotPoint;

        float relativeScaleFactor = scale / transform.localScale.x;
        Vector3 finalPosition = pivotPoint + pivotToAnchor * relativeScaleFactor;

        transform.position = finalPosition;
        transform.localScale = scale * Vector3.one;
    }

    private void colorRed()
    {
        foreach (Graphic graphic in GraphicsToHueOnHighlight)
        {
            // save original hue
            originalColorCache.Remove(graphic);
            originalColorCache.Add(graphic, graphic.color);

            // tint to red hue
            Color.RGBToHSV(graphic.color, out _, out float S, out float V);
            graphic.color = Color.HSVToRGB(5f / 360f, 0.5f + S * 0.5f, V);
        }
        foreach (BeveledUIMesh mesh in BeveledMeshesToHueOnHighlight)
        {
            // save original hue
            Renderer renderer = mesh.transform.GetChild(0).GetComponent<Renderer>();
            originalColorCache.Remove(renderer);
            originalColorCache.Add(renderer, renderer.material.color);

            // tint to red hue
            Color.RGBToHSV(renderer.material.color, out _, out float S, out float V);
            renderer.material.color = Color.HSVToRGB(5f / 360f, 0.5f + S * 0.5f, V);
        }
    }

    private void restoreColor()
    {
        foreach (Graphic graphic in GraphicsToHueOnHighlight)
        {
            graphic.color = originalColorCache[graphic];
        }
        foreach (BeveledUIMesh mesh in BeveledMeshesToHueOnHighlight)
        {
            Renderer renderer = mesh.transform.GetChild(0).GetComponent<Renderer>();
            renderer.material.color = originalColorCache[renderer];
        }
    }
    
}
