﻿using System;
using System.Collections;
using UnityEngine;
using ISA.Modelling;
using TMPro;
using ISA.UI;
using Valve.VR.InteractionSystem;
using ISA.util;
using UnityEngine.UI;

public class SystemPreviewManager : MonoBehaviour
{
    public int index { get; private set; }

    public Transform scalableCapsule;
    public NamePlateManager namePlate;

    public GameObject clientPreviewDummy;

    public TMP_Text clientInfoText;
    public Color emptyColor = Color.gray;
    public Color busyColor = new Color(255f / 255f, 144f / 255f, 64f / 255f);

    private ModelStorageMessageEntry entry;
    private Action<ModelStorageMessageEntry> callBackOnSystemLoad;

    private bool currentlyPickedUp;
    private Vector3 initialLocalPosition;
    private Quaternion initialLocalRotataion;

    private int numberOfConnectedClients;



    public void Initialize(int index, Action<ModelStorageMessageEntry> callBackSystemLoad)
    {
        this.index = index + 1;
        entry = ModelLoader.instance.Entries[index];
        callBackOnSystemLoad = callBackSystemLoad;

        namePlate.SetName(entry.systemName);

        float maxRadius = 0.09f / 2f;
        float maxVolume = (4f / 3f) * Mathf.PI * Mathf.Pow(maxRadius, 3);

        float relativeVolume = maxVolume * ModelStatistics.GetRelativeSizeInContextOfAllAvailableModels(entry);
        float relativeRadius = Mathf.Pow((3f / 4f) * relativeVolume / Mathf.PI, 1f / 3f);

        scalableCapsule.localScale = relativeRadius * 2f * Vector3.one;
        scalableCapsule.GetComponent<MeshRenderer>().material = Instantiate(Resources.Load("Models/Visualization/Materials/Capsule Preview Material") as Material);
        scalableCapsule.GetComponent<MeshRenderer>().material.SetFloat("_Pattern_Tiling", scalableCapsule.localScale.x * 200f);

        initialLocalPosition = transform.localPosition;
        initialLocalRotataion = transform.localRotation;

        clientPreviewDummy.SetActive(false);
        numberOfConnectedClients = -1;
        SetNumberOfConnectedClients(0, 0);
    }

    public void Load()
    {
        callBackOnSystemLoad(entry);
    }

    public IEnumerator Destroy()
    {
        Hand h = GetComponent<Interactable>().attachedToHand;
        if (h != null)
            h.DetachObject(gameObject, true);

        Rigidbody rb = GetComponent<Rigidbody>();
        for(int i=0; i<10; i++)
        {
            // Seems too weak
            rb.AddForce(Vector3.up * 1, ForceMode.Acceleration);
            yield return new WaitForSeconds(0.2f);
        }

        Destroy(gameObject, 3);
    }



    public string GetSystemName()
    {
        return entry.systemName;
    }

    public void SetNumberOfConnectedClients(int newNumber, int maxNumber)
    {
        if (newNumber == numberOfConnectedClients)
            return;
        numberOfConnectedClients = newNumber;

        foreach (Transform t in clientPreviewDummy.transform.parent)
        {
            if (t.Equals(clientPreviewDummy.transform))
                continue;
            Destroy(t.gameObject);
        }

        clientInfoText.text = newNumber + "<color=#ffffff44>/" + maxNumber + "</color>";
        foreach (Graphic graphic in clientInfoText.transform.parent.GetComponentsInChildren<Graphic>())
            graphic.color = (newNumber > 0) ? busyColor : emptyColor;

        for (int i=0; i< numberOfConnectedClients; i++)
        {
            GameObject previewGameObject = Instantiate(clientPreviewDummy);
            Vector2 positionInCircle = 0.03f * LayoutUtil.CalculateVertexPositionInUnitCircle(360f * i / newNumber);
            previewGameObject.transform.parent = clientPreviewDummy.transform.parent;
            previewGameObject.transform.localPosition = new Vector3(positionInCircle.x, 0f, positionInCircle.y);
            previewGameObject.transform.LookAt(clientPreviewDummy.transform.parent);
            previewGameObject.SetActive(true);
        }
    }

    private void Update()
    {
        if (numberOfConnectedClients > 0)
            clientPreviewDummy.transform.parent.localRotation = Quaternion.Euler(0, (Time.time * 10f) % 360f, 0);
    }



    public void WasPickedUp()
    {
        currentlyPickedUp = true;
    }

    public void WasLetGo()
    {
        currentlyPickedUp = false;
    }

    public void ResetPosition()
    {
        StartCoroutine(ResetOncePossible());
    }

    private IEnumerator ResetOncePossible()
    {
        while (currentlyPickedUp)
            yield return null;

        if (TryGetComponent(out Rigidbody rb))
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }

        transform.localPosition = initialLocalPosition;
        transform.localRotation = initialLocalRotataion;
    }
}
