﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.Modelling;
using TMPro;
using UnityEngine.UI;
using System.Threading;
using System.Threading.Tasks;
using System;
using ISA.util;
using Valve.VR.InteractionSystem;

namespace ISA.UI
{

    public class TableManager : MonoBehaviour, SceneListener, FallbackCameraGUIExtension
    {

        public static TableManager INSTANCE;



        public List<ActiveToggleable> ShowOnLoadingError;
        public List<ActiveToggleable> HideOnConnectedToServer;

        public TMP_Text errorText;

        public float previewSpawnCircleRadius = 0.25f;
        public Transform systemPreviewAnchorTransform;
        private List<SystemPreviewManager> previewManagers;

        public new ParticleSystem particleSystem;
        private ParticleSystem.EmissionModule emission;
        private float defaultEmissionRate;
        private float currentEmissionRate;

        public Slider GenerationProgressSlider;
        public TMP_Text GenerationProgressStateText;

        public Image loadIcon;
        public float loadAnimationRotationSpeed = 200;
        private Coroutine spinLoadIconRoutine;

        private CancellationToken token;

        private bool loadTriggered = false;
        private Coroutine generationRoutine;

        private int currentGenerationWork;
        private int totalGenerationWork;



        private void Start()
        {
            INSTANCE = this;
            SceneManager.INSTANCE.AddListener(this);
            ISAFallbackCameraController.RegisterListener(this);

            emission = particleSystem.emission;
            defaultEmissionRate = emission.rateOverTime.constant;
            currentEmissionRate = defaultEmissionRate;

            token = ThreadingUtility.QuitToken;
            previewManagers = new List<SystemPreviewManager>();

            Reset(true);
        }

        public void Reset(bool initialCall = false)
        {
            emission.enabled = false;

            if (generationRoutine != null)
                StopCoroutine(generationRoutine);

            spinLoadIconRoutine = StartCoroutine(SpawnAndSpinLoadingIcon());
            if (!initialCall)
            {
                ModelLoader.instance.SetEntries(null);
                ModelLoader.instance.SetSelectedEntry(ModelLoader.instance.SelectedEntry);
                SceneManager.INSTANCE.DestroySceneElements();
            }

            foreach (ActiveToggleable toHide in HideOnConnectedToServer)
                toHide.Show();

            DestroyPreviews();

            errorText.gameObject.SetActive(false);
            GenerationProgressSlider.gameObject.SetActive(false);
            GenerationProgressStateText.gameObject.SetActive(false);

            _ = HTTPRequestHandler.INSTANCE.SendRequest("models", null, SetUpUI, SetUpModelLoadErrorMessage);
        }

        public async void SetUpModelLoadErrorMessage(string response)
        {
            foreach (ActiveToggleable toShow in ShowOnLoadingError)
                toShow.Show();

            try
            {
                token.ThrowIfCancellationRequested();
            }
            catch (OperationCanceledException)
            {
                return;
            }

            await Task.Delay(500);
            _ = HTTPRequestHandler.INSTANCE.SendRequest("models", null, SetUpUI, SetUpModelLoadErrorMessage);
        }

        private void SetUpUI(string response)
        {
            ModelLoader.instance.SetEntries(response);

            StopCoroutine(spinLoadIconRoutine);
            loadIcon.gameObject.SetActive(false);

            foreach (ActiveToggleable toHide in HideOnConnectedToServer)
                toHide.Hide();

#if (UNITY_EDITOR)
            if(SceneManager.INSTANCE.quickLoadSystem)
                if (SceneManager.INSTANCE.quickLoadSystemName != null && SceneManager.INSTANCE.quickLoadSystemName.Length > 0)
                    foreach (ModelStorageMessageEntry entry in ModelLoader.instance.Entries)
                        if(entry.systemName.Equals(SceneManager.INSTANCE.quickLoadSystemName))
                            LoadSystem(entry);
#endif

            StartCoroutine(SpawnSystemPreviewsInCircleOnTable());

            AnimateParticleBurst(0.5f);
        }

        private IEnumerator SpawnSystemPreviewsInCircleOnTable()
        {
            for (int i = 0; i < ModelLoader.instance.Entries.Length; i++)
            {
                GameObject previewGameObject = Instantiate(Resources.Load("Models/Table/System Preview Prefab") as GameObject);
                previewGameObject.name = ModelLoader.instance.Entries[i].systemName;
                previewGameObject.transform.parent = systemPreviewAnchorTransform.transform;
                previewGameObject.transform.localScale = Vector3.one;

                // old layouting in strechted hald-circle
                {
                    //float width = 0.5f;
                    //float depth = 0.1f;

                    ////float x = -width + 2 * width * ((float)i / Mathf.Max(1f, (float)ModelLoader.instance.Entries.Length - 1));
                    //float x;
                    //if (ModelLoader.instance.Entries.Length == 1)
                    //    x = 0;
                    //else
                    //    x = -width + 2 * width * ((float)i / (ModelLoader.instance.Entries.Length - 1));

                    //float y = Mathf.Pow(x / width, 2) * depth;
                    //previewGameObject.transform.localPosition = new Vector3(x, 0.02f, y);
                }
                Vector2 positionInCircle = previewSpawnCircleRadius * LayoutUtil.CalculateVertexPositionInUnitCircle(360f * i / Mathf.Max(8, ModelLoader.instance.Entries.Length));
                previewGameObject.transform.localPosition = new Vector3(positionInCircle.x, 0f, positionInCircle.y);

                SystemPreviewManager previewManager = previewGameObject.GetComponent<SystemPreviewManager>();
                previewManager.Initialize(i, LoadSystem);

                previewManagers.Add(previewManager);
                yield return null;
            }

            fetchStatusUpdates = true;

            int index = 0;
            while (true)
            {
                yield return new WaitForSeconds(1f / previewManagers.Count);
                if (index >= previewManagers.Count)
                {
                    index = 0;
                    Vector3 rotAngles = transform.rotation.eulerAngles;
                    if (rotAngles.x > float.Epsilon || rotAngles.z > float.Epsilon)
                        GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(new Vector3(0, rotAngles.y, 0)));
                    continue;
                }

                SystemPreviewManager previewManager = previewManagers[index++];
                if (previewManager.transform.position.y < 0.2f)
                    previewManager.ResetPosition();
            }
        }

        public SystemPreviewManager GetPreviewManager(int index)
        {
            return previewManagers[index-1];
        }

        private void DestroyPreviews()
        {
            foreach (SystemPreviewManager preview in previewManagers)
            {
                foreach (Hand hand in Player.instance.hands)
                    hand.DetachObject(preview.gameObject, false);
                Destroy(preview.gameObject);
            }
            previewManagers.Clear();
        }



        private bool fetchStatusUpdates = false;
        private float lastUpdate = 0;
        void Update()
        {
            if (!fetchStatusUpdates || Time.time - lastUpdate < 1)
                return;
            lastUpdate = Time.time;
            fetchStatusUpdates = false;

            _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/status", null,
            delegate (string response)
            {
                ServerStatusMessage message = JsonUtility.FromJson<ServerStatusMessage>(response);
                if (message.entries.Length != previewManagers.Count)
                {
                    // A system was added
                    DestroyPreviews();
                    fetchStatusUpdates = false;
                    _ = HTTPRequestHandler.INSTANCE.SendRequest("models", null, SetUpUI, SetUpModelLoadErrorMessage);
                    return;
                }

                foreach (SystemPreviewManager preview in previewManagers)
                {
                    foreach (ServerStatusMessageEntry entry in message.entries)
                    {
                        if (preview.GetSystemName().Equals(entry.systemName))
                        {
                            preview.SetNumberOfConnectedClients(entry.connectedClients.Length, message.maxClients);
                            break;
                        }
                    }
                }

                foreach (ServerStatusMessageEntry entry in message.entries)
                {
                    if (ModelLoader.instance.VisualizationModel != null && ModelLoader.instance.VisualizationModel.name.Equals(entry.systemName))
                    {
                        foreach (ServerStatusMessageEntryClientInfo client in entry.connectedClients)
                        {
                            Collaborator collaborator = SceneManager.INSTANCE.udpClient.GetCollaborator(client.id);
                            if (collaborator != null)
                                // Can be null in case we are not connected yet
                                collaborator.UpdateColor(client.color);
                        }
                    }
                }

                fetchStatusUpdates = true;
            },
            delegate (string response)
            {
                Debug.LogError("Problem with fetching server status: " + response);
                fetchStatusUpdates = true;
                //fetchStatusUpdates = false;
                //Reset();
            });
        }



        private IEnumerator SpawnAndSpinLoadingIcon()
        {
            loadIcon.gameObject.SetActive(true);
            while (true)
            {
                Vector3 loadIconRotation = loadIcon.transform.localEulerAngles;
                loadIconRotation.z += Time.deltaTime * loadAnimationRotationSpeed;
                loadIcon.transform.localEulerAngles = loadIconRotation;
                yield return null;
            }
        }



        public void LoadSystem(ModelStorageMessageEntry selectedEntry)
        {
            if (loadTriggered)
                return;
            loadTriggered = true;

            SceneManager.INSTANCE.quickLoadSystem = false;

            SceneManager.INSTANCE.DestroySceneElements();
            SceneManager.INSTANCE.udpClient.Disconnect();

            currentEmissionRate = 5f * defaultEmissionRate;
            emission.rateOverTime = currentEmissionRate;

            StartNewTask("Loading Model from Eclipse...", 1);

            ModelLoader.instance.SetSelectedEntry(selectedEntry);
            _ = HTTPRequestHandler.INSTANCE.SendRequest("models?systemName=" + selectedEntry.systemName, null, callBackFromLoadingXML, null);
        }

        private void callBackFromLoadingXML(string xmlModelCode)
        {
            ModelLoader.instance.ParseVisualizationModel(xmlModelCode);

            if (ModelLoader.instance.VisualizationModel == null || ModelLoader.instance.VisualizationModel.subjectSystemCapsule == null)
            {
                errorText.gameObject.SetActive(true);
                errorText.text += "\n\nCould not load visualization model - is the model empty or corrupted?";
                Debug.LogWarning(errorText.text);
            }
            else
            {
                StartNewTask("Generating Geometry...", (int)ModelLoader.instance.SelectedEntry.numberOfClassifiers);

                SystemGenerator generator = new SystemGenerator();
                if (generationRoutine != null)
                    StopCoroutine(generationRoutine);
                generationRoutine = StartCoroutine(generator.GenerateSystem(ModelLoader.instance.VisualizationModel));
            }
        }



        public void StartNewTask(string description, int totalWork)
        {
            GenerationProgressSlider.gameObject.SetActive(true);
            GenerationProgressStateText.gameObject.SetActive(true);
            GenerationProgressStateText.text = description;
            currentGenerationWork = 0;
            totalGenerationWork = totalWork;
            IncreaseGenerationCount(0);
        }

        public void IncreaseGenerationCount(int increment)
        {
            currentGenerationWork += increment;
            GenerationProgressSlider.value = Mathf.Max(0, Mathf.Min(1, (float)currentGenerationWork / (float)totalGenerationWork));
            GenerationProgressSlider.transform.rotation = Quaternion.LookRotation(systemPreviewAnchorTransform.position - Player.instance.headCollider.transform.position);
        }

        public void OnSceneFinishedLoading()
        {
            loadTriggered = false;

            GenerationProgressSlider.gameObject.SetActive(false);
            GenerationProgressStateText.gameObject.SetActive(false);

            VisualizationSynchronizer.instance.Reset();
            WhiteboardSynchronizer.StartWhiteboardUpdates();

            currentEmissionRate = defaultEmissionRate;
            emission.rateOverTime = currentEmissionRate;
        }



        private void AnimateParticleBurst(float duration)
        {
            if (particleBurstAnimation != null)
                StopCoroutine(particleBurstAnimation);
            particleBurstAnimation = ParticleBurstAnimation(duration);
            StartCoroutine(particleBurstAnimation);
        }

        IEnumerator particleBurstAnimation;
        private IEnumerator ParticleBurstAnimation(float duration)
        {
            emission.enabled = true;
            emission.rateOverTime = 20f * defaultEmissionRate;
            yield return new WaitForSeconds(duration);
            emission.rateOverTime = currentEmissionRate;
        }



        public void DrawGUI()
        {
            if (ModelLoader.instance == null || ModelLoader.instance.Entries == null)
                return;

            for (int i = 0; i < ModelLoader.instance.Entries.Length; i++)
            {
                ModelStorageMessageEntry entry = ModelLoader.instance.Entries[i];
                if(GUI.Button(new Rect(Screen.width - 10 - 200, Screen.height - 10 - 30 * (i+1), 200, 25), "Load \"" + entry.systemName + "\""))
                    LoadSystem(entry);
            }
        }
    }

}

[System.Serializable]
public class ServerStatusMessage
{
    public int maxClients;
    public ServerStatusMessageEntry[] entries;
}

[System.Serializable]
public class ServerStatusMessageEntry
{
    public string systemName;
    public ServerStatusMessageEntryClientInfo[] connectedClients;
}

[System.Serializable]
public class ServerStatusMessageEntryClientInfo
{
    public byte id;
    public byte color;
}