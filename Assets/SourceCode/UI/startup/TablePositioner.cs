using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using ISA.util;
using System;

public class TablePositioner : MonoBehaviour
{

    public bool adjustHeight = false;
    public bool adjustRotation = false;
    public bool fixRotationGlitches = true;

    public float speed = 100f;
    public float heightOffset = -1.5f;
    public float minY = 1f;

    private float lastDistanceToTake = 0;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if(adjustHeight)
        {
            Vector3 target = Player.instance.headCollider.transform.position;
            float heightDiff = Mathf.Max(minY, target.y + heightOffset) - transform.position.y;

            float distanceToTake = heightDiff * Mathf.Min(Time.deltaTime, 1) * Mathf.Max(speed / 100f, lastDistanceToTake * speed / 100f);
            lastDistanceToTake = distanceToTake;

            transform.position = new Vector3(transform.position.x, transform.position.y + distanceToTake, transform.position.z);
            //rb.MovePosition(new Vector3(transform.position.x, transform.position.y + distanceToTake, transform.position.z));
        }
        if (adjustRotation)
        {
            Vector3 flatHeadPosition = Player.instance.headCollider.transform.position;
            flatHeadPosition.y = 0;

            Vector3 flatTablePosition = transform.position;
            flatTablePosition.y = 0;

            Quaternion target = Quaternion.LookRotation((flatHeadPosition - flatTablePosition).normalized, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Mathf.Min(Time.deltaTime, 1) / 10f);
        }
        if (fixRotationGlitches)
        {
            rb.MoveRotation(Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0));
        }
    }

}
