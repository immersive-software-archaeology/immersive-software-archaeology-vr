﻿using ISA.util;
using System;
using System.Collections;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using ISA.Modelling.Multiplayer;
using System.Collections.Generic;
using ISA.Modelling.Whiteboard;

public class HandheldCamera : PermanentAttachable
{

    public MeshRenderer screenRenderer;
    public Camera pictureCamera;
    public Transform pictureDummy;
    public Transform pictureStack;

    [SerializeField]
    private Vector2 photoResolution;
    private RenderTexture lowResRenderTexture;

    [SerializeField]
    private SteamVR_Action_Boolean actionForTakingPhoto;

    [SerializeField]
    private SteamVR_Action_Boolean zoomInAction;
    [SerializeField]
    private SteamVR_Action_Boolean zoomOutAction;

    [SerializeField]
    private float minZoom = 20;
    [SerializeField]
    private float maxZoom = 120;



    protected override void Init()
    {
        pictureCamera.enabled = false;
        pictureCamera.forceIntoRenderTexture = true;

        lowResRenderTexture = pictureCamera.targetTexture;

        pictureDummy.gameObject.SetActive(false);
    }

    protected override void OnAttach()
    {
        pictureCamera.enabled = true;
    }

    protected override void OnDetach()
    {
        pictureCamera.enabled = false;
        screenRenderer.material.SetTexture("_EmissionMap", CreateBlackTexture());
    }



    //int lastScreenUpdate;
    void Update()
    {
        if (!attached)
            return;

        try
        {
            if (actionForTakingPhoto.GetStateDown(hand.handType) && Time.time - attachTime > 0.5f)
                TakePhoto();

            if (zoomInAction.GetState(hand.handType))
                pictureCamera.fieldOfView = Mathf.Min(maxZoom, pictureCamera.fieldOfView + Time.deltaTime * 100f);
            if (zoomOutAction.GetState(hand.handType))
                pictureCamera.fieldOfView = Mathf.Max(minZoom, pictureCamera.fieldOfView - Time.deltaTime * 100f);
        }
        catch (Exception e) {
            Debug.LogError(e);
        }

        //int thisScreenUpdate = Mathf.FloorToInt(Time.time * 10);
        //if (thisScreenUpdate > lastScreenUpdate)
        {
            //lastScreenUpdate = thisScreenUpdate;

            //RenderTexture renderTexture = CreateRenderTexture(1024 / liveResolutionDivider, (int)(0.875f * 1024 / liveResolutionDivider), FilterMode.Point);
            //pictureCamera.targetTexture = renderTexture;
            //pictureCamera.Render();

            screenRenderer.material.SetTexture("_EmissionMap", ConvertToTexture2D(pictureCamera.targetTexture, FilterMode.Point, true));

            //renderTexture.Release();
            //Destroy(renderTexture);
        }

        DetachIfShakingHand();
    }



    public void TakePhoto()
    {
        foreach (Transform p in pictureStack)
        {
            if (!p.TryGetComponent(out HandheldCameraPhoto pic))
                continue;
            pic.DissolveAndDestroy(true);

            //CameraPhotoDestroyedEvent removePhotoEvent = new CameraPhotoDestroyedEvent();
            //removePhotoEvent.photoId = pic.id;
            //HTTPRequestHandler.INSTANCE.SubmitEvent(removePhotoEvent, null, delegate (string msg) { Debug.LogError(msg); });
        }

        RenderTexture tempUpscaledRenderTexture = CreateRenderTexture((int)photoResolution.x, (int)photoResolution.x, FilterMode.Bilinear);
        pictureCamera.targetTexture = tempUpscaledRenderTexture;
        pictureCamera.Render();

        GetComponent<AudioClipHelper>().Play("photo");

        Texture2D texture = ConvertToTexture2D(tempUpscaledRenderTexture, FilterMode.Bilinear, false);
        SpawnPhoto(texture);

        pictureCamera.targetTexture = lowResRenderTexture;

        tempUpscaledRenderTexture.Release();
        Destroy(tempUpscaledRenderTexture);
    }

    public void SpawnPhoto(Texture2D texture)
    {
        GameObject photoObject = Instantiate(pictureDummy.gameObject);
        
        HandheldCameraPhoto photo = photoObject.GetComponent<HandheldCameraPhoto>();
        photo.Initialize(texture);

        List<VisibleRelation> visibleRelations = new List<VisibleRelation>();
        VisualizationSynchronizer.instance.ExecuteOnCurrentlyShownRelations(
            delegate (AbstractVisualElement3D element, RelationType relationType, bool forward)
            {
                VisibleRelation r = new VisibleRelation();
                r.elementName = element.qualifiedName;
                r.relationType = (int)relationType;
                r.forward = forward;
                visibleRelations.Add(r);
            });

        List<string> openCapsuleElementNames = new List<string>();
        foreach (TransparentCapsule3D capsule in SceneManager.INSTANCE.RootTransform.GetComponentsInChildren<TransparentCapsule3D>())
            if (capsule.isOpened)
                openCapsuleElementNames.Add(capsule.qualifiedName);

        photo.SetRestoreSystemData(transform.position, transform.rotation.eulerAngles, SceneManager.INSTANCE.RootTransform.position, SceneManager.INSTANCE.RootTransform.localScale.x, visibleRelations.ToArray(), openCapsuleElementNames.ToArray());

        photoObject.transform.parent = pictureStack;
        photoObject.transform.position = pictureDummy.position;
        photoObject.transform.rotation = pictureDummy.rotation;
        photoObject.transform.localScale = pictureDummy.localScale;

        GetComponent<AudioClipHelper>().Play("print", 0.2f);
    }



    private RenderTexture CreateRenderTexture(int width, int height, FilterMode mode)
    {
        RenderTexture renderTexture = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32);
        renderTexture.name = "Handheld Camera Render Texture";
        renderTexture.anisoLevel = 0;
        renderTexture.filterMode = mode;
        renderTexture.useMipMap = false;
        renderTexture.autoGenerateMips = false;
        //renderTexture.enableRandomWrite = true;
        renderTexture.Create();
        return renderTexture;
    }

    private Texture2D ConvertToTexture2D(RenderTexture renderTexture, FilterMode filterMode, bool showInfo)
    {
        // ReadPixels looks at the active RenderTexture.
        RenderTexture previouslyActiveTexture = RenderTexture.active;
        RenderTexture.active = renderTexture;

        int width = renderTexture.width;
        int height = renderTexture.height;

        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        if(showInfo)
        {
            for (int x = 0; x < width; x++)
            {
                int y = height / 3;
                tex.SetPixel(x, 1 * y, Color.Lerp(tex.GetPixel(x, 1 * y), Color.white, 0.25f));
                tex.SetPixel(x, 2 * y, Color.Lerp(tex.GetPixel(x, 2 * y), Color.white, 0.25f));
            }
            for (int y = 0; y < height; y++)
            {
                if (y == height / 3 || y == 2 * (height / 3))
                    continue;

                int x = width / 3;
                tex.SetPixel(1 * x, y, Color.Lerp(tex.GetPixel(1 * x, y), Color.white, 0.25f));
                tex.SetPixel(2 * x, y, Color.Lerp(tex.GetPixel(2 * x, y), Color.white, 0.25f));
            }

            int zoomSliderMarginX = 3;
            int zoomSliderMarginY = 5;
            int zoomSliderHeight = 20;
            int zoomSliderWidth = 3;
            for (int y = height - (zoomSliderMarginY + 1); y > height - (zoomSliderMarginY + zoomSliderHeight); y--)
            {
                int x = zoomSliderMarginX + zoomSliderWidth/2;
                tex.SetPixel(x, y, Color.Lerp(tex.GetPixel(x, y), Color.white, 0.25f));
            }
            for (int x = zoomSliderMarginX; x < zoomSliderMarginX + zoomSliderWidth; x++)
            {
                int y = height - zoomSliderMarginY;
                tex.SetPixel(x, y, Color.Lerp(tex.GetPixel(x, y), Color.white, 0.25f));
                y = height - (zoomSliderMarginY + zoomSliderHeight);
                tex.SetPixel(x, y, Color.Lerp(tex.GetPixel(x, y), Color.white, 0.25f));

                y = height - (zoomSliderMarginY + (int)Mathf.Round(zoomSliderHeight * ((pictureCamera.fieldOfView-minZoom) / (maxZoom-minZoom))));
                tex.SetPixel(x, y-1, Color.white);
                tex.SetPixel(x, y, Color.white);
                tex.SetPixel(x, y+1, Color.white);
            }
        }
        tex.Apply();
        tex.filterMode = filterMode;

        RenderTexture.active = previouslyActiveTexture;
        return tex;
    }

    private Texture2D CreateBlackTexture()
    {
        Texture2D tex = new Texture2D(2, 2, TextureFormat.RGB24, false);
        for (int x = 0; x < tex.width; x++)
            for (int y = 0; y < tex.height; y++)
                tex.SetPixel(x, y, Color.black);
        tex.Apply();
        return tex;
    }

}