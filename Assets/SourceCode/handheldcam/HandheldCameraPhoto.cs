using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.Modelling.Whiteboard;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class HandheldCameraPhoto : MonoBehaviour
{
    public GameObject cameraGhostPrefab;

    public Renderer pictureRenderer;

    public GameObject pictureParent;
    public GameObject attachmentIndicator;

    public Vector3 cameraPosition { get; private set; }
    public Vector3 cameraRotation { get; private set; }
    public Vector3 systemPositionToRestore { get; private set; }
    public float systemSizeToRestore { get; private set; }
    public VisibleRelation[] relationsToRestore { get; private set; }
    public string[] openCapsuleElementNames { get; private set; }



    public WhiteboardHandler containingWhiteboard { get; private set; }
    public Texture2D photoTexture { get; private set; }

    public int id { get; private set; }

    private Coroutine destroyRoutine;

    public Vector3 localSpacePositionOnDrawingBoard { get; private set; }
    public Quaternion localSpaceRotationOnDrawingBoard { get; private set; }

    private bool attached = false;



    public void Initialize(Texture2D texture, int id = -1)
    {
        this.id = id;
        name = "Photo (id=" + id + ")";

        photoTexture = texture;
        pictureRenderer.materials[1].mainTexture = photoTexture;

        gameObject.SetActive(true);
        attachmentIndicator.SetActive(false);

        //foreach(ISAUIInteractable interactable in GetComponentsInChildren<ISAUIInteractable>(true))
        //    interactable.onTriggerReleased.AddListener(RestoreVisualizationState);
    }

    public void SetRestoreSystemData(Vector3 cameraPosition, Vector3 cameraRotation, Vector3 systemPositionToRestore, float systemSizeToRestore, VisibleRelation[] relationsToRestore, string[] openCapsuleElementNames)
    {
        this.cameraPosition = cameraPosition;
        this.cameraRotation = cameraRotation;

        this.systemPositionToRestore = systemPositionToRestore;
        this.systemSizeToRestore = systemSizeToRestore;

        this.relationsToRestore = relationsToRestore;
        this.openCapsuleElementNames = openCapsuleElementNames;
    }

    public void RestoreVisualizationState()
    {
        if (ModelLoader.instance.VisualizationModel == null)
            return;

        List<AbstractEvent> eventBatch = new List<AbstractEvent>();

        // hide all relations
        {
            ClearAllRelationsEvent e = new ClearAllRelationsEvent();
            e.systemName = ModelLoader.instance.VisualizationModel.name;
            e.clientId = SceneManager.INSTANCE.udpClient.id;

            eventBatch.Add(e);
        }

        // move the system to where it was when the photo was taken
        {
            SystemMoveEvent ev = new SystemMoveEvent();
            ev.systemName = ModelLoader.instance.VisualizationModel.name;
            ev.clientId = SceneManager.INSTANCE.udpClient.id;
            ev.position = EcoreUtil.ConvertToEcoreMultiplayer(systemPositionToRestore);
            ev.rotation = EcoreUtil.ConvertToEcoreMultiplayer(SceneManager.INSTANCE.RootTransform.rotation.eulerAngles); // not relevant actually
            ev.scale = systemSizeToRestore;

            eventBatch.Add(ev);
        }

        // open and close capsules
        {
            TransparentCapsule3D[] allCapsules = SceneManager.INSTANCE.RootTransform.GetComponentsInChildren<TransparentCapsule3D>();
            foreach (TransparentCapsule3D capsule in allCapsules)
                // Close the root capsule(s)
                eventBatch.Add(CreateCapsuleEvent(capsule.qualifiedName, false));

            foreach (TransparentCapsule3D capsule in allCapsules)
                foreach (string elementName in openCapsuleElementNames)
                    if (capsule.qualifiedName.Equals(elementName))
                        eventBatch.Add(CreateCapsuleEvent(elementName, true));
        }

        // show relations that were visible when the photo was taken
        if (relationsToRestore != null && relationsToRestore.Length > 0)
        {
            foreach (VisibleRelation relationToShow in relationsToRestore)
            {
                ShowRelationEvent e = new ShowRelationEvent();
                e.systemName = ModelLoader.instance.VisualizationModel.name;
                e.clientId = SceneManager.INSTANCE.udpClient.id;
                e.elementName = relationToShow.elementName;
                e.relationType = (RelationType)relationToShow.relationType;
                e.direction = relationToShow.forward ? Direction.FORWARD : Direction.BACKWARD;

                eventBatch.Add(e);
            }
        }

        EventLog eventBatchLog = new EventLog();
        eventBatchLog.log = eventBatch.ToArray();

        string operationAsString = ModelLoader.encodeModelAsXML(eventBatchLog);
        _ = HTTPRequestHandler.INSTANCE.SendRequest("synchronization/push/?systemName=" + ModelLoader.instance.VisualizationModel.name,
            operationAsString, null, delegate (string r) { Debug.LogError(r); });

        GameObject cameraGhost = Instantiate(cameraGhostPrefab);
        cameraGhost.transform.position = cameraPosition;
        cameraGhost.transform.rotation = Quaternion.Euler(cameraRotation);

        Vector3 newPlayerPosition = cameraPosition - (Player.instance.headCollider.transform.position - Player.instance.trackingOriginTransform.position);
        newPlayerPosition -= cameraGhost.transform.forward * 0.3f;
        newPlayerPosition.y = 0;
        Player.instance.trackingOriginTransform.position = newPlayerPosition;
    }

    private SphereOpenCloseEvent CreateCapsuleEvent(string elementQualifiedName, bool nowOpen)
    {
        SphereOpenCloseEvent ev = new SphereOpenCloseEvent();
        ev.systemName = ModelLoader.instance.VisualizationModel.name;
        ev.clientId = SceneManager.INSTANCE.udpClient.id;
        ev.elementName = elementQualifiedName;
        ev.nowOpen = nowOpen;
        return ev;
    }



    public void ResetPosition()
    {
        UpdatePosition(containingWhiteboard, localSpacePositionOnDrawingBoard, localSpaceRotationOnDrawingBoard);
    }

    public void UpdatePosition(WhiteboardHandler whiteboard, Vector3 localSpacePosition, Quaternion localSpaceRotation)
    {
        this.containingWhiteboard = whiteboard;

        localSpacePositionOnDrawingBoard = localSpacePosition;
        localSpaceRotationOnDrawingBoard = localSpaceRotation;

        transform.localPosition = localSpacePosition;
        transform.localRotation = localSpaceRotation;
    }

    public void OnHandAttach()
    {
        attached = true;
    }

    public void OnHandHeldUpdate()
    {
        if (!attached)
            return;

        if (FindTargetWhiteboard(out WhiteboardHandler whiteboard, out Vector3 worldSpaceCoordinate, out _))
        {
            attachmentIndicator.SetActive(true);
            pictureParent.transform.position = worldSpaceCoordinate - whiteboard.transform.up * 0.01f;
            AdjustPictureTransform(whiteboard);
        }
        else
        {
            attachmentIndicator.SetActive(false);
            pictureParent.transform.localPosition = Vector3.zero;
            pictureParent.transform.localRotation = Quaternion.identity;
        }
    }

    public void OnHandDetach()
    {
        attached = false;
        
        if (!FindTargetWhiteboard(out WhiteboardHandler foundWhiteboard, out Vector3 worldSpaceCoordinate, out Vector2 textureSpaceCoordinate))
        {
            // no whiteboard found: invalid placement

            if (containingWhiteboard != null)
                containingWhiteboard.RemovePhoto(this, true);
            else
                // photo was only local, just remove
                DissolveAndDestroy(false);
        }
        else
        {
            // whiteboard found: valid placement
            AdjustPictureTransform(foundWhiteboard);

            if (containingWhiteboard != null)
            {
                if (foundWhiteboard.Equals(containingWhiteboard))
                {
                    foundWhiteboard.MovePhoto(this, textureSpaceCoordinate, worldSpaceCoordinate, pictureParent.transform.forward, pictureParent.transform.up);
                }
                else
                {
                    containingWhiteboard.RemovePhoto(this, false);
                    foundWhiteboard.AddPhoto(this, textureSpaceCoordinate, worldSpaceCoordinate, pictureParent.transform.forward, pictureParent.transform.up);
                }
            }
            else
            {
                // photo was only local so far, attach to whiteboard via operation and remove this local version
                foundWhiteboard.AddPhoto(this, textureSpaceCoordinate, worldSpaceCoordinate, pictureParent.transform.forward, pictureParent.transform.up);
                Destroy(gameObject);
            }
        }

        attachmentIndicator.SetActive(false);
        pictureParent.transform.localPosition = Vector3.zero;
        pictureParent.transform.localRotation = Quaternion.identity;
    }

    private void AdjustPictureTransform(WhiteboardHandler boardHandler)
    {
        Vector3 whiteboardNormal = - boardHandler.transform.up;
        Vector3 photoForwardProjectedOnWhiteboard = Vector3.ProjectOnPlane(transform.forward, whiteboardNormal);
        pictureParent.transform.rotation = Quaternion.LookRotation(photoForwardProjectedOnWhiteboard, whiteboardNormal);

    }

    private bool FindTargetWhiteboard(out WhiteboardHandler whiteboard, out Vector3 worldSpaceCoordinate, out Vector2 textureSpaceCoordinate)
    {
        textureSpaceCoordinate = Vector2.zero;
        worldSpaceCoordinate = Vector2.zero;
        whiteboard = null;

        foreach (RaycastHit hit in Physics.RaycastAll(transform.position + transform.up * 0.1f, -transform.up, 0.3f))
        {
            if (hit.collider.gameObject.TryGetComponent(out whiteboard))
            {
                // is a drawing board!
                textureSpaceCoordinate = hit.textureCoord * whiteboard.textureSize;
                worldSpaceCoordinate = hit.point;
                return true;
            }
        }
        return false;
    }



    public void DissolveAndDestroy(bool applyBackwardsForce)
    {
        transform.parent = null;
        gameObject.SetActive(true);

        Rigidbody rb = transform.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.useGravity = true;

        if (applyBackwardsForce)
            rb.AddForce(transform.forward * -0.01f + transform.up * 0.005f, ForceMode.Impulse);

        if (destroyRoutine != null)
            StopCoroutine(destroyRoutine);
        destroyRoutine = StartCoroutine(DestroyRoutine());
    }

    private IEnumerator DestroyRoutine()
    {
        yield return new WaitForSeconds(2);

        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>(false);

        float dissolveProgress = -0.2f;
        while (true)
        {
            yield return null;
            dissolveProgress += Time.deltaTime;

            foreach (MeshRenderer renderer in renderers)
                foreach (Material mat in renderer.materials)
                    mat.SetFloat("_Dissolve_Progress", dissolveProgress);

            if (dissolveProgress > 1.2f)
                break;
        }
        Destroy(gameObject);
    }

}
