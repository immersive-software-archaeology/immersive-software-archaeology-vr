﻿using AfGD;
using System.Collections;
using UnityEngine;

public class CurveUtil : MonoBehaviour
{

    public static CurveSegment CalculateCurve(Vector3 startPos, Vector3 endPos, Vector3 startHandle, Vector3 endHandle, float minBezierSize = 0)
    {
        float bezierHandleMultiplier = Mathf.Max(minBezierSize, Vector3.Distance(startPos, endPos) / 2f);
        startHandle *= bezierHandleMultiplier;
        endHandle *= bezierHandleMultiplier;

        return new CurveSegment(
            startPos,
            startPos + startHandle,
            endPos + endHandle,
            endPos,
            CurveType.BEZIER);
    }

    public static void RenderCurve(LineRenderer renderer, CurveSegment curve, bool useLocalSpace = true)
    {
        for (int i = 0; i < renderer.positionCount; i++)
        {
            float u = (float)i / ((float)renderer.positionCount - 1f);
            if (useLocalSpace)
                renderer.SetPosition(i, renderer.transform.InverseTransformPoint(curve.Evaluate(u)));
            else
                renderer.SetPosition(i, curve.Evaluate(u));
        }
    }

    public static float CalculateCurveLength(CurveSegment curve, int positionCount)
    {
        float curveLength = 0;
        Vector3 lastSegmentPosition = curve.Evaluate(0);
        for (int i = 1; i < positionCount; i++)
        {
            float u = (float)i / ((float)positionCount - 1f);
            Vector3 nextSegmentPosition = curve.Evaluate(u);

            curveLength += Vector3.Distance(lastSegmentPosition, nextSegmentPosition);
            lastSegmentPosition = nextSegmentPosition;
        }
        return curveLength;
    }

}