﻿using ISA.Modelling.Multiplayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class RelationManager
{

    public class RelationInfo
    {
        public AbstractVisualElement3D globalOriginElement;
        public AbstractVisualElement3D globalTargetElement;

        public float weight;
        public RelationType type;

        //public bool forward;

        //public List<Coroutine> animationRoutines;
        //public List<RelationLineSegment> createdLineSegments;
    }



    private GameObject relationLineSegmentPrefab;

    private List<RelationLineSegment> visibleRelationSegments;
    private Dictionary<RelationInfo, int> displayedRelationsOccurrences;
    private Dictionary<AbstractVisualElement3D, List<Tuple<RelationType, bool>>> elementsWithDisplayedRelations;

    public void Init()
    {
        relationLineSegmentPrefab = UnityEngine.Object.Instantiate(Resources.Load("Models/Visualization/Connections Root Prefab") as GameObject);
        relationLineSegmentPrefab = relationLineSegmentPrefab.transform.GetChild(0).gameObject;
        relationLineSegmentPrefab.transform.position = Vector3.down * 10f;
        relationLineSegmentPrefab.SetActive(false);

        visibleRelationSegments = new List<RelationLineSegment>();
        displayedRelationsOccurrences = new Dictionary<RelationInfo, int>();
        elementsWithDisplayedRelations = new Dictionary<AbstractVisualElement3D, List<Tuple<RelationType, bool>>>();
    }



    private void RegisterShownRelations(AbstractVisualElement3D element, RelationType relationType, bool forward)
    {
        if (!elementsWithDisplayedRelations.TryGetValue(element, out List<Tuple<RelationType, bool>> displayedTypesAndDirections))
        {
            displayedTypesAndDirections = new List<Tuple<RelationType, bool>>();
            elementsWithDisplayedRelations.Add(element, displayedTypesAndDirections);
        }

        foreach (Tuple<RelationType, bool> displayedTypeAndDirection in displayedTypesAndDirections)
            if (displayedTypeAndDirection.Item1 == relationType && displayedTypeAndDirection.Item2 == forward)
                // already registered
                return;
        displayedTypesAndDirections.Add(new Tuple<RelationType, bool>(relationType, forward));
    }

    private void UnregisterShownRelations(AbstractVisualElement3D element, RelationType relationType, bool forward)
    {
        if (elementsWithDisplayedRelations.TryGetValue(element, out List<Tuple<RelationType, bool>> displayedTypesAndDirections))
        {
            foreach (Tuple<RelationType, bool> displayedTypeAndDirection in displayedTypesAndDirections)
            {
                if (displayedTypeAndDirection.Item1 == relationType && displayedTypeAndDirection.Item2 == forward)
                {
                    // already registered
                    displayedTypesAndDirections.Remove(displayedTypeAndDirection);
                    if (displayedTypesAndDirections.Count == 0)
                        elementsWithDisplayedRelations.Remove(element);
                    return;
                }
            }
        }
    }

    public List<AbstractVisualElement3D> GetAllElementsWithDisplayedRelationships()
    {
        return new List<AbstractVisualElement3D>(elementsWithDisplayedRelations.Keys);
    }

    public bool IsRelationCurrentlyShown(AbstractVisualElement3D element, RelationType relationType, bool forward)
    {
        if (elementsWithDisplayedRelations.TryGetValue(element, out List<Tuple<RelationType, bool>> displayedTypesAndDirections))
            foreach (Tuple<RelationType, bool> displayedTypeAndDirection in displayedTypesAndDirections)
                if (displayedTypeAndDirection.Item1 == relationType && displayedTypeAndDirection.Item2 == forward)
                    return true;
        return false;
    }

    public void ExecuteOnCurrentlyShownRelations(AbstractVisualElement3D element, Action<AbstractVisualElement3D, RelationType, bool> action)
    {
        if (elementsWithDisplayedRelations.TryGetValue(element, out List<Tuple<RelationType, bool>> displayedTypesAndDirections))
            foreach (Tuple<RelationType, bool> displayedTypeAndDirection in displayedTypesAndDirections)
                action(element, displayedTypeAndDirection.Item1, displayedTypeAndDirection.Item2);
    }



    public void ShowRelations(AbstractVisualElement3D element, RelationType relationType, bool forward)
    {
        if (IsRelationCurrentlyShown(element, relationType, forward))
        {
            Debug.LogWarning("Already showing " + (forward?"forward":"backwards") + " relations of type " + relationType + " for element " + element.name);
            return;
        }

        RegisterShownRelations(element, relationType, forward);

        List<RelationInfo> relationsToShow;
        if (forward)
            relationsToShow = element.GetOutgoingRelationships(relationType);
        else
            relationsToShow = element.GetIncomingRelationships(relationType);

        foreach (RelationInfo relation in relationsToShow)
        {
            if (displayedRelationsOccurrences.ContainsKey(relation))
            {
                // The same relation was already displayed via another operation
                // e.g., the user displays outgoing relations for a class, thereafter outgoing relations of its parent package
                displayedRelationsOccurrences[relation] = displayedRelationsOccurrences[relation] + 1;
            }
            else
            {
                // New relation, display it
                displayedRelationsOccurrences.Add(relation, 1);
                SpawnOrAddToRelationLineSegments(relation.globalOriginElement, relation.globalTargetElement, relation.type, relation.weight);
            }
        }
    }



    public void HideAllRelations()
    {
        foreach (AbstractVisualElement3D element in new List<AbstractVisualElement3D>(elementsWithDisplayedRelations.Keys))
            HideAllRelationsForElement(element);
    }

    public void HideAllRelationsForElement(AbstractVisualElement3D element)
    {
        if (elementsWithDisplayedRelations.TryGetValue(element, out List<Tuple<RelationType, bool>> displayedTypesAndDirections))
        {
            List<Tuple<RelationType, bool>> safeCopy = new List<Tuple<RelationType, bool>>(displayedTypesAndDirections);
            foreach (Tuple<RelationType, bool> displayedTypeAndDirection in safeCopy)
                HideRelationsForElement(element, displayedTypeAndDirection.Item1, displayedTypeAndDirection.Item2);
        }
    }

    public void HideRelationsForElement(AbstractVisualElement3D element, RelationType relationType, bool forward)
    {
        if (!IsRelationCurrentlyShown(element, relationType, forward))
            return;

        UnregisterShownRelations(element, relationType, forward);

        List<RelationInfo> relationsToHide;
        if (forward)
            relationsToHide = element.GetOutgoingRelationships(relationType);
        else
            relationsToHide = element.GetIncomingRelationships(relationType);

        foreach (RelationInfo relation in relationsToHide)
        {
            if (!displayedRelationsOccurrences.ContainsKey(relation))
                continue;

            displayedRelationsOccurrences[relation] = displayedRelationsOccurrences[relation] - 1;
            if(displayedRelationsOccurrences[relation] == 0)
            {
                // actually hide it or reduce its weight
                displayedRelationsOccurrences.Remove(relation);
                DestroyOrRemoveFromRelationLineSegments(relation.globalOriginElement, relation.globalTargetElement, relation.type, relation.weight);
            }
        }
    }



    private void SpawnOrAddToRelationLineSegments(AbstractVisualElement3D globalOrigin, AbstractVisualElement3D globalTarget, RelationType type, float weight)
    {
        AbstractVisualElement3D localOrigin = globalOrigin;
        for(int iteration=0; true; iteration++)
        {
            AbstractVisualElement3D localTarget = RelationCreationUtil.DeriveNextDirectTargetOnPathThroughSystemCapsules(localOrigin, globalTarget);
            if (localOrigin.Equals(globalTarget))
                break;

            RelationLineSegment existingSegment = GetExistingSegment(localOrigin, localTarget, type);
            if(existingSegment != null)
            {
                existingSegment.ChangeWeight(+weight);
            }
            else
            {
                GameObject newRelationLineSegmentObject = UnityEngine.Object.Instantiate(relationLineSegmentPrefab);
                newRelationLineSegmentObject.name = localOrigin.simpleName + " -> " + localTarget.simpleName;
                //newRelationObject.transform.parent = originalRelationManager.transform.parent;
                // Set into scene as is so that the co routine can definitely be started
                newRelationLineSegmentObject.transform.parent = null;
                newRelationLineSegmentObject.transform.localPosition = Vector3.zero;
                newRelationLineSegmentObject.transform.localScale = Vector3.one;

                RelationCreationUtil.CalculateBezierHandleMultipliers(localOrigin, localTarget, out float originBezierMultiplier, out float targetBezierMultiplier);

                RelationLineSegment newRelationLineSegment = newRelationLineSegmentObject.GetComponent<RelationLineSegment>();
                newRelationLineSegment.Initialize(localOrigin, localTarget, originBezierMultiplier, targetBezierMultiplier, type);
                newRelationLineSegment.SetVisible(localOrigin.GetParentCapsule().isOpened && localTarget.GetParentCapsule().isOpened);
                newRelationLineSegment.ChangeWeight(+weight);
                newRelationLineSegment.SetActiveAndStartAnimation(true, iteration);

                visibleRelationSegments.Add(newRelationLineSegment);
                ISAUIInteractionManager.INSTANCE.RegisterPositioningListener(localOrigin, newRelationLineSegment);
                ISAUIInteractionManager.INSTANCE.RegisterPositioningListener(localTarget, newRelationLineSegment);
            }

            if (localTarget.Equals(globalTarget))
                break;
            localOrigin = localTarget;
        }
    }

    private RelationLineSegment GetExistingSegment(AbstractVisualElement3D localOrigin, AbstractVisualElement3D localTarget, RelationType type)
    {
        foreach (RelationLineSegment segment in visibleRelationSegments)
        {
            if (!segment.relationType.Equals(type))
                continue;
            if (!segment.localOriginElement.Equals(localOrigin))
                continue;
            if (!segment.localTargetElement.Equals(localTarget))
                continue;
            return segment;
        }
        return null;
    }



    private void DestroyOrRemoveFromRelationLineSegments(AbstractVisualElement3D globalOrigin, AbstractVisualElement3D globalTarget, RelationType type, float weight)
    {
        AbstractVisualElement3D localOrigin = globalOrigin;
        for (int iteration = 0; true; iteration++)
        {
            AbstractVisualElement3D localTarget = RelationCreationUtil.DeriveNextDirectTargetOnPathThroughSystemCapsules(localOrigin, globalTarget);
            if (localOrigin.Equals(globalTarget))
                break;

            RelationLineSegment existingSegment = GetExistingSegment(localOrigin, localTarget, type);
            if (existingSegment != null)
            {
                existingSegment.ChangeWeight(-weight);

                if(existingSegment.weight <= float.Epsilon)
                {
                    visibleRelationSegments.Remove(existingSegment);
                    ISAUIInteractionManager.INSTANCE.RemovePositioningListener(existingSegment);

                    UnityEngine.Object.Destroy(existingSegment.gameObject);
                }
            }
            else
            {
                Debug.Log("Tried to hide a relation line segment that is not available!");
            }

            if (localTarget.Equals(globalTarget))
                break;
            localOrigin = localTarget;
        }
    }



    public void ChangeRelationSegmentVisibilityInCapsule(TransparentCapsule3D capsule, bool visible)
    {
        foreach (RelationLineSegment relationSegment in visibleRelationSegments)
        {
            bool isSegmentContainedInCapsule = false;
            if (relationSegment.localOriginElement.IsContainedInHierarchyOfCapsule(capsule))
                isSegmentContainedInCapsule = true;
            if (relationSegment.localTargetElement.IsContainedInHierarchyOfCapsule(capsule))
                isSegmentContainedInCapsule = true;

            if(isSegmentContainedInCapsule)
                relationSegment.SetVisible(visible);
        }
    }



    //private IEnumerator ShowRelationWithDelayOLD(MultiViewElement3D subjectElement, MultiViewElement3D globalOrigin, MultiViewElement3D globalTarget, RelationType relationType, bool forward)
    //{
    //    MultiViewElement3D currentOrigin = globalOrigin;
    //    while (true)
    //    {
    //        MultiViewElement3D localTarget = RelationCreationUtil.DeriveNextDirectTargetOnPathThroughSystemCapsules(currentOrigin, globalTarget);
    //        if (currentOrigin.Equals(globalTarget))
    //            break;

    //        RelationLineSegment relationLineManagerFromSpawnPrecedure = currentOrigin.GetOutgoingRelationshipLineManagerPrototype(globalTarget);

    //        RelationInfo relationInfo = GetRelationInfo(subjectElement, relationType, forward);
    //        if (relationInfo.createdLineSegments == null)
    //            relationInfo.createdLineSegments = new List<RelationLineSegment>();

    //        GameObject copiedRelationObject = Instantiate(relationLineManagerFromSpawnPrecedure.gameObject);
    //        copiedRelationObject.name = globalOrigin.simpleName + " -> " + globalTarget.simpleName;
    //        //newRelationObject.transform.parent = originalRelationManager.transform.parent;
    //        // Set into scene as is so that the co routine can definitely be started
    //        copiedRelationObject.transform.parent = null;
    //        copiedRelationObject.transform.localPosition = Vector3.zero;
    //        copiedRelationObject.transform.localScale = Vector3.one;

    //        RelationLineSegment copiedRelationManager = copiedRelationObject.GetComponent<SystemRelationLineManager>();
    //        RelationCreationUtil.CalculateBezierHandleMultipliers(currentOrigin, localTarget, out float originBezierMultiplier, out float targetBezierMultiplier);
    //        copiedRelationManager.Initialize(currentOrigin.outgoingRelationsHookTransform, localTarget.incomingRelationsHookTransform, originBezierMultiplier, targetBezierMultiplier);
    //        copiedRelationManager.CopyEntriesFrom(relationLineManagerFromSpawnPrecedure);
    //        if (copiedRelationManager.RemoveAllEntriesExceptRelationsBetween(globalOrigin, globalTarget) > 0)
    //        {
    //            bool bla = false;
    //            foreach (List<RelationInfo> registeredRelationInfos in registeredRelations.Values)
    //            {
    //                foreach (RelationInfo registeredRelationInfo in registeredRelationInfos)
    //                {
    //                    if (registeredRelationInfo.createdLineSegments == null)
    //                        continue;
    //                    if (registeredRelationInfo == relationInfo)
    //                        continue;

    //                    foreach (RelationLineSegment segment in registeredRelationInfo.createdLineSegments)
    //                    {
    //                        if (!segment.localOrigin.Equals(relationLineManagerFromSpawnPrecedure.localOrigin))
    //                            continue;
    //                        if (!segment.localTarget.Equals(relationLineManagerFromSpawnPrecedure.localTarget))
    //                            continue;

    //                        // Found a segment that is already present!
    //                        // Increase its weight instead of spawning a new one that is overlayed with the already present one
    //                        segment.IncreaseWeight(globalOrigin, globalTarget, relationType, copiedRelationManager.totalWeight);
    //                        segment.Refresh();

    //                        relationInfo.createdLineSegments.Add(segment);

    //                        Destroy(copiedRelationObject);
    //                        bla = true;
    //                    }
    //                }
    //            }

    //            if (!bla)
    //            // in case the above loop did not increase the weight of an already present segment...
    //            {
    //                copiedRelationManager.SetActiveAndStartAnimation(true);

    //                relationInfo.createdLineSegments.Add(copiedRelationManager);

    //                ISAUIInteractionManager.INSTANCE.RegisterPositioningListener(currentOrigin, copiedRelationManager);
    //                ISAUIInteractionManager.INSTANCE.RegisterPositioningListener(localTarget, copiedRelationManager);
    //            }
    //        }
    //        else
    //        {
    //            Destroy(copiedRelationObject);
    //        }

    //        if (localTarget.Equals(globalTarget))
    //            break;
    //        currentOrigin = localTarget;
    //        yield return new WaitForSeconds(RelationLineSegment.ANIMATION_DURATION);
    //    }
    //}

}
