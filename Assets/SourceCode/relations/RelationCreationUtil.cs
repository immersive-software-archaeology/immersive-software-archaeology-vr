﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelationCreationUtil
{



    public static List<TransparentCapsule3D> CalculatePathThroughSystemCapsules(AbstractVisualElement3D origin, AbstractVisualElement3D target)
    {
        // Calculate a path to the target
        List<TransparentCapsule3D> pathTroughCapsules = new List<TransparentCapsule3D>();

        // Is target contained in same capsule?
        pathTroughCapsules.Add(origin.GetParentCapsule());
        if (pathTroughCapsules[0].Contains(target))
            // We are done
            return pathTroughCapsules;

        // Dig all the way up until we find the capsule that contains the target somewhere in its hierarchy!
        while (true)
        {
            pathTroughCapsules.Add(pathTroughCapsules[pathTroughCapsules.Count - 1].GetParentCapsule());
            foreach (TransparentCapsule3D child in pathTroughCapsules[pathTroughCapsules.Count - 1].containedSubCapsules)
            {
                if (child.Contains(target))
                {
                    pathTroughCapsules.Remove(pathTroughCapsules[pathTroughCapsules.Count - 1]);
                    pathTroughCapsules.Add(child);
                    goto digdown;
                }
            }
        }

    digdown:
        foreach (TransparentCapsule3D child in pathTroughCapsules[pathTroughCapsules.Count - 1].containedSubCapsules)
        {
            if (child.Contains(target))
            {
                pathTroughCapsules.Add(child);
                goto digdown;
            }
        }

        // At this point, there is no sub capsule of the last path entry that contains the target
        // -> the target must be contained in the last path entry itself
        return pathTroughCapsules;
    }



    public static AbstractVisualElement3D DeriveNextDirectTargetOnPathThroughSystemCapsules(AbstractVisualElement3D origin, AbstractVisualElement3D target, bool avoidUnneccesaryJumps = true)
    {
        if (origin is SpaceStation3D || origin is AbstractSpaceStationComponent3D)
        {
            // Special case: start point is a spacestation or a part of one
            if (origin.GetParentCapsule().ContainsDirectly(target))
                // target element is contained in the same capsule, return the target directly!
                return target;

            // Otherwise, walk up into the containing capsule
            // ... but only, if we wouldn't immediately walk down the hierarchy again...
            if (avoidUnneccesaryJumps)
                return AvoidUnneccessaryRoutesThroughParentCapsules(origin.GetParentCapsule().directContainment, target);
            return origin.GetParentCapsule().directContainment;
        }

        else if (origin is TransparentCapsuleContainment3D)
        {
            foreach (SpaceStation3D station in ((TransparentCapsuleContainment3D)origin).containedSpaceStations)
            {
                if (station.Equals(target))
                    return target;
                if (station.Contains(target))
                    return target;
            }

            // Walk up into the containing capsule
            // ... but only, if we wouldn't immediately walk down the hierarchy again...
            if (avoidUnneccesaryJumps)
                return AvoidUnneccessaryRoutesThroughParentCapsules(origin.GetParentCapsule(), target);
            return origin.GetParentCapsule();
        }

        else if (origin is TransparentCapsule3D)
        {
            // Check its sub capsules to see whether the target is contained in there
            // If so, walk in there
            foreach (TransparentCapsule3D childCapsule in ((TransparentCapsule3D)origin).containedSubCapsules)
            {
                if (childCapsule.Contains(target))
                    return childCapsule;
            }

            if (((TransparentCapsule3D)origin).ContainsDirectly(target))
                return ((TransparentCapsule3D)origin).directContainment;

            // Otherwise, walk up the hierarchy
            // ... but only, if we wouldn't immediately walk down the hierarchy again...
            if (avoidUnneccesaryJumps)
                return AvoidUnneccessaryRoutesThroughParentCapsules(origin.GetParentCapsule(), target);
            return origin.GetParentCapsule();
        }

        else throw new Exception("Unexpected element type: " + origin);
    }

    /// <summary>
    /// Avoids unneccesary steps through parent capsules, i.e., going up and immediate down again while establishing relations
    /// </summary>
    private static AbstractVisualElement3D AvoidUnneccessaryRoutesThroughParentCapsules(AbstractVisualElement3D initialParent, AbstractVisualElement3D target)
    {
        AbstractVisualElement3D nextElementAfterParentCapsule = DeriveNextDirectTargetOnPathThroughSystemCapsules(initialParent, target, false);
        if (nextElementAfterParentCapsule.GetNestingLevel() >= initialParent.GetNestingLevel())
            return nextElementAfterParentCapsule;

        //if (nextElementAfterParentCapsule is AbstractSpaceStationComponent3D)
        //{
        //    TransparentCapsule3D containingCapsule = nextElementAfterParentCapsule.GetParentCapsule();
        //    if (containingCapsule.GetNestingLevel() + 1 == origin.GetNestingLevel())
        //        return nextElementAfterParentCapsule;
        //}

        return initialParent;
    }



    public static void CalculateBezierHandleMultipliers(AbstractVisualElement3D origin, AbstractVisualElement3D target, out float originBezierMultiplier, out float targetBezierMultiplier)
    {
        originBezierMultiplier = 1;
        targetBezierMultiplier = 1;

        if (origin is TransparentCapsule3D || target is TransparentCapsule3D || origin is TransparentCapsuleContainment3D || target is TransparentCapsuleContainment3D)
        {
            if (origin.GetNestingLevel() < target.GetNestingLevel() && origin.Contains(target))
                originBezierMultiplier = -1;
            else if (origin.GetNestingLevel() > target.GetNestingLevel() && target.Contains(origin))
                targetBezierMultiplier = -1;
            return;
        }

        // Neither origin nor target are a capsule
        // Because of the way relations are set up and routed through the system's capsule structure, origin and target are contained in the same capsule

        if (origin.directParent.Contains(target))
        {
            // inner space station relation: increase handle multiplier and handle specific cases

            //originBezierMultiplier = 3;
            //targetBezierMultiplier = 3;

            if (origin is SpaceStationBlock3D && target is SpaceStationAntenna3D)
            {
                originBezierMultiplier = float.NaN;
            }
            else if (origin is SpaceStationAntenna3D && target is SpaceStationBlock3D)
            {
                targetBezierMultiplier = float.NaN;
            }

            else if (origin is SpaceStationAntenna3D && target is SpaceStationAntenna3D)
            {
            }
            else if (origin is SpaceStationBlock3D && target is SpaceStationBlock3D)
            {
                originBezierMultiplier *= -1;
                targetBezierMultiplier *= -1;
            }

            else if (origin is SpaceStationBlock3D && target is SpaceStation3D)
            {
                originBezierMultiplier *= -1;
            }
            else if (origin is SpaceStation3D && target is SpaceStationBlock3D)
            {
                targetBezierMultiplier *= -1;
            }

            if (origin is SpaceStation3D)
            {
                originBezierMultiplier = 1;
            }
            if (target is SpaceStation3D)
            {
                targetBezierMultiplier = 1;
            }

            return;
        }

        if (origin.directParent.Equals(target))
        {
            originBezierMultiplier = 10;
            targetBezierMultiplier = 10;
            return;
        }
        
        if (origin is SpaceStationBlock3D)
        {
            // reference is going from a block to another station in the same capsule

            originBezierMultiplier = -1;
            if (target is SpaceStationBlock3D)
                targetBezierMultiplier = -1;
            return;
        }
    }
}