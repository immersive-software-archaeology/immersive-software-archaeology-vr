using AfGD;
using ISA.Modelling.Multiplayer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(LineRenderer))]
public class RelationLineSegment : MonoBehaviour, ISAPositionableElementListener
{

    private static readonly float ANIMATION_DURATION = 1f;



    private LineRenderer lineRenderer;

    public AbstractVisualElement3D localOriginElement { get; private set; }
    public AbstractVisualElement3D localTargetElement { get; private set; }

    public Transform localOrigin { get; private set; }
    public Transform localTarget { get; private set; }

    private float originBezierMultiplier;
    private float targetBezierMultiplier;

    //private List<RelationSegmentEntry> entries;
    public float weight { get; private set; }
    public RelationType relationType { get; private set; }

    private float animationStart;



    public void Initialize(AbstractVisualElement3D localOriginElement, AbstractVisualElement3D localTargetElement, float originBezierMultiplier, float targetBezierMultiplier, RelationType type)
    {
        //this.entries = new List<RelationSegmentEntry>();
        this.weight = 0;
        this.relationType = type;

        this.localOriginElement = localOriginElement;
        this.localTargetElement = localTargetElement;

        this.localOrigin = localOriginElement.outgoingRelationsHookTransform;
        this.localTarget = localTargetElement.incomingRelationsHookTransform;

        this.originBezierMultiplier = originBezierMultiplier;
        this.targetBezierMultiplier = targetBezierMultiplier;

        lineRenderer = GetComponent<LineRenderer>();

        lineRenderer.receiveShadows = false;
        lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        Color lineColor = ISAColorUtil.GetColorForRelationType(relationType);
        lineRenderer.startColor = lineColor;
        lineRenderer.endColor = lineColor;

        gameObject.SetActive(false);
    }

    public void ChangeWeight(float delta)
    {
        weight += delta;
        Refresh();
    }

    public void SetActiveAndStartAnimation(bool active, int iterationsToWaitFor)
    {
        if (lineRenderer != null && active)
        {
            animationStart = Time.time + iterationsToWaitFor * ANIMATION_DURATION;
            gameObject.SetActive(true);
            StartCoroutine(RenderAnimation(iterationsToWaitFor));
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private IEnumerator RenderAnimation(int iterationsToWaitFor)
    {
        while (Time.time - animationStart <= ANIMATION_DURATION)
        {
            Refresh();
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);
        Refresh();
    }

    public void SetVisible(bool visible)
    {
        lineRenderer.enabled = visible;
    }



    public void NotifyOnElementPositionChanged(object e)
    {
        Refresh();
    }



    private void Refresh()
    {
        if (!gameObject.activeSelf)
            return;

        SetVisible(localOriginElement.GetParentCapsule().isOpened && localTargetElement.GetParentCapsule().isOpened);

        float distance = Vector3.Distance(localOrigin.position, localTarget.position);
        CurveSegment curve = CalculateCurve(distance);
        float curveLength = CurveUtil.CalculateCurveLength(curve, lineRenderer.positionCount);

        //lineRenderer.widthMultiplier = Mathf.Pow(totalWeight, 0.6f) * 0.5f * gameObject.transform.lossyScale.x;// / gameObject.transform.parent.localScale.x;
        lineRenderer.positionCount = 6 + (int) Mathf.Ceil(Mathf.Min(distance * 20f, 10f));

        float minWidth = 0.5f;
        float curveWidth = Mathf.Max(minWidth, weight / 30f);
        lineRenderer.widthCurve = new AnimationCurve(new Keyframe(0, minWidth), new Keyframe(0.1f, curveWidth), new Keyframe(0.9f, curveWidth), new Keyframe(1, minWidth));
        lineRenderer.widthMultiplier = SceneManager.INSTANCE.RootTransform.transform.lossyScale.x;

        lineRenderer.material.SetFloat("_Tiling", curveLength / (curveWidth * lineRenderer.widthMultiplier));
        //lineRenderer.material.SetFloat("_MoveSpeed", 0.1f * (1f / lineRenderer.widthMultiplier) * curveLength);

        if (Time.time < animationStart)
        {
            // Animation has not started yet, position all points under the floor
            SetPositions(Vector3.down, 0, lineRenderer.positionCount - 1);
        }
        else if (Time.time >= animationStart + ANIMATION_DURATION)
        {
            // animation is over, show all segments in most efficient way
            SetPositions(curve, 0, lineRenderer.positionCount - 1);
        }
        else
        {
            // animate the segment
            float t_01 = (Time.time - animationStart) / ANIMATION_DURATION;
            t_01 = EasingFunctions.Crossfade(EasingFunctions.SmoothStart2, EasingFunctions.SmoothStop2, t_01, t_01);
            if (t_01 >= 1f)
                return;
            int fullyExtendedPoints = (int)Mathf.Floor(t_01 * (float)(lineRenderer.positionCount - 1f));

            // set positions of segments that are already fully extended
            SetPositions(curve, 0, fullyExtendedPoints);

            // extend the remaining segments
            SetPositions(curve.Evaluate(t_01), fullyExtendedPoints + 1, lineRenderer.positionCount - 1);
        }
    }

    private CurveSegment CalculateCurve(float distance)
    {
        Vector3 originHandle = localOrigin.up * (distance / 2f) * originBezierMultiplier;
        Vector3 targetHandle = localTarget.up * (distance / 2f) * targetBezierMultiplier;

        if (float.IsNaN(originBezierMultiplier))
            originHandle = targetHandle;
        if (float.IsNaN(targetBezierMultiplier))
            targetHandle = originHandle;

        return new CurveSegment(
            localOrigin.position,
            localOrigin.position + originHandle,
            localTarget.position + targetHandle,
            localTarget.position,
            CurveType.BEZIER);
    }

    private void SetPositions(CurveSegment curve, int startIndexIncl, int endIndexIncl)
    {
        for (int i = startIndexIncl; i <= endIndexIncl; i++)
        {
            float u = (float)i / ((float)lineRenderer.positionCount - 1f);
            //lineRenderer.SetPosition(i, curve.Evaluate(u));
            lineRenderer.SetPosition(i, lineRenderer.transform.InverseTransformPoint(curve.Evaluate(u)));
        }
    }

    private void SetPositions(Vector3 fixedPosition, int startIndexIncl, int endIndexIncl)
    {
        for (int i = startIndexIncl; i <= endIndexIncl; i++)
        {
            lineRenderer.SetPosition(i, lineRenderer.transform.InverseTransformPoint(fixedPosition));
        }
    }

}
