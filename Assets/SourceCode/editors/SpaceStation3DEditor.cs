﻿#if (UNITY_EDITOR)

using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(SpaceStation3D))]
public class SpaceStation3DEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SpaceStation3D element = (SpaceStation3D)target;

        EditorGUILayout.Space(50);

        if (GUILayout.Button("Open UI!"))
        {
            element.RecursivelySetActive(false);
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(element, true);
        }

        if (GUILayout.Button("Close UI!"))
        {
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(element, false);
            foreach (SpaceStationBlock3D block in element.Blocks)
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(block, false);
            foreach (SpaceStationAntenna3D antenna in element.Antennas)
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(antenna, false);
        }
    }
}

#endif