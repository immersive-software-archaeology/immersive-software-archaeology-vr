#if (UNITY_EDITOR)

using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(WhiteboardHandler))]
public class WhiteboardHandlerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WhiteboardHandler board = (WhiteboardHandler)target;



        EditorGUILayout.Space(50);

        GUILayout.Label("ID = " + board.id);



        EditorGUILayout.Space(20);

        if (GUILayout.Button("Enlarge"))
            board.Enlarge();

        if (GUILayout.Button("Duplicate"))
            board.Duplicate();

        if (GUILayout.Button("Destroy"))
            board.Destroy();



        EditorGUILayout.Space(20);

        if (GUILayout.Button("Clear Paint"))
            board.ClearPaint();

        if (GUILayout.Button("Clear Pins"))
            board.ClearPins();



        EditorGUILayout.Space(20);

        if (GUILayout.Button("Undo"))
            board.UndoLastAction();

        if (GUILayout.Button("Redo"))
            board.RedoLastAction();

    }
}

#endif