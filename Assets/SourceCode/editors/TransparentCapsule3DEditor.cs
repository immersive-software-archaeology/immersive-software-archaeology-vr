﻿#if (UNITY_EDITOR)

using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(TransparentCapsule3D))]
public class TransparentCapsule3DEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TransparentCapsule3D element = (TransparentCapsule3D)target;

        EditorGUILayout.Space(50);
        if (GUILayout.Button("Open UI"))
        {
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(element, true);
        }
        if (GUILayout.Button("Close UI"))
        {
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(element, false);
        }

        EditorGUILayout.Space(20);
        EditorGUILayout.LabelField("Currently: " + (element.isOpened ? "Opened" : "Closed"));
        if (GUILayout.Button("Open Capsule"))
        {
            element.SendOpenRequest();
        }
        if (GUILayout.Button("Close Capsule"))
        {
            element.SendCloseRequest();
        }
    }
}

#endif