#if (UNITY_EDITOR)

using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(HandheldCamera))]
public class HandheldCameraEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        HandheldCamera manager = (HandheldCamera)target;

        EditorGUILayout.Space(50);
        if(GUILayout.Button("Take Photo!"))
        {
            manager.TakePhoto();
        }
    }
}

#endif