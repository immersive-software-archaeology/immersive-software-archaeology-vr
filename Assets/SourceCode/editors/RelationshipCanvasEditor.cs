#if (UNITY_EDITOR)

using ISA.Modelling.Multiplayer;
using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(RelationshipCanvas))]
public class RelationshipCanvasEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        RelationshipCanvas canvas = (RelationshipCanvas)target;

        EditorGUILayout.Space(50);
        if (GUILayout.Button("Toggle Forward Type References"))
            canvas.SendToggleTypeReferencesRequest(true);
        if (GUILayout.Button("Toggle Backwards Type References"))
            canvas.SendToggleTypeReferencesRequest(false);

        EditorGUILayout.Space(20);
        if (GUILayout.Button("Toggle Forward Method Calls"))
            canvas.SendToggleMethodCallsRequest(true);
        if (GUILayout.Button("Toggle Backwards Method Calls"))
            canvas.SendToggleMethodCallsRequest(false);

        EditorGUILayout.Space(20);
        if (GUILayout.Button("Toggle Forward Field Accesses"))
            canvas.SendToggleFieldAccessesRequest(true);
        if (GUILayout.Button("Toggle Backwards Field Accesses"))
            canvas.SendToggleFieldAccessesRequest(false);

        EditorGUILayout.Space(20);
        if (GUILayout.Button("Clear Relations for this element"))
            canvas.SendClearRequest();
    }
}

#endif