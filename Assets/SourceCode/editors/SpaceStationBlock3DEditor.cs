﻿#if (UNITY_EDITOR)

using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(SpaceStationBlock3D))]
public class SpaceStationBlock3DEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SpaceStationBlock3D element = (SpaceStationBlock3D)target;
        SpaceStation3D parent = (SpaceStation3D)element.directParent;

        EditorGUILayout.Space(50);

        if (GUILayout.Button("Open UI!"))
        {
            parent.RecursivelySetActive(false);
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(element, true);
        }

        if (GUILayout.Button("Close UI!"))
        {
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(parent, false);
            foreach (SpaceStationBlock3D block in parent.Blocks)
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(block, false);
            foreach (SpaceStationAntenna3D antenna in parent.Antennas)
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(antenna, false);
        }
    }
}

#endif