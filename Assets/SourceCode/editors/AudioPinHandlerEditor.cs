﻿#if (UNITY_EDITOR)

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AudioPinHandler))]
public class AudioPinHandlerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space(50);

        if (GUILayout.Button("Play / Pause"))
            ((AudioPinHandler)target).SendPlaybackToggleRequest();
    }
}

#endif