﻿#if (UNITY_EDITOR)

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MicrophoneHandler))]
public class MicrophoneHandlerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space(50);

        if (GUILayout.Button("Toggle Recording"))
            ((MicrophoneHandler)target).ToggleRecording();

        EditorGUILayout.Space(20);

        if (GUILayout.Button("Previous Device"))
            ((MicrophoneHandler)target).SwitchMicrophone(-1);
        if (GUILayout.Button("Next Device"))
            ((MicrophoneHandler)target).SwitchMicrophone(1);
    }
}

#endif