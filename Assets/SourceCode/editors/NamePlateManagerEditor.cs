#if (UNITY_EDITOR)

using ISA.Modelling.Multiplayer;
using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(NamePlateManager))]
public class NamePlateManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        NamePlateManager nameplate = (NamePlateManager)target;

        EditorGUILayout.Space(50);
        GUILayout.Label("Active Visibility Tokens:\n\n" + nameplate.PrintActiveTokens());
    }
}

#endif