﻿#if (UNITY_EDITOR)

using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;
using Valve.VR;
using Valve.VR.InteractionSystem;

[CustomEditor(typeof(BeveledUIMesh))]
public class BeveledUIMeshEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space(50);

        if (GUILayout.Button("Generate Mesh"))
            ((BeveledUIMesh)target).GenerateMesh();
    }
}

#endif