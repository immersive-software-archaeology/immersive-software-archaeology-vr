﻿using ISA.UI;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ElementUpdater : MonoBehaviour
{

    private bool running = false;

    public static ElementUpdater instance;



    [SerializeField]
    private int workloadPerUpdate = 50;

    [SerializeField]
    private float LODToggleMinDistance = 500;

    [SerializeField]
    private float namePlateToggleMinDistance = 200;
    [SerializeField]
    private float namePlateToggleDistanceFallOff = 1f / 4f;



    private List<AbstractVisualElement3D> softwareElements;

    private List<NamePlateManager> softwareNamePlates;
    private List<Transform> softwareNamePlateTransforms;

    private List<NamePlateManager> pinNamePlates;



    void Start()
    {
        instance = this;

        softwareElements = new List<AbstractVisualElement3D>();

        softwareNamePlates = new List<NamePlateManager>();
        softwareNamePlateTransforms = new List<Transform>();

        pinNamePlates = new List<NamePlateManager>();
    }



    public void UpdateElementsContinuously()
    {
        running = true;
    }

    public void StopUpdatingElements()
    {
        running = false;

        softwareElements.Clear();

        softwareNamePlates.Clear();
        softwareNamePlateTransforms.Clear();

        pinNamePlates.Clear();
    }



    public void AddSoftwareElement(AbstractVisualElement3D element)
    {
        softwareElements.Add(element);
    }

    public void AddSoftwareNamePlate(NamePlateManager namePlate, Transform transformToRegisterFor)
    {
        if (softwareNamePlates.Contains(namePlate))
            RemoveSoftwareNamePlate(namePlate);

        softwareNamePlates.Add(namePlate);
        softwareNamePlateTransforms.Add(transformToRegisterFor);
    }

    public void RemoveSoftwareNamePlate(NamePlateManager namePlate)
    {
        if (!softwareNamePlates.Contains(namePlate))
            return;

        int index = softwareNamePlates.IndexOf(namePlate);
        softwareNamePlates.RemoveAt(index);
        softwareNamePlateTransforms.RemoveAt(index);
    }

    public void AddPinNamePlate(NamePlateManager namePlate)
    {
        if (pinNamePlates.Contains(namePlate))
            RemovePinNamePlate(namePlate);

        pinNamePlates.Add(namePlate);
    }

    public void RemovePinNamePlate(NamePlateManager namePlate)
    {
        pinNamePlates.Remove(namePlate);
    }




    private int indexInElements, indexInSoftwareNamePlates, indexInPinNamePlates;

    void Update()
    {
        if (!running)
            return;

        bool isOnMaxZoom = SceneManager.INSTANCE.RootTransform.localScale.x >= SceneManager.INSTANCE.maxScaleFactor;

        int workedLoad = 0;
        while (softwareElements.Count > 0)
        {
            indexInElements = (indexInElements + 1) % softwareElements.Count;
            AbstractVisualElement3D element = softwareElements[indexInElements];

            if (element is SpaceStation3D)
            {
                if (((SpaceStation3D)element).EnableDetailedColliders(isOnMaxZoom))
                    workedLoad += 1;

                float distanceToHead = Vector3.Distance(element.gameObject.transform.position, Player.instance.headCollider.transform.position);
                if (distanceToHead <= LODToggleMinDistance * SceneManager.INSTANCE.RootTransform.localScale.x)
                {
                    if (((SpaceStation3D)element).SetLevelOfDetail(true))
                        workedLoad += 10;
                }
                else
                {
                    if (((SpaceStation3D)element).SetLevelOfDetail(false))
                        workedLoad += 3;
                }
            }

            workedLoad += 1;
            if (workedLoad >= workloadPerUpdate)
                break;
        }

        workedLoad = 0;
        while (softwareNamePlates.Count > 0)
        {
            indexInSoftwareNamePlates = (indexInSoftwareNamePlates + 1) % softwareNamePlates.Count;
            NamePlateManager namePlate = softwareNamePlates[indexInSoftwareNamePlates];
            Transform namePlateTransform = softwareNamePlateTransforms[indexInSoftwareNamePlates];

            if (namePlate != null)
            {
                float distanceToHead = Vector3.Distance(namePlateTransform.position, Player.instance.headCollider.transform.position);
                workedLoad += ShowOrHideNamePlateBasedOnDistance(distanceToHead, SceneManager.INSTANCE.RootTransform.localScale.x, namePlate, isOnMaxZoom);
            }

            workedLoad += 1;
            if (workedLoad >= workloadPerUpdate)
                break;
        }

        workedLoad = workloadPerUpdate / 2;
        while (pinNamePlates.Count > 0)
        {
            indexInPinNamePlates = (indexInPinNamePlates + 1) % pinNamePlates.Count;
            NamePlateManager namePlate = pinNamePlates[indexInPinNamePlates];

            if (namePlate != null)
            {
                float distanceToHead = Vector3.Distance(namePlate.transform.position, Player.instance.headCollider.transform.position);
                workedLoad += ShowOrHideNamePlateBasedOnDistance(distanceToHead, SceneManager.INSTANCE.maxScaleFactor, namePlate, true);
            }

            workedLoad += 1;
            if (workedLoad >= workloadPerUpdate)
                break;
        }
    }



    private int ShowOrHideNamePlateBasedOnDistance(float distanceToHead, float distanceMultiplier, NamePlateManager namePlate, bool showWhenLookedAt)
    {
        bool namePlateShown = false;

        if (showWhenLookedAt)
        {
            if (distanceToHead < namePlateToggleMinDistance * distanceMultiplier)
            {
                {
                    namePlateShown = true;
                    if (namePlate.AddVisibilityToken(NamePlateVisibilityToken.IN_RANGE))
                        return 3;
                }
            }
        }

        if (!namePlateShown)
        {
            if (namePlate.RemoveVisibilityToken(NamePlateVisibilityToken.IN_RANGE))
                return 1;
        }
        return 0;
    }
}