using ISA.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public class DisableInteractionWhenGrabbed : MonoBehaviour
{

    private List<Hand> disabledHands = new List<Hand>();



    // Magic calls from SteamVR system
    private void OnAttachedToHand(Hand hand)
    {
        SceneManager.INSTANCE.DisableInteraction(hand, this);
        disabledHands.Add(hand);
    }

    // Magic calls from SteamVR system
    private void OnDetachedFromHand(Hand hand)
    {
        SceneManager.INSTANCE.EnableInteraction(hand, this);
        disabledHands.Remove(hand);
    }



    void OnDestroy()
    {
        foreach (Hand hand in disabledHands)
            SceneManager.INSTANCE.EnableInteraction(hand, this);
    }

}
