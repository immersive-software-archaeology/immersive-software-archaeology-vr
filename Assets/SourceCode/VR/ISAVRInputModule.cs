using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

namespace ISA.VR
{

    public class ISAVRInputModule : BaseInputModule
    {

        public float triggerClickFingerDistance = 0.01f;

        private GameObject CurrentObject = null;
        private PointerEventData PointerData = null;

        private GameObject lastPressedObject = null;
        private float lastPressedDistance = -1;



        protected override void Awake()
        {
            base.Awake();
            StartCoroutine(waitUntilPointerAvailable());
        }

        private IEnumerator waitUntilPointerAvailable()
        {
            ISAVRPointer pointer = null;
            while (pointer == null)
            {
                pointer = SceneManager.INSTANCE.GetActivePointer();
                yield return null;
            }

            PointerData = new PointerEventData(eventSystem);
            PointerData.Reset();
            PointerData.position = new Vector2(pointer.eventCamera.pixelWidth / 2f, pointer.eventCamera.pixelHeight / 2f);
        }



        public override void Process()
        {
            if (PointerData == null)
                return;

            try
            {
                eventSystem.RaycastAll(PointerData, m_RaycastResultCache);
            }
            catch(Exception)
            {
                return;
            }
            PointerData.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
            CurrentObject = PointerData.pointerCurrentRaycast.gameObject;

            m_RaycastResultCache.Clear();

            HandlePointerExitAndEnter(PointerData, CurrentObject);

            //if (ClickAction.GetStateDown(TargetInputSource))
            //    ProcessPress();
            //if (ClickAction.GetStateUp(TargetInputSource))
            //    ProcessRelease();

            // very similar code exists in the ISAVRPointer class
            float dist = PointerData.pointerCurrentRaycast.distance;
            if (dist > triggerClickFingerDistance)
            {
                // not pressed
                if (lastPressedObject != null)
                {
                    // just changed from having previously pressed to not pressing
                    ProcessRelease();
                    lastPressedObject = null;
                    lastPressedDistance = -1;
                }
            }
            else
            {
                // currently pressing something
                if (lastPressedObject == null)
                {
                    // just changed from pressing nothing to pressing this thing
                    if (lastPressedDistance >= 0 && lastPressedDistance > dist)
                    {
                        ProcessPress();
                        lastPressedObject = CurrentObject;
                    }
                    lastPressedDistance = dist;
                }
            }

            //if (pointer.WasFingerPressedInLastFrame())
            //if (pointer.WasFingerReleasedInLastFrame())

            ExecuteEvents.ExecuteHierarchy(PointerData.pointerDrag, PointerData, ExecuteEvents.dragHandler);
        }

        public PointerEventData GetData()
        {
            return PointerData;
        }

        private void ProcessPress()
        {
            PointerData.pointerPressRaycast = PointerData.pointerCurrentRaycast;

            // Check for object hit -> get down handler and call
            GameObject newPointerPress = ExecuteEvents.ExecuteHierarchy(CurrentObject, PointerData, ExecuteEvents.pointerDownHandler);
            if (newPointerPress == null)
                newPointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(CurrentObject);

            GameObject newPointerDrag = ExecuteEvents.ExecuteHierarchy(CurrentObject, PointerData, ExecuteEvents.beginDragHandler);
            if (newPointerDrag == null)
                newPointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(CurrentObject);

            //pointer.GetComponent<AudioClipHelper>().Play("press");

            PointerData.pressPosition = PointerData.position;
            PointerData.pointerPress = newPointerPress;
            PointerData.pointerDrag = newPointerDrag;
            PointerData.rawPointerPress = CurrentObject;
        }

        private void ProcessRelease()
        {
            ExecuteEvents.Execute(PointerData.pointerPress, PointerData, ExecuteEvents.pointerUpHandler);

            GameObject pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(CurrentObject);
            if(PointerData.pointerPress == pointerUpHandler)
                ExecuteEvents.Execute(PointerData.pointerPress, PointerData, ExecuteEvents.pointerClickHandler);
            ExecuteEvents.Execute(PointerData.pointerDrag, PointerData, ExecuteEvents.endDragHandler);

            //pointer.GetComponent<AudioClipHelper>().Play("release");
            eventSystem.SetSelectedGameObject(null);

            PointerData.pressPosition = Vector2.zero;
            PointerData.pointerPress = null;
            PointerData.pointerDrag = null;
            PointerData.rawPointerPress = null;
        }
    }

}