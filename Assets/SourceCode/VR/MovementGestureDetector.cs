using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public class MovementGestureDetector : MonoBehaviour
{

    public UnityEvent onGestureDetected = new UnityEvent();

    public Hand hand { get; private set; }

    [SerializeField]
    private float toleranceAngleInDegreesStart = 60;
    [SerializeField]
    private float toleranceAngleInDegreesEnd = 15;
    [SerializeField]
    private float maxTimeBetweenPosesInSeconds = 0.5f;

    private float firstMarkTime = 0;



    private void HandAttachedUpdate(Hand hand)
    {
        if (Vector3.Angle(transform.up, Vector3.down) < toleranceAngleInDegreesStart)
        {
            firstMarkTime = Time.time;
        }

        if (Time.time - firstMarkTime < maxTimeBetweenPosesInSeconds)
        {
            if (Vector3.Angle(transform.up, Vector3.up) < toleranceAngleInDegreesEnd)
                onGestureDetected.Invoke();
        }
    }



    private void OnAttachedToHand(Hand hand)
    {
        this.hand = hand;
        firstMarkTime = -maxTimeBetweenPosesInSeconds;
    }

    private void OnDetachedFromHand(Hand hand)
    {
        this.hand = null;
        firstMarkTime = -maxTimeBetweenPosesInSeconds;
    }

}
