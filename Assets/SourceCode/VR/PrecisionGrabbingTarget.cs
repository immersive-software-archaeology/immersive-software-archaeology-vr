using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PrecisionGrabbingTarget : MonoBehaviour
{
    [SerializeField]
    private string defaultLayer = "Drawing";
    [SerializeField]
    private string grabbedLayer = "Water";

    [SerializeField]
    private Transform transformToLetFollow;
    private int currentGrabCount = 0;



    public void OnPickup()
    {
        currentGrabCount++;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        SetLayer(gameObject, LayerMask.NameToLayer(grabbedLayer));
    }

    public void OnDetach()
    {
        currentGrabCount--;
        if (currentGrabCount == 0)
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            SetLayer(gameObject, LayerMask.NameToLayer(defaultLayer));
        }
    }

    private void SetLayer(GameObject o, int layer)
    {
        if (o.name.Equals("Pen"))
            return;

        o.layer = layer;
        foreach (Transform child in o.transform)
            SetLayer(child.gameObject, layer);
    }



    public void OnHandHeldUpdate()
    {
        if (transformToLetFollow == null)
            return;

        transformToLetFollow.position = transform.position;
        transformToLetFollow.rotation = transform.rotation;
    }

}
