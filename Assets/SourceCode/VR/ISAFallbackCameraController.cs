﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ISAFallbackCameraController : FallbackCameraController
{

	private static List<FallbackCameraGUIExtension> listeners = new List<FallbackCameraGUIExtension>();

	public static void RegisterListener(FallbackCameraGUIExtension listener)
	{
		listeners.Add(listener);
	}

	void OnGUI()
	{
		if (showInstructions)
		{
			GUI.Label(new Rect(10, 10, 400, 400),
				"WASD EQ/Arrow Keys to translate the camera\n" +
				"Right mouse click to rotate the camera\n" +
				"Left mouse click for standard interactions.\n");

			foreach (FallbackCameraGUIExtension l in listeners)
				l.DrawGUI();
		}
	}

}

public interface FallbackCameraGUIExtension
{
    public void DrawGUI();
}