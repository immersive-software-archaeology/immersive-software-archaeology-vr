using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class HeadUIPositioner : MonoBehaviour
{

    [SerializeField]
    private float maxDistForUpdates = 0.5f;
    [SerializeField]
    private float maxAngleForUpdates = 20f;

    [SerializeField]
    private float activationDownLookThreshold = -0.6f;
    [SerializeField]
    private float deactivationDownLookThreshold = -0.5f;

    [SerializeField]
    private float distFromHeadXZ = 0.3f;
    [SerializeField]
    private float distFromHeadY = -0.5f;

    private bool currentlyVisible = true;

    [SerializeField]
    private Transform[] showOnlyWhenPlayerLooksDownwards = new Transform[0];
    private bool[] showOnlyWhenPlayerLooksDownwardsVisibleStates;



    void FixedUpdate()
    {
        Vector3 headPos = Player.instance.headCollider.transform.position;
        Vector3 headLookDirAlongXZ = new Vector3(Player.instance.headCollider.transform.forward.x, 0, Player.instance.headCollider.transform.forward.z).normalized;
        float headLookDirAlongY = Player.instance.headCollider.transform.forward.y;

        Vector3 idealPosition = headPos + headLookDirAlongXZ * distFromHeadXZ + Vector3.up * distFromHeadY;
        Quaternion idealRotation = Quaternion.LookRotation(transform.position - Player.instance.headCollider.transform.position, Player.instance.headCollider.transform.up);

        if (headLookDirAlongY > deactivationDownLookThreshold && currentlyVisible)
        {
            currentlyVisible = false;

            showOnlyWhenPlayerLooksDownwardsVisibleStates = new bool[showOnlyWhenPlayerLooksDownwards.Length];
            for (int i = 0; i < showOnlyWhenPlayerLooksDownwards.Length; i++)
                showOnlyWhenPlayerLooksDownwardsVisibleStates[i] = showOnlyWhenPlayerLooksDownwards[i].gameObject.activeSelf;

            foreach (Transform t in showOnlyWhenPlayerLooksDownwards)
                t.gameObject.SetActive(currentlyVisible);
        }
        if (headLookDirAlongY <= activationDownLookThreshold && !currentlyVisible)
        {
            currentlyVisible = true;
            for (int i = 0; i < showOnlyWhenPlayerLooksDownwards.Length; i++)
                showOnlyWhenPlayerLooksDownwards[i].gameObject.SetActive(showOnlyWhenPlayerLooksDownwardsVisibleStates[i]);

            transform.position = idealPosition;
            transform.rotation = idealRotation;
        }

        if(IsDifferenceTooLarge(idealPosition, idealRotation))
        {
            transform.position = idealPosition;
            transform.rotation = idealRotation;
        }
        else
        {
            transform.position = Vector3.Slerp(transform.position, idealPosition, Mathf.Min(Time.deltaTime * 5f, 1));
            transform.rotation = Quaternion.Slerp(transform.rotation, idealRotation, Mathf.Min(Time.deltaTime * 10f, 1));
        }
    }

    private bool IsDifferenceTooLarge(Vector3 idealPosition, Quaternion idealRotation)
    {
        if (Vector3.Distance(idealPosition, transform.position) > maxDistForUpdates)
            return true;
        if (Quaternion.Angle(idealRotation, transform.rotation) > maxAngleForUpdates)
            return true;
        return false;
    }

}


