using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
[RequireComponent(typeof(Throwable))]
public class ISAInteractableAttachmentEventForwarder : MonoBehaviour
{

    public Interactable forwardInteractable;
    public Throwable forwardThrowable;

    private void OnAttachedToHand(Hand hand)
    {
        if (forwardInteractable == null || forwardThrowable == null)
            return;

        hand.DetachObject(gameObject, GetComponent<Throwable>().restoreOriginalParent);
        hand.AttachObject(forwardInteractable.gameObject, hand.GetBestGrabbingType(), forwardThrowable.attachmentFlags);
    }

    //private void OnDetachedFromHand(Hand hand)
    //{
    //    if (forwardInteractable == null || forwardThrowable == null)
    //        return;

    //    hand.DetachObject(forwardInteractable.gameObject, forwardThrowable.restoreOriginalParent);
    //}

}
