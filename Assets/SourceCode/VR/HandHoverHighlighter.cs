using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;



namespace ISA.VR
{
	[RequireComponent(typeof(Interactable))]
	public class HandHoverHighlighter : MonoBehaviour
	{
		public bool showGrabHint = true;

		public Color HovorColor = new Color(255f / 255f, 144f / 255f, 64f / 255f);
		public Color ActiveColor = Color.white;
		public Color MarkedInWhiteboardColor = new Color(64f / 255f, 176f / 255f, 255f / 255f);
		public float OutlineWidth = 5f;

		private Outline outline;

		private bool active = false;
		private bool handHighlighted = false;
		private bool otherwiseHighlighted = false;
		private bool markedInWhiteboard = false;

		public UnityEvent notifyOnHoverBegin;
		public UnityEvent notifyOnHoverEnd;



		// Magic calls from SteamVR system
		private void OnHandHoverBegin(Hand hand)
		{
			if(showGrabHint)
				hand.ShowGrabHint();

			handHighlighted = true;
			Evaluate();

			notifyOnHoverBegin.Invoke();
		}

		// Magic calls from SteamVR system
		private void OnHandHoverEnd(Hand hand)
		{
			hand.HideGrabHint();
			showGrabHint = false;

			handHighlighted = false;
			Evaluate();

			notifyOnHoverEnd.Invoke();
		}

		public void SetActive(bool state)
		{
			if (active == state)
				return;
			active = state;
			Evaluate();
		}

		public void SetHighlighted(bool state)
		{
			if (otherwiseHighlighted == state)
				return;
			otherwiseHighlighted = state;
			Evaluate();
		}

		public void SetMarkedInWhiteboard(bool state)
        {
			if (markedInWhiteboard == state)
				return;
			markedInWhiteboard = state;
			Evaluate();
		}



		private void Evaluate()
        {
			if (handHighlighted || otherwiseHighlighted)
			{
				activateOutline(HovorColor);
			}
			else if (markedInWhiteboard)
			{
				activateOutline(MarkedInWhiteboardColor);
			}
			else if (active)
			{
				activateOutline(ActiveColor);
			}
			else if (outline != null)
			{
				if(outline.gameObject != null)
					Destroy(outline);
			}
        }

		private void activateOutline(Color c)
		{
			if (outline == null)
				outline = gameObject.GetComponent<Outline>();
			if (outline == null)
				outline = gameObject.AddComponent<Outline>();

			outline.OutlineMode = Outline.Mode.OutlineVisible;
			outline.OutlineWidth = OutlineWidth;
			outline.OutlineColor = c;
		}

	}

}