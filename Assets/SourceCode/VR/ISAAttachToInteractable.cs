using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;



namespace ISA.VR
{
	[RequireComponent(typeof(Interactable))]
	public class ISAAttachToInteractable : MonoBehaviour
	{

		public SteamVR_Input_Sources targetInputSource;
		public GrabTypes grabTypeToListenTo = GrabTypes.None;

		public Transform attachmentOffset;

		public UnityEvent NotifyOnHoverBegin;
		public UnityEvent NotifyOnHoverEnd;

		public UnityEvent NotifyOnAttach;
		public UnityEvent NotifyOnDetach;



		public Hand CurrentHand { get; private set; }

		private Interactable interactableComponent;



		void Start()
		{
			interactableComponent = GetComponent<Interactable>();
		}



		private void OnHandHoverBegin(Hand hand)
		{
			hand.ShowGrabHint();
			CurrentHand = hand;
			NotifyOnHoverBegin.Invoke();
		}

		private void OnHandHoverEnd(Hand hand)
		{
			hand.HideGrabHint();
			CurrentHand = hand;
			NotifyOnHoverEnd.Invoke();
		}



		private void HandHoverUpdate(Hand hand)
		{
            //string side = (hand.handType == SteamVR_Input_Sources.RightHand) ? "right" : "left";
			if (hand.handType != targetInputSource && targetInputSource != SteamVR_Input_Sources.Any)
				return;

            // Grabbing
            GrabTypes grabType = hand.GetGrabStarting();

            //if (grabType != GrabTypes.None)
            //	DebugVR.Log(grabType);

            if ((grabTypeToListenTo != GrabTypes.None && grabType == grabTypeToListenTo) || (grabTypeToListenTo == GrabTypes.None && grabType != GrabTypes.None))
            //if (hand.uiInteractAction.GetStateDown(hand.handType))
            {
				//DebugVR.Log("GRAB STARTED!");

				if (interactableComponent.attachedToHand == null)
				{
					hand.HideGrabHint();
					hand.HoverLock(interactableComponent);

					hand.AttachObject(gameObject, GrabTypes.Grip, Hand.defaultAttachmentFlags, attachmentOffset);
					//hand.AttachObject(gameObject, grabType); //, Hand.AttachmentFlags.DetachFromOtherHand | Hand.AttachmentFlags.SnapOnAttach);
				}
			}

            // Releasing
            else if (hand.IsGrabEnding(gameObject))
            //else if (hand.uiInteractAction.GetStateUp(hand.handType))
			{
				//DebugVR.Log("GRAB ENDED!");

				hand.DetachObject(gameObject, true);
				hand.HoverUnlock(interactableComponent);
			}
		}



		private void OnAttachedToHand(Hand hand)
		{
            //DebugVR.Log("OnAttachedToHand");
            CurrentHand = hand;
			NotifyOnAttach.Invoke();
		}

		private void OnDetachedFromHand(Hand hand)
		{
			//DebugVR.Log("OnDetachedFromHand");
			CurrentHand = hand;
			NotifyOnDetach.Invoke();
		}
	}

}