﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public abstract class PermanentAttachable : MonoBehaviour
{
    [SerializeField]
    protected ActiveToggleable dropzone;

    [SerializeField]
    protected SteamVR_Input_Sources targetInputSource;
    [SerializeField]
    protected GrabTypes grabTypeToListenTo = GrabTypes.None;

    protected Hand hand;
    protected Interactable interactable;

    protected Transform originalParent;
    protected bool attached;
    protected float attachTime;

    private List<Vector3> lastVelocities;



    protected void Start()
    {
        originalParent = transform.parent;
        Init();
    }

    protected abstract void Init();



    private void OnHandHoverBegin(Hand hand)
    {
        if (!attached)
            hand.ShowGrabHint();
    }

    private void OnHandHoverEnd(Hand hand)
    {
        hand.HideGrabHint();
    }

    private void HandHoverUpdate(Hand hand)
    {
        //string side = (hand.handType == SteamVR_Input_Sources.RightHand) ? "right" : "left";
        if (hand.handType != targetInputSource && targetInputSource != SteamVR_Input_Sources.Any)
            return;

        // Grabbing
        GrabTypes grabType = hand.GetGrabStarting();

        if ((grabTypeToListenTo != GrabTypes.None && grabType == grabTypeToListenTo) || (grabTypeToListenTo == GrabTypes.None && grabType != GrabTypes.None))
        {
            this.hand = hand;
            Attach();
        }
    }



    public void Attach()
    {
        OnAttach();

        attached = true;
        attachTime = Time.time;
        lastVelocities = new List<Vector3>();

        dropzone.Show();

        hand.HideGrabHint();
        hand.HoverLock(null);

        hand.AttachObject(gameObject, GrabTypes.Scripted, Hand.AttachmentFlags.ParentToHand | Hand.AttachmentFlags.DetachOthers | Hand.AttachmentFlags.DetachFromOtherHand | Hand.AttachmentFlags.TurnOffGravity | Hand.AttachmentFlags.SnapOnAttach);
    }

    protected abstract void OnAttach();



    public void OnDropZoneEntered()
    {
        if (hand == null)
            return;
        if (Time.time - attachTime < 0.5f)
            return;
        //if (collision.collider.transform.parent == null)
        //    return;
        //if (!collision.collider.transform.parent.Equals(dropzone.transform))
        //    return;

        Detach();
    }

    public void Detach()
    {
        OnDetach();

        attached = false;

        dropzone.Hide();

        hand.DetachObject(gameObject, false);
        hand.HoverUnlock(null);
        transform.parent = originalParent;

        hand = null;

        transform.parent = dropzone.transform.parent;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

    protected abstract void OnDetach();



    protected void DetachIfShakingHand()
    {
        Vector3 velocity = hand.GetTrackedObjectVelocity();
        if (velocity.magnitude < 1)
            return;

        lastVelocities.Add(velocity);
        if (lastVelocities.Count > 10)
            lastVelocities.RemoveAt(0);

        int directionChanges = 0;
        for(int i=0; i<lastVelocities.Count-1; i++)
        {
            Vector3 vel1 = lastVelocities[i];
            Vector3 vel2 = lastVelocities[i+1];

            if (Vector3.Angle(vel1, vel2) > 120)
                directionChanges++;
        }

        if (directionChanges > 2)
            Detach();
    }



}