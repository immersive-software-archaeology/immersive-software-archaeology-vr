using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugVR : MonoBehaviour, FallbackCameraGUIExtension
{

    public static DebugVR INSTANCE;

    public int MaxMessages = 50;

    public Color MetaColor = Color.gray;

    public Color AssertColor = Color.blue;
    public Color ErrorColor = Color.red;
    public Color ExceptionColor = Color.red;
    public Color LogColor = Color.white;
    public Color WarningColor = Color.yellow;

    private Queue<string> currentLines = new Queue<string>();

    [SerializeField]
    private ActiveToggleable consoleRootToggleable;
    [SerializeField]
    private TMP_Text consoleText;



    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
        INSTANCE = this;
    }

    void OnDisable()
    {
        INSTANCE = null;
        Application.logMessageReceived -= HandleLog;
    }

    void Start()
    {
        ISAFallbackCameraController.RegisterListener(this);
        Flush();
#if !UNITY_EDITOR
        consoleRootToggleable.Hide();
#endif
    }



    public void HandleLog(string logString, string stackTrace, LogType type)
    {
        string metaColor = "#" + ColorUtility.ToHtmlStringRGBA(MetaColor);

        string fontColor;
        if (type == LogType.Assert)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(AssertColor);
        else if (type == LogType.Error)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(ErrorColor);
        else if (type == LogType.Exception)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(ExceptionColor);
        else if (type == LogType.Log)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(LogColor);
        else if (type == LogType.Warning)
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(WarningColor);
        else
            fontColor = "#" + ColorUtility.ToHtmlStringRGBA(Color.magenta);

        currentLines.Enqueue("<color=" + metaColor + ">[" + TimeUtil.FormatSecondsInDigitalClockStyle(Time.time, true) + "]</color> <color=" + fontColor + ">" + logString + "</color>");
        if (currentLines.Count > MaxMessages)
            currentLines.Dequeue();

        Flush();
    }

    private void Flush()
    {
        if (INSTANCE == null)
            return;

        string contentAsString = "";
        foreach (string entry in currentLines)
        {
            contentAsString += entry + "\n";
        }
        INSTANCE.consoleText.text = contentAsString;
        INSTANCE.consoleText.GetComponent<RectTransform>().sizeDelta = new Vector2(INSTANCE.consoleText.preferredWidth + 25, INSTANCE.consoleText.preferredHeight + 200);
    }



    public void DrawGUI()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.white;
        style.overflow = new RectOffset(0, 0, 0, 0);
        GUI.TextArea(new Rect(10, 80, 400, Screen.height - 80 - 10), INSTANCE.consoleText.text, style);
    }
}
