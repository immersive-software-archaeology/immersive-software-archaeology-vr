using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace ISA.VR
{

    public class ISAVRPointer : MonoBehaviour
    {
        public LayerMask ignoredLayers;

        public RaycastHit hit;
        public float maxRayLength = 0.1f;

        public GameObject fingerTipVisuals;
        public GameObject rayCastLineObject;
        public GameObject rayCastCrossCounterpartObject;
        public GameObject rayCastCrossObject;
        private bool showCross;

        public ISAVRInputModule inputModule;
        public Hand hand;

        public Camera eventCamera;

        private GameObject lastHovoredObject = null;
        private GameObject lastPressedObject = null;
        private float lastPressedDistance = -1;

        private Dictionary<Hand, HashSet<object>> disabledHands = new Dictionary<Hand, HashSet<object>>();



        void Awake()
        {
            eventCamera.farClipPlane = maxRayLength;
            showCross = rayCastCrossObject != null;

            StartCoroutine(attachToFingerRoutine());
        }

        private IEnumerator attachToFingerRoutine()
        {
            Transform fingerTransform = null;
            while(fingerTransform == null)
            {
                fingerTransform = FindIndexFingerTip(hand.transform);
                yield return null;
            }

            transform.parent = fingerTransform;

            //transform.localPosition = new Vector3(-0.015f, 0.003f, 0);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(45, 90, 0);
            transform.localScale = Vector3.one;

            eventCamera.transform.localPosition = new Vector3(0, -0.015f, -0.015f);

            if (!disabledHands.ContainsKey(hand))
                fingerTipVisuals.SetActive(true);
            else
                fingerTipVisuals.SetActive(false);

            yield return null;
            SceneManager.INSTANCE.RegisterVRPointer(this);
        }

        public static Transform FindIndexFingerTip(Transform t)
        {
            if(t.name.StartsWith("finger_index_") && t.name.EndsWith("_end"))
                return t;
            foreach(Transform child in t)
            {
                Transform result = FindIndexFingerTip(child);
                if (result != null)
                    return result;
            }
            return null;
        }



        public void EnableHard()
        {
            disabledHands.Clear();
            fingerTipVisuals.SetActive(true);
        }

        public void Enable(Hand handToDisable, object blockingObject)
        {
            if(disabledHands.TryGetValue(handToDisable, out HashSet<object> blockingObjects))
            {
                blockingObjects.Remove(blockingObject);
                if(blockingObjects.Count == 0)
                    disabledHands.Remove(handToDisable);
            }

            if (!disabledHands.ContainsKey(hand))
                fingerTipVisuals.SetActive(true);
        }

        public void Disable(Hand handToDisable, object blockingObject)
        {
            if (!disabledHands.TryGetValue(handToDisable, out HashSet<object> blockingObjects))
            {
                blockingObjects = new HashSet<object>();
                disabledHands.Add(handToDisable, blockingObjects);
            }
            blockingObjects.Add(blockingObject);

            fingerTipVisuals.SetActive(false);
        }

        public bool IsDisabled()
        {
            return disabledHands.ContainsKey(hand);
        }





        void Update()
        {
            if (!hand.isActive || !hand.isPoseValid)
                return;
            if (disabledHands.ContainsKey(hand))
                return;

            AdjustCamera();
            Ray ray = new Ray(eventCamera.transform.position, eventCamera.transform.forward);

            float eventSystemRayLength = maxRayLength;
            PointerEventData data = inputModule.GetData();
            if (data != null)
                if (data.pointerCurrentRaycast.distance > 0 && data.pointerCurrentRaycast.distance < eventSystemRayLength)
                    eventSystemRayLength = data.pointerCurrentRaycast.distance;

            Physics.Raycast(ray, out hit, eventSystemRayLength, ~ignoredLayers);

            if (hit.collider != null)
            {
                GameObject currentHitObject = hit.collider.gameObject;

                if (!currentHitObject.Equals(lastHovoredObject))
                {
                    // hovering
                    TriggerHoverEnd(lastHovoredObject);
                    TriggerHoverStart(currentHitObject);
                    lastHovoredObject = currentHitObject;
                }

                // very similar code exists in the ISAVRInputModule class
                float dist = Vector3.Distance(hit.point, eventCamera.transform.position);
                if (dist > inputModule.triggerClickFingerDistance)
                {
                    // not pressed
                    if(lastPressedObject != null)
                    {
                        // just changed from having previously pressed to not pressing
                        TriggerReleased(lastPressedObject);
                        lastPressedObject = null;
                        lastPressedDistance = -1;
                    }
                }
                else
                {
                    // currently pressing something
                    if (lastPressedObject == null)
                    {
                        // just changed from pressing nothing to pressing this thing
                        if(lastPressedDistance >= 0 && lastPressedDistance > dist)
                        {
                            TriggerPressed(currentHitObject);
                            lastPressedObject = currentHitObject;
                        }
                        lastPressedDistance = dist;
                    }
                    else if (!lastPressedObject.Equals(currentHitObject))
                    {
                        // just changed from pressing the other thing to pressing this thing
                        // however, do not trigger a press event, because we only want that to happen when the finger moves in the intended direction etc.

                        //TriggerReleased(lastPressedObject);
                        //TriggerPressed(currentHitObject);
                        //lastPressedObject = currentHitObject;
                    }
                }
            }
            else
            {
                if (lastHovoredObject != null)
                {
                    TriggerHoverEnd(lastHovoredObject);
                    lastHovoredObject = null;
                }

                if (lastPressedObject != null)
                {
                    // just changed from having previously pressed to not pressing
                    TriggerReleased(lastPressedObject);
                    lastPressedObject = null;
                    lastPressedDistance = -1;
                }
            }
        }



        private Collider AdjustCamera()
        {
            if (Player.instance.rightHand.mainRenderModel == null)
                return null;

            Collider bestCollider = null;
            Vector3 bestDirectionToColliderSurface = 1000f * Vector3.one;

            foreach (Collider collider in Physics.OverlapSphere(eventCamera.transform.position, maxRayLength, ~ignoredLayers))
            {
                if (!(collider is BoxCollider || collider is SphereCollider || collider is CapsuleCollider || (collider is MeshCollider && ((MeshCollider) collider).convex == true)))
                    continue;

                Vector3 directionToColliderSurface = collider.ClosestPoint(eventCamera.transform.position) - eventCamera.transform.position;

                float distance = directionToColliderSurface.magnitude;
                if (bestCollider == null || distance < bestDirectionToColliderSurface.magnitude)
                {
                    bestCollider = collider;
                    bestDirectionToColliderSurface = directionToColliderSurface;
                }
            }

            //if (bestCollider != null)
            //{
            //    Debug.Log(transform.name + ": " + bestCollider.name +
            //        ",    dist=" + Mathf.Round(bestDirectionToColliderSurface.magnitude * 100f) / 100f +
            //        ",    dir=" + Mathf.Round(bestDirectionToColliderSurface.normalized.x * 100f) / 100f +
            //            " | " + Mathf.Round(bestDirectionToColliderSurface.normalized.y * 100f) / 100f +
            //            " | " + Mathf.Round(bestDirectionToColliderSurface.normalized.z * 100f) / 100f);

            //    VisualDebugger.SpawnCubeAt(this, transform.position + bestDirectionToColliderSurface, Vector3.one * 0.01f);
            //}

            if (bestCollider != null && bestDirectionToColliderSurface.magnitude > 0 && Vector3.Angle(bestDirectionToColliderSurface.normalized, Vector3.up) != 0)
                eventCamera.transform.rotation = Quaternion.LookRotation(bestDirectionToColliderSurface.normalized, Vector3.up);
            else
                eventCamera.transform.localRotation = Quaternion.identity;

            return bestCollider;
        }



        private void TriggerPressed(GameObject obj)
        {
            //Debug.Log("normal: PRESSED!");
            ISAUIInteractable interactable = obj.GetComponent<ISAUIInteractable>();
            if (interactable != null)
            {
                interactable.onTriggerPressed.Invoke();
                GetComponent<AudioClipHelper>().Play("press");
            }
        }

        private void TriggerReleased(GameObject obj)
        {
            //Debug.Log("normal: released...");
            ISAUIInteractable interactable = obj.GetComponent<ISAUIInteractable>();
            if (interactable != null)
            {
                interactable.onTriggerReleased.Invoke();
                GetComponent<AudioClipHelper>().Play("release");
            }
        }



        private void TriggerHoverStart(GameObject obj)
        {
            ISAUIInteractable interactable = obj.GetComponent<ISAUIInteractable>();
            if (interactable != null)
                interactable.onHoverStart.Invoke();
        }

        private void TriggerHoverEnd(GameObject obj)
        {
            if (obj != null)
            {
                ISAUIInteractable lastInteractable = obj.GetComponent<ISAUIInteractable>();
                if (lastInteractable != null)
                    lastInteractable.onHoverEnd.Invoke();
            }
        }



        public void ResetHovering()
        {
            lastHovoredObject = null;
        }
    }

}