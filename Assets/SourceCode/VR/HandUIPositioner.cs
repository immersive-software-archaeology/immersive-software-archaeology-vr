﻿using ISA.Modelling;
using System.Collections;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class HandUIPositioner : MonoBehaviour
{

    [SerializeField]
    private SteamVR_Action_Boolean[] actionsForUIOpen;

    [SerializeField]
    private Transform[] transformsToToggleAlways = new Transform[0];
    [SerializeField]
    private Transform[] transformsToToggleWhenInSystem = new Transform[0];
    [SerializeField]
    private Transform[] transformsToMirror = new Transform[0];

    [SerializeField]
    private WhiteboardSpawnBehavior whiteboardSpawner;

    private bool currentlyOpen = false;
    private bool currentlyMirrored = false;



    private void Start()
    {
        Toggle(false);
    }

    private void Toggle(bool visible)
    {
        currentlyOpen = visible;

        foreach (Transform trans in transformsToToggleAlways)
            trans.gameObject.SetActive(visible);

        bool currentlyInSystem = ModelLoader.instance != null && ModelLoader.instance.VisualizationModel != null;
        foreach (Transform trans in transformsToToggleWhenInSystem)
            trans.gameObject.SetActive(visible && currentlyInSystem);

        //if (!visible)
        //    whiteboardSpawner.Reset(true);
    }



    private void Update()
    {
        if (Player.instance == null)
            return;
        if (Player.instance.hands == null || Player.instance.hands.Length == 0)
            return;

        ProcessActions();
        
        Quaternion perfectRotation = Quaternion.LookRotation(transform.position - Player.instance.headCollider.transform.position, Player.instance.headCollider.transform.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, perfectRotation, Mathf.Min(1, Time.deltaTime * 5f));
    }

    private void ProcessActions()
    {
        foreach (SteamVR_Action_Boolean action in actionsForUIOpen)
        {
            foreach (Hand hand in Player.instance.hands)
            {
                if (hand.handType == SteamVR_Input_Sources.Any)
                    // Ignore this, because otherwise fallback objects overwrite all other checks
                    continue;
                if (hand.AttachedObjects.Count > 0)
                    // Hand is currently holding something, do not open menu here
                    continue;

                if (action.GetStateDown(hand.handType))
                {
                    if (!currentlyOpen)
                        OpenUI(hand);
                    else
                        CloseUI();
                    return;
                }
            }
        }
    }



    private void OpenUI(Hand hand)
    {
        transform.parent = hand.objectAttachmentPoint;
        transform.localPosition = Vector3.up * -0.1f;
        transform.rotation = Quaternion.LookRotation(transform.position - Player.instance.headCollider.transform.position, Player.instance.headCollider.transform.up);

        Toggle(true);

        bool mirror = hand.handType == SteamVR_Input_Sources.RightHand;
        if (mirror == currentlyMirrored)
            return;
        currentlyMirrored = mirror;

        foreach (Transform trans in transformsToMirror)
        {
            Vector3 pos = trans.localPosition;
            Vector3 rot = trans.localRotation.eulerAngles;
            trans.localPosition = new Vector3(-pos.x, pos.y, pos.z);
            trans.localRotation = Quaternion.Euler(new Vector3(rot.x, -rot.y, -rot.z));
        }
    }

    private void CloseUI()
    {
        Toggle(false);
    }

}