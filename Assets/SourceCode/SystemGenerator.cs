using ISA.Modelling;
using ISA.Modelling.Visualization;
using ISA.UI;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;

public class SystemGenerator
{
    public List<AbstractVisualElement3D> AllElementsInBottomUpOrder { private set; get; }

    // These are sorted in a sense of being created in depth-search manner
    private Dictionary<ISANamedVisualElement, TransparentCapsule3D> allCapsules;
    private Dictionary<ISANamedVisualElement, SpaceStation3D> allStations;
    private Dictionary<ISANamedVisualElement, SpaceStationAntenna3D> allAntennas;
    private Dictionary<ISANamedVisualElement, SpaceStationBlock3D> allBlocks;
    private List<ISANamedVisualElement> allLibraryElements;

    private Dictionary<ISATransparentCapsule, Vector2> hueSaturationColorMap;

    private GameObject transparentCapsulePrefab;
    private GameObject spaceStationPrefab;
    private GameObject spaceStationSatellitePrefab;
    private GameObject antennaPrefab;
    private GameObject blockPrefab;



    public SystemGenerator()
    {
        allCapsules = new Dictionary<ISANamedVisualElement, TransparentCapsule3D>();
        allStations = new Dictionary<ISANamedVisualElement, SpaceStation3D>();
        allAntennas = new Dictionary<ISANamedVisualElement, SpaceStationAntenna3D>();
        allBlocks = new Dictionary<ISANamedVisualElement, SpaceStationBlock3D>();
        allLibraryElements = new List<ISANamedVisualElement>();

        hueSaturationColorMap = new Dictionary<ISATransparentCapsule, Vector2>();

        transparentCapsulePrefab = Resources.Load("Models/Visualization/TransparentCapsule Prefab") as GameObject;
        spaceStationPrefab = Resources.Load("Models/Visualization/SpaceStation Prefab") as GameObject;
        spaceStationSatellitePrefab = Resources.Load("Models/Visualization/SpaceStation Satellite Prefab") as GameObject;
        antennaPrefab = Resources.Load("Models/Visualization/Antenna Prefab") as GameObject;
        blockPrefab = Resources.Load("Models/Visualization/Block Prefab") as GameObject;

        TransparentCapsule3D.LoadMaterialsFromDisk();
        SpaceStationAntenna3D.LoadMaterialsFromDisk();
        SpaceStationBlock3D.LoadMaterialsFromDisk();
    }



    private void InitializeListOfAllElementsInBottomUpOrder()
    {
        List<AbstractVisualElement3D> allElements = new List<AbstractVisualElement3D>();
        allElements.AddRange(allAntennas.Values);
        allElements.AddRange(allBlocks.Values);
        allElements.AddRange(allStations.Values);
        allElements.AddRange(allCapsules.Values);
        foreach (TransparentCapsule3D capsule in allCapsules.Values)
            allElements.Add(capsule.directContainment);

        AllElementsInBottomUpOrder = new List<AbstractVisualElement3D>();
        int currentLevel = 0;
        while (AllElementsInBottomUpOrder.Count != allElements.Count)
        {
            foreach (AbstractVisualElement3D element in allElements)
            {
                if (element.GetNestingLevel() == currentLevel)
                    AllElementsInBottomUpOrder.Add(element);
            }
            currentLevel++;

            if (currentLevel == 100)
            {
                Debug.LogError("Creation of bottom up list of elements does not finish!");
                Debug.LogError("Elements missing: " + (allElements.Count - AllElementsInBottomUpOrder.Count));
                //foreach(AbstractVisualElement3D el in allElements)
                //    if(!AllElementsInBottomUpOrder.Contains(el))
                //        Debug.LogError("\t> " + el);
                break;
            }
        }

        AllElementsInBottomUpOrder.Reverse();
    }

    public AbstractVisualElement3D getElement3D(ISANamedVisualElement ecoreElement)
    {
        if (allCapsules.ContainsKey(ecoreElement))
            return allCapsules[ecoreElement];
        if (allStations.ContainsKey(ecoreElement))
            return allStations[ecoreElement];
        if (allBlocks.ContainsKey(ecoreElement))
            return allBlocks[ecoreElement];
        if (allAntennas.ContainsKey(ecoreElement))
            return allAntennas[ecoreElement];
        return null;

        // The library generation iss commented out for now... (kill your darlings)
        //if (allLibraryElements.Contains(ecoreElement))
        //    return null;
        //throw new Exception("Cannot find a 3D element for ecore element " + ecoreElement.name + " (" + ecoreElement.GetType().Name + ")");
    }



    public IEnumerator GenerateSystem(ISAVisualizationModel model)
    {
        float startTime = Time.time;
        Debug.Log("Generation for \"" + model.name + "\" started...");

        InitializeColorMap(model);

        //GenerateLibraryCapsule(model.libraryCapsules[0], null, out TransparentCapsule3D libRootCapsule);
        //libRootCapsule.gameObject.SetActive(false);

        AbstractVisualElement3D.softwareElementLastGeneratedId = 0;
        yield return SceneManager.INSTANCE.StartCoroutine(GenerateModuleCapsule(model.subjectSystemCapsule, null));

        InitializeListOfAllElementsInBottomUpOrder();
        yield return null;

        float geometryGenerationTime = Time.time - startTime;

        // Initialize the bounding volumes and positions of all capsules
        TableManager.INSTANCE.StartNewTask("Initializing Interaction System...", AllElementsInBottomUpOrder.Count);
        int elementsPerLoopRun = 100;
        for (int i = 0; i < AllElementsInBottomUpOrder.Count; i++)
        {
            AllElementsInBottomUpOrder[i].InitializeInteractionSystem();
            if (AllElementsInBottomUpOrder[i] is TransparentCapsule3D)
                ((TransparentCapsule3D)AllElementsInBottomUpOrder[i]).FinalizeLayout();

            if (i % elementsPerLoopRun == elementsPerLoopRun-1)
            {
                TableManager.INSTANCE.IncreaseGenerationCount(elementsPerLoopRun);
                yield return null;
            }
        }

        float interactionInitTime = Time.time - startTime - geometryGenerationTime;
        yield return null;

        // Loop over all elements and set relations
        // Do it after the generation so that all information are available
        TableManager.INSTANCE.StartNewTask("Initializing Relationships...", AllElementsInBottomUpOrder.Count);
        ModelReferenceResolver refResolver = new ModelReferenceResolver(model);
        for (int i = 0; i < AllElementsInBottomUpOrder.Count; i++)
        {
            AllElementsInBottomUpOrder[i].InitializeRelations(this, refResolver);

            if (i % elementsPerLoopRun == elementsPerLoopRun - 1)
            {
                TableManager.INSTANCE.IncreaseGenerationCount(elementsPerLoopRun);
                yield return null;
            }
        }

        float relationsInitTime = Time.time - startTime - geometryGenerationTime - interactionInitTime;

        TableManager.INSTANCE.StartNewTask("Activating elements...", allCapsules.Count + allStations.Count);

        SceneManager.INSTANCE.RootTransform.localScale = SceneManager.INSTANCE.startScaleFactor * Vector3.one;
        SceneManager.INSTANCE.RootTransform.position = (1f + allCapsules[model.subjectSystemCapsule].GetCurrentWorldSpaceRadius()) * SceneManager.INSTANCE.RootTransform.forward;

        //bool farEnough = false;
        //while (!farEnough)
        //{
        //    farEnough = true;
        //    foreach (TransparentCapsule3D capsule in allCapsules.Values)
        //    {
        //        //if (capsule.directContainment.containedSpaceStations.Count == 0)
        //        //    continue;
        //        if (Vector3.Distance(TableManager.INSTANCE.transform.position, capsule.transform.position) < capsule.BoundingVolume.radius * SceneManager.INSTANCE.startScaleFactor + 1f)
        //        {
        //            farEnough = false;
        //            SceneManager.INSTANCE.RootTransform.position += SceneManager.INSTANCE.RootTransform.forward * 0.1f;
        //            break;
        //        }
        //    }
        //}
        SceneManager.INSTANCE.RootTransform.position += Vector3.down * 100f;

        List<AbstractVisualElement3D> elementsToActivate = new List<AbstractVisualElement3D>();
        foreach (Transform rootTransform in SceneManager.INSTANCE.RootTransform)
        {
            if (rootTransform.TryGetComponent(out TransparentCapsule3D capsule))
                CollectElementsToActivateInOrder(capsule, elementsToActivate);
            yield return null;
        }
        elementsPerLoopRun = 3;
        for (int i = 0; i < elementsToActivate.Count; i++)
        {
            elementsToActivate[i].gameObject.SetActive(true);

            if (i % elementsPerLoopRun == elementsPerLoopRun - 1)
            {
                TableManager.INSTANCE.IncreaseGenerationCount(elementsPerLoopRun);
                yield return null;
            }
        }
        SceneManager.INSTANCE.RootTransform.position += Vector3.up * (100f + 1.2f);

        float activationTime = Time.time - startTime - geometryGenerationTime - interactionInitTime - relationsInitTime;

        TransparentCapsule3D rootCapsule = allCapsules[model.subjectSystemCapsule];
        OpenTopMostCapsules(rootCapsule, 0);

        SceneManager.INSTANCE.NotifyFinishedLoading(AllElementsInBottomUpOrder);
        Debug.Log("Generation finished. Total Time: " + TimeUtil.FormatSecondsInDigitalClockStyle(Time.time - startTime, true) + "\n" +
            "\t> Generating Geometry: " + TimeUtil.FormatSecondsInDigitalClockStyle(geometryGenerationTime, true) + "\n" +
            "\t> Initializing Interaction System: " + TimeUtil.FormatSecondsInDigitalClockStyle(interactionInitTime, true) + "\n" +
            "\t> Initializing Relationships: " + TimeUtil.FormatSecondsInDigitalClockStyle(relationsInitTime, true) + "\n" +
            "\t> Activating Elements: " + TimeUtil.FormatSecondsInDigitalClockStyle(activationTime, true));
    }

    private void OpenTopMostCapsules(TransparentCapsule3D currentCapsule, int depthToOpen)
    {
        currentCapsule.OpenLocally();
        if(depthToOpen > 0)
            foreach(TransparentCapsule3D subCapsule in currentCapsule.containedSubCapsules)
                OpenTopMostCapsules(subCapsule, depthToOpen-1);
    }

    private void CollectElementsToActivateInOrder(AbstractVisualElement3D element, List<AbstractVisualElement3D> elementsToActivate)
    {
        elementsToActivate.Add(element);

        if (element is SpaceStation3D)
        {
            SpaceStation3D station = (SpaceStation3D)element;
            foreach (SpaceStation3D subStation in station.SatelliteStations)
                CollectElementsToActivateInOrder(subStation, elementsToActivate);
        }
        else if (element is TransparentCapsule3D)
        {
            TransparentCapsule3D capsule = (TransparentCapsule3D)element;
            foreach (SpaceStation3D station in capsule.directContainment.containedSpaceStations)
                CollectElementsToActivateInOrder(station, elementsToActivate);
            foreach (TransparentCapsule3D childCapsule in capsule.containedSubCapsules)
                CollectElementsToActivateInOrder(childCapsule, elementsToActivate);
        }
    }

    private void InitializeColorMap(ISAVisualizationModel model)
    {
        List<ISATransparentCapsule> leafCapsules = new List<ISATransparentCapsule>();
        collectLeafCapsulesAndSetNestingDepth(leafCapsules, model.subjectSystemCapsule, 0);

        float deepestNesting = 1;
        foreach (ISATransparentCapsule leafCapsule in leafCapsules)
        {
            if (hueSaturationColorMap[leafCapsule].y > deepestNesting)
                deepestNesting = hueSaturationColorMap[leafCapsule].y;
        }

        for (int i=0; i<leafCapsules.Count; i++)
        {
            ISATransparentCapsule leafCapsule = leafCapsules[i];
            hueSaturationColorMap[leafCapsule] = new Vector2((float)i / (float)leafCapsules.Count, hueSaturationColorMap[leafCapsule].y / deepestNesting * 0.75f);
        }

        applyColors(model.subjectSystemCapsule, 0, deepestNesting);
    }

    private void collectLeafCapsulesAndSetNestingDepth(List<ISATransparentCapsule> leafCapsules, ISATransparentCapsule currentCapsule, int depth)
    {
        if (currentCapsule.subCapsules == null || currentCapsule.subCapsules.Length == 0)
            leafCapsules.Add(currentCapsule);
        else
            foreach(ISATransparentCapsule child in currentCapsule.subCapsules)
                collectLeafCapsulesAndSetNestingDepth(leafCapsules, child, depth + 1);

        hueSaturationColorMap.Add(currentCapsule, new Vector2(-1, depth));
    }

    private void applyColors(ISATransparentCapsule currentCapsule, int depth, float deepestNesting)
    {
        if (hueSaturationColorMap[currentCapsule].x >= 0)
            return;

        float averageChildHue = 0;
        foreach (ISATransparentCapsule child in currentCapsule.subCapsules)
        {
            applyColors(child, depth + 1, deepestNesting);
            averageChildHue += hueSaturationColorMap[child].x;
        }

        averageChildHue /= (float)currentCapsule.subCapsules.Length;
        hueSaturationColorMap[currentCapsule] = new Vector2(averageChildHue, depth / deepestNesting * 0.75f);
    }


    public IEnumerator GenerateModuleCapsule(ISATransparentCapsule capsuleEcore, TransparentCapsule3D parentCapsule3D)
    {
        GameObject capsuleGameObject = UnityEngine.Object.Instantiate(transparentCapsulePrefab);
        capsuleGameObject.transform.parent = SceneManager.INSTANCE.RootTransform;
        capsuleGameObject.SetActive(false);

        TransparentCapsule3D generatedCapsule3D = capsuleGameObject.GetComponent<TransparentCapsule3D>();
        generatedCapsule3D.Initialize(capsuleEcore, parentCapsule3D, hueSaturationColorMap[capsuleEcore]);

        allCapsules.Add(capsuleEcore, generatedCapsule3D);

        foreach (ISATransparentCapsule subCapsuleEcore in capsuleEcore.subCapsules ?? new ISATransparentCapsule[0])
        {
            yield return SceneManager.INSTANCE.StartCoroutine(GenerateModuleCapsule(subCapsuleEcore, generatedCapsule3D));
        }

        foreach (ISASpaceStation stationEcore in capsuleEcore.spaceStations ?? new ISASpaceStation[0])
        {
            yield return SceneManager.INSTANCE.StartCoroutine(GenerateSpaceStation(stationEcore, generatedCapsule3D));
        }

        generatedCapsule3D.InitializeGeometry();
        if (parentCapsule3D != null)
            parentCapsule3D.AddSubCapsule(generatedCapsule3D);
    }

    private IEnumerator GenerateSpaceStation(ISASpaceStation stationEcore, AbstractVisualElement3D parentElement3D)
    {
        GameObject spaceStationGameObject;
        if (parentElement3D is SpaceStation3D)
            spaceStationGameObject = UnityEngine.Object.Instantiate(spaceStationSatellitePrefab);
        else
            spaceStationGameObject = UnityEngine.Object.Instantiate(spaceStationPrefab);
        spaceStationGameObject.SetActive(false);
        SpaceStation3D generatedStation3D = spaceStationGameObject.GetComponent<SpaceStation3D>();
        generatedStation3D.Initialize(stationEcore, parentElement3D);

        ElementUpdater.instance.AddSoftwareElement(generatedStation3D);

        allStations.Add(stationEcore, generatedStation3D);

        foreach (ISASpaceStation satelliteStationEcore in stationEcore.satelliteStations ?? new ISASpaceStation[0])
        {
            yield return SceneManager.INSTANCE.StartCoroutine(GenerateSpaceStation(satelliteStationEcore, generatedStation3D));
        }

        foreach (ISASpaceStationAntenna antennaEcore in stationEcore.antennas ?? new ISASpaceStationAntenna[0])
        {
            GameObject antennaGameObject = UnityEngine.Object.Instantiate(antennaPrefab);
            SpaceStationAntenna3D antenna3D = antennaGameObject.GetComponent<SpaceStationAntenna3D>();
            antenna3D.Initialize(antennaEcore, generatedStation3D);

            allAntennas.Add(antennaEcore, antenna3D);
            generatedStation3D.AddAttributeAntenna(antenna3D);

            antenna3D.InitializeGeometry();
        }

        foreach (ISASpaceStationBlock blockEcore in stationEcore.blocks ?? new ISASpaceStationBlock[0])
        {
            GameObject blockGameObject = UnityEngine.Object.Instantiate(blockPrefab);
            SpaceStationBlock3D block3D = blockGameObject.GetComponent<SpaceStationBlock3D>();
            block3D.Initialize(blockEcore, generatedStation3D);

            allBlocks.Add(blockEcore, block3D);
            generatedStation3D.AddFunctionBlock(block3D);

            block3D.InitializeGeometry();
        }

        generatedStation3D.InitializeGeometry();

        if (parentElement3D is SpaceStation3D)
            ((SpaceStation3D)parentElement3D).AddSatelliteStation(generatedStation3D);
        else if (parentElement3D is TransparentCapsule3D)
            ((TransparentCapsule3D)parentElement3D).AddSpaceStation(generatedStation3D);
        else throw new InvalidCastException("Unexpected type of station parent: " + parentElement3D.GetType().Name);

        TableManager.INSTANCE.IncreaseGenerationCount(1);
        yield return null;
    }

}