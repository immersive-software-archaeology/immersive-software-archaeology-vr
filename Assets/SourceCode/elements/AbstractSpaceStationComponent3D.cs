﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.Modelling.Visualization;
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractSpaceStationComponent3D : AbstractVisualElement3D
{
    public ISAAbstractSpaceStationComponent ecoreModel { get; private set; }

    public bool isPublic { get; protected set; }



    public void Initialize(ISAAbstractSpaceStationComponent ecoreElement)
    {
        base.Initialize(ecoreElement);
        this.ecoreModel = ecoreElement;
    }



    public override void InitializeRelations(SystemGenerator generator, ModelReferenceResolver referenceResolver)
    {
        foreach (ISASpaceStationRelation relation in ecoreModel.relationsToSpaceStationCenters ?? new ISASpaceStationRelation[0])
            RegisterRelation(generator, referenceResolver, RelationType.TYPE_REFERENCE, relation.target, relation.weight);

        foreach (ISABlockRelation relation in ecoreModel.relationsToBlocks ?? new ISABlockRelation[0])
            RegisterRelation(generator, referenceResolver, RelationType.METHOD_CALL, relation.target, relation.weight);

        foreach (ISAAntennaRelation relation in ecoreModel.relationsToAntennas ?? new ISAAntennaRelation[0])
            RegisterRelation(generator, referenceResolver, RelationType.FIELD_ACCESS, relation.target, relation.weight);
    }
}
