﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.Modelling.Visualization;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class SpaceStation3D : BoundInteractableMultiViewElement3D
{
    public ISASpaceStation ecoreModel { get; private set; }

    public List<SpaceStation3D> SatelliteStations { get; private set; }
    public List<SpaceStationAntenna3D> Antennas { get; private set; }
    public List<SpaceStationBlock3D> Blocks { get; private set; }

    private Transform BaseTransform;

    private CapsuleCollider SimpleCollider;
    private MeshCollider BaseTransformCollider;
    private MeshCollider MeshTransformCollider;

    public SoftwareElementInfoCanvas infoCanvas { private set; get; }

    public float MaxDiameter { get; private set; }
    public float PublicHeight { get; private set; }
    public float PrivateHeight { get; private set; }
    public float TotalHeight { get; private set; }

    public static readonly float MARGIN_TO_NEIGHBOR_STATIONS = 10f;

    private static readonly float CENTER_BAR_WIDTH = 0.5f;
    private static readonly float BASE_DIAMETER = 5f;
    private static readonly float BASE_HEIGHT = 5f;
    private static readonly float GAP_BETWEEN_BLOCKS = 0.25f;

    private bool isVisible = true;
    private bool isSimpleColliderMode = false;
    private bool isHighLOD = true;

    private Material material;



    public void Initialize(ISASpaceStation ecoreModel, AbstractVisualElement3D parentElement3D)
    {
        base.Initialize(ecoreModel);

        this.ecoreModel = ecoreModel;
        this.SetParent(parentElement3D);

        SatelliteStations = new List<SpaceStation3D>();
        Antennas = new List<SpaceStationAntenna3D>();
        Blocks = new List<SpaceStationBlock3D>();
    }



    public void AddSatelliteStation(SpaceStation3D newChild)
    {
        SatelliteStations.Add(newChild);
        newChild.SetParent(this);
    }

    public void AddAttributeAntenna(SpaceStationAntenna3D antenna3D)
    {
        Antennas.Add(antenna3D);
    }

    public void AddFunctionBlock(SpaceStationBlock3D block)
    {
        Blocks.Add(block);
    }

    public override bool Contains(AbstractVisualElement3D element)
    {
        foreach (SpaceStation3D station in SatelliteStations)
        {
            if (station.Equals(element))
                return true;
            if (station.Contains(element))
                return true;
        }

        if (element is SpaceStationAntenna3D && Antennas.Contains((SpaceStationAntenna3D)element))
            return true;
        if (element is SpaceStationBlock3D && Blocks.Contains((SpaceStationBlock3D)element))
            return true;

        return false;
    }

    public override void ChangeVisibility(bool visible)
    {
        isVisible = visible;

        MeshTransform.GetComponent<Renderer>().enabled = isVisible;
        BaseTransform.GetComponent<Renderer>().enabled = isVisible;

        MeshTransform.GetComponent<Collider>().enabled = isVisible; // && !isSimpleColliderMode;
        BaseTransform.GetComponent<Collider>().enabled = isVisible && !isSimpleColliderMode;
        SimpleCollider.enabled = isVisible && isSimpleColliderMode;

        foreach (SpaceStation3D satellite in SatelliteStations)
        {
            satellite.ChangeVisibility(visible);
        }
        foreach (SpaceStationBlock3D block in Blocks)
        {
            block.MeshTransform.GetComponent<Renderer>().enabled = isVisible;
            block.collider.enabled = isVisible && !isSimpleColliderMode;
        }
        foreach (SpaceStationAntenna3D antenna in Antennas)
        {
            antenna.CylinderTransform.GetComponent<Renderer>().enabled = isVisible;
            antenna.collider.enabled = isVisible && !isSimpleColliderMode;
        }

        if (isVisible)
            proximityVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
        else
            proximityVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
    }



    public override void InitializeGeometry()
    {
        ISA.Modelling.Visualization.ISAVector3 positionEcore = ecoreModel.position;
        if (positionEcore != null)
        {
            // TODO
            // set position from information already present in model
        }

        float baseDiameter = BASE_DIAMETER;
        if (directParent is SpaceStation3D)
            baseDiameter /= 2f;
        gameObject.name = ToString();

        Throwable throwable = gameObject.GetComponent<Throwable>();
        throwable.onHeldUpdate.AddListener(delegate { ISAUIInteractionManager.INSTANCE.PositionChanged(this); });
        throwable.onPickUp.AddListener(delegate { ISAUIInteractionManager.INSTANCE.PositionChanged(this); });

        MaxDiameter = baseDiameter;
        float averageDiameter = 0;
        mass = Mathf.PI * Mathf.Pow(baseDiameter / 2, 2) * BASE_HEIGHT;

        PublicHeight = GAP_BETWEEN_BLOCKS;
        PrivateHeight = GAP_BETWEEN_BLOCKS;

        Blocks.Reverse();
        // public functions / methods
        {
            foreach (SpaceStationBlock3D functionBlock in Blocks)
            {
                if (!functionBlock.isPublic)
                    continue;

                functionBlock.gameObject.transform.parent = gameObject.transform;
                functionBlock.gameObject.transform.localPosition = (BASE_HEIGHT / 2 + PublicHeight + functionBlock.height / 2) * Vector3.up;

                mass += functionBlock.mass;

                PublicHeight += functionBlock.height + GAP_BETWEEN_BLOCKS;
                averageDiameter += functionBlock.diameter / Blocks.Count;
                if (functionBlock.diameter > MaxDiameter)
                    MaxDiameter = functionBlock.diameter;
            }
        }

        Blocks.Reverse();
        // private functions / methods
        {
            foreach (SpaceStationBlock3D functionBlock in Blocks)
            {
                if (functionBlock.isPublic)
                    continue;

                functionBlock.gameObject.transform.parent = gameObject.transform;
                functionBlock.gameObject.transform.localPosition = (BASE_HEIGHT / 2 + PrivateHeight + functionBlock.height / 2) * Vector3.down;

                mass += functionBlock.mass;

                PrivateHeight += functionBlock.height + GAP_BETWEEN_BLOCKS;
                averageDiameter += functionBlock.diameter / Blocks.Count;
                if (functionBlock.diameter > MaxDiameter)
                    MaxDiameter = functionBlock.diameter;
            }
        }

        TotalHeight = PrivateHeight + PublicHeight + BASE_HEIGHT;

        // attribute / field / enum constant antennas
        {
            //foreach (AttributeAntenna3D antenna in attributeAntennas)
            for (int i = 0; i < Antennas.Count; i++)
            {
                SpaceStationAntenna3D antenna = Antennas[i];

                antenna.gameObject.transform.parent = gameObject.transform;
                antenna.gameObject.transform.localPosition = Vector3.up * (BASE_HEIGHT / 2f - 1f);
                antenna.gameObject.transform.rotation = Quaternion.Euler(0, (float)i / (float)Antennas.Count * 360f, 0);

                antenna.MeshTransform.localPosition = Vector3.forward * baseDiameter / 2;
                antenna.incomingRelationsHookTransform.localPosition = Vector3.forward * (baseDiameter / 2 + antenna.MeshTransform.localScale.z);
                antenna.outgoingRelationsHookTransform.localPosition = Vector3.forward * (baseDiameter / 2 + antenna.MeshTransform.localScale.z);

                mass += antenna.mass;
            }
        }

        proximityVisibleNamePlate = gameObject.transform.Find("NameTag").GetComponent<NamePlateManager>();
        proximityVisibleNamePlate.transform.localPosition = Vector3.up * (BASE_HEIGHT + PublicHeight);
        proximityVisibleNamePlate.offsetY = MaxDiameter;
        proximityVisibleNamePlate.offsetZ = 0; //MaxDiameter / 2f;
        proximityVisibleNamePlate.SetClassifierName(simpleName);

        ElementUpdater.instance.AddSoftwareNamePlate(proximityVisibleNamePlate, transform);

        MeshTransform = gameObject.transform.Find("Mesh");
        BaseTransform = gameObject.transform.Find("Base");

        MeshRenderer baseRenderer = GeometryGenerationUtil.GenerateCylinder(BaseTransform.gameObject, 8, true, BASE_HEIGHT / 5f, Mathf.PI * baseDiameter / 10f);
        BaseTransform.localScale = new Vector3(baseDiameter, BASE_HEIGHT / 2, baseDiameter);
        BaseTransformCollider = BaseTransform.GetComponent<MeshCollider>();

        MeshRenderer meshRenderer = GeometryGenerationUtil.GenerateCylinder(MeshTransform.gameObject, 8, true, TotalHeight / 5f, Mathf.PI * 0.5f / 10f);
        MeshTransform.localScale = new Vector3(CENTER_BAR_WIDTH, TotalHeight / 2, CENTER_BAR_WIDTH);
        MeshTransform.localPosition = Vector3.up * (PublicHeight - PrivateHeight) / 2f;
        MeshTransformCollider = MeshTransform.GetComponent<MeshCollider>();

        incomingRelationsHookTransform = gameObject.transform.Find("IncomingRelationsHookTransform");
        incomingRelationsHookTransform.localPosition = Vector3.up * (MeshTransform.localScale.y + MeshTransform.localPosition.y);
        outgoingRelationsHookTransform = gameObject.transform.Find("OutgoingRelationsHookTransform");
        outgoingRelationsHookTransform.localPosition = Vector3.up * (MeshTransform.localScale.y + MeshTransform.localPosition.y);

        SimpleCollider = GetComponent<CapsuleCollider>();
        SimpleCollider.center = MeshTransform.localPosition;
        SimpleCollider.height = TotalHeight;
        SimpleCollider.radius = MaxDiameter / 2f;

        if (ecoreModel.type == ISASpaceStationType.INTERFACE)
        {
            material = GetParentCapsule().AbstractStationsMaterialInCapsuleColor;
            material.SetFloat("_WireThickness", 500);
            meshRenderer.enabled = false;
        }
        else
        {
            material = GetParentCapsule().ConcreteStationsMaterialInCapsuleColor;
            meshRenderer.material = material;
        }
        baseRenderer.material = material;

        Transform pinPreview = gameObject.transform.Find("PinPreview");
        if(pinPreview != null)
            pinPreview.localPosition = MeshTransform.localPosition + Vector3.down * (TotalHeight / 2 + 2);

        // Set the bounding volume, might be overriden by what follows beneath, but this is needed either way!
        {
            float additionalSpaceForAntennas = 0;
            foreach (SpaceStationAntenna3D antenna in Antennas)
            {
                additionalSpaceForAntennas = SpaceStationAntenna3D.PRIVATE_LENGTH;
                if (antenna.isPublic)
                {
                    additionalSpaceForAntennas = SpaceStationAntenna3D.PUBLIC_LENGTH;
                    break;
                }
            }
            BoundingVolume = new Sphere(gameObject.transform.position, MaxDiameter / 2 + additionalSpaceForAntennas); // + MARGIN_TO_NEIGHBOR_STATIONS);
        }

        if (SatelliteStations.Count > 0)
        {
            LayoutUtil.LayoutElementsWithEvenMargins(SatelliteStations, gameObject.transform, MARGIN_TO_NEIGHBOR_STATIONS, BoundingVolume.radius);
        }

        RecalculateBoundingSphereSimple(false);

        SetLevelOfDetail(false);
        EnableDetailedColliders(false);
    }

    //public void SetColor(Color color)
    //{
    //    material.SetColor("_BaseColor", color);
    //    material.SetColor("_WireColor", color);
    //}

    public bool SetLevelOfDetail(bool showDetails)
    {
        if (isHighLOD == showDetails)
            return false;
        isHighLOD = showDetails;

        foreach (SpaceStationBlock3D block in Blocks)
            block.gameObject.SetActive(showDetails);
        foreach (SpaceStationAntenna3D antenna in Antennas)
            antenna.gameObject.SetActive(showDetails);

        return true;
    }

    public bool EnableDetailedColliders(bool enableDetailed)
    {
        if (isSimpleColliderMode != enableDetailed)
            return false;

        isSimpleColliderMode = !enableDetailed;
        SimpleCollider.enabled = isVisible && isSimpleColliderMode;

        //MeshTransformCollider.enabled = isVisible && !isSimpleColliderMode;
        BaseTransformCollider.enabled = isVisible && !isSimpleColliderMode;

        foreach (SpaceStationBlock3D block in Blocks)
            block.collider.enabled = isVisible && !isSimpleColliderMode;
        foreach (SpaceStationAntenna3D antenna in Antennas)
            antenna.collider.enabled = isVisible && !isSimpleColliderMode;

        return true;
    }

    //public void OnAttachedToHand()
    //{
    //    if (this.enabled)
    //    {
    //        attachedToHand = true;
    //    }
    //}

    //public void OnDetachedFromhand()
    //{
    //    //if (this.enabled)
    //    {
    //        attachedToHand = false;
    //        HideIndicator();
    //    }
    //}



    public override void InitializeInteractionSystem()
    {
        gameObject.GetComponent<PositionResetter>().Init();

        MeshTransform.gameObject.GetComponent<MeshCollider>().sharedMesh = MeshTransform.gameObject.GetComponent<MeshFilter>().mesh;
        BaseTransform.gameObject.GetComponent<MeshCollider>().sharedMesh = BaseTransform.gameObject.GetComponent<MeshFilter>().mesh;

        if (TryGetComponent(out ISAUIInteractable rootSimpleInteractable))
        {
            rootSimpleInteractable.onHoverStart.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverStart(this); });
            rootSimpleInteractable.onHoverEnd.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverEnd(this); });
            rootSimpleInteractable.onTriggerPressed.AddListener(delegate {
                RecursivelySetActive(false);
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(this, true);
            });
        }

        if (BaseTransform.gameObject.TryGetComponent(out ISAUIInteractable baseInteractable))
        {
            InitializeInteractionSystem(baseInteractable);

            baseInteractable.onTriggerPressed.AddListener(delegate {
                RecursivelySetActive(false);
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(this, true);
            });
        }

        if (directParent is TransparentCapsuleContainment3D)
        {
            Transform infoCanvasTransform = gameObject.transform.Find("Canvas");
            if(infoCanvasTransform != null)
            {
                if(infoCanvasTransform.TryGetComponent(out SoftwareElementInfoCanvas canvas))
                {
                    SetInfoCanvas(canvas);
                    infoCanvas.Initialize(this);
                }
            }
        }

        GetComponent<Interactable>().onAttachedToHand += OnHandHeldStart;
        GetComponent<Interactable>().onDetachedFromHand += OnHandHeldEnd;
        GetComponent<Throwable>().onHeldUpdate.AddListener(
            delegate (Hand hand)
            {
                OnHandHeldUpdate(hand);
            });
    }

    private void SetInfoCanvas(SoftwareElementInfoCanvas canvas)
    {
        this.infoCanvas = canvas;
        foreach (SpaceStation3D satellite in SatelliteStations)
            satellite.SetInfoCanvas(canvas);
    }

    /// <summary>
    /// Recursively set the active state of all members of the given classifier.
    /// If the classifier is a nested/satellite classifier, search its top most parent and start from there.
    /// </summary>
    public void RecursivelySetActive(bool state)
    {
        if (directParent is SpaceStation3D)
            ((SpaceStation3D)directParent).RecursivelySetActive(state);
        else
            DoRecursivelySetActive(state);
    }

    /// <summary>
    /// Execute the downwards recursion and set the active state of the given classifier's members accordingly.
    /// </summary>
    private void DoRecursivelySetActive(bool state)
    {
        ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(this, state);
        foreach (SpaceStationBlock3D block in Blocks)
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(block, state);
        foreach (SpaceStationAntenna3D antenna in Antennas)
            ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(antenna, state);
        foreach (SpaceStation3D satellite in SatelliteStations)
            satellite.DoRecursivelySetActive(state);
    }



    public override void InitializeRelations(SystemGenerator generator, ModelReferenceResolver referenceResolver)
    {
        foreach (ISASpaceStationRelation relation in ecoreModel.inheritanceRelations ?? new ISASpaceStationRelation[0])
            RegisterRelation(generator, referenceResolver, RelationType.TYPE_REFERENCE, relation.target, relation.weight);
    }

    public override List<RelationManager.RelationInfo> GetOutgoingRelationships(RelationType relationType)
    {
        List<RelationManager.RelationInfo> resultRelations = base.GetOutgoingRelationships(relationType);

        foreach (SpaceStation3D satellite in SatelliteStations)
            resultRelations.AddRange(satellite.GetOutgoingRelationships(relationType));
        foreach (SpaceStationBlock3D block in Blocks)
            resultRelations.AddRange(block.GetOutgoingRelationships(relationType));
        foreach (SpaceStationAntenna3D antenna in Antennas)
            resultRelations.AddRange(antenna.GetOutgoingRelationships(relationType));

        return resultRelations;
    }

    public override List<RelationManager.RelationInfo> GetIncomingRelationships(RelationType relationType)
    {
        List<RelationManager.RelationInfo> resultRelations = base.GetIncomingRelationships(relationType);

        foreach (SpaceStation3D satellite in SatelliteStations)
            resultRelations.AddRange(satellite.GetIncomingRelationships(relationType));
        foreach (SpaceStationBlock3D block in Blocks)
            resultRelations.AddRange(block.GetIncomingRelationships(relationType));
        foreach (SpaceStationAntenna3D antenna in Antennas)
            resultRelations.AddRange(antenna.GetIncomingRelationships(relationType));

        return resultRelations;
    }

    public override float GetRelationWeightTo(AbstractVisualElement3D targetElement, bool includeContainedElements)
    {
        float weight = base.GetRelationWeightTo(targetElement, includeContainedElements);
        foreach (SpaceStation3D satellite in SatelliteStations)
            weight += satellite.GetRelationWeightTo(targetElement, includeContainedElements);
        foreach (SpaceStationBlock3D block in Blocks)
            weight += block.GetRelationWeightTo(targetElement, includeContainedElements);
        foreach (SpaceStationAntenna3D antenna in Antennas)
            weight += antenna.GetRelationWeightTo(targetElement, includeContainedElements);
        return weight;
    }



    protected override IEnumerable<BoundInteractableMultiViewElement3D> GetContainedBoundInteractableElements()
    {
        return SatelliteStations;
    }

    protected override void ApplyBoundingBoxChanges(Vector3 boundingboxOffset)
    {
    }



    public Transform locallyHoveringIndexFingerTip { get; private set; }

    public void OnHandHeldStart(Hand holdingHand)
    {
        locallyHoveringIndexFingerTip = null;

        Hand hoveringHand = holdingHand.otherHand;
        if (hoveringHand == null)
            return;

        locallyHoveringIndexFingerTip = ISAVRPointer.FindIndexFingerTip(hoveringHand.transform);
    }



    public void OnHandHeldEnd(Hand holdingHand = null)
    {
        if (smoothSetbackRoutine != null)
            StopCoroutine(smoothSetbackRoutine);
        smoothSetbackRoutine = StartCoroutine(SmoothlySetBackSize());
    }

    private Coroutine smoothSetbackRoutine;
    private IEnumerator SmoothlySetBackSize()
    {
        List<Vector3> originalLocalPositions = new List<Vector3>();

        float currentHeight = GAP_BETWEEN_BLOCKS;
        Blocks.Reverse();
        // public functions / methods
        foreach (SpaceStationBlock3D functionBlock in Blocks)
        {
            if (!functionBlock.isPublic)
                continue;

            originalLocalPositions.Add((BASE_HEIGHT / 2 + currentHeight + functionBlock.height / 2) * Vector3.up);
            currentHeight += functionBlock.height + GAP_BETWEEN_BLOCKS;
        }

        currentHeight = GAP_BETWEEN_BLOCKS;
        Blocks.Reverse();
        // private functions / methods
        foreach (SpaceStationBlock3D functionBlock in Blocks)
        {
            if (functionBlock.isPublic)
                continue;

            originalLocalPositions.Add((BASE_HEIGHT / 2 + currentHeight + functionBlock.height / 2) * Vector3.down);
            currentHeight += functionBlock.height + GAP_BETWEEN_BLOCKS;
        }

        float startTime = Time.time;
        while(Time.time - startTime < 5f)
        {
            int i = 0;

            Blocks.Reverse();
            // public functions / methods
            foreach (SpaceStationBlock3D block in Blocks)
            {
                if (!block.isPublic)
                    continue;

                block.transform.localPosition = Vector3.Lerp(block.transform.localPosition, originalLocalPositions[i++], Mathf.Min(1, Time.deltaTime));
                block.transform.localScale = Vector3.Lerp(block.transform.localScale, Vector3.one, Mathf.Min(1, Time.deltaTime));
            }

            Blocks.Reverse();
            // private functions / methods
            foreach (SpaceStationBlock3D block in Blocks)
            {
                if (block.isPublic)
                    continue;

                block.transform.localPosition = Vector3.Lerp(block.transform.localPosition, originalLocalPositions[i++], Mathf.Min(1, Time.deltaTime));
                block.transform.localScale = Vector3.Lerp(block.transform.localScale, Vector3.one, Mathf.Min(1, Time.deltaTime));
            }

            yield return null;
        }

        foreach (SpaceStationBlock3D block in Blocks)
            block.transform.localScale = Vector3.one;
    }



    [SerializeField]
    private float hoverBloatScale = 2f;
    [SerializeField]
    private float hoverBloatVerticalFalloff = 0.05f;
    [SerializeField]
    private float hoverBloatHorizontalMin = 0.1f;
    [SerializeField]
    private float hoverBloatHorizontalMax = 0.2f;
    public void OnHandHeldUpdate(Hand holdingHand = null)
    {
        if (locallyHoveringIndexFingerTip == null)
            return;
        OnHandHeldUpdate(locallyHoveringIndexFingerTip.position);
    }

    public void OnHandHeldUpdate(Vector3 indexFingerTipPostion)
    {
        if (isSimpleColliderMode)
            return;

        Vector3 a = transform.position;
        Vector3 n = transform.up;
        Vector3 p = indexFingerTipPostion;

        // https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
        Vector3 closestPoint = a + Vector3.Dot(p - a, n) * n;

        //VisualDebugger.SpawnCubeAt(this, closestPoint, Vector3.one * 0.01f, 0.1f);

        float currentHeight = GAP_BETWEEN_BLOCKS;
        Blocks.Reverse();
        // public functions / methods from colored core to outside
        foreach (SpaceStationBlock3D block in Blocks)
        {
            if (!block.isPublic)
                continue;

            currentHeight = AdjustBlockScaleAndPosition(block, currentHeight, indexFingerTipPostion, closestPoint, Vector3.up);
        }

        currentHeight = GAP_BETWEEN_BLOCKS;
        Blocks.Reverse();
        // public functions / methods from colored core to outside
        foreach (SpaceStationBlock3D block in Blocks)
        {
            if (block.isPublic)
                continue;

            currentHeight = AdjustBlockScaleAndPosition(block, currentHeight, indexFingerTipPostion, closestPoint, Vector3.down);
        }
    }

    private float AdjustBlockScaleAndPosition(SpaceStationBlock3D block, float currentHeight, Vector3 indexFingerTipPostion, Vector3 closestPoint, Vector3 diff)
    {
        float verticalDistance = Vector3.Distance(block.transform.position, closestPoint);
        float horizontalDistance = Vector3.Distance(block.transform.position, indexFingerTipPostion);

        float verticalScaleRelative = Mathf.Min(1, Mathf.Max(0, hoverBloatVerticalFalloff - verticalDistance) / hoverBloatVerticalFalloff);
        float horizontalScaleRelative = Mathf.Min(1, Mathf.Max(0, hoverBloatHorizontalMax - horizontalDistance) / (hoverBloatHorizontalMax - hoverBloatHorizontalMin));
        float combinedScale = 1f + verticalScaleRelative * horizontalScaleRelative * hoverBloatScale;

        block.transform.localScale = combinedScale * Vector3.one;
        block.transform.localPosition = (BASE_HEIGHT / 2 + currentHeight + combinedScale * block.height / 2 + (combinedScale - 1f) * GAP_BETWEEN_BLOCKS) * diff;

        return currentHeight + combinedScale * (block.height + GAP_BETWEEN_BLOCKS);
    }

}


