﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.Modelling.Visualization;



public class Library3D
{

    public string name { get; private set; }

    public List<LibraryClassifier3D> classifierEntries { get; private set; }



    public Library3D(string name)
    {
        this.name = name;
        classifierEntries = new List<LibraryClassifier3D>();
    }

    public void AddEntry(LibraryClassifier3D libClassifier)
    {
        classifierEntries.Add(libClassifier);
    }
}



public class LibraryClassifier3D
{

    public string qualifiedname { get; private set; }



    public LibraryClassifier3D(ISASpaceStation stationEcore)
    {
        qualifiedname = stationEcore.qualifiedName;
    }



}