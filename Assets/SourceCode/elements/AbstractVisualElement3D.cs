﻿
using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.Modelling.Visualization;
using ISA.UI;
using ISA.VR;
using System;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public abstract class AbstractVisualElement3D : MonoBehaviour, ISAHoverableElementListener, ISAActivatableElementListener
{
    public static int softwareElementLastGeneratedId;

    public int id { get; private set; }

    public AbstractVisualElement3D directParent { get; protected set; }
    private TransparentCapsule3D parentCapsule;

    private Dictionary<RelationType, List<RelationManager.RelationInfo>> outgoingRelations;
    private Dictionary<RelationType, List<RelationManager.RelationInfo>> incomingRelations;

    public string simpleName { get; protected set; }
    public string qualifiedName { get; protected set; }

    public Transform MeshTransform { get; protected set; }

    public Transform incomingRelationsHookTransform { get; protected set; }
    public Transform outgoingRelationsHookTransform { get; protected set; }

    public float mass { get; protected set; }

    public HandHoverHighlighter handHoverHighlighter;
    public NamePlateManager proximityVisibleNamePlate { get; protected set; }

    private int NestingLevel = -1;
    private HashSet<WhiteboardHandler> containingDrawingBoards = new HashSet<WhiteboardHandler>();



    public virtual void Initialize(ISANamedVisualElement ecoreElement)
    {
        outgoingRelations = new Dictionary<RelationType, List<RelationManager.RelationInfo>>();
        incomingRelations = new Dictionary<RelationType, List<RelationManager.RelationInfo>>();

        if (ecoreElement != null)
        {
            qualifiedName = ecoreElement.qualifiedName ?? "?";
            simpleName = ecoreElement.name ?? "Unnamed Element";
        }

        id = ++softwareElementLastGeneratedId;
    }

    public override string ToString()
    {
        return GetType().Name + ": " + simpleName + " (" + qualifiedName + ")" + " [" + GetHashCode() + "]";
    }



    public void RegisterContainingDrawingboard(WhiteboardHandler board)
    {
        containingDrawingBoards.Add(board);
        handHoverHighlighter.SetMarkedInWhiteboard(containingDrawingBoards.Count > 0);
    }

    public void UnregisterContainingDrawingboard(WhiteboardHandler board)
    {
        containingDrawingBoards.Remove(board);
        handHoverHighlighter.SetMarkedInWhiteboard(containingDrawingBoards.Count > 0);
    }



    public abstract void ChangeVisibility(bool visible);

    public void ChangeVisibilityOld(bool visible)
    {
        foreach (MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>(true))
            renderer.enabled = visible;

        if(this is TransparentCapsule3D)
        {
            //proximityVisibleNamePlate.gameObject.SetActive(visible);
            //((TransparentCapsule3D)this).permanentlyVisibleNamePlate.gameObject.SetActive(visible);

            if (visible)
            {
                proximityVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
                ((TransparentCapsule3D)this).permanentlyVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
            }
            else
            {
                proximityVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
                ((TransparentCapsule3D)this).permanentlyVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
            }
        }
    }



    /// <summary>
    /// Returns the nesting level of an element.
    /// The higher the value, the deeper an element is nested in the hierarchy.
    /// Note that space station blocks are nested one level deeper than their containing space stations,
    /// thus they receive a deeper nesting level number from this method.
    /// </summary>
    /// <returns>the nesting level of an element as integer</returns>
    public int GetNestingLevel()
    {
        if (NestingLevel > 0)
            return NestingLevel;

        AbstractVisualElement3D currentElement = this;
        NestingLevel = 0;
        while (currentElement.directParent != null)
        {
            currentElement = currentElement.directParent;
            NestingLevel++;
        }
        return NestingLevel;
    }

    public void SetParent(AbstractVisualElement3D directParentElement)
    {
        directParent = directParentElement;
    }

    private bool parentDetermined = false;
    public TransparentCapsule3D GetParentCapsule()
    {
        if (parentDetermined)
            return parentCapsule;
        parentDetermined = true;

        AbstractVisualElement3D current = directParent;
        while (current != null && !(current is TransparentCapsule3D))
        {
            current = current.directParent;
        }
        if (current == null)
        {
            Debug.LogWarning("No parent capsule: " + qualifiedName + ". This should only print once per system load.");
            return null;
        }
        parentCapsule = (TransparentCapsule3D)current;
        return parentCapsule;
    }

    public abstract bool Contains(AbstractVisualElement3D element);




    public abstract void InitializeInteractionSystem();

    protected void InitializeInteractionSystem(ISAUIInteractable interactable)
    {
        if (TryGetComponent(out ISAInteractableAttachmentEventForwarder forwarder))
        {
            forwarder.forwardInteractable = directParent.GetComponent<Interactable>();
            forwarder.forwardThrowable = directParent.GetComponent<Throwable>();
        }

        if (this is TransparentCapsule3D)
            handHoverHighlighter.MarkedInWhiteboardColor = ((TransparentCapsule3D)this).OutlineColor;
        else
            handHoverHighlighter.MarkedInWhiteboardColor = GetParentCapsule().OutlineColor;

        handHoverHighlighter.notifyOnHoverBegin.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverStart(this); });
        handHoverHighlighter.notifyOnHoverEnd.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverEnd(this); });

        interactable.onHoverStart.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverStart(this); });
        interactable.onHoverEnd.AddListener(delegate { ISAUIInteractionManager.INSTANCE.HoverEnd(this); });

        ISAUIInteractionManager.INSTANCE.RegisterActivationListener(this, this);
        ISAUIInteractionManager.INSTANCE.RegisterHoverListener(this, this);
    }

    public virtual void NotifyOnElementHoverStateChanged(object e, bool isHovered)
    {
        handHoverHighlighter.SetHighlighted(isHovered);

        if (proximityVisibleNamePlate == null)
        {
            Debug.LogWarning("name plate not set");
            return;
        }

        if (isHovered)
        {
            if (e is SpaceStation3D)
                proximityVisibleNamePlate.SetClassifierName(simpleName);
            else if (e is SpaceStationBlock3D || e is SpaceStationAntenna3D)
                proximityVisibleNamePlate.SetMemberName(simpleName);

            proximityVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.HOVERED);
        }
        else
        {
            if (e is SpaceStationBlock3D)
                proximityVisibleNamePlate.SetClassifierName(directParent.simpleName);
            else if (e is SpaceStationAntenna3D)
                proximityVisibleNamePlate.SetClassifierName(directParent.simpleName);

            proximityVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.HOVERED);
        }
    }

    public virtual void NotifyOnElementActiveStateChanged(object e, bool isActive, Vector3 position, Vector3 rotation)
    {
        handHoverHighlighter.SetActive(isActive);

        if (isActive)
            proximityVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.ACTIVE);
        else
            proximityVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.ACTIVE);
    }



    /// <summary>
    /// This method is called by the generator in a bottom-up manner
    /// on all elements _after_ it was called on their child elements
    /// </summary>
    public abstract void InitializeGeometry();

    /// <summary>
    /// This method is called in bottom up order over all software elements!
    /// </summary>
    /// <param name="generator"></param>
    /// <param name="referenceResolver"></param>
    public abstract void InitializeRelations(SystemGenerator generator, ModelReferenceResolver referenceResolver);

    protected void RegisterRelation(SystemGenerator generator, ModelReferenceResolver referenceResolver, RelationType relationType, string relationTargetString, float relationWeight)
    {
        ISANamedVisualElement usageRelationTargetElementEcore = referenceResolver.resolveReferenceByEcoreString<ISANamedVisualElement>(relationTargetString);
        AbstractVisualElement3D relationTargetElement3D = generator.getElement3D(usageRelationTargetElementEcore);

        if (relationTargetElement3D != null)
        {
            // is a reference to a system element
            RelationManager.RelationInfo newInfo = new RelationManager.RelationInfo();
            newInfo.globalOriginElement = this;
            newInfo.globalTargetElement = relationTargetElement3D;
            newInfo.type = relationType;
            newInfo.weight = relationWeight;

            this.GetOutgoingRelationships(relationType).Add(newInfo);
            relationTargetElement3D.GetIncomingRelationships(relationType).Add(newInfo);
        }
    }



    public virtual List<RelationManager.RelationInfo> GetIncomingRelationships(RelationType relationType)
    {
        if (!incomingRelations.TryGetValue(relationType, out List<RelationManager.RelationInfo> relations))
        {
            relations = new List<RelationManager.RelationInfo>();
            incomingRelations.Add(relationType, relations);
        }
        return relations;
    }

    public virtual List<RelationManager.RelationInfo> GetOutgoingRelationships(RelationType relationType)
    {
        if (!outgoingRelations.TryGetValue(relationType, out List<RelationManager.RelationInfo> relations))
        {
            relations = new List<RelationManager.RelationInfo>();
            outgoingRelations.Add(relationType, relations);
        }
        return relations;
    }

    public virtual float GetRelationWeightTo(AbstractVisualElement3D targetElement, bool includeContainedElements)
    {
        float totalWeight = 0;
        foreach (RelationType type in outgoingRelations.Keys)
            if (outgoingRelations.TryGetValue(type, out List<RelationManager.RelationInfo> relations))
                foreach (RelationManager.RelationInfo relation in relations)
                    if(targetElement.Equals(relation.globalTargetElement) || (includeContainedElements && targetElement.Contains(relation.globalTargetElement)))
                        totalWeight += relation.weight;
        return totalWeight;
    }



    public bool IsContainedInSameCapsuleAs(AbstractVisualElement3D other)
    {
        return this.GetParentCapsule().Equals(other.GetParentCapsule());
    }

    public bool IsContainedInHierarchyOfCapsule(TransparentCapsule3D capsule)
    {
        if (capsule == null)
            return false;
        if (GetParentCapsule() == null)
            return false;

        if (capsule.Equals(GetParentCapsule()))
            return true;

        return GetParentCapsule().IsContainedInHierarchyOfCapsule(capsule);
    }



    public GameObject CreateSimpleClone(Transform parentToSet)
    {
        GameObject clone = Instantiate(gameObject, parentToSet);
        RemoveAllAdditionalScripts(clone.transform);
        return clone;
    }

    private void RemoveAllAdditionalScripts(Transform t)
    {
        Component[] comps = t.GetComponents<Component>();
        foreach (Component comp in comps)
        {
            if (comp is Transform)
                continue;
            if (comp is MeshRenderer)
                continue;
            if (comp is MeshFilter)
                continue;
            Destroy(comp);
        }

        foreach (Transform child in t)
            RemoveAllAdditionalScripts(child);

        if (t.childCount == 0 && t.GetComponents<Component>().Length == 0)
            Destroy(t.gameObject);
    }

}
