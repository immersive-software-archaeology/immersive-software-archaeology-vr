﻿using ISA.Modelling;
using ISA.Modelling.Visualization;
using ISA.util;
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class BoundInteractableMultiViewElement3D : AbstractVisualElement3D
{

    public static readonly float BOUNDING_VOLUME_PADDING = 10f;

    /// <summary>
    /// IMPORTANT: The bounding volume must always be centered with its containing capsule's position!
    /// </summary>
    public Sphere BoundingVolume;




    public override void Initialize(ISANamedVisualElement ecoreElement)
    {
        base.Initialize(ecoreElement);
    }



    /// <summary>
    /// This method recalculates the bounding sphere for module capsules using Ritters bounding sphere algorithm.<br>
    /// <strong>IMPORTANT</strong>: It must be called bottom-up (starting with low level modules, working its way up)
    /// </summary>
    protected void RecalculateBoundingSpherePrecise()
    {
        List<Sphere> containedVolumes = new List<Sphere>();
        foreach (BoundInteractableMultiViewElement3D element in GetContainedBoundInteractableElements())
        {
            //containedVolumes.Add(new Sphere(cuboid.GameObject.transform.position + cuboid.PublicHeight * Vector3.up * cuboid.GameObject.transform.lossyScale.z, GameObject.transform.lossyScale.x * BOUNDING_VOLUME_PADDING));
            //containedVolumes.Add(new Sphere(cuboid.GameObject.transform.position + cuboid.PrivateHeight * Vector3.down * cuboid.GameObject.transform.lossyScale.z, GameObject.transform.lossyScale.x * BOUNDING_VOLUME_PADDING));

            containedVolumes.Add(element.BoundingVolume);
        }

        Sphere newVolume;
        if (containedVolumes.Count == 0)
            // do nothing, this case is entered for spacestations
            newVolume = BoundingVolume;
        else if (containedVolumes.Count == 1)
            newVolume = new Sphere(containedVolumes[0]);
        else if (containedVolumes.Count == 2)
            newVolume = BoundingVolumeUtil.calculateBoundingSphere(containedVolumes[0], containedVolumes[1]);
        else
            newVolume = BoundingVolumeUtil.CalculateBoundingSphereRitter(containedVolumes);

        BoundingVolume = new Sphere(gameObject.transform.position, newVolume.radius + BOUNDING_VOLUME_PADDING);
        ApplyBoundingBoxChanges(gameObject.transform.position - newVolume.center);
    }

    protected void RecalculateBoundingSphereSimple(bool applyPadding)
    {
        float largestRadius = 0f;
        Vector3 positionOfOneElement = Vector3.zero;
        foreach (BoundInteractableMultiViewElement3D element in GetContainedBoundInteractableElements())
        {
            positionOfOneElement = element.gameObject.transform.position;
            if (element.BoundingVolume.radius > largestRadius)
                largestRadius = element.BoundingVolume.radius;
        }

        if(largestRadius > 0f)
        {
            float boundingVolumeRadius = Vector3.Distance(gameObject.transform.position, positionOfOneElement) + largestRadius + (applyPadding ? BOUNDING_VOLUME_PADDING : 0);
            BoundingVolume = new Sphere(gameObject.transform.position, boundingVolumeRadius);
        }

        ApplyBoundingBoxChanges(Vector3.zero);
    }



    protected abstract IEnumerable<BoundInteractableMultiViewElement3D> GetContainedBoundInteractableElements();
    protected abstract void ApplyBoundingBoxChanges(Vector3 boundingboxOffset);

}


