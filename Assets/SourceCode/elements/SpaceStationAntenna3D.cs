﻿using ISA.Modelling;
using ISA.Modelling.Visualization;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SpaceStationAntenna3D : AbstractSpaceStationComponent3D
{

    private static Material material;

    public static void LoadMaterialsFromDisk()
    {
        material = Resources.Load("Models/Materials/Exterior1") as Material;
    }



    public new MeshCollider collider { get; private set; }

    public static readonly float PUBLIC_LENGTH = 3;
    public static readonly float PRIVATE_LENGTH = 1;

    public static readonly float FIELD_DIAMETER = 0.5f;
    public static readonly float ENUM_CONSTANT_DIAMETER = 0.5f;

    public bool isEnumConstant { get; private set; }

    public Transform CylinderTransform { get; private set; }



    public void Initialize(ISASpaceStationAntenna ecoreModel, SpaceStation3D parentStation)
    {
        base.Initialize(ecoreModel);
        SetParent(parentStation);

        isPublic = ecoreModel.@public;
        isEnumConstant = ecoreModel.enumConstant;
    }

    public override bool Contains(AbstractVisualElement3D element)
    {
        return false;
    }

    public override void ChangeVisibility(bool visible)
    {
        // handled in parent station!
    }



    public override void InitializeGeometry()
    {
        gameObject.name = ToString();

        MeshTransform = gameObject.transform.Find("Mesh");
        CylinderTransform = MeshTransform.Find("Cylinder");

        incomingRelationsHookTransform = gameObject.transform.Find("IncomingRelationsHookTransform");
        outgoingRelationsHookTransform = gameObject.transform.Find("OutgoingRelationsHookTransform");

        float diameter = isEnumConstant ? ENUM_CONSTANT_DIAMETER : FIELD_DIAMETER;
        float height = isPublic ? PUBLIC_LENGTH : PRIVATE_LENGTH;

        MeshRenderer baseRenderer = GeometryGenerationUtil.GenerateCylinder(CylinderTransform.gameObject, 6, true, height / 5f, Mathf.PI * diameter / 10f);
        baseRenderer.material = material;

        MeshTransform = gameObject.transform.Find("Mesh");
        MeshTransform.localScale = new Vector3(diameter, diameter, height);

        collider = CylinderTransform.gameObject.GetComponent<MeshCollider>();
        collider.sharedMesh = CylinderTransform.GetComponent<MeshFilter>().mesh;
        //collider.radius = diameter / 2f;
        //collider.height = height;

        mass = Mathf.PI * Mathf.Pow(diameter / 2, 2) * height;
    }



    public override void InitializeInteractionSystem()
    {
        proximityVisibleNamePlate = directParent.proximityVisibleNamePlate;

        if (CylinderTransform.gameObject.TryGetComponent(out ISAUIInteractable interactable))
        {
            interactable.onTriggerPressed.AddListener(delegate {
                ((SpaceStation3D)directParent).RecursivelySetActive(false);
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(this, true);
            });
            InitializeInteractionSystem(interactable);
        }
    }
}
