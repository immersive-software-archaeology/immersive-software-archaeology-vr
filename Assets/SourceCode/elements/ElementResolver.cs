﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementResolver
{
    private Dictionary<int, AbstractVisualElement3D> idToElementMap;

    public ElementResolver(List<AbstractVisualElement3D> allElements)
    {
        idToElementMap = new Dictionary<int, AbstractVisualElement3D>();
        foreach (AbstractVisualElement3D el in allElements)
        {
            if (idToElementMap.ContainsKey(el.id))
                Debug.LogError("Two elements with the same id registered in element resolver! This WILL cause unintended behavior!");
            idToElementMap.Add(el.id, el);
        }
    }

    public ICollection<AbstractVisualElement3D> GetAllElements()
    {
        return idToElementMap.Values;
    }

    public List<AbstractVisualElement3D> GetListWithCopyOfAllElements()
    {
        return new List<AbstractVisualElement3D>(idToElementMap.Values);
    }

    public AbstractPinHandler FindPinById(int id)
    {
        foreach (WhiteboardHandler board in SceneManager.INSTANCE.GetWhiteboards())
        {
            AbstractPinHandler pin = board.GetPinWithId(id);
            if (pin != null)
                return pin;
        }
        return null;
    }

    public HandheldCameraPhoto FindPhotoById(int id)
    {
        foreach (WhiteboardHandler board in SceneManager.INSTANCE.GetWhiteboards())
        {
            HandheldCameraPhoto photo = board.GetPhotoWithId(id);
            if (photo != null)
                return photo;
        }
        return null;
    }

    public AbstractVisualElement3D FindElementByQualifiedName(string qualifiedName)
    {
        foreach (AbstractVisualElement3D element in idToElementMap.Values)
        {
            if (element is TransparentCapsuleContainment3D)
                continue;

            if (element.qualifiedName == null)
                Debug.LogError("Element with null name!");
            if (element.qualifiedName.Equals(qualifiedName))
                return element;
        }
        return null;
    }

    public AbstractVisualElement3D FindElementById(int id)
    {
        if (idToElementMap.TryGetValue(id, out AbstractVisualElement3D element))
            return element;
        return null;
    }

    public SoftwareElementInfoCanvas FindCanvasForElementWithId(int id)
    {
        AbstractVisualElement3D displayedElement = FindElementById(id);

        SoftwareElementInfoCanvas canvas;
        if (displayedElement is TransparentCapsule3D)
            canvas = ((TransparentCapsule3D)displayedElement).infoCanvas;
        else if (displayedElement is SpaceStation3D)
            canvas = ((SpaceStation3D)displayedElement).infoCanvas;
        else if (displayedElement is SpaceStationBlock3D)
            canvas = ((SpaceStation3D)((SpaceStationBlock3D)displayedElement).directParent).infoCanvas;
        else if (displayedElement is SpaceStationAntenna3D)
            canvas = ((SpaceStation3D)((SpaceStationAntenna3D)displayedElement).directParent).infoCanvas;
        else
            throw new Exception("Tried to get an information canvas for an unexpected type of software element: " + displayedElement.GetType().Name);

        return canvas;
    }

}