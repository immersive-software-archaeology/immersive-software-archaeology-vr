﻿using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.Modelling.Visualization;
using ISA.UI;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class TransparentCapsule3D : BoundInteractableMultiViewElement3D
{
    private static Material innerCapsuleMaterial;

    private static Material innerMaterial;
    private static Material outerMaterial;

    private static Material concreteStationsMaterial;
    private static Material abstractStationsMaterial;

    public static void LoadMaterialsFromDisk()
    {
        innerCapsuleMaterial = Resources.Load("Models/Visualization/Materials/Capsule Preview Material") as Material;

        innerMaterial = Resources.Load("Models/Visualization/Materials/Module Capsule Inner Material") as Material;
        outerMaterial = Resources.Load("Models/Visualization/Materials/Module Capsule Outer Material") as Material;

        abstractStationsMaterial = Resources.Load("Models/Visualization/Materials/Wireframe") as Material;
        concreteStationsMaterial = Resources.Load("Models/Materials/Exterior2") as Material;
    }



    public static readonly float MARGIN_BETWEEN_CAPSULES = 10f; // 5



    private List<TransparentCapsule3DOpenCloseObserver> openCloseObservers;



    public ISATransparentCapsule ecoreModel { get; private set; }
    public string[] Tags { get; private set; }
    public Color CapsuleColor { get; private set; }
    public Color WhiteboardColor { get; private set; }
    public Color StationColor { get; private set; }
    public Color CanvasColor { get; private set; }
    public Color OutlineColor { get; private set; }
    public Material ConcreteStationsMaterialInCapsuleColor { get; private set; }
    public Material AbstractStationsMaterialInCapsuleColor { get; private set; }

    public List<TransparentCapsule3D> containedSubCapsules { get; private set; }
    public TransparentCapsuleContainment3D directContainment { get; private set; }

    private Transform MeshOuterTransform;
    private Transform MeshInnerTransform;
    private GameObject grabbableCenterSphere;
    public Transform CenterTransform { private set; get; }

    private bool subCapsulesLayouted = false;

    private static float lastOpenEventTimestamp;
    public bool isOpened { private set; get; }
    private bool isVisible;

    public NamePlateManager permanentlyVisibleNamePlate { private set; get; }

    public SoftwareElementInfoCanvas infoCanvas { private set; get; }



    public void Initialize(ISATransparentCapsule ecoreModel, TransparentCapsule3D parent, Vector2 hueSaturationValues)
    {
        base.Initialize(ecoreModel);
        this.ecoreModel = ecoreModel;
        Tags = ecoreModel.tags ?? new string[0];

        openCloseObservers = new List<TransparentCapsule3DOpenCloseObserver>();
        isOpened = true;

        CapsuleColor = Color.HSVToRGB(hueSaturationValues.x, hueSaturationValues.y, 1f);
        WhiteboardColor = Color.HSVToRGB(hueSaturationValues.x, Mathf.Min(1f, hueSaturationValues.y * 1.5f), 0.8f);
        StationColor = Color.HSVToRGB(hueSaturationValues.x, hueSaturationValues.y, 0.6f);
        CanvasColor = Color.HSVToRGB(hueSaturationValues.x, hueSaturationValues.y * 0.5f, 0.4f);
        OutlineColor = Color.HSVToRGB(hueSaturationValues.x, hueSaturationValues.y * 0.75f, 1f);

        ConcreteStationsMaterialInCapsuleColor = Instantiate(concreteStationsMaterial);
        ConcreteStationsMaterialInCapsuleColor.SetColor("_BaseColor", StationColor);
        ConcreteStationsMaterialInCapsuleColor.SetColor("_WireColor", StationColor);

        AbstractStationsMaterialInCapsuleColor = Instantiate(abstractStationsMaterial);
        AbstractStationsMaterialInCapsuleColor.SetColor("_BaseColor", StationColor);
        AbstractStationsMaterialInCapsuleColor.SetColor("_WireColor", StationColor);

        this.SetParent(parent);

        containedSubCapsules = new List<TransparentCapsule3D>();

        GameObject directContainmentGameObject = new GameObject("Direct Containment");
        directContainment = directContainmentGameObject.AddComponent<TransparentCapsuleContainment3D>();
        directContainment.Initialize(this);
    }

    public void AddListener(TransparentCapsule3DOpenCloseObserver newListener)
    {
        openCloseObservers.Add(newListener);
    }



    public float GetCurrentWorldSpaceRadius()
    {
        return MeshOuterTransform.lossyScale.x / 2f;
    }

    public bool IsRoot()
    {
        return directParent == null;
    }

    public bool IsLeaf()
    {
        return containedSubCapsules.Count == 0;
    }

    public void AddSubCapsule(TransparentCapsule3D capsule3D)
    {
        containedSubCapsules.Add(capsule3D);
        capsule3D.SetParent(this);
    }

    public void AddSpaceStation(SpaceStation3D station3D)
    {
        directContainment.containedSpaceStations.Add(station3D);
        station3D.SetParent(directContainment);
    }

    public override bool Contains(AbstractVisualElement3D element)
    {
        if (directContainment.Equals(element))
            return true;
        if (ContainsDirectly(element))
            return true;

        foreach (TransparentCapsule3D subCapsule in containedSubCapsules)
        {
            if (subCapsule.Equals(element))
                return true;
            if (subCapsule.Contains(element))
                return true;
        }

        return false;
    }

    public bool ContainsDirectly(AbstractVisualElement3D element)
    {
        return directContainment.Contains(element);
    }

    public List<TransparentCapsule3D> GetSiblings()
    {
        if (IsRoot())
            return new List<TransparentCapsule3D>(0);

        List<TransparentCapsule3D> siblings = new List<TransparentCapsule3D>();
        foreach (TransparentCapsule3D sibling in GetParentCapsule().containedSubCapsules)
            if(!sibling.Equals(this))
                siblings.Add(sibling);

        return siblings;
    }

    public override void ChangeVisibility(bool visible)
    {
        isVisible = visible;

        MeshOuterTransform.GetComponent<Renderer>().enabled = isVisible;
        MeshInnerTransform.GetComponent<Renderer>().enabled = isVisible;
        grabbableCenterSphere.GetComponent<Renderer>().enabled = isVisible;

        MeshOuterTransform.GetComponent<Collider>().enabled = isVisible && !isOpened;
        grabbableCenterSphere.GetComponent<Collider>().enabled = isVisible && isOpened;

        directContainment.ChangeVisibility(visible);
        foreach (TransparentCapsule3D subCapsule in containedSubCapsules)
            subCapsule.ChangeVisibility(visible);

        if (isVisible)
        {
            proximityVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
            permanentlyVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
        }
        else
        {
            proximityVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
            permanentlyVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
        }
    }



    public override void InitializeGeometry()
    {
        if (gameObject == null)
            // can happen if the table is reset in the middle of a generation
            return;

        gameObject.name = ToString();

        CenterTransform = gameObject.transform.Find("Center");
        grabbableCenterSphere = CenterTransform.GetChild(0).gameObject;
        grabbableCenterSphere.GetComponent<MeshRenderer>().material = Instantiate(innerCapsuleMaterial);
        grabbableCenterSphere.GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", CapsuleColor);

        MovementGestureDetector gestureDetector = grabbableCenterSphere.GetComponent<MovementGestureDetector>();
        gestureDetector.onGestureDetected.AddListener(delegate {
            gestureDetector.hand.DetachObject(grabbableCenterSphere, true);
            SendCloseRequest();
        });

        incomingRelationsHookTransform = gameObject.transform.Find("IncomingRelationsHookTransform");
        outgoingRelationsHookTransform = gameObject.transform.Find("OutgoingRelationsHookTransform");

        MeshOuterTransform = gameObject.transform.Find("Mesh Outer");
        MeshOuterTransform.GetComponent<MeshRenderer>().material = Instantiate(outerMaterial);
        MeshOuterTransform.GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", CapsuleColor);

        MeshInnerTransform = gameObject.transform.Find("Mesh Inner");
        MeshInnerTransform.GetComponent<MeshRenderer>().material = Instantiate(innerMaterial);
        MeshInnerTransform.GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", CapsuleColor);

        permanentlyVisibleNamePlate = gameObject.transform.Find("Permanent NamePlate").GetComponent<NamePlateManager>();
        permanentlyVisibleNamePlate.SetPackageName(GetParentCapsule(), qualifiedName);

        proximityVisibleNamePlate = gameObject.transform.Find("Additional NamePlate").GetComponent<NamePlateManager>();
        proximityVisibleNamePlate.SetPackageName(GetParentCapsule(), qualifiedName);
        proximityVisibleNamePlate.transform.parent = grabbableCenterSphere.transform;
        ElementUpdater.instance.AddSoftwareNamePlate(proximityVisibleNamePlate, grabbableCenterSphere.transform);

        //rigidbody = GameObject.GetComponent<Rigidbody>();
        //rigidbody.mass = mass;

        // layout space stations
        directContainment.InitializeGeometry();
        if (directContainment.containedSpaceStations.Count > 0)
        {
            LayoutUtil.LayoutElementsWithEvenMargins(directContainment.containedSpaceStations, directContainment.gameObject.transform, SpaceStation3D.MARGIN_TO_NEIGHBOR_STATIONS, 10f);
            RecalculateBoundingSphereSimple(true);

            foreach (SpaceStation3D station in directContainment.containedSpaceStations)
                station.gameObject.transform.LookAt(gameObject.transform.position, Vector3.up);
        }

        foreach (SpaceStation3D station in directContainment.containedSpaceStations)
        {
            //station.SetColor(StationColor);
            mass += station.mass;
        }
        foreach (TransparentCapsule3D capsule in containedSubCapsules)
            mass += capsule.mass;
    }



    public override void InitializeInteractionSystem()
    {
        ISAUIInteractable centerInteractable = grabbableCenterSphere.GetComponent<ISAUIInteractable>();
        InitializeInteractionSystem(centerInteractable);
        centerInteractable.onTriggerPressed.AddListener(delegate() { ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(this, true); });

        //ISAUIInteractable outerInteractable = MeshOuterTransform.GetComponent<ISAUIInteractable>();
        //InitializeInteractionSystem(outerInteractable);
        //outerInteractable.onTriggerPressed.AddListener(delegate { ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(this, true); });

        Transform infoCanvasTransform = gameObject.transform.Find("Canvas");
        infoCanvas = infoCanvasTransform.GetComponent<SoftwareElementInfoCanvas>();
        infoCanvas.Initialize(this);
    }



    public override void InitializeRelations(SystemGenerator generator, ModelReferenceResolver referenceResolver)
    {
        // nothing to do, relations are initialized when initializing the relations of this capsule's containment
    }

    private Dictionary<RelationType, List<RelationManager.RelationInfo>> cachedIncomingRelations = new Dictionary<RelationType, List<RelationManager.RelationInfo>>();
    public override List<RelationManager.RelationInfo> GetIncomingRelationships(RelationType relationType)
    {
        if (cachedIncomingRelations.TryGetValue(relationType, out List<RelationManager.RelationInfo> cachedRelations))
            return cachedRelations;

        cachedRelations = new List<RelationManager.RelationInfo>();
        cachedIncomingRelations.Add(relationType, cachedRelations);

        foreach (TransparentCapsule3D subCapsule in containedSubCapsules)
            cachedRelations.AddRange(subCapsule.GetIncomingRelationships(relationType));
        foreach (SpaceStation3D station in directContainment.containedSpaceStations)
            cachedRelations.AddRange(station.GetIncomingRelationships(relationType));

        // we want to consider only relations that start from outside of this capsule!
        List<RelationManager.RelationInfo> relationsToRemove = new List<RelationManager.RelationInfo>();
        foreach (RelationManager.RelationInfo relation in cachedRelations)
            if (this.Contains(relation.globalOriginElement))
                relationsToRemove.Add(relation);

        foreach (RelationManager.RelationInfo relationToRemove in relationsToRemove)
            cachedRelations.Remove(relationToRemove);

        return cachedRelations;
    }

    private Dictionary<RelationType, List<RelationManager.RelationInfo>> cachedOutgoingRelations = new Dictionary<RelationType, List<RelationManager.RelationInfo>>();
    public override List<RelationManager.RelationInfo> GetOutgoingRelationships(RelationType relationType)
    {
        if (cachedOutgoingRelations.TryGetValue(relationType, out List<RelationManager.RelationInfo> cachedRelations))
            return cachedRelations;

        cachedRelations = new List<RelationManager.RelationInfo>();
        cachedOutgoingRelations.Add(relationType, cachedRelations);

        foreach (TransparentCapsule3D subCapsule in containedSubCapsules)
            cachedRelations.AddRange(subCapsule.GetOutgoingRelationships(relationType));
        foreach (SpaceStation3D station in directContainment.containedSpaceStations)
            cachedRelations.AddRange(station.GetOutgoingRelationships(relationType));

        // we want to consider only relations that target something outside of this capsule!
        List<RelationManager.RelationInfo> relationsToRemove = new List<RelationManager.RelationInfo>();
        foreach (RelationManager.RelationInfo relation in cachedRelations)
            if (this.Contains(relation.globalTargetElement))
                relationsToRemove.Add(relation);

        foreach (RelationManager.RelationInfo relationToRemove in relationsToRemove)
            cachedRelations.Remove(relationToRemove);

        return cachedRelations;
    }

    private Dictionary<AbstractVisualElement3D, float> relationWeightCache = new Dictionary<AbstractVisualElement3D, float>();
    public override float GetRelationWeightTo(AbstractVisualElement3D targetElement, bool includeContainedElements)
    {
        if (relationWeightCache.TryGetValue(targetElement, out float weight))
            return weight;
        
        weight = base.GetRelationWeightTo(targetElement, includeContainedElements);
        foreach (TransparentCapsule3D capsule in containedSubCapsules)
            weight += capsule.GetRelationWeightTo(targetElement, includeContainedElements);
        foreach (SpaceStation3D station in directContainment.containedSpaceStations)
            weight += station.GetRelationWeightTo(targetElement, includeContainedElements);

        relationWeightCache.Add(targetElement, weight);
        return weight;
    }



    /// <summary>
    /// Initializes the positions of contained elements and calculates the capsule's bounding volume
    /// MUST BE CALLED IN BOTTOM UP ORDER so that capsules iteratively layout their children (which must have been layouted beforehand)
    /// </summary>
    public void FinalizeLayout()
    {
        // Layout contained capsules: do that here because we can go through the modules bottom up with their space stations already layed out
        if (containedSubCapsules.Count > 0)
        {
            float innerRadius = Mathf.Max(0.2f, BoundingVolume.radius);
            LayoutUtil.LayoutElementsWithEvenMargins(containedSubCapsules, gameObject.transform, MARGIN_BETWEEN_CAPSULES, innerRadius);
            subCapsulesLayouted = true;
            RecalculateBoundingSpherePrecise();
        }
    }

    protected override IEnumerable<BoundInteractableMultiViewElement3D> GetContainedBoundInteractableElements()
    {
        List<BoundInteractableMultiViewElement3D> elements = new List<BoundInteractableMultiViewElement3D>();
        elements.AddRange(directContainment.containedSpaceStations);
        if (subCapsulesLayouted)
            elements.AddRange(containedSubCapsules);
        return elements;
    }

    protected override void ApplyBoundingBoxChanges(Vector3 boundingboxOffset)
    {
        // Adjust positions of children instead of having the bounding sphere's center someplace else than the local origin
        directContainment.gameObject.transform.position += boundingboxOffset;
        foreach (BoundInteractableMultiViewElement3D element in containedSubCapsules)
        {
            element.gameObject.transform.position += boundingboxOffset;
            element.BoundingVolume = new Sphere(element.gameObject.transform.position, element.BoundingVolume.radius);
        }

        MeshOuterTransform.position = BoundingVolume.center;
        MeshOuterTransform.localScale = Vector3.one * BoundingVolume.radius * 2f / gameObject.transform.lossyScale.x;
        MeshInnerTransform.position = MeshOuterTransform.position;
        MeshInnerTransform.localScale = MeshOuterTransform.localScale;

        CenterTransform.gameObject.transform.position += boundingboxOffset;
        CenterTransform.localScale = Vector3.one / SceneManager.INSTANCE.maxScaleFactor; // always make hand palm sized

        grabbableCenterSphere.GetComponent<PositionResetter>().Init();

        permanentlyVisibleNamePlate.transform.position = BoundingVolume.center + Vector3.up * (BoundingVolume.radius / gameObject.transform.lossyScale.x + 2f); // + boundingboxOffset;

        incomingRelationsHookTransform.position = BoundingVolume.center + Vector3.up * BoundingVolume.radius / gameObject.transform.lossyScale.x;
        outgoingRelationsHookTransform.position = BoundingVolume.center + Vector3.up * BoundingVolume.radius / gameObject.transform.lossyScale.x;

        directContainment.incomingRelationsHookTransform.position = BoundingVolume.center + Vector3.up * BoundingVolume.radius / 2f / gameObject.transform.lossyScale.x + boundingboxOffset;
        directContainment.outgoingRelationsHookTransform.position = BoundingVolume.center + Vector3.up * BoundingVolume.radius / 2f / gameObject.transform.lossyScale.x + boundingboxOffset;

        float scale = BoundingVolume.radius / gameObject.transform.lossyScale.x;
        MeshInnerTransform.GetComponent<MeshRenderer>().material.SetFloat("_Pattern_Tiling", scale / 1.5f);
        MeshOuterTransform.GetComponent<MeshRenderer>().material.SetFloat("_Pattern_Tiling", scale / 1.5f);
        MeshOuterTransform.GetComponent<MeshRenderer>().material.SetFloat("_Minimum_Pattern_Visibility", 0);
    }



    public void SendOpenRequest()
    {
        if (GetParentCapsule() != null && !GetParentCapsule().isOpened)
            // can only be opened when directly visible!
            return;

        if (Time.time - lastOpenEventTimestamp < 1)
            return;
        lastOpenEventTimestamp = Time.time;

        SphereOpenCloseEvent ev = new SphereOpenCloseEvent();
        ev.systemName = ModelLoader.instance.VisualizationModel.name;
        ev.clientId = SceneManager.INSTANCE.udpClient.id;
        ev.elementName = qualifiedName;
        ev.nowOpen = true;

        HTTPRequestHandler.INSTANCE.SubmitEvent(ev, null, delegate (string r) { Debug.LogError(r); });
    }

    public void SendCloseRequest()
    {
        SphereOpenCloseEvent ev = new SphereOpenCloseEvent();
        ev.systemName = ModelLoader.instance.VisualizationModel.name;
        ev.clientId = SceneManager.INSTANCE.udpClient.id;
        ev.elementName = qualifiedName;
        ev.nowOpen = false;

        HTTPRequestHandler.INSTANCE.SubmitEvent(ev, null, delegate (string r) { Debug.LogError(r); });
    }

    public void OpenLocally()
    {
        if (GetParentCapsule() != null && !GetParentCapsule().isOpened)
            // can only be opened when directly visible!
            return;

        isOpened = true;
        ChangeVisibility(true);

        foreach (TransparentCapsule3D subCapsule in containedSubCapsules)
            subCapsule.CloseLocally();

        MeshOuterTransform.GetComponent<MeshRenderer>().material.SetFloat("_Minimum_Pattern_Visibility", 0);
        //MeshOuterTransform.GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", Color.white);
        //MeshInnerTransform.GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", Color.white);

        VisualizationSynchronizer.instance.ShowRelationSegmentsInCapsule(this);
        SceneManager.INSTANCE.PositionOrScaleChanged();

        foreach (TransparentCapsule3DOpenCloseObserver listener in openCloseObservers)
            listener.NotifyOnCapsuleOpenCloseEvent(this, true);
    }

    public void CloseLocally()
    {
        isOpened = false;

        if(GetParentCapsule() == null || GetParentCapsule().isOpened)
        {
            ChangeVisibility(true);
            directContainment.ChangeVisibility(false);

            grabbableCenterSphere.GetComponent<Renderer>().enabled = false;
            grabbableCenterSphere.GetComponent<Collider>().enabled = false;
            proximityVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.LOCAL_INVISIBLE);
        }
        else
        {
            ChangeVisibility(false);
        }

        foreach (TransparentCapsule3D subCapsule in containedSubCapsules)
            subCapsule.CloseLocally();

        MeshOuterTransform.GetComponent<MeshRenderer>().material.SetFloat("_Minimum_Pattern_Visibility", 0.1f);
        //MeshOuterTransform.GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", CapsuleColor);
        //MeshInnerTransform.GetComponent<MeshRenderer>().material.SetColor("_ColorNormal", CapsuleColor);

        VisualizationSynchronizer.instance.HideRelationSegmentsInCapsule(this);
        SceneManager.INSTANCE.PositionOrScaleChanged();

        foreach (TransparentCapsule3DOpenCloseObserver listener in openCloseObservers)
            listener.NotifyOnCapsuleOpenCloseEvent(this, false);
    }

}

public interface TransparentCapsule3DOpenCloseObserver
{
    public void NotifyOnCapsuleOpenCloseEvent(TransparentCapsule3D capsule, bool nowOpen);
}