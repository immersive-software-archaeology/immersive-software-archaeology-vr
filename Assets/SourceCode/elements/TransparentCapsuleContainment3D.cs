﻿using ISA.Modelling;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentCapsuleContainment3D : AbstractVisualElement3D
{

    public List<SpaceStation3D> containedSpaceStations { get; private set; }
    public float radius { get; private set; }



    public void Initialize(TransparentCapsule3D parentCapsule)
    {
        base.Initialize(null);
        containedSpaceStations = new List<SpaceStation3D>();
        SetParent(parentCapsule);
    }



    public override bool Contains(AbstractVisualElement3D element)
    {
        foreach (SpaceStation3D station in containedSpaceStations)
        {
            if (station.Equals(element))
                return true;
            if (station.Contains(element))
                return true;
        }
        return false;
    }

    public override void ChangeVisibility(bool visible)
    {
        foreach (SpaceStation3D station in containedSpaceStations)
            station.ChangeVisibility(visible);
    }



    public override void InitializeGeometry()
    {
        TransparentCapsule3D parentCapsule = GetParentCapsule();

        gameObject.transform.parent = parentCapsule.gameObject.transform;
        gameObject.transform.localPosition = Vector3.zero;

        incomingRelationsHookTransform = Instantiate(parentCapsule.incomingRelationsHookTransform);
        outgoingRelationsHookTransform = Instantiate(parentCapsule.outgoingRelationsHookTransform);

        incomingRelationsHookTransform.parent = gameObject.transform;
        outgoingRelationsHookTransform.parent = gameObject.transform;

        foreach (SpaceStation3D station in containedSpaceStations)
        {
            station.gameObject.transform.parent = gameObject.transform;

            float thisRadius = station.transform.localPosition.magnitude + station.BoundingVolume.radius;
            if (thisRadius > radius)
                radius = thisRadius;
        }
    }

    public override void InitializeInteractionSystem()
    {
    }

    public override void InitializeRelations(SystemGenerator generator, ModelReferenceResolver referenceResolver)
    {
        // Nothing to do...
    }

}