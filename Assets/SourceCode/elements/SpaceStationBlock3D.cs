﻿using ISA.Modelling;
using ISA.UI;
using ISA.VR;
using ISA.Modelling.Visualization;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SpaceStationBlock3D : AbstractSpaceStationComponent3D
{

    private static Material concreteMat;
    private static Material abstractMat;

    public static void LoadMaterialsFromDisk()
    {
        concreteMat = Resources.Load("Models/Materials/Exterior1") as Material;
        abstractMat = Resources.Load("Models/Visualization/Materials/Wireframe") as Material;
    }



    public new MeshCollider collider { get; private set; }

    public bool isAbstract { get; private set; }

    public bool isConstructor { get; private set; }
    public bool isSystemEntry { get; private set; }

    public float height { get; private set; }
    public float diameter { get; private set; }



    public void Initialize(ISASpaceStationBlock ecoreModel, SpaceStation3D parentStation)
    {
        base.Initialize(ecoreModel);
        SetParent(parentStation);

        isAbstract = ecoreModel.@abstract;
        isPublic = ecoreModel.@public;

        isConstructor = ecoreModel.constructor;
        isSystemEntry = ecoreModel.systemEntry;

        height = ecoreModel.height;
        diameter = ecoreModel.diameter;
    }

    public override bool Contains(AbstractVisualElement3D element)
    {
        return false;
    }

    public override void ChangeVisibility(bool visible)
    {
        // handled in parent station!
    }



    public override void InitializeGeometry()
    {
        gameObject.name = ToString();

        incomingRelationsHookTransform = gameObject.transform.Find("IncomingRelationsHookTransform");
        outgoingRelationsHookTransform = gameObject.transform.Find("OutgoingRelationsHookTransform");
        MeshTransform = gameObject.transform.Find("Mesh");
        
        float perimeter = Mathf.PI * diameter;
        MeshRenderer meshRenderer = GeometryGenerationUtil.GenerateCylinder(MeshTransform.gameObject, 8, false, height / 5f, perimeter / 10f);
        if (!isAbstract)
        {
            meshRenderer.material = concreteMat;
        }
        else
        {
            meshRenderer.material = abstractMat;
        }

        MeshTransform.localScale = new Vector3(diameter, height/2, diameter);

        collider = MeshTransform.gameObject.GetComponent<MeshCollider>();
        collider.sharedMesh = MeshTransform.gameObject.GetComponent<MeshFilter>().mesh;
        //collider.radius = diameter / 2f;
        //collider.height = height;

        mass = Mathf.PI * Mathf.Pow(diameter / 2, 2) * height;
    }



    public override void InitializeInteractionSystem()
    {
        proximityVisibleNamePlate = directParent.proximityVisibleNamePlate;

        if (MeshTransform.gameObject.TryGetComponent(out ISAUIInteractable interactable))
        {
            interactable.onTriggerPressed.AddListener(delegate {
                ((SpaceStation3D)directParent).RecursivelySetActive(false);
                ISAUIInteractionManager.INSTANCE.SubmitActiveStateChangeEvent(this, true);
            });
            InitializeInteractionSystem(interactable);
        }
    }
}
