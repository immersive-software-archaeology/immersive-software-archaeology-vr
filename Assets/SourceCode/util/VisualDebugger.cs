using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ISA.util
{

    public class VisualDebugger
    {

        public static void DrawPoint(Vector3 p)
        {
            DrawPoint(p, 0.1f * Vector3.one, Color.red);
        }

        public static void DrawPoint(Vector3 p, Vector3 size)
        {
            DrawPoint(p, size, Color.red);
        }

        public static void DrawPoint(Vector3 p, Color color)
        {
            DrawPoint(p, 0.1f * Vector3.one, color);
        }

        public static void DrawPoint(Vector3 p, Vector3 size, Color color)
        {
            DrawPoint(p, size, color, float.MaxValue);
        }

        public static void DrawPoint(Vector3 p, Vector3 size, Color color, float duration)
        {
            Debug.DrawLine(p + size.x * Vector3.left, p + size.x * Vector3.right, color, duration);
            Debug.DrawLine(p + size.y * Vector3.down, p + size.y * Vector3.up, color, duration);
            Debug.DrawLine(p + size.z * Vector3.forward, p + size.z * Vector3.back, color, duration);
        }



        public static void SpawnCubeAt(MonoBehaviour routineHost, Vector3 position, Vector3 scale, float duration = 0.5f, string name = "Debug Cube", Transform parent = null)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.name = name;
            if(parent == null)
            {
                cube.transform.position = position;
            }
            else
            {
                cube.transform.parent = parent;
                cube.transform.localPosition = position;
            }
            cube.transform.localScale = scale;

            routineHost.StartCoroutine(DelayedDestroy(cube, duration));
        }

        private static IEnumerator DelayedDestroy(GameObject objectToDestroy, float delay)
        {
            yield return new WaitForSeconds(delay);
            Object.Destroy(objectToDestroy);
        }

    }

}
