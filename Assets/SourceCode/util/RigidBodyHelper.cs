using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.util
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidBodyHelper : MonoBehaviour
    {

        private Rigidbody rigidBody;

        void Start()
        {
            rigidBody = GetComponent<Rigidbody>();
            rigidBody.centerOfMass = Vector3.zero;
            // might have to recalculate its inertia tensor
        }
    }
}
