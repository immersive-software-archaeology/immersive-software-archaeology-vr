using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionSynchronizer : MonoBehaviour
{

    public void SynchronizeToWorldPosition(Transform transformToCopyFrom)
    {
        transform.position = transformToCopyFrom.position;
    }

}
