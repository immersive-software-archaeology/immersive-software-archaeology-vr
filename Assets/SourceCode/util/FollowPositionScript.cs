using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPositionScript : MonoBehaviour
{

    public Transform transformToFollow;
    public float MaxDistance = 0;
    public Vector3 Offset = Vector3.zero;
    public Vector3 MinPosition = Vector3.negativeInfinity;
    public Vector3 MaxPosition = Vector3.positiveInfinity;

    void FixedUpdate()
    {
        if (transformToFollow == null)
            return;

        Vector3 targetPosition = Vector3.Min(MaxPosition, Vector3.Max(MinPosition, transformToFollow.position));

        Vector3 diff = (targetPosition + Offset) - transform.position;
        float dist = diff.magnitude;

        if(dist > MaxDistance)
        {
            transform.position += diff.normalized * (diff.magnitude - MaxDistance);
        }
    }

}
