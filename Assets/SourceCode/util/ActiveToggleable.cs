using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveToggleable : MonoBehaviour
{

    public bool showAtStart = true;
    public Transform AttachToAtStart = null;
    public Canvas canvasToRegister;

    [Tooltip("If set to a value <= 0, the object will never be destroyed.")]
    public float delayedDestroyAfterSeconds = 0;
    [Tooltip("Only considered if delayed destroy is active.")]
    public Renderer[] renderersToFadeOutWhileDestroying;



    public void Start()
    {
        if (AttachToAtStart != null)
        {
            Vector3 localPosition = transform.localPosition;
            Vector3 localScale = transform.localScale;
            Quaternion localRotation = transform.localRotation;

            transform.parent = AttachToAtStart;
            transform.localPosition = localPosition;
            transform.localScale = localScale;
            transform.localRotation = localRotation;
        }

        if (delayedDestroyAfterSeconds > 0)
            StartCoroutine(DestroyAfter(delayedDestroyAfterSeconds));

        if (canvasToRegister != null)
            SceneManager.INSTANCE.RegisterCanvas(canvasToRegister);

        if (!showAtStart)
            gameObject.SetActive(false);
    }

    private IEnumerator DestroyAfter(float sec)
    {
        if (renderersToFadeOutWhileDestroying == null || renderersToFadeOutWhileDestroying.Length == 0)
        {
            yield return new WaitForSeconds(sec);
        }
        else
        {
            Color[] originalColors = new Color[renderersToFadeOutWhileDestroying.Length];
            for (int i = 0; i < renderersToFadeOutWhileDestroying.Length; i++)
                originalColors[i] = renderersToFadeOutWhileDestroying[i].material.color;

            for (int t=0; t < sec * 20; t++)
            {
                for (int i = 0; i < renderersToFadeOutWhileDestroying.Length; i++)
                {
                    Color c = originalColors[i];
                    c.a = originalColors[i].a * Mathf.Pow(1f - t / (sec * 20f), 0.5f);
                    renderersToFadeOutWhileDestroying[i].material.color = c;
                }
                yield return new WaitForSeconds(1f/20f);
            }
        }

        if (gameObject == null)
            // destroyed already
            yield break;
        Destroy(gameObject);
    }

    public void ToggleActive()
    {
        if (this.isActiveAndEnabled)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
