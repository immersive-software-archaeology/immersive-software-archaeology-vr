using ISA.util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryGenerationUtil : MonoBehaviour
{

    public static MeshRenderer GenerateCylinder(GameObject gameObject, int resolution = 12, bool smoothShading = true,
        float uvVerticalStretch = 1f, float uvHorizontalStretch = 1f,
        float height = 2f, float radius = 0.5f)
    {
        if (resolution < 3)
            throw new System.Exception("Resolution of a cylinder cannot be less than 3");

        MeshFilter filter;
        MeshRenderer renderer;

        if (!gameObject.TryGetComponent<MeshRenderer>(out renderer))
            renderer = gameObject.AddComponent<MeshRenderer>();
        if (!gameObject.TryGetComponent<MeshFilter>(out filter))
            filter = gameObject.AddComponent<MeshFilter>();

        Mesh mesh = new Mesh();

        {
            mesh.Clear();
            mesh.subMeshCount = 1;

            // 4 triangles per segment: one top, one bottom, 2 as a quad on the side
            // -> 1 top vertex, 1 bottom vertex, 2 vertices per side, shared with neighbor segments
            Vector3[] vertices = new Vector3[2 + resolution * (1+1+2)];
            Vector2[] uvs = new Vector2[vertices.Length];

            // number of indices in triangle array: number of triangles x 3
            int[] triangles = new int[resolution * 4 * 3];

            // Set one vertex for the top and bottom center
            vertices[0] = Vector3.up * height / 2;
            vertices[1] = Vector3.down * height / 2;
            uvs[0] = Vector2.one / 2f;
            uvs[1] = uvs[0];

            int vertexIndex = 2;
            int uvIndex = 2;
            int triangleIndex = 0;

            // top part
            {
                for (int segmentIndex = 0; segmentIndex < resolution; segmentIndex++)
                {
                    Vector2 positionInUnitCircle = LayoutUtil.CalculateVertexPositionInUnitCircle(segmentIndex, resolution) * radius;
                    vertices[vertexIndex++] = new Vector3(positionInUnitCircle.x, height / 2, positionInUnitCircle.y);
                    uvs[uvIndex++] = uvHorizontalStretch * positionInUnitCircle / 2f + Vector2.one / 2f;
                }

                for (int segmentIndex = 0; segmentIndex < resolution; segmentIndex++)
                {
                    triangles[triangleIndex++] = 0;
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex + 0) % resolution);
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex + 1) % resolution);
                }
            }

            // bottom part
            {
                for (int segmentIndex = 0; segmentIndex < resolution; segmentIndex++)
                {
                    Vector2 positionInUnitCircle = LayoutUtil.CalculateVertexPositionInUnitCircle(segmentIndex, resolution) * radius;
                    vertices[vertexIndex++] = new Vector3(positionInUnitCircle.x, -height / 2, positionInUnitCircle.y);
                    uvs[uvIndex++] = uvHorizontalStretch * positionInUnitCircle / 2f + Vector2.one / 2f;
                }

                for (int segmentIndex = 0; segmentIndex < resolution; segmentIndex++)
                {
                    triangles[triangleIndex++] = 1;
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex + 1) % resolution);
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex + 0) % resolution);
                }
            }

            // side part
            {
                for (int segmentIndex = 0; segmentIndex < resolution; segmentIndex++)
                {
                    Vector2 positionInUnitCircle = LayoutUtil.CalculateVertexPositionInUnitCircle(segmentIndex, resolution) * radius;
                    vertices[vertexIndex++] = new Vector3(positionInUnitCircle.x, height / 2, positionInUnitCircle.y);
                    vertices[vertexIndex++] = new Vector3(positionInUnitCircle.x, -height / 2, positionInUnitCircle.y);
                    uvs[uvIndex++] = new Vector2((float)segmentIndex / (float)(resolution - 1f) * uvHorizontalStretch, 0.5f + 0.5f * uvVerticalStretch);
                    uvs[uvIndex++] = new Vector2((float)segmentIndex / (float)(resolution - 1f) * uvHorizontalStretch, 0.5f - 0.5f * uvVerticalStretch);
                }

                for (int segmentIndex = 0; segmentIndex < resolution; segmentIndex++)
                {
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex * 2 + 0) % (resolution * 2));
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex * 2 + 2) % (resolution * 2));
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex * 2 + 1) % (resolution * 2));

                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex * 2 + 1) % (resolution * 2));
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex * 2 + 2) % (resolution * 2));
                    triangles[triangleIndex++] = vertexIndex - 1 - ((segmentIndex * 2 + 3) % (resolution * 2));
                }
            }

            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.SetTriangles(triangles, 0);

            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();
        }

        filter.mesh = mesh;
        return renderer;
    }

}
