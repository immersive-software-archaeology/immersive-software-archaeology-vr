using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPrinter : MonoBehaviour
{

    public void PrintTest()
    {
        Print("Test!");
    }

    public void Print(string text)
    {
        Debug.Log(text);
    }

}
