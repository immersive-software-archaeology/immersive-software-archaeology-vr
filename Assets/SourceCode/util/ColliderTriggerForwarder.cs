using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderTriggerForwarder : MonoBehaviour
{

    public Collider[] relevantColliders;
    public UnityEvent onTriggerEnter;

    private void OnTriggerEnter(Collider other)
    {
        if (relevantColliders == null)
            return;

        for (int i = 0; i < relevantColliders.Length; i++)
        {
            if (relevantColliders[i] == other)
            {
                onTriggerEnter.Invoke();
                return;
            }
        }
    }

}
