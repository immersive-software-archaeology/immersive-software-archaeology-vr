using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionResetter : MonoBehaviour
{

    private static readonly float smoothInMultiplier = 1f;
    private static readonly float smoothInPower = 6f;

    private Transform parent;
    private Vector3 localPosition;
    private Quaternion localRotation;
    private float localScale;

    private AbstractVisualElement3D visualizationElement;



    public void Init()
    {
        if (transform.TryGetComponent(out AbstractVisualElement3D visualizationElement))
            this.visualizationElement = visualizationElement;

        parent = transform.parent;
        localPosition = transform.localPosition;
        localRotation = transform.localRotation;
        localScale = transform.localScale.x;
    }

    public void StopAnimation()
    {
        StopAllCoroutines();
    }

    public void ResetToRememberedPositionAnimated()
    {
        if (parent == null)
            return;

        if(visualizationElement is SpaceStation3D)
            if (!visualizationElement.GetParentCapsule().isOpened)
                visualizationElement.ChangeVisibility(false);

        StopAllCoroutines();
        StartCoroutine(animate());

        foreach (PositionResetter resetter in GetComponentsInChildren<PositionResetter>())
            if (resetter.gameObject != gameObject)
                resetter.ResetToRememberedPositionAnimated();
    }

    public void ResetToRememberedPositionHard()
    {
        StopAllCoroutines();
        transform.localPosition = localPosition;
        transform.localRotation = localRotation;
        transform.localScale = localScale * Vector3.one;
        ISAUIInteractionManager.INSTANCE.PositionChanged(visualizationElement);

        foreach (PositionResetter resetter in GetComponentsInChildren<PositionResetter>())
            if (resetter.gameObject != gameObject)
                resetter.ResetToRememberedPositionHard();
    }

    private IEnumerator animate()
    {
        yield return new WaitForSeconds(0.5f);
        float startTime = Time.time;

        while(true)
        {
            float dist = Vector3.Distance(parent.TransformPoint(localPosition), transform.position);
            if (dist <= 0.001f)
                break;

            float t = Mathf.Min(1, Mathf.Pow((Time.time - startTime) / dist * smoothInMultiplier, smoothInPower)) * Mathf.Min(Time.deltaTime, 1f);

            transform.localPosition = Vector3.Lerp(transform.localPosition, localPosition, t);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, localRotation, t);
            transform.localScale = Mathf.Lerp(transform.localScale.x, localScale, t) * Vector3.one;

            ISAUIInteractionManager.INSTANCE.PositionChanged(visualizationElement);
            yield return null;
        }

        transform.localPosition = localPosition;
        transform.localRotation = localRotation;
        transform.localScale = localScale * Vector3.one;
        ISAUIInteractionManager.INSTANCE.PositionChanged(visualizationElement);
    }

}
