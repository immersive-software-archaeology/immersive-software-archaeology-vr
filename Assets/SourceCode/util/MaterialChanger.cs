using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class MaterialChanger : MonoBehaviour
{

    private MeshRenderer meshRenderer;



    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }



    public Color GetEmission(int materialIndex = 0)
    {
        return GetColor("_EmissionColor", materialIndex);
    }

    public Color GetColor(string key = "_Color", int materialIndex = 0)
    {
        if (meshRenderer != null)
            return meshRenderer.materials[materialIndex].GetColor(key);
        return Color.magenta;
    }



    public void SetDrawing(bool value)
    {
        SetBool(value, "_IsDrawing");
    }

    public void SetHighlighted(bool value)
    {
        SetBool(value, "_IsHighlighted");
    }

    public void SetHovered(bool value)
    {
        SetBool(value, "_IsHovered");
    }



    public void SetBool(bool value, string key, int materialIndex = 0)
    {
        SetFloat(value ? 1f : 0f, key);
    }

    public void SetEmission(Color value, int materialIndex = 0)
    {
        SetColor(value, "_EmissionColor", materialIndex);
    }

    public void SetColor(Color value, string key = "_Color", int materialIndex = -1)
    {
        if (meshRenderer != null)
        {
            if(materialIndex < 0)
                meshRenderer.material.SetColor(key, value);
            else
                meshRenderer.materials[materialIndex].SetColor(key, value);
        }
    }

    public void SetFloat(float value, string key, int materialIndex = 0)
    {
        if(meshRenderer != null)
            meshRenderer.materials[materialIndex].SetFloat(key, value);
    }

    public void SetInt(int value, string key, int materialIndex = 0)
    {
        if (meshRenderer != null)
            meshRenderer.materials[materialIndex].SetInteger(key, value);
    }

    public void SetVector(Vector4 value, string key, int materialIndex = 0)
    {
        if (meshRenderer != null)
            meshRenderer.materials[materialIndex].SetVector(key, value);
    }

    public void SetTexture(Texture value, string key, int materialIndex = 0)
    {
        if (meshRenderer != null)
            meshRenderer.materials[materialIndex].SetTexture(key, value);
    }


}
