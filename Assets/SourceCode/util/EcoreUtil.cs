﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EcoreUtil
{

    public static Vector3 ConvertToUnity(ISA.Modelling.Multiplayer.ISAVector3 v)
    {
        if (v == null)
            return Vector3.zero;
        return new Vector3(v.x, v.y, v.z);
    }

    public static Vector3 ConvertToUnity(ISA.Modelling.Clientinfo.ISAVector3 v)
    {
        if (v == null)
            return Vector3.zero;
        return new Vector3(v.x, v.y, v.z);
    }



    public static List<List<Vector2>> ConvertToUnity(string[] ecoreLines)
    {
        List<List<Vector2>> unityLines = new List<List<Vector2>>();
        if (ecoreLines == null)
            return unityLines;

        for (int i = 0; i < ecoreLines.Length; i++)
        {
            string ecoreLine = ecoreLines[i];
            string[] pointsOnLineSplit = ecoreLine.Trim().Split(' ');

            List<Vector2> unityLine = new List<Vector2>();
            foreach (string pointAsString in pointsOnLineSplit)
            {
                string[] pointCoordinates = pointAsString.Split(',');
                try
                {
                    Vector2 v = new Vector2(float.Parse(pointCoordinates[0]), float.Parse(pointCoordinates[1]));
                    unityLine.Add(v);
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
            unityLines.Add(unityLine);
        }
        return unityLines;
    }

    public static Vector2 ConvertToUnity(ISA.Modelling.Whiteboard.ISAVector2 v)
    {
        if (v == null)
            return Vector2.zero;
        return new Vector2(v.x, v.y);
    }

    public static Vector3 ConvertToUnity(ISA.Modelling.Whiteboard.ISAVector3 v)
    {
        if (v == null)
            return Vector3.zero;
        return new Vector3(v.x, v.y, v.z);
    }

    public static Color ConvertToUnity(ISA.Modelling.Whiteboard.ISAColor c)
    {
        if (c == null)
            return Color.magenta;
        return new Color(c.r, c.g, c.b, c.a);
    }



    public static ISA.Modelling.Multiplayer.ISAVector3 ConvertToEcoreMultiplayer(Vector3 v)
    {
        ISA.Modelling.Multiplayer.ISAVector3 output = new ISA.Modelling.Multiplayer.ISAVector3();
        output.x = v.x;
        output.y = v.y;
        output.z = v.z;
        return output;
    }

    public static string[] convertToEcoreWhiteboard(List<List<Vector2>> registeredPointsOnBoard)
    {
        string[] result = new string[registeredPointsOnBoard.Count];
        for (int i = 0; i < registeredPointsOnBoard.Count; i++)
        {
            List<Vector2> pointsOnLine = registeredPointsOnBoard[i];

            string line = "";
            for (int j = 0; j < pointsOnLine.Count; j++)
                line += (int)Mathf.Round(pointsOnLine[j].x) + "," + Mathf.Round(pointsOnLine[j].y) + " ";

            result[i] = line;
        }
        return result;
    }

    public static ISA.Modelling.Whiteboard.ISAVector2 ConvertToEcoreWhiteboard(Vector2 v)
    {
        ISA.Modelling.Whiteboard.ISAVector2 result = new ISA.Modelling.Whiteboard.ISAVector2();
        result.x = v.x;
        result.y = v.y;
        return result;
    }

    public static ISA.Modelling.Whiteboard.ISAVector3 ConvertToEcoreWhiteboard(Vector3 v)
    {
        ISA.Modelling.Whiteboard.ISAVector3 result = new ISA.Modelling.Whiteboard.ISAVector3();
        result.x = v.x;
        result.y = v.y;
        result.z = v.z;
        return result;
    }

    public static ISA.Modelling.Whiteboard.ISAColor ConvertToEcoreWhiteboard(Color c)
    {
        ISA.Modelling.Whiteboard.ISAColor result = new ISA.Modelling.Whiteboard.ISAColor();
        result.r = c.r;
        result.g = c.g;
        result.b = c.b;
        result.a = c.a;
        return result;
    }

}