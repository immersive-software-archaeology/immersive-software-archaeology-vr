using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using ISA.util;
using System;

namespace ISA.SolarOverview
{
    [RequireComponent(typeof(Rigidbody))]
    public class FloatingBehavior : MonoBehaviour
    {
        public Vector2 initialSpinMinMax = new Vector2(.1f, .2f);
        public bool DampSpin = true;
        public bool DampFloat = true;
        public bool MaintainSpin = false;

        public float dampingFactor = 5f;
        public float accelPower = 1.5f;
        public float breakPower = 2f;

        public float rotationPower = 1f; 

        public Transform transformToFloatTo;
        public Transform transformToRotateTo = null;

        private Vector3 initialVelocity;
        private Rigidbody rigidBody;



        public void Start()
        {
            initialVelocity = new Vector3(0, -1 * RandomFactory.INSTANCE.getRandomFloat(initialSpinMinMax.x, initialSpinMinMax.y), 0);

            rigidBody = GetComponent<Rigidbody>();
            //rigidBody.angularVelocity = initialVelocity;
            //rigidBody.mass = Mathf.PI * Mathf.Pow(radius, 3) * 4 / 3;
        }

        private void FixedUpdate()
        {
            if (rigidBody == null)
                return;

            Vector3 force = Vector3.zero;
            Vector3 diff = transformToFloatTo.position - transform.position;
            float distance = diff.magnitude;
            if(distance > 0.001f)
            {
                // Add a force that pulls the object back to the original position
                force += Mathf.Pow(dampingFactor * distance, accelPower) * diff;

                if (Vector3.Dot(diff.normalized, rigidBody.velocity.normalized) < 0)
                {
                    // counteract overshooting
                    force += Mathf.Pow(dampingFactor * distance, breakPower) * diff;
                }
            }
            if (DampFloat)
            {
                force += -0.5f * dampingFactor * rigidBody.velocity;
            }
            rigidBody.AddForce(force);

            Vector3 torque = Vector3.zero;
            if (transformToRotateTo != null)
            {
                Vector3 targetPositionProjectedToTableHeight = transformToRotateTo.position;
                targetPositionProjectedToTableHeight.y = transform.position.y;
                Quaternion rotDiff = transform.rotation * Quaternion.Inverse(Quaternion.LookRotation(transform.position - targetPositionProjectedToTableHeight));

                float spinRightFactor = 0.1f * Mathf.Pow(dampingFactor * distance, rotationPower) * (180 - rotDiff.eulerAngles.y);
                if (Mathf.Sign(rigidBody.angularVelocity.y) != Mathf.Sign(180 - rotDiff.eulerAngles.y))
                {
                    // counteract overshooting
                    spinRightFactor *= dampingFactor;
                }

                torque += spinRightFactor * Vector3.forward;

            }
            if(DampSpin)
            {
                torque += -0.5f * dampingFactor * rigidBody.angularVelocity;
            }
            else if(MaintainSpin)
            {
                if (rigidBody.angularVelocity.magnitude > initialVelocity.magnitude)
                    torque += -0.002f * rigidBody.angularVelocity;
                else
                    torque += 0.001f * initialVelocity;
            }

            rigidBody.AddTorque(torque);
        }

        public void Reset()
        {
            if(rigidBody != null)
            {
                rigidBody.velocity = Vector3.zero;
                rigidBody.angularVelocity = Vector3.zero;
            }
        }
    }
}
