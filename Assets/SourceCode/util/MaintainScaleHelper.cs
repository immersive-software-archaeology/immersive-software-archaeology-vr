using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class MaintainScaleHelper : MonoBehaviour
{

    public Vector3 targetSize = Vector3.one;

    void Update()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * targetSize.x / transform.lossyScale.x,
            transform.localScale.y * targetSize.y / transform.lossyScale.y,
            transform.localScale.z * targetSize.z / transform.lossyScale.z
            );
    }

}
