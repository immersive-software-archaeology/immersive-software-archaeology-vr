using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace ISA.util
{
    public class FollowRotationScript : MonoBehaviour
    {

        public Transform TransformToFollow;
        public bool Inversed = false;
        public bool SetToPlayerHead = false;


        void OnEnable()
        {
            if (SetToPlayerHead)
                TransformToFollow = Player.instance.headCollider.transform;

            if (TransformToFollow != null)
                Update();
        }

        void Update()
        {
            Vector3 lookRotation;
            if (!Inversed)
                lookRotation = TransformToFollow.position - transform.position;
            else
                lookRotation = transform.position - TransformToFollow.position;

            if (lookRotation.magnitude > Vector3.kEpsilon)
                transform.rotation = Quaternion.LookRotation(lookRotation);
        }
    }
}