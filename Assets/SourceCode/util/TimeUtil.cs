﻿using System;
using System.Collections;
using UnityEngine;

public class TimeUtil
{

    public static double serverRTTMilliSeconds { get; private set; }

    /// <summary>
    /// How many milliseconds are we roughly running LATER than the server?
    /// </summary>
    public static long timeDifferenceToServerMilliSeconds { get; private set; }

    public static void RegisterServerRTT(double rtt)
    {
        serverRTTMilliSeconds = rtt;
    }

    public static void RegisterTimeDeltaToServer(long delta)
    {
        timeDifferenceToServerMilliSeconds = delta;
    }



    public static string UnixMillisecondsToString(long unixMillis, bool withMillis = false)
    {
        String timeWithMillis = DateTimeOffset.FromUnixTimeMilliseconds(unixMillis).TimeOfDay.ToString();
        if (withMillis)
            return timeWithMillis;
        return timeWithMillis.Split('.')[0];
    }

    /// <summary>
    /// Formats a float containing seconds into the style of a digital clock,
    /// e.g. 85.123f into "01:25" or "01:25:123"
    /// </summary>
    /// <param name="sec"></param>
    /// <param name="includeMillis"></param>
    /// <returns></returns>
    public static string FormatSecondsInDigitalClockStyle(float sec, bool includeMillis)
    {
        int minutes = (int)Mathf.Floor(sec / 60f);
        int seconds = (int)Mathf.Floor(sec) % 60;

        if(!includeMillis)
        {
            return ToStringWithLeadingZero(minutes) + ":" + ToStringWithLeadingZero(seconds);
        }
        else
        {
            int millis = (int)Mathf.Round(1000 * (sec - Mathf.Floor(sec)));
            return ToStringWithLeadingZero(minutes) + ":" + ToStringWithLeadingZero(seconds) + ":" + ToStringWithLeadingZero(millis, 3);
        }
    }

    public static string ToStringWithLeadingZero(int number, int targetLength = 2)
    {
        string result = number.ToString();
        while (result.Length < targetLength)
            result = "0" + result;
        return result;
    }
}