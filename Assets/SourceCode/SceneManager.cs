using ISA.Modelling;
using ISA.Modelling.Multiplayer;
using ISA.Modelling.Whiteboard;
using ISA.UI;
using ISA.util;
using ISA.VR;
using Mono.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using UnityEngine.Scripting;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class SceneManager : MonoBehaviour, FallbackCameraGUIExtension
{
    public static SceneManager INSTANCE;

    public bool finishedLoading { private set; get; }

    public Texture2D testTexure;

    public float startScaleFactor = 0.001f;

    public float minScaleFactor = 0.001f;
    public float maxScaleFactor = 0.01f;

    public float minPositionY = 0.3f;
    public float maxPositionY = 1.8f;

    [SerializeField]
    private Button namePlateVisibilityToggleButton;
    public bool namePlatesVisible { get; private set; }

    public Transform RootTransform;
    public Transform SoftwareElementInfoCanvasRootTransform;

    public GameObject[] elementsToActivateOnModelLoaded;
    public GameObject[] elementsToActivateOnStartupFinished;

    public bool waitForHeadsetOnHeadBeforeActivatingTeleport = true;
    public GameObject teleportObject;

    private List<SceneListener> listeners;
    private List<WhiteboardHandler> whiteboards;

    private List<Canvas> canvases;
    private List<ISAVRPointer> vrPointers;

    public bool showTutorialWhiteboards = false;

    public bool quickLoadSystem = true;
    public string quickLoadSystemName;

    public ElementResolver elementResolver { private set; get; }
    public ISAUdpClient udpClient { private set; get; }



    void Awake()
    {
        GarbageCollector.GCModeChanged += (GarbageCollector.Mode mode) =>
        {
            Debug.Log("Garbage Collection Mode Changed: " + mode);
        };

        GarbageCollector.GCMode = GarbageCollector.Mode.Enabled;

        INSTANCE = this;
        listeners = new List<SceneListener>();
        whiteboards = new List<WhiteboardHandler>();

        namePlatesVisible = true;

        vrPointers = new List<ISAVRPointer>();
        canvases = new List<Canvas>();

        ISAFallbackCameraController.RegisterListener(this);

        RelationshipCanvas.InitiallyLoadSpritesFromDisk();
        udpClient = new ISAUdpClient();

        StartCoroutine(WaitAndActivateTeleport());
    }

    private IEnumerator WaitAndActivateTeleport()
    {
        while (SteamVR.initializedState != SteamVR.InitializedStates.InitializeSuccess)
            yield return null;
        while (Player.instance == null)
            yield return null;

        while(true)
        {
            try
            {
                if (Player.instance.rightHand.isActive || Player.instance.leftHand.isActive)
                    break;
            }
            catch (Exception e) { }
            yield return null;
        }

        while (Player.instance.rig2DFallback.gameObject.activeSelf)
            yield return null;
#if UNITY_EDITOR
        if (waitForHeadsetOnHeadBeforeActivatingTeleport)
        {
            while (Player.instance.headsetOnHead == null)
                yield return null;
            while (!Player.instance.headsetOnHead.GetState(SteamVR_Input_Sources.Any))
                yield return null;
        }
#endif

        teleportObject.SetActive(true);

        while (Teleport.instance == null)
            yield return null;

        Debug.Log("Disabling Teleport Hints");
        yield return new WaitForSeconds(3);
        Teleport.instance.CancelTeleportHint();
    }

    public void Quit()
    {
        Application.Quit();
    }



    private List<AbstractVisualElement3D> elementsThatMoved = new List<AbstractVisualElement3D>();
    private int currentIndex;
    private int notificationIndexStart = -1;

    public void PositionOrScaleChanged()
    {
        if (elementResolver == null)
            return;
        elementsThatMoved = elementResolver.GetListWithCopyOfAllElements();
        if (currentIndex >= elementsThatMoved.Count)
            currentIndex = 0;
        notificationIndexStart = currentIndex;
    }

    void Update()
    {
        if (notificationIndexStart != -1)
        {
            for (int i = 0; i < 100; i++)
            {
                currentIndex++;
                if (currentIndex >= elementsThatMoved.Count)
                    // don't overflow
                    currentIndex = 0;

                object movedElement = elementsThatMoved[currentIndex];
                ISAUIInteractionManager.INSTANCE.PositionChanged(movedElement);

                if (currentIndex == notificationIndexStart)
                {
                    // worked all the way through the list
                    notificationIndexStart = -1;
                    break;
                }
            }
        }

        for (int i = 0; i < 50; i++)
        {
            ISAVRPointer pointer = GetActivePointer();
            if (pointer == null)
                break;

            if (canvasToActivateIndex >= canvases.Count)
            {
                canvasToActivateIndex = 0;
                break;
            }

            Canvas c = canvases[canvasToActivateIndex];
            if (c == null || c.gameObject == null)
                canvases.RemoveAt(canvasToActivateIndex--);
            else if (c.isActiveAndEnabled)
                c.worldCamera = pointer.eventCamera;

            canvasToActivateIndex++;
        }
    }

    private int canvasToActivateIndex = 0;
    public void RegisterCanvas(Canvas newCanvas)
    {
        canvases.Add(newCanvas);
    }



    public void DestroySceneElements()
    {
        finishedLoading = false;
        ElementUpdater.instance.StopUpdatingElements();
        WhiteboardSynchronizer.StopWhiteboardUpdates();
        ISAUIInteractionManager.INSTANCE.Reset();

        GameObject newEmptyParent = new GameObject("new temporary root for deleted elements");

        List<Rigidbody> bodiesToLetFallToGround = new List<Rigidbody>();
        foreach (Transform t in RootTransform)
            CollectElementsToLetFallToGround(t, bodiesToLetFallToGround);
        foreach(Rigidbody body in bodiesToLetFallToGround)
        {
            body.transform.parent = newEmptyParent.transform;
            body.isKinematic = false;
            body.useGravity = true;

            body.AddForce(RandomFactory.INSTANCE.getRandomVector3(0.5f, 0.75f), ForceMode.Impulse);
        }

        foreach (Transform t in RootTransform)
            Destroy(t.gameObject);

        for(int i=0; i<newEmptyParent.transform.childCount; i++)
            Destroy(newEmptyParent.transform.GetChild(i).gameObject, 2f + i/100f);

        RootTransform.localScale = Vector3.one;
        RootTransform.position = Vector3.zero;

        Destroy(newEmptyParent, 10f);

        foreach (Hand hand in Player.instance.hands)
            foreach (Hand.AttachedObject attachedObject in hand.AttachedObjects)
                if (attachedObject.attachedObject.TryGetComponent<AbstractVisualElement3D>(out _))
                    hand.DetachObject(attachedObject.attachedObject, false);

        foreach (WhiteboardHandler boardCopy in new List<WhiteboardHandler>(GetWhiteboards()))
        {
            UnregisterWhiteboard(boardCopy);
            Destroy(boardCopy.GetRoot().gameObject);
        }

        foreach (GameObject rootObject in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            if (rootObject.TryGetComponent<SoftwareElementInfoCanvas>(out _))
                Destroy(rootObject);
            if (rootObject.TryGetComponent<RelationLineSegment>(out _))
                Destroy(rootObject);
        }
    }

    private void CollectElementsToLetFallToGround(Transform t, List<Rigidbody> collection)
    {
        if (!t.gameObject.activeSelf)
            return;
        if (t.TryGetComponent(out Rigidbody b))
            collection.Add(b);
        foreach (Behaviour c in t.GetComponents<Behaviour>())
        {
            //if (c is Rigidbody)
            //    continue;
            //if (c is Transform)
            //    continue;
            //if (c is Collider)
            //    continue;
            //if (c is MeshRenderer || c is MeshFilter)
            //    continue;
            c.enabled = false;
        }
        foreach (Transform child in t)
            CollectElementsToLetFallToGround(child, collection);
    }

    public void NotifyFinishedLoading(List<AbstractVisualElement3D> allElementsInBottomUpOrder)
    {
        finishedLoading = true;

        elementResolver = new ElementResolver(allElementsInBottomUpOrder);
        udpClient.Init();

        foreach (SceneListener listener in listeners)
            listener.OnSceneFinishedLoading();
        foreach (GameObject obj in elementsToActivateOnModelLoaded)
            obj.SetActive(true);

        ApplyNamePlateVisibility(true);
        PositionOrScaleChanged();
        ElementUpdater.instance.UpdateElementsContinuously();
    }



    public void SendNamePlateVisibilityToggleRequest()
    {
        ChangeNameplateVisibilityEvent ev = new ChangeNameplateVisibilityEvent();
        ev.systemName = ModelLoader.instance.VisualizationModel.name;
        ev.clientId = udpClient.id;
        ev.visible = !namePlatesVisible;

        HTTPRequestHandler.INSTANCE.SubmitEvent(ev, null, delegate (string r) { Debug.LogError(r); });
    }

    public void ApplyNamePlateVisibility(bool visible)
    {
        namePlatesVisible = visible;
        foreach (AbstractVisualElement3D element in elementResolver.GetAllElements())
        {
            if (element is TransparentCapsule3D)
            {
                //((TransparentCapsule3D)element).permanentlyVisibleNamePlate.gameObject.SetActive(namePlatesVisible);

                if (visible)
                    ((TransparentCapsule3D)element).permanentlyVisibleNamePlate.RemoveVisibilityToken(NamePlateVisibilityToken.GLOBAL_INVISIBLE);
                else
                    ((TransparentCapsule3D)element).permanentlyVisibleNamePlate.AddVisibilityToken(NamePlateVisibilityToken.GLOBAL_INVISIBLE);
            }
        }

        if(visible)
        {
            namePlateVisibilityToggleButton.transform.GetChild(0).GetComponent<SpriteToggler>().ChangeSprite(0);
            namePlateVisibilityToggleButton.transform.GetChild(1).GetComponent<TMP_Text>().text = "Capsule Names";
        }
        else
        {
            namePlateVisibilityToggleButton.transform.GetChild(0).GetComponent<SpriteToggler>().ChangeSprite(1);
            namePlateVisibilityToggleButton.transform.GetChild(1).GetComponent<TMP_Text>().text = "Capsule Names";
        }
    }



    public WhiteboardHandler GetWhiteboard(int id)
    {
        foreach (WhiteboardHandler board in whiteboards)
        {
            if (board.id == id)
                return board;
        }
        return null;
    }

    public List<WhiteboardHandler> GetWhiteboards()
    {
        return whiteboards;
    }

    public void RegisterNewWhiteboard(WhiteboardHandler newBoard)
    {
        if (GetWhiteboard(newBoard.id) != null)
            Debug.Log("A whiteboard with ID '" + newBoard.id + "' was already registered.");

        whiteboards.Add(newBoard);
    }

    public bool UnregisterWhiteboard(WhiteboardHandler boardToUnregister)
    {
        foreach (WhiteboardHandler thisBoard in whiteboards)
        {
            if (thisBoard.id == boardToUnregister.id)
            {
                whiteboards.Remove(boardToUnregister);
                return true;
            }
        }
        return false;
    }



    public void RegisterVRPointer(ISAVRPointer newPointer)
    {
        vrPointers.Add(newPointer);
    }

    public ISAVRPointer GetActivePointer()
    {
        float shortestDist = float.MaxValue;
        ISAVRPointer pointer = null;

        foreach (ISAVRPointer p in vrPointers)
        {
            if (p.IsDisabled())
                continue;

            if (p.hit.distance > 0 && p.hit.distance < shortestDist)
            {
                shortestDist = p.hit.distance;
                pointer = p;
            }
        }

        return pointer;
    }

    public void ResetInteraction()
    {
        foreach (ISAVRPointer pointer in vrPointers)
            pointer.EnableHard();
    }

    public void DisableInteraction(Hand hand, object blockingObject)
    {
        foreach (ISAVRPointer pointer in vrPointers)
            pointer.Disable(hand, blockingObject);
    }

    public void EnableInteraction(Hand hand, object blockingObject)
    {
        StartCoroutine(EnableInteractionWithDelay(hand, blockingObject));
    }

    private IEnumerator EnableInteractionWithDelay(Hand hand, object blockingObject)
    {
        yield return new WaitForSeconds(0.5f);
        foreach (ISAVRPointer pointer in vrPointers)
            pointer.Enable(hand, blockingObject);
    }



    public void AddListener(SceneListener newListener)
    {
        listeners.Add(newListener);
    }

    public bool RemoveListener(SceneListener listener)
    {
        return listeners.Remove(listener);
    }



    public void DrawGUI()
    {
        if (ModelLoader.instance == null || ModelLoader.instance.VisualizationModel == null)
            return;
        if (!finishedLoading)
            return;

        float scale = GUI.Slider(new Rect(Screen.width / 2f - 100, Screen.height - 10 - 30 * 3, 200, 25), RootTransform.localScale.x, minScaleFactor / 10f, 0, maxScaleFactor + minScaleFactor, GUI.skin.horizontalSlider, GUI.skin.horizontalSliderThumb, true, 0);
        scale = Mathf.Max(minScaleFactor, Mathf.Min(maxScaleFactor, scale));
        if (scale != RootTransform.localScale.x)
        {
            RootTransform.localScale = scale * Vector3.one;
            PositionOrScaleChanged();

            Hand fallBackHand = null;
            foreach (Hand hand in Player.instance.hands)
                if (hand != Player.instance.leftHand && hand != Player.instance.rightHand)
                    fallBackHand = hand;
            if (fallBackHand != null)
                udpClient.RegisterSystemMovement(fallBackHand);
        }
        else
        {
            udpClient.UnregisterSystemMovement();
        }

        if(GUI.Button(new Rect(Screen.width / 2f - 100, Screen.height - 10 - 30 * 4, 200, 25), "Toogle Name Visbility"))
            SendNamePlateVisibilityToggleRequest();
    }
}

public interface SceneListener
{

    public void OnSceneFinishedLoading();

}