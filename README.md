
# **Immersive Software Archaeology (ISA) - Virtual Reality**

This is ISA's visualization front-end application that lets you to inspect a subject software system in virtual reality.
In order for that to work, you need to install the [ISA Eclipse plugins](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-eclipse), which serve as back-end server for the VR application to connect to and synchronize information with, e.g., source code of software elements or activities of collaborating archaeologists.

The ISA VR application is built with **Unity 3D** and **SteamVR**.

**Important remark**: This project was developed and tested on a Windows machine!



&nbsp;



## **Tool Versions**

ISA exists in two versions with different visual metaphors and different feature sets.

- **Solar System Metaphor** ([other branch](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr/-/tree/solarsystem?ref_type=heads)):  
  In this version of the tool, ISA visualizes the architectural components of a subject system as planets with hierarchies of continents/islands and cities, where the buildings of a city represent the classes of the system.
  You can navigate the solar system in a space ship in VR and visit the various cities to inspect code and investigate relationships between software elements with a relationship graph.  
  *Important*: In case you wish to use this version of ISA's VR client, please make sure to use the [corresponding Eclipse backend](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-eclipse/-/tree/solarsystem?ref_type=heads) version.

- **Abstract Metaphor for Collaborative Exploration with Note Taking** (this branch):  
  In this version of ISA, we use a more abstract metaphor with Nested Folder Spheres and Class Cylinders (cf. screenshots below).  
  Find a highlight of various tool features of this version in the section [Tool Features](#tool-features) beneath.



&nbsp;



## **Tool Features**

#### **Collaborative Exploration**
Multiple VR clients can connect to one Eclipse plug-in backend and collaboratively explore a subject system - locally in the same network or over the internet.  
[![Collaborative Exploration with Multimedia Notes](readme-resources/youtube-preview-collaborative-exploration.jpg?raw=true)](https://www.youtube.com/watch?v=32EIpf4V3b4 "Collaborative Exploration with Multimedia Notes")

The following animation shows a remotely connected collaborator working in a visualization hosted on the recording client's machine:  
![Short Multiplayer Demo GIF](readme-resources/multiplayer.gif?raw=true)

#### **Detailed Interactive Relation Graphs**
The tool interactively displays different types of relationships between software elements, i.e., type references, method calls, field accesses.  
![Short Relationship Graph Demo GIF](readme-resources/relationship-graph.gif?raw=true)

#### **Virtual Whiteboards for Freehand Sketching**
Capture insights and plans by pinning elements from the visualization, attaching screenshots, and drawing freehandedly on a virtual whiteboard.
The tool automatically interprets sketches and provides you with conformance checks to the ground-truth structure of your subject system.  
[![VR Freehand Sketching](readme-resources/youtube-preview-whiteboards.jpg?raw=true)](https://www.youtube.com/watch?v=NKC5YpH3n4Y "VR Freehand Sketching")



&nbsp;



## **Publications**

- **[FSE'21 Vision Paper](https://pure.itu.dk/ws/portalfiles/portal/86190732/fse21_21_07_07_camera_ready_authors_version.pdf)** - short paper on our vision for the project
- **[VISSOFT'22 Technical Paper](https://pure.itu.dk/ws/portalfiles/portal/92854114/preprint_vissoft22.pdf)** - software exploration via solar system metaphor
- **[SANER'24 Tool Paper](#)** - tool demo paper on the solar system metaphor
- **[ICSME'23 Technical Paper](#)** - freehand sketching in virtual reality
- **[ICPC'24 Technical Paper](#)** - collaborative software exploration with note taking
- **[ICPC'24 Tool Paper](#)** - tool demo paper on collaborative software exploration with note taking



&nbsp;



## **Installation**

There are **two alternative ways** of installing the ISA VR application on your machine.
These are explained beneath.

#### Option A: Installation via a **Binary Build** (Windows only)

This way of installing the ISA VR plugin is the fastest.  
Download and extract the ZIP-folder from the [release section](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr/-/releases) of this repository and start the provided executable.

#### Option B: Installation via **Source Code**

Check out this repository and open it as Unity project using the specified Unity version (will be automatically recognized by Unity).  
We recommend using the [Unity Hub](https://unity3d.com/get-unity/download).



&nbsp;



## **Usage**

The ISA VR application acts as visualization front-end in combination with a running Eclipse runtime hosting the [ISA Eclipse plugins](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-eclipse).
The ISA VR application communicated with the ISA Eclipse plugins, thereby fetches information on a priorly analyzed subject software system, and visualizes these in immersive VR.


#### **SteamVR**

The ISA VR application is developed on top of [SteamVR](https://store.steampowered.com/app/250820/SteamVR/).     
ISA can thus be used in combination with all SteamVR compatible headsets.  
We have tested ISA with the Valve Index, Pico 4, and Meta Quest 2.
We are grateful for feedback and experiences with other headsets (or those mentioned above - we welcome all kinds of feedback!)

Please note that you can change the binding of controller buttons to VR actions via the SteamVR controller binding menu (launched from the SteamVR UI).


#### **Inspecting Source Code**

It is possible to inspect source code in VR.
However, that requires the workspace of your back-end Eclipse runtime to contain respective Java source code.

**Please note**: Names of Eclipse projects are relevant! These *must* match the names of the projects as they were set during the analysis of the system.



&nbsp;



## **Contact**

In case you have questions, please don't hesitate to contact me: [adho@itu.dk](adho@itu.dk).



&nbsp;
